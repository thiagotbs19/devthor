/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de teste da classe EX3_CriarCasoRecepcao
* Empresa: everis do Brasil
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@isTest
public with sharing class EX3_CriarCasoRecepcaoTest
{
    @isTest static void criarCasoTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_situacao__c = '21';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c =  '128319'; //'NumProtocolo1';
        lCase1.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();
        lCase1.EX3_Valor_da_causa__c = 100;

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        lCase2.EX3_Motivo_situacao__c = '21';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '1239182';//'NumProtocolo2';
        lCase2.EX3_Numero_do_Processo__c = 'NumProcesso2';
        lCase2.EX3_Prioridade__c = 'Urgente';
        lCase2.EX3_Data_de_Entrada__c = system.now();
        lCase2.EX3_Valor_da_causa__c = 1;

        Case lCase3 = new Case();
        lCase3.RecordTypeId = lRTCaseRecepcao;
        lCase3.EX3_Motivo_situacao__c = '21'; 
        lCase3.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase3.EX3_Numero_do_Protocolo__c = '1281298'; //'NumProtocolo3';
        lCase3.EX3_Numero_do_Processo__c = 'NumProcesso3';
        lCase3.EX3_Prioridade__c = 'Urgente';
        lCase3.EX3_Data_de_Entrada__c = system.now();
        lCase3.EX3_Valor_da_causa__c = 100;

        Case lCase4 = new Case();
        lCase4.RecordTypeId = lRTCaseRecepcao;
        lCase4.EX3_Motivo_situacao__c = '21';
        lCase4.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase4.EX3_Numero_do_Protocolo__c = '1238128'; //'NumProtocolo4';
        lCase4.EX3_Numero_do_Processo__c = 'NumProcesso4';
        lCase4.EX3_Prioridade__c = 'Urgente';
        lCase4.EX3_Data_de_Entrada__c = system.now();
        lCase4.EX3_Valor_da_causa__c = 1;

        List<Case> lLstCase = new List<Case>{ lCase1, lCase2, lCase3, lCase4};

        Test.startTest();

        database.insert(lLstCase); 

        Test.stopTest();
        List<Case> lLstCase2 = [SELECT Id FROM Case WHERE Id = :lLstCase];
 
        system.assert(!lLstCase2.isEmpty(), 'Caso não foi inserido'); 
    }

}