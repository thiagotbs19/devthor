/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste responsável por cobrir e testar a EX3_BloqueiaCalculoOwner 
*
* 
*Apex Class: EX3_BloqueiaCalculoOwnerTest
* Empresa: everis do Brasil 
* Autor: Isabella Tannús 
*
********************************************************************************/

@isTest
public without sharing class EX3_BloqueiaCalculoOwnerTest {

    @TestSetup 
    static void createData(){

        String lProfOperacional = 'Analista Operacional'; 
        User lUserOperacional = EX3_BI_DataLoad.getUser(lProfOperacional);
        Database.insert(lUserOperacional);  

        Case lCase = EX3_BI_DataLoad.getCase(); 

        Database.insert(lCase);  

        EX3_Calculo__c lCalculo = new EX3_Calculo__c();
        lCalculo.OwnerId = UserInfo.getUserId();
        lCalculo.EX3_Status_do_calculo__c = 'Cálculo Interno em execução'; 
        
        System.runAs(lUserOperacional){
            Database.insert(lCalculo);  
        } 
        
    }
   
    @isTest static void testCalculoOwnerInsert(){ 

        User lUserOperacional = [SELECT Id FROM User LIMIT 1];

        Case lCase = [SELECT Id FROM Case LIMIT 1]; 
        
        EX3_Calculo__c lCalculo = [SELECT Id FROM EX3_Calculo__c LIMIT 1];
      
        lCalculo.OwnerId = lUserOperacional.Id;
        Database.update(lCalculo);  
        
        EX3_Calculo__c lSegCalculo = new EX3_Calculo__c();  
        lSegCalculo.OwnerId = UserInfo.getUserId();
        lSegCalculo.EX3_Status_do_calculo__c = 'Cálculo Interno em execução';

        Test.startTest(); 

        System.runAs(lUserOperacional){ 
            Database.insert(lSegCalculo, false);   
        }  
        Test.stopTest();   
        
        
        Database.SaveResult lResult = Database.update(lSegCalculo, false);                             
        System.assert(!lResult.isSuccess(), 'Erro ao inserir o Cálculo'); 
        
    } 
    
    
}