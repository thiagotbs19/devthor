/*********************************************************************************
*                                    Itaú - 2020
*
* Classe TriggerHandler do objeto EX3_Tipo_Documento  
* 
* Autor: Thiago Barbosa de Souza
  Company: Everis do Brasil
*
********************************************************************************/

public with sharing class EX3_Tipo_DocumentoTriggerHandler implements ITrigger {
    
    public void bulkBefore(){}  

    public void bulkAfter(){}

    public void beforeInsert(){}
    public void beforeUpdate(){ 
        EX3_AtualizaStatusSubsidioDocAnexado.execute(); 
    }   
    public void beforeDelete(){}
    
    public void afterInsert(){} 
    
    public void afterUpdate(){} 
    public void afterDelete(){} 
    public void andFinally(){}
}