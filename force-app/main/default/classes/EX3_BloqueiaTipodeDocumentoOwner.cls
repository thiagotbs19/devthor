/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável para que o analista não consiga puxar um registro para iniciar
* um atendimento se o mesmo ja possuir um registro de Tipo de Documentos.   
*
* 
*Apex Class: EX3_BloqueiaTipodeDocumentoOwner 
* Empresa: everis do Brasil 
* Autor: Thiago Barbosa 
*
********************************************************************************/


public with sharing class EX3_BloqueiaTipodeDocumentoOwner {
    
    
    public static void execute(){
        Set<Id> lSetTipoDocOwner = new Set<Id>();
        List<EX3_Tipo_Documento__c> lLstTipoDoc = new List<EX3_Tipo_Documento__c>();
        for(EX3_Tipo_Documento__c iTipoDoc : (List<EX3_Tipo_Documento__c>) Trigger.new){
            if(EX3_TriggerHelper.changedField(iTipoDoc, 'OwnerId') && 
               iTipoDoc.OwnerId == UserInfo.getUserId()){
                   lSetTipoDocOwner.add(iTipoDoc.OwnerId);
                   lLstTipoDoc.add(iTipoDoc);
               }
        }
        
        if(lSetTipoDocOwner.isEmpty() || lSetTipoDocOwner == null){ return; } 
        tipoDocumentoOwnerError(lSetTipoDocOwner, lLstTipoDoc); 
    }
    
    
    
    
    private static void tipoDocumentoOwnerError(Set<Id> aSetTipoDocOwner, List<EX3_Tipo_Documento__c> aLstTipoDoc){
        
        
        if(aSetTipoDocOwner.isEmpty() || aLstTipoDoc.isEmpty() || aSetTipoDocOwner == null || aLstTipoDoc == null){
            return;  
        }  
        String lNumeroProcesso;   
        if(!aLstTipoDoc.isEmpty()){   
          for(EX3_Tipo_Documento__c iTipoDoc : [SELECT Id, OwnerId, EX3_Status_do_Documento__c, EX3_Numero_do_Processo__c 
                                                  FROM EX3_Tipo_Documento__c WHERE OwnerId =: aSetTipoDocOwner AND EX3_Status_do_Documento__c NOT IN ('Documento solicitado')]){
                                                      
            lNumeroProcesso = iTipoDoc.EX3_Numero_do_processo__c;   
                                                  }    
        }   
        if(lNumeroProcesso == null){  return;}             
         for(EX3_Tipo_Documento__c iTipoDoc : aLstTipoDoc){             
            iTipoDoc.addError(Label.EX3_Pasta_Numero + lNumeroProcesso + '' +  Label.EX3_Tratativa);
        }               
    }
}