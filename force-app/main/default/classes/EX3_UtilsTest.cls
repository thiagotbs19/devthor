/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por testar a classe EX3_Utils
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@isTest 
public with sharing class EX3_UtilsTest 
{
    @isTest static void getRecordTypeIdByDevNameTest()
    {
        Test.startTest();  
        Id lRTFound = EX3_Utils.getRecordTypeIdByDevName('Case','EX3_Recepcao');
        Id lRTUndefined = EX3_Utils.getRecordTypeIdByDevName('OldNotFound','OldNotFound');
        system.assert(lRTFound != null, 'Encontrou o RecordType'); 
        system.assert(lRTUndefined == null, 'Não encontrou o RecordType');
        Test.stopTest();  
    }

    @isTest static void getFieldSetMembersByFieldSetDeveloperNameTest()
    {
        Test.startTest();
        List<Schema.FieldSetMember> lFieldSetFound = EX3_Utils.getFieldSetMembersByFieldSetDeveloperName('Case','EX3_Acionamento_de_Diligencia');
        List<Schema.FieldSetMember> lFieldSetUndefined = EX3_Utils.getFieldSetMembersByFieldSetDeveloperName('Case','OldNotFound');
        system.assert(lFieldSetFound != null, 'Encontrou o fieldSet');
        system.assert(lFieldSetUndefined == null, 'Não encontrou o fieldSet');
        Test.stopTest();
    }

    @isTest static void getPicklistValuesByFieldAPITest()
    {
        Test.startTest();
        List<Schema.PicklistEntry> lPicklistFound = EX3_Utils.getPicklistValuesByFieldAPI('Case','EX3_Motivo_da_Situacao__c');
        List<Schema.PicklistEntry> lPicklistUndefined = EX3_Utils.getPicklistValuesByFieldAPI('Case','OldNotFound');
        system.assert(lPicklistFound != null, 'Encontrou a picklist');
        system.assert(lPicklistUndefined == null, 'Não encontrou a picklist');
        Test.stopTest();
    }

    @isTest static void getFieldValuesByObjectAPITest()
    {
        Test.startTest();
        List<Schema.DescribeFieldResult> lFieldFound = EX3_Utils.getFieldValuesByObjectAPI('Case','EX3_Motivo_da_Situacao__c');
        List<Schema.DescribeFieldResult> lFiledUndefined = EX3_Utils.getFieldValuesByObjectAPI('OldNotFound','OldNotFound');
        system.assert(lFieldFound != null, 'Encontrou o campo'); 
        system.assert(lFiledUndefined == null, 'Não encontrou o campo');
        Test.stopTest();
    }

    @isTest static void getLightningIconByContentDocumentFileExtensionTest()
    {
        Test.startTest();
        String lIconFoundBlank = EX3_Utils.getLightningIconByContentDocumentFileExtension('');
        String lIconImage = EX3_Utils.getLightningIconByContentDocumentFileExtension('png');
        String lIconMP4 = EX3_Utils.getLightningIconByContentDocumentFileExtension('mp4');
        String lIconExcel = EX3_Utils.getLightningIconByContentDocumentFileExtension('xls');
        String lIconWord = EX3_Utils.getLightningIconByContentDocumentFileExtension('doc');
        String lIconPowerPoint = EX3_Utils.getLightningIconByContentDocumentFileExtension('ppt');
        String lIconUnknown = EX3_Utils.getLightningIconByContentDocumentFileExtension('unknown');
        system.assert(String.isNotBlank(lIconFoundBlank), 'Icone vazio');
        system.assert(String.isNotBlank(lIconImage), 'Icone de imagem');
        system.assert(String.isNotBlank(lIconMP4), 'Icone de mp4');
        system.assert(String.isNotBlank(lIconExcel), 'Icone de excel');
        system.assert(String.isNotBlank(lIconWord), 'Icone de word');
        system.assert(String.isNotBlank(lIconPowerPoint), 'Icone de power point');
        system.assert(String.isNotBlank(lIconUnknown), 'Icone não encontrado');
        Test.stopTest();
    }

    @isTest static void getByteSizeTest()
    {
        Test.startTest(); 
        String lByte = EX3_Utils.getByteSize(1);
        String lKb = EX3_Utils.getByteSize(1024);
        String lMB = EX3_Utils.getByteSize(1048576);
        String lGB = EX3_Utils.getByteSize(1073741824);
        String lUnknown = EX3_Utils.getByteSize(-1);
        system.assert(String.isNotBlank(lByte), 'Entre 1 byte a 1 Kb');
        system.assert(String.isNotBlank(lKb), 'Entre 1Kb gb a 1 Mb');
        system.assert(String.isNotBlank(lMB), 'Entre 1 Mb a 1 Gb');
        system.assert(String.isNotBlank(lGB), 'Entre 1 gb a 1 tb');
        system.assert(String.isNotBlank(lUnknown), 'Unidade de byte desconhecida ou tamanho maior que 1024GB');
        Test.stopTest();
    }
}