/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por testar a classe EX3_AcaoDiligenciaController
*
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@isTest
public with sharing class EX3_AcaoDiligenciaControllerTest
{ 
    @isTest static void cancelarProtocoloDiligenciaTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase = new Case();
        lCase.RecordTypeId = lRTCaseRecepcao; 
        lCase.EX3_Situacao__c = '21';
        lCase.EX3_Motivo_situacao__c = '25';
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '129128'; //NumProtocolo1';
        lCase.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();

        database.insert(lCase);

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;

        database.insert(lCase1); 

        Test.startTest();
        Case lCaseCancelado = EX3_AcaoDiligenciaController.cancelarCaseDiligencia(lCase.Id);

        system.assert(lCaseCancelado.EX3_Motivo_Situacao__c == '25');
        Test.stopTest();
    }

    @isTest static void isNotDiligenciaTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase = new Case();
        lCase.RecordTypeId = lRTCaseRecepcao; 
        lCase.EX3_Situacao__c = '20';
        lCase.EX3_Motivo_situacao__c = '28';
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '120129';//NumProtocolo1';
        lCase.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();

        database.insert(lCase);
        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;

        database.insert(lCase1); 

        Test.startTest();
        Case lProtocoloIsDiligencia = EX3_AcaoDiligenciaController.isNotDiligencia(lCase1.Id);
        Boolean notOrCancelledDiligencia = (String.isBlank(lProtocoloIsDiligencia.EX3_Situacao__c)
                                            || lProtocoloIsDiligencia.EX3_Situacao__c != '21'
                                            || lProtocoloIsDiligencia.EX3_Motivo_situacao__c == '25');
        system.assert(notOrCancelledDiligencia);
        Test.stopTest();
    } 
}