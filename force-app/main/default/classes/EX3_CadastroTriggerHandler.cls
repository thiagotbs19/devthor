/*********************************************************************************
*                                    Itaú - 2020
*
* Classe TriggerHandler do objeto EX3_Cadastro 
* 
* Autor: Thiago Barbosa de Souza
  Company: Everis do Brasil
*
********************************************************************************/

public with sharing class EX3_CadastroTriggerHandler implements ITrigger{


   public void bulkBefore(){

   }
    public void bulkAfter(){

    }
    public void beforeInsert(){ 
		    EX3_CadastroBeforeInsert.atualizaDataEntrada();
        EX3_BloqueiaCadastroOwner.execute(); 

    }
    public void beforeUpdate(){

        EX3_BloqueiaCadastroOwner.execute();
    }
    public void beforeDelete(){}
    
    public void afterInsert()
    {
        
    }
    
    public void afterUpdate(){

    }
    public void afterDelete(){

    }
    public void andFinally(){

    }
}