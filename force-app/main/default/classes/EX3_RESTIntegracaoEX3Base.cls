/**
 * Created by jmariane on 2020-04-02.
 */

public virtual with sharing class EX3_RESTIntegracaoEX3Base {
    protected String token;
    protected Map<String, String> fieldReplaces;
    protected Map<String, String> fixedFields;
    protected Map<String, Object> data;
    public EX3_RESTIntegracaoEX3Base(SObject obj, Map<String, String> fieldReplaces, Map<String, String> fixedFields) {
        this.fieldReplaces = fieldReplaces;
        this.fixedFields = fixedFields;
        data = substituirCamposApi(obj.getPopulatedFieldsAsMap());
        inserirFixedFields(data);

    }

    public EX3_RESTIntegracaoEX3Base(SObject obj, Map<String, String> fieldReplaces) {
        this.fieldReplaces = fieldReplaces;
        data = substituirCamposApi(obj.getPopulatedFieldsAsMap());
    }


    private Map<String, Object> substituirCamposApi(Map<String, Object> objOriginal) {
        Map<String, Object> mapaRetorno = new Map<String, Object>();
        for (String k : fieldReplaces.keySet()) {
            if (objOriginal.containsKey(k)) {
                mapaRetorno.put(fieldReplaces.get(k), objOriginal.get(k));
            }
        }
        return mapaRetorno;
    }

    private Map<String, Object> inserirFixedFields(Map<String, Object> objOriginal) {
        for (String k : fixedFields.keySet()) {
            objOriginal.put(k, fixedFields.get(k));
        }
        return objOriginal;
    }

    private void authenticate() {

    }

    public HttpResponse create() {
        authenticate();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setBody(JSON.serialize(data));
        Http http = new Http();
        HttpResponse response = http.send(req);
        return response;
    }
}