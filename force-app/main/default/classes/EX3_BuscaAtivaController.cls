/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsável por prover fomulario para preenchimento de dados referentes a
* ação de Busca Ativa
* Empresa: everis do Brasil
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/
public with sharing class EX3_BuscaAtivaController
{ 
   @AuraEnabled  
   public static EX3_Centralizador__c getCentralizador()
   {
      EX3_Centralizador__c lCentral;
      String lException;
      try
      {
         lCentral = new EX3_Centralizador__c();
         lCentral.EX3_hasDocuments__c = false; 
         database.insert(lCentral);
      }
      catch(DmlException ex)
      {
         lException = ex.getMessage();
         SA7_CustomdebugLog.logError('EX3', 'EX3_BuscaAtivaController', 'getCentralizador', 'Erro ao inserir ' +
                                       lException, ex, null);
      }
      finally
      {
         if (String.isBlank(lException))
         {
            SA7_CustomdebugLog.logWarn('EX3', 'EX3_BuscaAtivaController', 'getCentralizador', 'Sucesso ao criar o registro', 
            new Map<String,String>{'EX3_Centralizador__c' => JSON.serialize(lCentral)});
         }
      }
      return lCentral;
   }

   @AuraEnabled
   public static Map<String,Object> getDocuments(String aCentral) 
   {
      EX3_Centralizador__c lNewCentral = (EX3_Centralizador__c)JSON.deserialize(aCentral, EX3_Centralizador__c.class);
      lNewCentral.EX3_hasDocuments__c = true;

      Set<String> lSetDocumentIds = new Set<String>();
      for(ContentDocumentLink iCl : [SELECT ContentDocumentId, LinkedEntityId 
                                     FROM  ContentDocumentLink WHERE LinkedEntityId =: lNewCentral.Id])
      {
         lSetDocumentIds.add(iCl.ContentDocumentId);
      }

      List<EX3_WrapperDocument> lLstWrapperDocument = new List<EX3_WrapperDocument>();
      for(ContentVersion iContentVersion : [SELECT ContentDocumentId, PathOnClient 
                                            FROM ContentVersion 
                                            WHERE ContentDocumentId IN :lSetDocumentIds])
      {
         EX3_WrapperDocument lWrapperDocument = new EX3_WrapperDocument();
         lWrapperDocument.Name = iContentVersion.PathOnClient;
         lWrapperDocument.DocumentId = iContentVersion.ContentDocumentId;
         lLstWrapperDocument.add(lWrapperDocument);
      }

      return new Map<String,Object>
      {
         'listDocument' => lLstWrapperDocument,
         'central' => lNewCentral
      };
   }

   @AuraEnabled
   public static String criarCase(String aFieldList, String aCentral)
   {
       String lException = '';
       String lProtocoloBody = ''; 
       try
       {
           EX3_Centralizador__c lNewCentral = (EX3_Centralizador__c)JSON.deserialize(aCentral, EX3_Centralizador__c.class);

           Case lCase = new Case();
           lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
           lCase.EX3_Canal_de_Entrada__c = '9'; 
           lCase.EX3_Motivo_situacao__c = (lNewCentral.EX3_hasDocuments__c) ? '12' : '2';

           List<AttributesTypes> lLstAttributesTypes = (List<AttributesTypes>) JSON.deserialize(aFieldList, List<AttributesTypes>.class);
           for (AttributesTypes iAttributesTypes : lLstAttributesTypes)
           {
               if (String.isBlank(iAttributesTypes.value)) { continue; }
               if (iAttributesTypes.api == 'EX3_CPF__c') { iAttributesTypes.value = iAttributesTypes.value.replaceAll('[^\\d]',''); }
               lCase.put(String.valueOf(iAttributesTypes.api), getFieldType(iAttributesTypes));
           }

           database.insert(lCase);
           
           User lUser = [SELECT Id, FuncionalColaborador__c FROM User WHERE Id = :UserInfo.getUserId()];

           EX3_ObjectFactory.IncluirProtocolo lIncluirProtocolo = new EX3_ObjectFactory.IncluirProtocolo();
           lIncluirProtocolo.codigo_protocolo_externo = lCase.Id; 
           lIncluirProtocolo.codigo_unico_processo = lCase.EX3_Numero_do_Processo__c;
           lIncluirProtocolo.data_protocolo_documento = lCase.CreatedDate;
           lIncluirProtocolo.codigo_canal_entrada_documento = Decimal.valueOf(lCase.EX3_Canal_de_Entrada__c); 
           lIncluirProtocolo.codigo_motivo_situacao = (lCase.EX3_Motivo_da_Situacao__c == '12') ? Decimal.valueOf('35') : Decimal.valueOf(lCase.EX3_Motivo_Situacao__c);
           lIncluirProtocolo.data_situacao = system.today();  
           lIncluirProtocolo.operador = lUser.FuncionalColaborador__c;
           lIncluirProtocolo.codigo_operacao = 3;
           lIncluirProtocolo.indicador_protocolo = 0;
           lProtocoloBody = JSON.serialize(lIncluirProtocolo); 
       } 
       catch(DmlException ex) 
       {
         lException = ex.getMessage();
         SA7_CustomdebugLog.logError('EX3', 'EX3_BuscaAtivaController', 'criarProtocolo', 'Erro ao inserir registro do Protocolo '+lException, ex, null);
       }
       catch(Exception ex) 
       {
         lException = ex.getMessage();
           system.debug('lException ==> ' +lException);
         SA7_CustomdebugLog.logError('EX3', 'EX3_BuscaAtivaController', 'criarProtocolo', 'Erro ao inserir registro do Protocolo '+lException, ex, null);
       }
       finally
       {
         if(String.isBlank(lException))
         {
            SA7_CustomdebugLog.logWarn('EX3', 'EX3_BuscaAtivaController', 'criarProtocolo', 'Sucesso ao inserir registro do protocolo', null);
         }
      }
      return lProtocoloBody;
   }
   
   @AuraEnabled(continuation=true)
   public static Continuation enviarProtocolo(String aParams)
   {
      system.debug('aParams ==> ' +aParams);
      Map<String,String> lMapParams = (Map<String,String>) JSON.deserialize(aParams, Map<String,String>.class);
       
      Map<String, String> lMapDadosRequest = new Map<String,String>
      {
         'central' => lMapParams.get('central'),
         'classe' => lMapParams.get('classe'),
         'body' => lMapParams.get('protocolo'),
         'callback' => lMapParams.get('callback'),
         'service' => 'incluirProtocolo'
      };

      Continuation lContinuation;
      try 
      {
         lContinuation = EX3_ServiceFactory.incluirProtocolo(lMapParams.get('protocolo'), lMapParams.get('classe'), lMapParams.get('callback'), lMapDadosRequest);
      }
      catch (Exception ex)
      {
         SA7_CustomdebugLog.logError('EX3', 'EX3_BuscaAtivaController', 'enviarProtocolo', 'Erro ao criar o Request '+ex.getMessage(), ex, null);
      }

      return lContinuation;
   }

   @AuraEnabled
   public static String enviarProtocoloCallback(Object state)
   {
      EX3_ContinuationUtils.StateInfo lRetorno = (EX3_ContinuationUtils.StateInfo)state;

      HttpResponse lResponse = Continuation.getResponse(lRetorno.state);
      if (lResponse.getStatusCode() > 202)
      {
         SA7_CustomdebugLog.logWarn('EX3', 'EX3_BuscaAtivaController', 'enviarProtocoloCallback','Status: ' + lResponse.getStatusCode() + '\n' + 
                                      lResponse.getBody(), null);
         Map<String,String> lResponseMsg = (Map<String, String>) JSON.deserialize(lResponse.getBody(), Map<String,String>.class);

         String lMensagem = 'Unkown Error';
         if (String.isNotBlank(lResponseMsg.get('message')))
         {
            lMensagem = lResponseMsg.get('message');
         }
         else if (String.isNotBlank(lResponseMsg.get('Message')))
         {
            lMensagem = lResponseMsg.get('Message');
         }

         return JSON.serialize(new Map<String, Object> 
         {
            'error' => lMensagem,
            'status' => lResponse.getStatusCode()
         });
      }

      Map<String,Object> lMapDataResponse = (Map<String,Object>)JSON.deserializeUntyped(lResponse.getBody());
      Map<String,Object> lMapBodyResponse = (Map<String,Object>)lMapDataResponse.get('data');

      Case lCase = fillCaseNumber(lMapBodyResponse);  
      lMapBodyResponse.put('case', lCase);  

      return JSON.serialize(lMapBodyResponse);
   }

   private static Case fillCaseNumber(Map<String,Object> aMapBodyResponse)
   {
      String lNumeroDoProtocolo = JSON.serialize(aMapBodyResponse.get('id_protocolo'));
      Case lCase = new Case();
      lCase.Id = (Id)aMapBodyResponse.get('codigo_protocolo_externo');
      lCase.EX3_Motivo_situacao__c = '12'; 
      database.update(lCase);

      return lCase; 
   }

   @AuraEnabled(Continuation=true)
   public static Continuation enviarArquivosEAtualizarCase(String aParams)
   {
      Map<String, String> lMapParams = (Map<String,String>) JSON.deserialize(aParams, Map<String,String>.class);

      Map<String, String> lMapDadosRequest = new Map<String,String>
      {
         'case' => lMapParams.get('case'),
         'classe' => lMapParams.get('classe'),
         'body' => lMapParams.get('aBody'),
         'callback' => lMapParams.get('callback'),
         'service' => 'incluirDocumento_AtualizarProtocolo'
      };
	  
      
      Continuation lContinuation; 
      Case lCase = (Case) JSON.deserialize(lMapParams.get('case'), Case.class);
      Map<String,String> lMapBodies = (Map<String,String>) JSON.deserialize(lMapParams.get('aBody'), Map<String,String>.class);

      try 
      {
         lContinuation = EX3_ServiceFactory.incluirDocumentoAtualizaProtocolo(lMapBodies, 'EX3_BuscaAtivaController', 'enviarArquivosEProtocoloCallback', lMapDadosRequest, String.valueOf(lCase.EX3_Numero_do_protocolo__c));
      } 
      catch (Exception ex)
      {
         SA7_CustomdebugLog.logError('EX3', 'EX3_BuscaAtivaController', 'enviarArquivos', 'Erro ao criar o Request '+ ex.getMessage(), ex, null);
      }
      return lContinuation;
   }

   @AuraEnabled
   public static String enviarArquivosECaseCallback(Object state)
   {
      EX3_ContinuationUtils.StateInfo lRetorno = (EX3_ContinuationUtils.StateInfo)state;
      Map<String,String> lMapContinuationState = (Map<String,String>)lRetorno.requisicao;

      HttpResponse lResponseIncluirDocumento = Continuation.getResponse(lMapContinuationState.get('incluirDocumento'));
      HttpResponse lResponseAtualizarProtocolo = Continuation.getResponse(lMapContinuationState.get('atualizarProtocolo'));

      Map<String,Object> lMap = new Map<String,Object>
      {
         'atualizarProtocolo' => lResponseAtualizarProtocolo.getBody(), 
         'incluirDocumento' => lResponseIncluirDocumento.getBody()
      };

      return JSON.serialize(lMap);
   }

   @AuraEnabled
   public static String criarDocumentos(String aProtocolo, String aCentral, String aFuncional)
   {
      Case lCase = (Case) JSON.deserialize(aProtocolo, Case.class);
      EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(aCentral, EX3_Centralizador__c.class);
       
      //aqui
       if(!lCentral.EX3_hasDocuments__c){
          lCase.EX3_Motivo_situacao__c = '2';
          Database.update(lCase);
          return 'Sem documento'; 
       }//aqui
         
	  system.debug('criarDocumentos === ' + aCentral);
      List<ContentDocumentLink> lLstContentDocumentLink = new List<ContentDocumentLink>();
      for(ContentDocumentLink iContentDocumentLink : [SELECT ContentDocumentId, ContentDocument.Title, ShareType, Visibility, LinkedEntityId FROM ContentDocumentLink
                                                WHERE LinkedEntityId = :lCentral.Id])
      {
         ContentDocumentLink lContentDocumentLink = iContentDocumentLink.clone();
         lContentDocumentLink.LinkedEntityId = lCase.Id;
         lContentDocumentLink.ShareType = 'I';
         lContentDocumentLink.Visibility = 'AllUsers';
         lLstContentDocumentLink.add(lContentDocumentLink);
      }
      database.insert(lLstContentDocumentLink);

      List<EX3_ObjectFactory.IncluirDocumento> lLstDocumento = new List<EX3_ObjectFactory.IncluirDocumento>();
      for(ContentDocumentLink iContentDocumentLink : [SELECT Id, ContentDocument.Title FROM ContentDocumentLink
                                                      WHERE LinkedEntityId = :lCase.Id])
      {
         EX3_ObjectFactory.IncluirDocumento lDocumento = new EX3_ObjectFactory.IncluirDocumento();
         lDocumento.nome_documento = iContentDocumentLink.ContentDocument.Title;
         lDocumento.codigo_documento_plataforma_externa = iContentDocumentLink.Id;
         lDocumento.operador_conglomerado = aFuncional;
         lLstDocumento.add(lDocumento);
      }
   
      EX3_ObjectFactory.AtualizarProtocolo lAtualizarProtocolo = new EX3_ObjectFactory.AtualizarProtocolo();
      lAtualizarProtocolo.id_protocolo = Decimal.valueOf(lCase.EX3_Numero_do_Protocolo__c);
      lAtualizarProtocolo.codigo_protocolo_externo = lCase.Id;
      lAtualizarProtocolo.codigo_motivo_situacao = (lCentral.EX3_hasDocuments__c) ? Decimal.valueOf('35') : Decimal.valueOf('2');//Decimal.valueOf(lProtocolo.EX3_Motivo_da_Situacao__c);//Decimal.valueOf(lProtocolo.EX3_Motivo_da_Situacao__c); aqui
      lAtualizarProtocolo.operador = aFuncional;
      lAtualizarProtocolo.codigo_operacao = 3;
      lAtualizarProtocolo.indicador_protocolo = 0;  

      return JSON.serialize(new Map<String,String>{ 'atualizarProtocolo' => JSON.serialize(new List<EX3_ObjectFactory.AtualizarProtocolo>{lAtualizarProtocolo}), 
                                                    'incluirDocumento' => JSON.serialize(lLstDocumento)});
   }

   @AuraEnabled
   public static String getCaseFields()
   {
       List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
       Set<String> lSetRequiredField = getRequiredFields();
       for (Schema.FieldSetMember iFieldSetMember : EX3_Utils.getFieldSetMembersByFieldSetDeveloperName('Case','EX3_Busca_Ativa'))
       {
           AttributesTypes lAttributesTypes = new AttributesTypes();
           lAttributesTypes.label = iFieldSetMember.getLabel();
           lAttributesTypes.api = iFieldSetMember.getFieldPath();
           lAttributesTypes.fieldType = EX3_Utils.convertApexTypeToLightningType(iFieldSetMember.getType());
           lAttributesTypes.apexType = iFieldSetMember.getType().name();
           lAttributesTypes.isDisable = (lAttributesTypes.api == 'EX3_Canal_de_Entrada__c');
           lAttributesTypes.required = lSetRequiredField.contains(iFieldSetMember.getFieldPath());
           if (lAttributesTypes.fieldType == 'number') { lAttributesTypes.min = '0'; }
           if (lAttributesTypes.api == 'EX3_Valor_da_causa__c') { lAttributesTypes.formatter = 'currency'; }

           if (lAttributesTypes.api == 'EX3_Prazo_dias__c') 
           { 
              lAttributesTypes.fieldType = 'text';
              lAttributesTypes.max = '999';
              lAttributesTypes.maxlength = '3'; 
           }
           if (lAttributesTypes.api == 'EX3_Numero_do_processo__c') { lAttributesTypes.maxlength = '25'; }

           if (lAttributesTypes.fieldType == 'picklist')
           {
               List<AttributesTypesPicklist> lLstAttributesTypesPicklist = new List<AttributesTypesPicklist>();
               for (Schema.PicklistEntry iPickList : EX3_Utils.getPicklistValuesByFieldAPI('Case', iFieldSetMember.getFieldPath()))
               {
                   if (lAttributesTypes.api == 'EX3_Canal_de_Entrada__c' && iPickList.getValue() != '9') { continue; }
                   AttributesTypesPicklist lAttributesTypesPicklist = new AttributesTypesPicklist();
                   lAttributesTypesPicklist.label = iPickList.getLabel();
                   lAttributesTypesPicklist.value = iPickList.getValue();
                   lLstAttributesTypesPicklist.add(lAttributesTypesPicklist);
               }
               lAttributesTypes.picklistValues = lLstAttributesTypesPicklist;
           }
           lLstAttributesTypes.add(lAttributesTypes);
       }
      return JSON.serialize(lLstAttributesTypes);
   }

   private static Set<String> getRequiredFields()
   {
      Set<String> lSetRequiredFields = new Set<String>
      {
         'EX3_Macro_carteira__c',
         'EX3_Numero_do_Processo__c',
         'EX3_Autor__c',
         'EX3_Comarca__c',
         'EX3_Orgao_Legal__c',
         'EX3_Vara__c',
         'EX3_UF__c',
         'EX3_Data_da_audiencia__c',
         'EX3_Hora_da_audiencia__c',
         'EX3_Data_ajuizamento__c',
         'EX3_Empresa_re__c' 
      };
      return lSetRequiredFields;
   }

    private static Object getFieldType(AttributesTypes aAttributesTypes)
    {
        String lFieldType = aAttributesTypes.apexType;
        Object lValue;
        if (lFieldType == 'BOOLEAN') { lValue = Boolean.valueOf(aAttributesTypes.value); }
        else if (lFieldType == 'DATETIME')
        {
            lValue = (DateTime)JSON.deserialize('"' + aAttributesTypes.value + '"', DateTime.class);
        }
        else if (lFieldType == 'DATE') { lValue = Date.valueOf(aAttributesTypes.value); }
        else if (lFieldType == 'INTEGER' || aAttributesTypes.api == 'EX3_Prazo_dias__c') { lValue = Integer.valueOf(aAttributesTypes.value); }
        else if (lFieldType == 'DOUBLE' ) { lValue = Double.valueOf(aAttributesTypes.value); }
        else { lValue = aAttributesTypes.value; }
        return lValue;
    }

   public class AttributesTypes
   {
      private String label {get; set;}
      private String value {get; set;}
      private String api {get;set;}
      private String fieldType {get;set;}
      private String apexType {get;set;}
      private boolean isDisable {get;set;}
      private boolean required {get;set;}
      private String formatter {get;set;}
      private String max{get;set;}
      private String min{get;set;}
      private String maxlength {get;set;}
      private List<AttributesTypesPicklist> picklistValues {get;set;}
   }

   public class AttributesTypesPicklist
   {
      private String label {get;set;}
      private String value {get;set;}
   } 
}