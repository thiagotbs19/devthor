/*********************************************************************************
*                                    Itaú - 2019
*
* Classe que cria ADJ Fila de OBF 
* Empresa: everis do Brasil
* Autor: Sem Autor
*
********************************************************************************/
@isTest
public class EX3_CreateFilaOBFControllerTest {
    
    
    @testSetup
    private static void testCreateData(){
        
        User lUser = EX3_BI_DataLoad.getUser();
        
        Database.insert(lUser);
        
        Case lCase = EX3_BI_DataLoad.getCase();
        
        Database.insert(lCase);
        
        EX3_ADJ__c lADJ = EX3_BI_DataLoad.getADJ(lCase);
        
        Database.insert(lADJ);
    }
    
    @isTest static void testredirectToObject(){
        	User lUser = [SELECT Id FROM User LIMIT 1];
        
        	Case lCase = [SELECT Id FROM Case LIMIT 1];
        
        	EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];
        
        Test.startTest();
        	String lReturnADJ = EX3_CreateFilaOBFController.redirectToObject(lADJ.Id);
        	String lReturnADJ2 = EX3_CreateFilaOBFController.redirectToObject2(lADJ.Id);
        Test.stopTest();
    }

}