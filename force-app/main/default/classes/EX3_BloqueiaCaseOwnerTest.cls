/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste responsável por cobrir e testar a EX3_BloqueiaCaseOwner 
*
* 
*Apex Class: EX3_BloqueiaCaseOwnerTest
* Empresa: everis do Brasil 
* Autor: Thiago Barbosa 
*
********************************************************************************/
@isTest
public with sharing class EX3_BloqueiaCaseOwnerTest { 

    @testSetup
    public static void createData(){ 
    	String lPerfilAnalistaOperacional = 'Analista Operacional'; 
        User lUserOperacional = EX3_BI_DataLoad.getUser(lPerfilAnalistaOperacional);
        Database.insert(lUserOperacional);
        List<Permissionsetassignment> psalist = new List<Permissionsetassignment>();

        Permissionsetassignment psa = new Permissionsetassignment();
        psa.AssigneeId = lUserOperacional.id;
        psa.PermissionSetId = [select id from Permissionset where name = 'EX3_Cadastro' limit 1].id;
        psalist.add(psa);

        insert psalist;

        Case lCase = new Case();
        lCase.EX3_Situacao__c = '1'; 
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
        lCase.EX3_Numero_do_protocolo__c = '001';
        lCase.OwnerId = lUserOperacional.Id;
        
        
        System.runAs(lUserOperacional){
            Database.insert(lCase);
        } 
    }
    
    @isTest static void testCaseOwnerTest() {

        User lUserOperacional = [SELECT Id FROM User LIMIT 1]; 
		

        List<Group> lLstGroup = [
                SELECT Id, Name
                FROM Group
                WHERE Name = :Label.EX3_Fila_FIFO
                AND Type = 'Queue'
        ];

        Case lCase = [SELECT Id FROM Case LIMIT 1];
        
        lCase.OwnerId = lUserOperacional.Id;
        
        Database.update(lCase);  

        Case lSegundoCaso = new Case();
        lSegundoCaso.EX3_Situacao__c = '1'; 
        lSegundoCaso.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
        lSegundoCaso.EX3_Numero_do_protocolo__c = '004';
        lSegundoCaso.OwnerId = lUserOperacional.Id;

		Test.startTest();
        
        system.runAs(lUserOperacional) { 
            Database.insert(lSegundoCaso); 
            lSegundoCaso.EX3_Situacao__c = '1';
            lSegundoCaso.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
            lSegundoCaso.EX3_Numero_do_protocolo__c = '004';
            lSegundoCaso.OwnerId = lUserOperacional.Id;
            
        }

        Database.SaveResult lResult = Database.update(lSegundoCaso, false);
        System.assert(lResult.isSuccess(), 'Registro não foi criado');   
        
        Test.stopTest(); 

    }

}