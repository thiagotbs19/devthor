@isTest
private class OpenIdItauInternalRegHandlerTest {

    public static User usuario;
    
    static testMethod void init(){
        usuario = new User();
        usuario.Username= 'fake@itautest.com';
        usuario.Email = 'fake@itautest.com';
        usuario.Lastname = 'testLast';
        usuario.Alias = 'fakem';
        usuario.TimeZoneSidKey = 'GMT';
        usuario.LocaleSidKey = 'en_US';
        usuario.EmailEncodingKey = 'ISO-8859-1';
        usuario.LanguageLocaleKey = 'en_US';
        usuario.FuncionalColaborador__c = '0000000';
        usuario.ProfileId = UserInfo.getProfileId();
        insert(usuario);
    }
    
   static testMethod void validateCreateUpdateUser() {
      init();
      Test.startTest();

         System.runAs ( usuario ) 
         {
            OpenIdItauInternalRegHandler reg = new OpenIdItauInternalRegHandler();
            Auth.UserData userData = new Auth.UserData('testId', 'testFirst', 'testLast',
             'testFirst testLast', 'fake@itautest.com', null, '0000000', 'en_US', 'Open Id Connect',null, new Map<String, String>{'usr' => '0000000'});
              User newUser = reg.createUser(null, userData);
              
                    System.assert(newUser != null);
                    
              reg.updateUser(usuario.Id, null, userData);
        
              User dbUser = [SELECT Id, Firstname, Lastname, Email FROM User WHERE Id = :usuario.Id];
                    
              System.assertEquals(dbUser.Lastname, 'testLast');
        
  		}
      Test.stopTest();
	}
    
    
    static testMethod void validateExtractUniqueIdentifier() {
        
        OpenIdItauInternalRegHandler reg = new OpenIdItauInternalRegHandler();
        
        list<String> lListJWT = new list<String>{'usr=0000000','1'};
            Auth.UserData userData = new Auth.UserData('testId', 'testFirst', 'testLast',
             'testFirst testLast', 'fake@itautest.com', null, '0000000', 'en_US', 'Open Id Connect',null, new Map<String, String>{'jwt_token' => String.valueOf(lListJWT) });
        
        
        reg.extractUniqueIdentifier(userData);
       
    }
}