/*********************************************************************************
*                                    Itaú - 2020
*
*Classe de teste da  EX3_BloqueiaEstrategiaOwner
*
*Apex Trigger : EX3_Definicao_de_estrategia__c
*Apex Class: EX3_BloqueiaEstrategiaOwnerTest
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/
@isTest
public with sharing class EX3_BloqueiaEstrategiaOwnerTest {
    

    @isTest  
    private static void testBloqueiaCadastroOwner(){
        
        User lUser = EX3_BI_DataLoad.getUser();
        Database.insert(lUser); 
        
        EX3_DefinicaoEstrategia__c lDefEstrategia = new EX3_DefinicaoEstrategia__c();
        lDefEstrategia.EX3_StatusEstrategia__c = 'Documento Pendente'; 
        lDefEstrategia.OwnerId = UserInfo.getUserId();
        
        Database.insert(lDefEstrategia);   
        
        EX3_DefinicaoEstrategia__c lSegDefEstrategia = new EX3_DefinicaoEstrategia__c(); 
        lSegDefEstrategia.EX3_StatusEstrategia__c = 'Documento Pendente'; 
        lSegDefEstrategia.OwnerId = lUser.Id;
         
        
        Test.startTest();  
        Database.insert(lSegDefEstrategia);   
        
        lSegDefEstrategia.EX3_StatusEstrategia__c = 'Documento Pendente'; 
        lSegDefEstrategia.OwnerId = UserInfo.getUserId();    

        Test.stopTest();                         
        Database.SaveResult result = Database.update(lSegDefEstrategia, false);     
        System.assertNotEquals(Label.EX3_Pasta_Numero + lSegDefEstrategia.Name + Label.EX3_Tratativa ,result.getErrors()[0].getMessage());
    }    
}