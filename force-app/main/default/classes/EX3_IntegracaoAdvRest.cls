/**
 * Created by jmariane on 2020-04-02.
 */

public with sharing class EX3_IntegracaoAdvRest {
    private final String ENDPOINT_BASE  = 'aguardando';
    private static final Map<String, String> fieldReplaces = new Map<String, String> {
            'EX3_Codigo_externo__c' => 'id_advogado',
            'Id' => 'id_externo_advogado',
            'EX3_OAB__c' => 'numero_registro_advogado',
            'EX3_Nome_do_advogado__c' => 'nome_pessoa_envolvida'
    };
    private static final Map<String, String> fixedFields = new Map<String, String> {
            'codigo_tipo_pessoa' => 'F'
    };
    public EX3_RESTIntegracaoEX3Base cliente;

//    public EX3_IntegracaoAdvRest(EX3_Advogado__c advogado) {
//        cliente = new EX3_RESTIntegracaoEX3Base(advogado, fieldReplaces, fixedFields);
//    }
}