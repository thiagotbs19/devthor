public class SA7_Dummy {
//Esse método será usado para a padronização da criação dos Processes Builder, sendo utilizado no primeiro ponto de decisão. Com o objetivo de criar um critério de entrada e processamento para os outros pontos de decisão garantindo que os mesmos são sejam avaliados exclusivamente para a regra de negócio em questão.
    @InvocableMethod(label='Dummy' description='Método utilizado para cumprir a obrigatoriedade da ação imediata na saída verdadeira do primeiro ponto de decisão dos Processes Builder')
    public static void dummy(){
        
    }
}