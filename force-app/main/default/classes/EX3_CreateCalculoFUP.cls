/*********************************************************************************
 *                                    Itaú - 2020
 *
 * Classe Responsável por criar o cálculo como Owner FUP. 
 *
 *Apex Trigger : EX3_Calculo__c 
 *Apex Class: EX3_CreateCalculoFUP
 *  
 *
 * Empresa: everis do Brasil
 * Autor: Thiago Barbosa
 *
 ********************************************************************************/


public without sharing class EX3_CreateCalculoFUP {

	public static final Set<String> lRecordTypeCalc = new Set<String> {
		EX3_Utils.getRecordTypeIdByDevName('EX3_Calculo__c', 'EX3_Calculista')
	};

	public static final Set<Id> lSetQueueIds = EX3_Utils.getQueueIdByName(new List<String> { EX3_Utils.FILA_CALCULO });

	public static void execute() {
		List<EX3_Calculo__c> lLstCalculo = new List<EX3_Calculo__c> ();
		for (EX3_Calculo__c iCalculo : (List<EX3_Calculo__c>) Trigger.new) {
			if (lRecordTypeCalc.contains(iCalculo.RecordTypeId) && lSetQueueIds.contains(iCalculo.OwnerId)) {
				lLstCalculo.add(iCalculo);
			}
		}
		if (lLstCalculo.isEmpty() || lLstCalculo.isEmpty()) {
			return;
		}
		insertStatusFilterGroup(lLstCalculo);
	}

	private static void insertStatusFilterGroup(List<EX3_Calculo__c> aLstCalculo) {

		if (aLstCalculo.isEmpty() || aLstCalculo == null) { return; }
		List<Group> lLstGroupFUP = [SELECT Id, OwnerId, DeveloperName FROM Group WHERE Type = 'Queue' AND DeveloperName = :EX3_Utils.FILA_FUP];
		List<EX3_Calculo__c> lLstNewCalculo = new List<EX3_Calculo__c> ();

        for (EX3_Calculo__c iCalc : aLstCalculo) {
			EX3_Calculo__c lCalc = iCalc.clone(false, false, false, false);			
			lCalc.OwnerId = lLstGroupFUP[0].Id;
			lLstNewCalculo.add(lCalc);
		}

		if (lLstNewCalculo.isEmpty()) { return; }
		Database.insert(lLstNewCalculo);
	}

}