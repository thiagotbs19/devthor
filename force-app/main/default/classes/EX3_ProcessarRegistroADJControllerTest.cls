/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste
* 
*Apex Class: EX3_ProcessarRegistroADJControllerTest
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa 
*
********************************************************************************/

@isTest
public with sharing class EX3_ProcessarRegistroADJControllerTest {



    @TestSetup
    static void createADJData(){
        User lUser = EX3_BI_DataLoad.getUser();

        Database.insert(lUser);

        Case lCase = EX3_BI_DataLoad.getCase();

        Database.insert(lCase);
		
        List<Group> lLstGroup = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName = 'EX3_Analise_de_decisao'];
        
        EX3_ADJ__c lADJ = EX3_BI_DataLoad.getADJ(lCase);
        lADJ.EX3_Data_da_Publicacao__c = Date.today();
    	lADJ.EX3_Tipo_de_Decisao__c = 'Liminar';
    	lADJ.EX3_Resultado_da_Decisao__c = 'Improcedente';
        lADJ.OwnerId = lLstGroup[0].Id;
    	lADJ.EX3_Multa_OBF__c = 'Arbitrada';
    	lADJ.EX3_Valor_Multa_OBF__c = 2.0;
        lADJ.EX3_Checklist_de_Cumprimento__c = 'Pagamento';

        Database.insert(lADJ);
        
    }

    @isTest
    private static void testProcessarRegistroADJ(){

        User lUser = [SELECT Id FROM User LIMIT 1];

        Case lCase = [SELECT Id FROM Case LIMIT 1];

        EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];
        lADJ.EX3_Checklist_de_Cumprimento__c = 'Pagamento; OBF';  
        lADJ.EX3_Classificacao_de_Decisao__c = 'Sem ônus';   
 
        Test.startTest();
            Map<String, Object> lMapReturn = EX3_ProcessarRegistroADJController.getRecordDetails(lADJ.Id);
        Test.stopTest(); 
        System.assert(!lMapReturn.isEmpty());
    }

    @isTest
    private static void testgetRecordTypeADJ(){

        User lUser = [SELECT Id FROM User LIMIT 1];

        Case lCase = [SELECT Id FROM Case LIMIT 1];

        EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1]; 

        Test.startTest();   
            String lReturn = EX3_ProcessarRegistroADJController.getRecordTypeADJ(lADJ.Id);
        Test.stopTest();
        System.assert(lReturn != null); 
        
    }  
    

    @isTest
    private static void testgetRecordTypeADJNull(){

        Test.startTest();     
            String lReturn = EX3_ProcessarRegistroADJController.getRecordTypeADJ(null);
        Test.stopTest();
        System.assert(lReturn != null);        
        
    } 

    @isTest
    private static void testHasPermission(){

        Test.startTest();
            Boolean lHasPermission = EX3_ProcessarRegistroADJController.getHasPermissionSet();
        Test.stopTest(); 
        System.assert(!lHasPermission, 'Não há permissão atribuída a este perfil');
    }
    
}