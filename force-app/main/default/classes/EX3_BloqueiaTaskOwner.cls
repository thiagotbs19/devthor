/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável para que o analista não consiga puxar um registro para iniciar
* um atendimento se o mesmo ja possuir um registro de Task.   
*
* 
*Apex Class: EX3_BloqueiaTaskOwner 
* Empresa: everis do Brasil  
* Autor: Thiago Barbosa 
*
********************************************************************************/






public without sharing class EX3_BloqueiaTaskOwner {
    
  private static Schema.SObjectType TipoGroup = Schema.getGlobalDescribe().get('Group');

 
    public static void execute(){  
        Set<Id> lSetOwnerTaskId = new Set<Id>();
        List<String> lLstSubject = new List<String>();
        List<Task> lLstTask = new List<Task>();
        for(Task iTask : (List<Task>) Trigger.new){
            if(EX3_TriggerHelper.changedField(iTask, 'OwnerId') && 
            iTask.OwnerId.getSObjectType() == TipoGroup){ 
                lSetOwnerTaskId.add(iTask.OwnerId); 
            	lLstSubject.add(iTask.Subject);  
                    lLstTask.add(iTask);
            }
        }  
        if(lSetOwnerTaskId.isEmpty() || lSetOwnerTaskId == null){ return; } 
        taskOwnerError(lSetOwnerTaskId, lLstTask, lLstSubject); 
    }   

 

    private static void taskOwnerError(Set<Id> aSetOwnerTaskId, List<Task> aLstTask, List<String> aLstSubject){

    
      if(aSetOwnerTaskId.isEmpty() || aSetOwnerTaskId == null){
        return;    
      }

      List<User> lLstUserProfile = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() AND Profile.Name NOT IN('System Administrator', 'Administrador do sistema')];
      if (lLstUserProfile.isEmpty()) { return; }     
      String lNumeroTask;      
      if(!aLstTask.isEmpty()){ 
          
      List<Task> lLstTask = [SELECT Id, Type,  WhatId, OwnerId, Subject   
                                           FROM Task WHERE OwnerId =: aSetOwnerTaskId AND 
                             				Status IN (:Label.EX3_Status_Task) AND Subject =: aLstSubject];
        for(Task iTask : lLstTask){
          lNumeroTask = iTask.Subject;           
        }    
      }   
      if(lNumeroTask == null){  return;}             
      for(Task iTask : aLstTask){  
         iTask.addError(Label.EX3_Pasta_Numero + lNumeroTask + ' ' +  Label.EX3_Tratativa);
        }               
}
}