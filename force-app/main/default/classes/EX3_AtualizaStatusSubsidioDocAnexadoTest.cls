/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste da EX3_AtualizaStatusSubsidioDocAnexado
* 
* Autor: Thiago Barbosa de Souza
  Company: Everis do Brasil 
*
********************************************************************************/
@isTest
public with sharing class EX3_AtualizaStatusSubsidioDocAnexadoTest {
    

    @isTest 
    public static void testatualizaStatusSubsidio(){

        EX3_Subsidios__c iSubsidios = new EX3_Subsidios__c();
        //iSubsidios.Status__c = 'Doc Pendente';

        Database.insert(iSubsidios);

        EX3_Tipo_Documento__c lTipoDoc = new EX3_Tipo_Documento__c();
        lTipoDoc.EX3_Arquivo_Anexado__c = false;
        lTipoDoc.EX3_Subsidios__c = iSubsidios.Id;
        
        Database.insert(lTipoDoc);

        lTipoDoc.EX3_Arquivo_Anexado__c = true; 

        Test.startTest();
            Database.update(lTipoDoc);   
        Test.stopTest();  

        List<EX3_Subsidios__c> lLstEX3Subsidios = [SELECT Id FROM EX3_Subsidios__c WHERE Id =: iSubsidios.Id];
        System.assert(!lLstEX3Subsidios.isEmpty(), 'O Status do Caso foi alterado para Finalizado');  
        

        

    }

}