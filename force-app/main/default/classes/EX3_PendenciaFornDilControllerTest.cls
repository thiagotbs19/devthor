/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por testar a classe EX3_PendenciaFornDilController
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@isTest
public with sharing class EX3_PendenciaFornDilControllerTest
{ 
    @isTest static void getPageTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devhulk';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devhulk';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        Case lCase = new Case();
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        lCase.EX3_Motivo_Situacao__c = '14'; 
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '2019001';
        lCase.EX3_Numero_do_Processo__c = '2019001';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();
        database.insert(lCase);

        Case lCase2 = new Case();
        lCase2.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        database.insert(lCase2); 

        Map<String,Object> lMapPage = EX3_PendenciaFornDilController.getPage();

        Test.startTest();
        String lColumnsAndSearch = (String) lMapPage.get('columns');
        system.assert(lColumnsAndSearch.contains('Protocolo'));
        system.assert(lColumnsAndSearch.contains('Processo'));
        system.assert(lColumnsAndSearch.contains('Prioridade'));
        system.assert(lColumnsAndSearch.contains('Motivo'));
        system.assert(lColumnsAndSearch.contains('Data de Entrada'));

        List<SObject> lLstCase = (List<SObject>)lMapPage.get('records');
        system.assert(!lLstCase.isEmpty());

        String lOptionsMotivoSituacao = (String)lMapPage.get('motivo');
        system.assert(lOptionsMotivoSituacao.contains('Solicitação devolvida ao credenciado'));
        system.assert(lOptionsMotivoSituacao.contains('Solicitação de diligência enviado ao credenciado'));
        system.assert(lOptionsMotivoSituacao.contains('Finalizado'));

        String lOptionPriority = (String)lMapPage.get('priority');
        system.assert(lOptionPriority.contains('Crítica'));
        system.assert(lOptionPriority.contains('Urgente'));
        system.assert(lOptionPriority.contains('Alta'));
        system.assert(lOptionPriority.contains('Média'));
        system.assert(lOptionPriority.contains('Baixa'));

        Test.stopTest();

    }
    
    /*@isTest static void getColumnsAndSearchTypeTest()
    {
        String lColumnsAndSearch = EX3_PendenciaFornDilController.getColumnsAndSearchType();
        Test.startTest();
        system.assert(lColumnsAndSearch.contains('Protocolo'));
        system.assert(lColumnsAndSearch.contains('Processo'));
        system.assert(lColumnsAndSearch.contains('Prioridade'));
        system.assert(lColumnsAndSearch.contains('Motivo'));
        system.assert(lColumnsAndSearch.contains('Data de Entrada'));
        Test.stopTest();
    }*/ 

    @isTest static void getCasesTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_Situacao__c = '9';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '2019001';
        lCase1.EX3_Numero_do_Processo__c = '2019001';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        lCase2.EX3_Motivo_Situacao__c = '14';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '2019002';
        lCase2.EX3_Numero_do_Processo__c = '2019002';
        lCase2.EX3_Prioridade__c = 'Média';
        lCase2.EX3_Data_de_Entrada__c = system.now();

        Case lCase3 = new Case();
        lCase3.RecordTypeId = lRTCaseRecepcao;
        lCase3.EX3_Motivo_Situacao__c = '42';
        lCase3.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase3.EX3_Numero_do_Protocolo__c = '2019003';
        lCase3.EX3_Numero_do_Processo__c = '2019003';
        lCase3.EX3_Prioridade__c = 'Alta';
        lCase3.EX3_Data_de_Entrada__c = system.now();

        database.insert(new List<Case> {lCase1, lCase2, lCase3});

        Case lCase4 = new Case();
        lCase4.RecordTypeId = lRTCaseRecepcao;

        Case lCase5 = new Case();
        lCase5.RecordTypeId = lRTCaseRecepcao; 

        Case lCase6 = new Case();
        lCase6.RecordTypeId = lRTCaseRecepcao;  

        List<Case> lLstCase = new List<Case>{lCase4, lCase5, lCase6};
        database.insert(lLstCase);

        Test.startTest();
        List<Case> lLstCaseReturn = EX3_PendenciaFornDilController.getCases();
        system.assert(!lLstCaseReturn.isEmpty());
        Test.stopTest();
    }

   /* @isTest static void optionsMotivoSituacaoTest()
    {
        String lOptionsMotivoSituacao = EX3_PendenciaFornDilController.optionsMotivoSituacao();
        Test.startTest();
        system.assert(lOptionsMotivoSituacao.contains('Solicitação devolvida ao credenciado'));
        system.assert(lOptionsMotivoSituacao.contains('Solicitação de diligência enviado ao credenciado'));
        system.assert(lOptionsMotivoSituacao.contains('Finalizado'));
        Test.stopTest();
    }

    @isTest static void optionPriorityTest()
    {
        String lOptionPriority = EX3_PendenciaFornDilController.optionPriority();
        Test.startTest();
        system.assert(lOptionPriority.contains('Urgente'));
        system.assert(lOptionPriority.contains('Alta'));
        system.assert(lOptionPriority.contains('Média'));
        system.assert(lOptionPriority.contains('Baixa'));
        Test.stopTest();
    }*/

    @isTest static void sortRecordsTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        String lRTProtocoloRecepcao = EX3_Utils.getRecordTypeIdByDevName('EX3_Protocolo__c', 'EX3_Protocolo_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_Situacao__c = '9';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '1281281';
        lCase1.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        lCase2.EX3_Motivo_Situacao__c = '14';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '1281282';
        lCase2.EX3_Numero_do_Processo__c = 'NumProcesso2';
        lCase2.EX3_Prioridade__c = 'Média';
        lCase2.EX3_Data_de_Entrada__c = system.now();

        Case lCase3 = new Case();  
        lCase3.RecordTypeId = lRTCaseRecepcao;
        lCase3.EX3_Motivo_Situacao__c = '42';
        lCase3.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase3.EX3_Numero_do_Protocolo__c = '1281283';
        lCase3.EX3_Numero_do_Processo__c = 'NumProcesso3';
        lCase3.EX3_Prioridade__c = 'Alta';
        lCase3.EX3_Data_de_Entrada__c = system.now(); 

        database.insert(new List<Case> {lCase1, lCase2, lCase3});

        Case lCase4 = new Case();
        lCase4.RecordTypeId = lRTCaseRecepcao;

        Case lCase5 = new Case();
        lCase5.RecordTypeId = lRTCaseRecepcao;

        Case lCase6 = new Case();
        lCase6.RecordTypeId = lRTCaseRecepcao;
        List<Case> lLstCase = new List<Case>{lCase4, lCase5, lCase6};
        database.insert(lLstCase);

        String lField = 'EX3_Numero_do_Protocolo__c';
        String lSort = 'ASC';
        String lJSONLstCases = JSON.serialize(lLstCase);

        Test.startTest();
        List<Case> lLstCaseSortRecords = EX3_PendenciaFornDilController.sortRecords(lField, lSort, lJSONLstCases);
        system.assert(!lLstCaseSortRecords.isEmpty());
        Test.stopTest();
    }

    @isTest static void filterRecordsTest() 
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_Situacao__c = '9';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '27262';
        lCase1.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        lCase2.EX3_Motivo_Situacao__c = '14';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '12516';
        lCase2.EX3_Numero_do_Processo__c = 'NumProcesso2';
        lCase2.EX3_Prioridade__c = 'Média';
        lCase2.EX3_Data_de_Entrada__c = system.now();

        Case lCase3 = new Case();
        lCase3.RecordTypeId = lRTCaseRecepcao;  
        lCase3.EX3_Motivo_Situacao__c = '42';
        lCase3.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase3.EX3_Numero_do_Protocolo__c = '12713';
        lCase3.EX3_Numero_do_Processo__c = 'NumProcesso3';
        lCase3.EX3_Prioridade__c = 'Alta';
        lCase3.EX3_Data_de_Entrada__c = system.now();

        database.insert(new List<Case> {lCase1, lCase2, lCase3});

        Case lCase4 = new Case();
        lCase4.RecordTypeId = lRTCaseRecepcao;

        Case lCase5 = new Case();
        lCase5.RecordTypeId = lRTCaseRecepcao;

        Case lCase6 = new Case();
        lCase6.RecordTypeId = lRTCaseRecepcao; 
        List<Case> lLstCase = new List<Case>{lCase4, lCase5, lCase6};
        database.insert(lLstCase);

        String lFieldNumProtocolo = 'EX3_Numero_do_Processo__c';
        String lSearchNumProtocolo = 'Num';

        String lFieldDataEntrada = 'EX3_Data_de_Entrada__c';
        DateTime lSearchDataInicio = system.now().addDays(-1);
    DateTime lSearchDataFIM = system.now().addDays(+1);

        Test.startTest();
        List<Case> lLstCase1 = EX3_PendenciaFornDilController.filterRecords(lFieldNumProtocolo, lSearchNumProtocolo, null, null);
        List<Case> lLstCase2 = EX3_PendenciaFornDilController.filterRecords(lFieldDataEntrada, null, lSearchDataInicio, lSearchDataFIM);
        system.assert(!lLstCase1.isEmpty()); 
        system.assert(!lLstCase2.isEmpty());
        Test.stopTest();
    }

    @isTest static void getExportExcelValuesTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        String lRTProtocoloRecepcao = EX3_Utils.getRecordTypeIdByDevName('EX3_Protocolo__c', 'EX3_Protocolo_Recepcao');

        List<Case> lLstCase = new List<Case>();

        Case lCase1 = new Case(); 
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_Situacao__c = '9';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '51277';
        lCase1.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();
        lLstCase.add(lCase1);

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        lCase2.EX3_Motivo_Situacao__c = '14';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '716126';
        lCase2.EX3_Numero_do_Processo__c = 'NumProcesso2';
        lCase2.EX3_Prioridade__c = 'Média';
        lCase2.EX3_Data_de_Entrada__c = system.now();
        lLstCase.add(lCase2);

        Case lCase3 = new Case();
        lCase3.RecordTypeId = lRTCaseRecepcao;
        lCase3.EX3_Motivo_Situacao__c = '42';
        lCase3.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase3.EX3_Numero_do_Protocolo__c = '771827';
        lCase3.EX3_Numero_do_Processo__c = 'NumProcesso3';
        lCase3.EX3_Prioridade__c = 'Alta'; 
        lCase3.EX3_Data_de_Entrada__c = system.now();
        lLstCase.add(lCase3);

        database.insert(lLstCase);

        String lFieldNumProtocolo = 'EX3_Numero_do_Processo__c';
        String lSearchNumProtocolo = 'Num'; 

        String lFieldDataEntrada = 'EX3_Data_de_Entrada__c';
        DateTime lSearchDatadeEntrada = system.now();

        Test.startTest();
        String lStringBlobExcel = EX3_PendenciaFornDilController.getExportExcelValues(JSON.serialize(lLstCase)); 
        String lFileContents = ((Blob)JSON.deserialize(lStringBlobExcel, Blob.class)).toString();

        system.assert(String.isNotBlank(lFileContents)); 
        system.assert(lFileContents.contains(lCase1.Id)); 
        system.assert(lFileContents.contains(lCase2.Id)); 
        system.assert(lFileContents.contains(lCase3.Id));
        Test.stopTest();
    } 
}