/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsável pela manipulação dos dados do usuário.
* 
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
public class UserDAO extends SObjectDAO { 

    public static final UserDAO instance = new UserDAO();

	private UserDAO(){}
	 
	public static UserDAO getInstance()
	{
		return instance;
	}

	public User buscarPorId(Id id)
	{
		User usuario = [SELECT FuncionalColaborador__c, FirstName, LastName, Email
						FROM User
						WHERE Id =: id];

		return usuario;
	}

	public User buscarPorFuncional(String funcional)
	{
		User usuario = [SELECT FuncionalColaborador__c, FirstName, LastName, Email
						FROM User
						WHERE FuncionalColaborador__c =: funcional];

		return usuario;
	}
    
}