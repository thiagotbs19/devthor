public with sharing class EX3_CreateNewAdvogado {
    
    
    @AuraEnabled
    public static string getSaveAdvog(String aFields){ 

        EX3_Advogado__c   lAdvogado = (EX3_Advogado__c ) Json.deserialize(aFields, EX3_Advogado__c.class);
        
        if(lAdvogado != null){   
            Database.insert(lAdvogado);  
            return lAdvogado.Id;
        } 
        return null; 
    }
}
