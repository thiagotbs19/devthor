/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável atribuir o status para cálculo Solicitado. 
*
*Apex Trigger : EX3_Calculo__c 
*Apex Class: EX3_CreateCalculoSolicitado
*  
*
* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/


public without sharing class EX3_CreateCalculoSolicitado {
    
    public static final String lRecordTypeCalculista = EX3_Utils.getRecordTypeIdByDevName('EX3_Calculo__c', 'EX3_Calculista');
  
    public static void execute(){
        List<EX3_Calculo__c> lLstCalculo = new List<EX3_Calculo__c>();
        for(EX3_Calculo__c iCalculo :(List<EX3_Calculo__c>) Trigger.new){
            if(iCalculo.RecordTypeId == lRecordTypeCalculista){
                lLstCalculo.add(iCalculo);   
            }
        }
        if(lLstCalculo.isEmpty() || lLstCalculo.isEmpty()){
            return ;
        }
        updateStatusFilterGroup(lLstCalculo);
    }

    private static void updateStatusFilterGroup(List<EX3_Calculo__c> aLstCalculo){

        if(aLstCalculo.isEmpty() || aLstCalculo == null){ return ;}

        for(EX3_Calculo__c iCalculo :  aLstCalculo){   
                 iCalculo.EX3_Status_do_calculo__c = EX3_Utils.CALCULO_SOLICITADO_STATUS;   
             }  
    }


}