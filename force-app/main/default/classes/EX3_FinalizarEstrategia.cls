/*********************************************************************************
*                                    Itaú - 2020
*
* Classe responsavel por executar o flow se a Estratégia for "Acordo ou Defesa". Se for
* Não Definido envia para o registro de captura de Documentos.
* AuraComponente: EX3_FinalizarEstrategia
* Empresa: everis do Brasil  
* Autor: Thiago Barbosa de Souza   
*
********************************************************************************/

public without sharing class EX3_FinalizarEstrategia {
    
    @AuraEnabled
    public static Boolean getHasPermissionSet(){
        Boolean lhasPermission = FeatureManagement.checkPermission('EX3_Criar_Tarefa'); 
        
        return lHasPermission;
   }
    
    @AuraEnabled
    public static String getflowExecuteEstrategia(String aRecordId){
        String lReturnEstrategia;
        Exception lEx; 
        try{
            if(aRecordId == null){SA7_CustomdebugLog.logError('EX3', 'EX3_CreateSubsidioController', 'redirectToObject', 'Não foi encontrado Id do registro' + lEx.getMessage(), lEx, null); return '';}
            EX3_DefinicaoEstrategia__c lDefEstrag = [SELECT Id, EX3_Subsidios__c, EX3_Estrategia__c,    
                                                     EX3_Subsidios__r.EX3_Cadastro__r.EX3_Numero_do_Processo__c, EX3_Controle_Flow__c,
                                                     EX3_Subsidios__r.EX3_Cadastro__r.EX3_Numero_Protocolo__c,
                                                     EX3_Subsidios__r.EX3_Cadastro__r.EX3_Caso__c,
                                                     EX3_Caso__c, EX3_Subsidios__r.EX3_Cadastro__c, EX3_Subsidios__r.EX3_Cadastro__r.EX3_Carteira__c,
                                                     EX3_Subsidios__r.EX3_Cadastro__r.EX3_Macro_Carteira__c  
                                                     FROM EX3_DefinicaoEstrategia__c 
                                                     WHERE Id=: aRecordId];
           
            if(lDefEstrag.EX3_Subsidios__r.EX3_Cadastro__r.EX3_Carteira__c != EX3_Utils.CARTEIRA_ICC    
               && lDefEstrag.EX3_Subsidios__r.EX3_Cadastro__r.EX3_Macro_Carteira__c != EX3_Utils.MACROCARTEIRA_CIVEL){
                   return EX3_Utils.FINALIZAR_ESTRATEGIA_MACRO;      
               }
            
            map<String, Object> flowMap = new map<String, Object>();
            flowMap.put(Label.EX3_Id_Tipo_Doc, aRecordId);   
            
            if(String.isBlank(lDefEstrag.EX3_Subsidios__c)){ 
                SA7_CustomdebugLog.logError('EX3', 'EX3_FinalizarEstrategia', 'getflowExecuteEstrategia', 'Não há Subsidio relacionado' + lEx.getMessage(), lEx, null); return '';        
            }    
            
            List<Group> lLstGroup = [SELECT Id, Name FROM Group WHERE Type='Queue'AND Name =: EX3_Utils.FILA_SUBSIDIOS];   
            
            EX3_Subsidios__c lSubsidio;
            if(lDefEstrag.EX3_Estrategia__c == Label.EX3_Nao_Definido){
                lSubsidio = new EX3_Subsidios__c(     
                    EX3_Numero_do_Processo__c = lDefEstrag.EX3_Subsidios__r.EX3_Cadastro__r.EX3_Numero_do_Processo__c,
                    EX3_Cod_Pasta__c = String.valueOf(lDefEstrag.EX3_Subsidios__r.EX3_Numero_Protocolo__c), 
                    OwnerId = lLstGroup[0].Id,      
                    EX3_Fase__c = Label.EX3_ChecklistComplementar,    
                    EX3_Cadastro__c = lDefEstrag.EX3_Subsidios__r.EX3_Cadastro__c,    
                    EX3_Caso__c = lDefEstrag.EX3_Caso__c       
                ); 
            }
            
            if(lSubsidio != null){    
                Database.insert(lSubsidio); 
                lReturnEstrategia = lSubsidio.Id; 
            }
            
            if(lDefEstrag.EX3_Estrategia__c ==  Label.EX3_Estrategia_Acordo || lDefEstrag.EX3_Estrategia__c == Label.EX3_Estrategia_Defesa){ 
                if(lDefEstrag.EX3_Controle_Flow__c == false){ 
                    Flow.Interview.EX3_Atualizar_Fila_Estrateg flowJob = new Flow.Interview.EX3_Atualizar_Fila_Estrateg(flowMap);
                    flowJob.start();
                    lReturnEstrategia = Label.EX3_Laudo_Cadastro;
                    return lReturnEstrategia;
                }
                else if(lDefEstrag.EX3_Controle_Flow__c == true){ 
                    return '';
                }
                
            } 
            
            List<EX3_Tipo_Documento__c> lLstTiposDoc = new List<EX3_Tipo_Documento__c>();

            List<EX3_Pedidos__c> lLstPedidos = [SELECT Id, EX3_Caso__c, EX3_Complemento_pedido__c, EX3_Pedido__c 
                                                FROM EX3_Pedidos__c 
                                                WHERE EX3_Caso__c =: lDefEstrag.EX3_Subsidios__r.EX3_Cadastro__r.EX3_Caso__c  AND   
                                                EX3_Pedido__c =: EX3_Utils.PEDIDO_PROCESSO_NAO_RECONHECIDO AND    
                                                EX3_Complemento_pedido__c IN ('EX3_Proposta_excluída', 'DOC/TED')];
            if(lLstPedidos.isEmpty()){return Label.EX3_Pedido ;  }  
            
            if(lDefEstrag.EX3_Estrategia__c == Label.EX3_Nao_Definido){
                for(EX3_Matriz_Tipo_Documento__c iMatriz : [SELECT EX3_Macro_Carteira__c, EX3_Matriz_Tipo_Documento__c.EX3_Documento__c, 
                                                            EX3_Matriz_Tipo_Documento__c.EX3_Tipo_Estrategia__c, EX3_Carteira__c, EX3_Assunto__c, EX3_Matriz_Tipo_Documento__c.EX3_Complemento_pedido__c   
                                                            FROM EX3_Matriz_Tipo_Documento__c WHERE EX3_Macro_Carteira__c IN (:EX3_Utils.MATRIZ_MACRO_CARTEIRA_CIVEL)
                                                            AND EX3_Carteira__c IN(:EX3_Utils.MATRIZ_CARTEIRA_IC) AND                  
                                                            EX3_Pedidos_do_Processo__c IN (:EX3_Utils.PEDIDO_PROCESSO_NAO_RECONHECIDO) AND  
                                                            EX3_Tipo_Estrategia__c IN ('Complementar') AND EX3_Documento__c != null])          
                {    
                    if(iMatriz.EX3_Complemento_pedido__c == lLstPedidos[0].EX3_Complemento_pedido__c){          
                        lLstTiposDoc.addAll(getListTipoDocumento(iMatriz.EX3_Documento__c ,lSubsidio.Id, lSubsidio.EX3_Caso__c));         
                    }   
                }
            }
            
            if(lLstTiposDoc.isEmpty()){ return lReturnEstrategia; }  
            Database.insert(lLstTiposDoc);    
            
        }catch(DmlException ex) { 
            SA7_CustomdebugLog.logError('EX3', 'EX3_FinalizarEstrategia', 'getflowExecuteEstrategia', 'Erro ao inserir o Tipo de Documento ' +
                                        ex.getMessage(), ex, new Map<String, String>());
        } 
        catch (Exception ex)  
        { 
            SA7_CustomdebugLog.logError('EX3', 'EX3_FinalizarEstrategia', 'getflowExecuteEstrategia', 'Erro ao inserir o Tipo de Documento '+ ex.getMessage(), ex, null);
        }            
        return lReturnEstrategia;        
    }   
    
    private static List<EX3_Tipo_Documento__c> getListTipoDocumento(String aDocumento, String aSubsidio, String aCaseId){
        
        List<EX3_Tipo_Documento__c> lLstTiposDoc = new List<EX3_Tipo_Documento__c>();
        if(aSubsidio == null){ return lLstTiposDoc;} 
        
        lLstTiposDoc.add(getTipoDocumento(aDocumento, aSubsidio, aCaseId,Label.EX3_CPF_do_Autor));   
        
        return lLstTiposDoc;  
    }  
    
    private static EX3_Tipo_Documento__c getTipoDocumento(String aDocumento,  String aSubsidio, String aCaseId, String aDadosBusca){
        return new EX3_Tipo_Documento__c(EX3_Documento__c	=  aDocumento, EX3_Caso__c = aCaseId, EX3_Subsidios__c= aSubsidio, EX3_Checklist__c = Label.EX3_ChecklistComplementar, EX3_Status_do_Documento__c = Label.EX3_Documento_Pendente, EX3_Dados_para_Busca__c = aDadosBusca);  
    }   
}