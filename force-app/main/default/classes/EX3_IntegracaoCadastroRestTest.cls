/**
 * Created by jmariane on 2020-04-08.
 */

@IsTest
private class EX3_IntegracaoCadastroRestTest {
    @IsTest
    static void createTest() {
        EX3_Cadastro__c cadastro = new EX3_Cadastro__c(EX3_Numero_Protocolo__c = 'teste123');
        insert cadastro;

        Test.setMock(HttpCalloutMock.class, new EX3_IntegracaoCadastroRestMock());
        EX3_IntegracaoCadastroRest integra = new EX3_IntegracaoCadastroRest(cadastro);
        Test.startTest();
        HttpResponse response = integra.cliente.create();
        System.assertEquals(200, response.getStatusCode());
        Test.stopTest();

    }
}