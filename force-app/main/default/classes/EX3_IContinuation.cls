/*********************************************************************************
*                                    Itaú - 2019
*
* Interface que prove metodos com continuation
*
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
public interface EX3_IContinuation {
  Continuation GetContinuation(string[] params);
    Object Callback(Object state);
}