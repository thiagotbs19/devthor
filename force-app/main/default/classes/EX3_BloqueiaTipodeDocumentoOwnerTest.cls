/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste
*
* 
*Apex Class: EX3_BloqueiaTipodeDocumentoOwnerTest
* Empresa: everis do Brasil 
* Autor: Thiago Barbosa 
*
********************************************************************************/
@isTest
public class EX3_BloqueiaTipodeDocumentoOwnerTest {
    
    
    @testSetup
    public static void createData(){
        
        User lUser = EX3_BI_DataLoad.getUser();
        
        Database.insert(lUser);
        
        Case lCase = EX3_BI_DataLoad.getCase();
        
        Database.insert(lCase);
        
		EX3_Tipo_Documento__c lEX3TipoDocumento = new EX3_Tipo_Documento__c();
        lEX3TipoDocumento.EX3_Documento__c	= 'Teste Doc';
        lEX3TipoDocumento.OwnerId = UserInfo.getUserId();
        lEX3TipoDocumento.EX3_Caso__c = lCase.Id;
        
        Database.insert(lEX3TipoDocumento);
    
    
    }
    
    @isTest
    public static void testCreateTipoDocumento(){
        
        User lUser = [SELECT Id FROM User LIMIT 1 ];
        
        Case lCase = [SELECT Id FROM Case LIMIT 1];
        
        EX3_Tipo_Documento__c lDocumento = [SELECT Id FROM EX3_Tipo_Documento__c LIMIT 1];
        lDocumento.OwnerId = UserInfo.getUserId();
        
        Database.update(lDocumento);
        
        Database.SaveResult lResult = Database.update(lDocumento); 
        System.assert(lResult.isSuccess(), 'Tipo de Documento não foi inserido');
}

}