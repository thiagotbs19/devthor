/*********************************************************************************
*                                    Itaú - 2020  
*
* Classe de Teste 
*  
*Apex Class: EX3_BloqueiaADJOwnerTest
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/
@isTest   
public with sharing class EX3_BloqueiaADJOwnerTest {


    @TestSetup
    static void createData(){

        String lPerfilAnalista = 'Analista Operacional';
        User lUserOperacional = EX3_BI_DataLoad.getUser(lPerfilAnalista);

        Database.insert(lUserOperacional);

        EX3_ADJ__c lADJ = new EX3_ADJ__c(); 
        lADJ.EX3_Status__c = 'Atendido'; 
        lADJ.EX3_Numero_da_Pasta__c = '003';
        lADJ.EX3_Data_da_Publicacao__c = Date.today();
        lADJ.EX3_Tipo_de_Decisao__c = 'Liminar';
        lADJ.EX3_Resultado_da_Decisao__c = 'Improcedente';
        lADJ.EX3_Multa_OBF__c = 'Arbitrada';
        lADJ.EX3_Valor_Multa_OBF__c = 2.0;
        lADJ.EX3_Checklist_de_Cumprimento__c = 'Pagamento';
        lADJ.OwnerId = lUserOperacional.Id; 
        lADJ.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Solicitacao_ADJ');

        Test.startTest();
            System.runAs(lUserOperacional){
                Database.insert(lADJ); 
            } 
        Test.stopTest();  
        
        EX3_ADJ__c lADJSegundo = new EX3_ADJ__c();
        lADJSegundo.EX3_Status__c = 'Atendido'; 
        lADJSegundo.EX3_Numero_da_Pasta__c = '003';
        lADJSegundo.EX3_Data_da_Publicacao__c = Date.today();
        lADJSegundo.EX3_Tipo_de_Decisao__c = 'Liminar';
        lADJSegundo.EX3_Resultado_da_Decisao__c = 'Improcedente';
        lADJSegundo.EX3_Multa_OBF__c = 'Arbitrada';
        lADJSegundo.EX3_Checklist_de_Cumprimento__c = 'Pagamento';
        lADJSegundo.EX3_Valor_Multa_OBF__c = 2.0;
        lADJSegundo.OwnerId =  lUserOperacional.Id; 
        
        System.runAs(lUserOperacional){ 
            Database.insert(lADJSegundo, false);  
        } 
      
        
    } 
    


     @isTest  
    private static void testBloqueiaADJOwner(){ 

        User lUserOperacional = [SELECT Id FROM User LIMIT 1]; 
        
        List<Group> lLstGroup = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName = 'EX3_Analise_de_decisao'];
        
        EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];  
        
        EX3_ADJ__c lADJSegundo = [SELECT Id FROM EX3_ADJ__c LIMIT 1]; 
        
        lADJSegundo.OwnerId = lUserOperacional.Id; 
        lADJSegundo.EX3_Numero_da_Pasta__c = '004';
        lADJSegundo.EX3_Status__c = 'Atendido';  
        
          
        Database.SaveResult lResult = Database.update(lADJSegundo, false); 
 
        System.assert(lResult.isSuccess(), 'Registro de ADJ não foi inserido');    
    }      
}