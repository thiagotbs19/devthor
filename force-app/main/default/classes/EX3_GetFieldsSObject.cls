public class EX3_GetFieldsSObject {
    
    public List<string> lstLabel {get;set;}
    public List<string> lstCampos {get;set;}
    
    public EX3_GetFieldsSObject(){
        lstCampos = new List<string>();
        
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get('EX3_Protocolo__c').getDescribe().fields.getMap();

        Set<string> setFlds = new Set<String>();
        
        for(Schema.SObjectField sfield : fieldMap.Values())
        {
        	schema.describefieldresult dfield = sfield.getDescribe();
            lstCampos.add(' | Label = '+String.valueOf(dfield.getLabel()) + ' --- ' + 'ApiName = ' + String.valueOf(dfield.getname()));
		}
        

    }

}