@istest
public class EX3_LookupSearchComponentControllerTest {
    @istest public static void getResults(){
        account a = new account(name='test1');
        insert a; 
        test.startTest();
        system.assertequals(true , EX3_LookupSearchComponentController.getResults('','account','name','test').size() >0);
        test.stopTest();
    }
    @istest public static void getResultsSOQL(){       
        account a = new account(name='test1');
        insert a;
        test.startTest(); 
        system.assertequals(true , EX3_LookupSearchComponentController.getResults('select id,name from account ','','name','test').size() >0);
        test.stopTest();
    } 
}