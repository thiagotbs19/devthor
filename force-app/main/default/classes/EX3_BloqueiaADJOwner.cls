/*********************************************************************************
*                                    Itaú - 2019
*
* Classe Responsável para que o analista não consiga puxar um registro para iniciar
* um atendimento se o mesmo ja possuir um registro de ADJ.  
*
*Apex Trigger : EX3_ADJ__c
*Apex Class: EX3_BloqueiaADJOwner
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

public with sharing class EX3_BloqueiaADJOwner {
    
    Private static Schema.SObjectType TipoGroup = Schema.getGlobalDescribe().get('Group');
    
    public static void execute(){ 
        
        Set<String> lSetADJnPasta = new Set<String>(); 
        Set<String> lSetOwner = new Set<String>();
        List<EX3_ADJ__c> lLstEX3ADJ = new List<EX3_ADJ__c>();
        for(EX3_ADJ__c iADJ : (List<EX3_ADJ__c>) Trigger.new){
            if(EX3_TriggerHelper.changedField(iADJ, 'OwnerId') &&
               iADJ.ownerId == UserInfo.getUserId()){
            lSetADJnPasta.add(iADJ.EX3_Numero_da_Pasta__c);
            lLstEX3ADJ.add(iADJ);
            lSetOwner.add(iADJ.OwnerId); 
               }
            
        }
        if(lSetADJnPasta == null || lSetADJnPasta.isEmpty()){ return ;}
        adjOwnerError(lSetADJnPasta, lLstEX3ADJ, lSetOwner);   
    } 
    
    private static void adjOwnerError(Set<String> aSetADJnPasta, List<EX3_ADJ__c> aLstEX3ADJ, Set<String> aSetOwner){
        
        
        if(aSetADJnPasta.isEmpty() || aSetADJnPasta == null || aLstEX3ADJ.isEmpty() || aLstEX3ADJ == null){
            return;
        } 
        
        List<User> lLstUserProfile = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() AND Profile.Name NOT IN('System Administrator', 'Administrador do sistema')];
      	if (lLstUserProfile.isEmpty()) { return; }  
        Map<String, String> lMapADJ = new Map<String, String>(); 
        
        for(EX3_ADJ__c iADJ : [SELECT Id, Name, OwnerId, EX3_Numero_da_Pasta__c FROM EX3_ADJ__c WHERE OwnerId =: aSetOwner AND 
                              EX3_Numero_da_Pasta__c =: aSetADJnPasta])
        {lMapADJ.put(iADJ.OwnerId , iADJ.EX3_Numero_da_Pasta__c); }
         
        if(lMapADJ == null || lMapADJ.isEmpty()) { return; }     
        
        for(EX3_ADJ__c iADJ : aLstEX3ADJ)
        {
            if (!lMapADJ.containsKey(iADJ.OwnerId)) { continue; }
            iADJ.adderror(Label.EX3_Pasta_Numero + lMapADJ.get(iADJ.OwnerId) + ' ' + Label.EX3_Tratativa);
        }
        
    }     
    
}