/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por listar protocolos da central
* AuraComponent: EX3_PendenciaCentral
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
public with sharing class EX3_PendenciaCentralController
{ 
    @AuraEnabled
    public static Map<String, Object> getPage()
    {
        String lColumnsAndSearchType = getColumnsAndSearchType();
        List<Case> lLstCases = getCases();
        String lOptionsMotivoSituacao = optionsMotivoSituacao();
        String lOptionPriority = optionPriority();
        String lCanalDeEntrada = getCanalDeEntrada();

        Map<String, Object> lMapPage = new Map<String,Object>
        {
            'columns' => lColumnsAndSearchType,
            'records' => lLstCases,
            'motivo' => lOptionsMotivoSituacao,
            'priority' => lOptionPriority,
            'canaldeentrada' => lCanalDeEntrada
        };
        return lMapPage;
    }

    private static String getColumnsAndSearchType()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();

        AttributesTypes lNumeroDoProtocolo = new AttributesTypes();
        lNumeroDoProtocolo.label = 'Protocolo';
        lNumeroDoProtocolo.value = 'EX3_Numero_do_Protocolo__c';

        lLstAttributesTypes.add(lNumeroDoProtocolo);

        AttributesTypes lNumeroDoProcesso = new AttributesTypes();
        lNumeroDoProcesso.label = 'Processo';
        lNumeroDoProcesso.value = 'EX3_Numero_do_Processo__c';
        lLstAttributesTypes.add(lNumeroDoProcesso);

        AttributesTypes lPrioridade = new AttributesTypes();
        lPrioridade.label = 'Prioridade';
        lPrioridade.value = 'EX3_Prioridade__c';
        lLstAttributesTypes.add(lPrioridade);

        AttributesTypes lSituacao = new AttributesTypes();
        lSituacao.label = 'Situação';
        lSituacao.value = 'EX3_Motivo_da_Situacao__c';
        lLstAttributesTypes.add(lSituacao);

        AttributesTypes lDataEntrada = new AttributesTypes();
        lDataEntrada.label = 'Data de Entrada';
        lDataEntrada.value = 'EX3_Data_de_Entrada__c';
        lLstAttributesTypes.add(lDataEntrada);

        AttributesTypes lCanalEntrada = new AttributesTypes();
        lCanalEntrada.label = 'Canal de entrada';
        lCanalEntrada.value = 'EX3_Canal_de_Entrada__c';
        lLstAttributesTypes.add(lCanalEntrada);

        AttributesTypes lPrazoJudicial = new AttributesTypes();
        lPrazoJudicial.label = 'Prazo judicial';
        lPrazoJudicial.value = 'EX3_Prazo_Judicial__c';
        lLstAttributesTypes.add(lPrazoJudicial);

        AttributesTypes lAutor = new AttributesTypes();
        lAutor.label = 'Autor';
        lAutor.value = 'EX3_Autor__c';
        lLstAttributesTypes.add(lAutor);

        return JSON.serialize(lLstAttributesTypes);
    }

    @AuraEnabled
    public static List<Case> getCases()
    {
        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        Set<String> lSetMotivoCentral = getMotivosCentral();
        List<User> lLstUser = [SELECT Id, EX3_Pertence__c FROM User WHERE Id = :UserInfo.getUserId()];
        String lPertence = lLstUser[0].EX3_Pertence__c;

        String lQuery = 'SELECT '+ String.escapeSingleQuotes(returnFields())
                      + ' FROM Case ' 
                      + 'WHERE RecordTypeId = :lRTCaseRecepcao AND '
                      + 'EX3_Motivo_Situacao__c = :lSetMotivoCentral LIMIT 2000 ';
		
        system.debug('lQuery === ' + lQuery);
        
        List<SObject> lLstCases = (List<SObject>) database.query(String.escapeSingleQuotes(lQuery));

        return lLstCases;
    }

    private static String optionsMotivoSituacao()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
        Set<String> lSetMotivoCentral = getMotivosCentral();
        lSetMotivoCentral.add('42');
        lSetMotivoCentral.add('Finalizado'); 

        for (Schema.PicklistEntry iPicklistLabelField : Case.EX3_Motivo_Situacao__c.getDescribe().getPicklistValues())
        {
            if (!lSetMotivoCentral.contains(iPicklistLabelField.getValue())) { continue; }
            AttributesTypes lAttributesTypes = new AttributesTypes();
            lAttributesTypes.label = iPicklistLabelField.getLabel();
            lAttributesTypes.value = iPicklistLabelField.getValue();
            lLstAttributesTypes.add(lAttributesTypes);
        }

       return JSON.serialize(lLstAttributesTypes);
    }

    private static String optionPriority()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
        for (Schema.PicklistEntry iPicklistLabelField : Case.EX3_Prioridade__c.getDescribe().getPicklistValues())
        {
            AttributesTypes lAttributesTypes = new AttributesTypes();
            lAttributesTypes.label = iPicklistLabelField.getLabel();
            lAttributesTypes.value = iPicklistLabelField.getLabel();

         lLstAttributesTypes.add(lAttributesTypes);
        }
        return JSON.serialize(lLstAttributesTypes);
    }

    private static String getCanalDeEntrada()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
        for (Schema.PicklistEntry iPicklistLabelField : Case.EX3_Canal_de_Entrada__c.getDescribe().getPicklistValues())
        {
            AttributesTypes lAttributesTypes = new AttributesTypes();
            lAttributesTypes.label = iPicklistLabelField.getLabel();
            lAttributesTypes.value = iPicklistLabelField.getLabel();
            lLstAttributesTypes.add(lAttributesTypes);
        } 
        return JSON.serialize(lLstAttributesTypes);

    }

    @AuraEnabled
    public static List<Case> sortRecords(String sortField, String sortAsc, String aCaseRecords)
    {
        List<Case> lLstCase = (List<Case>) JSON.deserialize(aCaseRecords, List<Case>.class);
        String lQuery = 'SELECT ' + String.escapeSingleQuotes(returnFields())
                      + ' FROM Case '
                      + 'WHERE Id = :lLstCase ';
               lQuery += ' ORDER BY '+String.escapeSingleQuotes(sortField)+' '+String.escapeSingleQuotes(sortAsc);

        List<SObject> lstCases = (List<SObject>) database.query(String.escapeSingleQuotes(lQuery));

        return lstCases;
    }

    @AuraEnabled
    public static List<Case> filterRecords(String aField, String aSearch, Datetime aDataInicio, Datetime aDataFim)
    {
        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Set<String> lSetMotivoCentral = getMotivosCentral();
        lSetMotivoCentral.add('42');
        lSetMotivoCentral.add('Finalizado');

        List<User> lLstUser = [SELECT Id, EX3_Pertence__c FROM User WHERE Id = :UserInfo.getUserId()];
        String lPertence = lLstUser[0].EX3_Pertence__c;

        Set<String> lSetDateFields = new Set<String> {'EX3_Data_de_Entrada__c', 'EX3_Prazo_Judicial__c'};
        Date lDataInicio = (lSetDateFields.contains(aField)) ? aDataInicio.dateGMT() : null;
        Date lDataFim = (lSetDateFields.contains(aField)) ? aDataFim.dateGMT() : null;

        String lQuery = 'SELECT ' + String.escapeSingleQuotes(returnFields()) + ' FROM Case ';

        String lWhereClause = 'WHERE RecordTypeId = :lRTCaseRecepcao AND '
                            + 'EX3_Motivo_Situacao__c = :lSetMotivoCentral ';
        if(!lSetDateFields.contains(aField))
        {
            lWhereClause += ' AND ' +String.escapeSingleQuotes(aField)+ ' LIKE\'%'+String.escapeSingleQuotes(aSearch)+'%\''  + ' LIMIT 2000';
        }
        else if (aField == 'EX3_Data_de_Entrada__c')
        {
            lWhereClause +=  ' AND DAY_ONLY('+String.escapeSingleQuotes(aField) +') >= :lDataInicio';
            lWhereClause += ' AND DAY_ONLY('+String.escapeSingleQuotes(aField) +') <= :lDataFim LIMIT 2000';
        }
        else if (aField == 'EX3_Prazo_Judicial__c')
        {
            lWhereClause +=  ' AND '+String.escapeSingleQuotes(aField) +' >= :lDataInicio';
            lWhereClause += ' AND '+String.escapeSingleQuotes(aField) +' <= :lDataFim LIMIT 2000';
        }

        String lEscapeQuery = String.escapeSingleQuotes(lQuery) + lWhereClause;
        List<SObject> lstCases = (List<SObject>) database.query(lEscapeQuery);
        return lstCases;
    }

    private static Set<String> getMotivosCentral()
    {
        Set<String> lSetMotivoCentral = new Set<String>
        {
            '2',  'Previsto de diligência',
            '3',  'Atributos obrigatórios não identificados pela IA',
            '4',  'Aguardando análise da carta precatória',
            '5',  'Aguardando análise de grandes causas',
            '9',  'Solicitação devolvida ao credenciado',
            '12', 'Aguardando validação IA',
            '14', 'Solicitação de diligência enviado ao credenciado',
            '20', 'Enviado pelo Parceiro',
            '21', 'Enviado pelo Itaú',
            '22', 'Em discussão',
            '26', 'Validação duplicidade do processo',
            '27', 'Aberto',
            '28', 'Aberto sem documento',
            '33', 'Devolvido'
        };
        return lSetMotivoCentral;
    }


    private static String returnFields()
    {
        Set<String> lSetFields = new Set<String>
        {
            'Id', 'RecordTypeId',
            'EX3_Numero_do_Protocolo__c', 'EX3_Numero_do_Processo__c',
            'EX3_Prioridade__c', 'toLabel(EX3_Motivo_Situacao__c)',
            'EX3_Data_de_Entrada__c', 'toLabel(EX3_Canal_de_Entrada__c)',
            'EX3_Prazo_Judicial__c', 'EX3_Autor__c',
            'EX3_Ciente__c'
        };

        String soqlQueryFields = '';

        for( String iField : lSetFields )
        {
            soqlQueryFields += iField + ',';
        }

        return soqlQueryFields.removeEnd(',');
    }

    @AuraEnabled
    public static String atualizaMotivoProtocolo(String aCase)
    {
        Map<String,String> lMapProtocolo;
        String lException = '';
        try 
        {
            List<Case> lLstCase = (List<Case>) JSON.deserialize(aCase, List<Case>.class);
            Map<String,String> lMapPicklistApiByLabel = new Map<String,String>();
            for (Schema.PicklistEntry iPicklist : EX3_Utils.getPicklistValuesByFieldAPI('Case', 'EX3_Canal_de_Entrada__c'))
            {
                lMapPicklistApiByLabel.put(iPicklist.getLabel(), iPicklist.getValue());
            }
            List<Case> lLstCaseNew = new List<Case>();
            List<Case> lLstCaseOld = new List<Case>();
            for (Case iCase : lLstCase)
            {
                lLstCaseOld.add(iCase.clone());

                Case lCase = iCase;
                lCase.EX3_Motivo_Situacao__c = '39';
                lCase.EX3_Canal_de_entrada__c = lMapPicklistApiByLabel.get(iCase.EX3_Canal_de_entrada__c);
                
                lLstCaseNew.add(lCase);
            }

            database.update(lLstCaseNew);

            List<EX3_ObjectFactory.AtualizarProtocolo> lLstAtualizarProtocolo = new List<EX3_ObjectFactory.AtualizarProtocolo>();
            User lUser = [SELECT Id, FuncionalColaborador__c FROM User WHERE Id = :UserInfo.getUserId()];
            for (Case iCase : lLstCaseNew)
            {
                EX3_ObjectFactory.AtualizarProtocolo lAtualizarProtocolo = new EX3_ObjectFactory.AtualizarProtocolo();
                lAtualizarProtocolo.id_protocolo = Decimal.valueOf(iCase.EX3_Numero_do_Protocolo__c);
                lAtualizarProtocolo.codigo_protocolo_externo = iCase.Id;
                lAtualizarProtocolo.codigo_motivo_situacao = Decimal.valueOf(iCase.EX3_Motivo_Situacao__c);
                lAtualizarProtocolo.operador = lUser.FuncionalColaborador__c;
                lAtualizarProtocolo.codigo_operacao = 3;
                lAtualizarProtocolo.indicador_protocolo = 0;

                lLstAtualizarProtocolo.add(lAtualizarProtocolo);
            }
            lMapProtocolo = new Map<String,String>
        	{
            	'oldProtocolo' => JSON.serialize(lLstCaseOld),
            	'newProtocolo' => JSON.serialize(lLstCaseNew),
            	'integracaoProtocolo' => JSON.serialize(lLstAtualizarProtocolo)
        	};
        }
        catch(DmlException ex) 
        {
          lException = ex.getMessage();
          SA7_CustomdebugLog.logError('EX3', 'EX3_PendenciaCentralController', 'atualizaMotivoProtocolo', 'Erro ao atualizar o registro do Protocolo '+lException, ex, null);
        }
        catch (Exception ex)
        {
           lException = ex.getMessage();
           SA7_CustomdebugLog.logError('EX3', 'EX3_PendenciaCentralController', 'atualizaMotivoProtocolo', 'Erro ao atualizar o registro '+ex.getMessage(), ex, null);
        }
        finally
        {
           if(String.isBlank(lException))
           {
              SA7_CustomdebugLog.logWarn('EX3', 'EX3_PendenciaCentralController', 'atualizaMotivoProtocolo', 'Sucesso ao atualizar registro do protocolo', null);
           }
        }

        return JSON.serialize(lMapProtocolo);
    }

    @AuraEnabled(Continuation=true)
    public static Continuation enviarProtocolo(String aParams)
    {
        Map<String,String> lMapParams = (Map<String,String>) JSON.deserialize(aParams, Map<String,String>.class);
        Map<String,String> lMapProtocolo = (Map<String,String>) JSON.deserialize(lMapParams.get('protocolos'), Map<String,String>.class);
        Map<String, String> lMapDadosRequest = new Map<String,String>
        {
           'classe' => lMapParams.get('classe'),
           'body' => lMapParams.get('integracaoProtocolo'),
           'callback' => lMapParams.get('callback'),
           'oldProtocolo' => lMapProtocolo.get('oldProtocolo'),
           'newProtocolo' => lMapProtocolo.get('newProtocolo'),
           'integracaoProtocolo' => lMapProtocolo.get('integracaoProtocolo'),
           'service' => 'atualizarProtocolo'
        };
        
        Continuation lContinuation;
        try 
        {
           lContinuation = EX3_ServiceFactory.atualizarProtocolo(lMapProtocolo.get('integracaoProtocolo'), lMapParams.get('classe'), lMapParams.get('callback'), lMapDadosRequest);
        }
        catch (Exception ex)
        {
           SA7_CustomdebugLog.logError('EX3', 'EX3_PendenciaCentralController', 'enviarProtocolo', 'Erro ao criar o Request '+ex.getMessage(), ex, null);
        }
        return lContinuation;
    }

    @AuraEnabled
    public static String enviarProtocoloCallback(Object state)
    {
        EX3_ContinuationUtils.StateInfo lRetorno = (EX3_ContinuationUtils.StateInfo)state;

        HttpResponse lResponse = Continuation.getResponse(lRetorno.state);

        Map<String,Object> lMapDataResponse = (Map<String,Object>)JSON.deserializeUntyped(lResponse.getBody());
        Map<String,Object> lMapBodyResponse = new Map<String,Object>();
        if (lResponse.getStatusCode() > 202)
        {
           SA7_CustomdebugLog.logWarn('EX3', 'EX3_PendenciaParceiroController', 'enviarProtocoloCallback','Status: ' + lResponse.getStatusCode() + '\n' + 
                                      lResponse.getBody(), null);
           Map<String,String> lResponseMsg = (Map<String, String>) JSON.deserialize(lResponse.getBody(), Map<String,String>.class);

           Map<String,String> lMapDados = (Map<String,String>)lRetorno.dados; 

           List<Case> lLstProtocolo = (List<Case>) JSON.deserialize(lMapDados.get('oldProtocolo'), List<Case>.class);
           database.update(lLstProtocolo, Case.EX3_Numero_do_Protocolo__c);
           
           String lMensagem = 'Unkown Error';
           if (String.isNotBlank(lResponseMsg.get('message')))
           {
              lMensagem = lResponseMsg.get('message');
           }
           else if (String.isNotBlank(lResponseMsg.get('Message')))
           {
              lMensagem = lResponseMsg.get('Message');
           }
           return JSON.serialize(new Map<String,Object> {
               'status' => lResponse.getStatusCode(),
               'error' => lMensagem
           });
        }
        return JSON.serialize(lMapDataResponse.get('data'));
    }

    private class AttributesTypes 
    {
        private Object label {get; set;}
        private Object value {get; set;}
    } 
}