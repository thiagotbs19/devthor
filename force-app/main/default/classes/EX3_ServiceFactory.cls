/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por realizar os request's das integracoes
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
public with sharing class EX3_ServiceFactory 
{
    public static Continuation incluirProtocolo(String aBody, String aClassName, String aCallback, Map<String,String> aDados)
    {
        return setRequestContinuation('incluirProtocolo', aBody, aClassName, aCallback, aDados);
    }

    public static Continuation atualizarProtocolo(String aBody, String aClassName, String aCallback, Map<String,String> aDados)
    {
        return setRequestContinuation('atualizarProtocolo', aBody, aClassName, aCallback, aDados);
    }

    public static Continuation incluirDocumento(String aBody, String aClassName, String aCallback, Map<String,String> aDados, String aProtocoloNumber)
    {
        return setRequestContinuation('incluirDocumento', aBody, aClassName, aCallback, aDados, aProtocoloNumber);
    }

    public static Continuation incluirDocumentoAtualizaProtocolo(Map<String,String> aBodies, String aClassName, String aCallBack, Map<String,String> aDados, String aProtocoloNumber)
    {
        return setRequestContinuation(new Set<String>{'incluirDocumento','atualizarProtocolo'}, aBodies, aClassName, aCallback, aDados, aProtocoloNumber);
    }

    private static Continuation setRequestContinuation(String aService, String aBody, String aClassName,String aCallback, Map<String,String> aDados)
    {
        Continuation__mdt lContinuationMetadata = [SELECT DeveloperName, Endpoint__c, Method__c, Timeout__c
                                                   FROM Continuation__mdt
                                                   WHERE DeveloperName = :String.escapeSingleQuotes(aService)];

        List<EX3_Parametrizacao__c> lLstToken = [SELECT EX3_Access_token__c FROM EX3_Parametrizacao__c
                                                 WHERE RecordtypeId  = :EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token')
                                                 AND EX3_Token_external__c = 'TokenSTS'];

        EX3_obter_token_STS__mdt lConfigToken = [SELECT Client_id__c FROM EX3_obter_token_STS__mdt 
                                                 WHERE DeveloperName = :'TokenEX3STS_Dev'];


        HttpRequest lRequest = new HttpRequest();
        lRequest.setMethod(lContinuationMetadata.Method__c);
        lRequest.setEndpoint(lContinuationMetadata.Endpoint__c);
        lRequest.setHeader('x-itau-apikey', lConfigToken.Client_id__c.substringAfter('client_id='));
        lRequest.setHeader('x-itau-correlationID', EX3_GuidUtil.newGuid());
        lRequest.setHeader('x-itau-flowID', EX3_GuidUtil.newGuid());
        lRequest.setHeader('Authorization',lLstToken[0].EX3_Access_token__c);
        lRequest.setHeader('Content-Type', 'application/json;charset=utf-8');
        lRequest.setBody(aBody);

        Continuation lContinuation = new Continuation(Integer.valueOf(lContinuationMetadata.Timeout__c));
        String lRequestLabel = lContinuation.addHttpRequest(lRequest);
        lContinuation.state = new EX3_ContinuationUtils.StateInfo(lRequestLabel, new Map<String, String> { aService => lRequestLabel }, aDados, aClassName);
        lContinuation.continuationMethod = aCallback;
        return lContinuation;
    }


    private static Continuation setRequestContinuation(String aService, String aBody, String aClassName,String aCallback, Map<String,String> aDados, String aProtocoloNumber)
    {
        Continuation__mdt lContinuationMetadata = [SELECT DeveloperName, Endpoint__c, Method__c, Timeout__c
                                                   FROM Continuation__mdt
                                                   WHERE DeveloperName = :String.escapeSingleQuotes(aService)];

        List<EX3_Parametrizacao__c> lLstToken = [SELECT EX3_Access_token__c FROM EX3_Parametrizacao__c
                                                 WHERE RecordtypeId  = :EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token')
                                                 AND EX3_Token_external__c = 'TokenSTS'];

        EX3_obter_token_STS__mdt lConfigToken = [SELECT Client_id__c FROM EX3_obter_token_STS__mdt 
                                                 WHERE DeveloperName = :'TokenEX3STS_Dev'];

        HttpRequest lRequest = new HttpRequest();
        lRequest.setMethod(lContinuationMetadata.Method__c);
        lRequest.setEndpoint(lContinuationMetadata.Endpoint__c.replace('{id_protocolo}', aProtocoloNumber));
        lRequest.setHeader('x-itau-apikey', lConfigToken.Client_id__c.substringAfter('client_id='));
        lRequest.setHeader('x-itau-correlationID', EX3_GuidUtil.newGuid());
        lRequest.setHeader('x-itau-flowID', EX3_GuidUtil.newGuid());
        lRequest.setHeader('Authorization',lLstToken[0].EX3_Access_token__c);
        lRequest.setHeader('Content-Type', 'application/json;charset=utf-8');
        lRequest.setBody(aBody);
        Continuation lContinuation = new Continuation(Integer.valueOf(lContinuationMetadata.Timeout__c));
        String lRequestLabel = lContinuation.addHttpRequest(lRequest);
        lContinuation.continuationMethod = aCallback;
        lContinuation.state = new EX3_ContinuationUtils.StateInfo(lRequestLabel, new Map<String, String> { aService => lRequestLabel }, aDados, aClassName);

        return lContinuation;
    }

    private static Continuation setRequestContinuation(Set<String> aServices, Map<String,String> aBodies, String aClassName,String aCallback, Map<String,String> aDados, String aProtocoloNumber)
    {
        List<Continuation__mdt> lLstContinuationMetadata = [SELECT DeveloperName, Endpoint__c, Method__c, Timeout__c
                                                   FROM Continuation__mdt
                                                   WHERE DeveloperName = :aServices];

        List<EX3_Parametrizacao__c> lLstToken = [SELECT EX3_Access_token__c FROM EX3_Parametrizacao__c
                                                 WHERE RecordtypeId  = :EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token')
                                                 AND EX3_Token_external__c = 'TokenSTS'];

        EX3_obter_token_STS__mdt lConfigToken = [SELECT Client_id__c FROM EX3_obter_token_STS__mdt 
                                                 WHERE DeveloperName = :'TokenEX3STS_Dev'];

        Continuation lContinuation = new Continuation(Integer.valueOf(lLstContinuationMetadata[0].Timeout__c));
        List<String> lLstRequestLabel = new List<String>();
        Map<String,String> lMapRequestLabel = new Map<String,String>();
        for (Continuation__mdt iContinuationMetadata: lLstContinuationMetadata)
        {
            String lEndpoint = (iContinuationMetadata.Endpoint__c.contains('{id_protocolo}')) 
                               ? iContinuationMetadata.Endpoint__c.replace('{id_protocolo}', aProtocoloNumber)
                               : iContinuationMetadata.Endpoint__c;

            HttpRequest lRequest = new HttpRequest();
            lRequest.setMethod(iContinuationMetadata.Method__c);
            lRequest.setEndpoint(lEndpoint);
            lRequest.setHeader('x-itau-apikey', lConfigToken.Client_id__c.substringAfter('client_id='));
            lRequest.setHeader('x-itau-correlationID', EX3_GuidUtil.newGuid());
            lRequest.setHeader('x-itau-flowID', EX3_GuidUtil.newGuid());
            lRequest.setHeader('Authorization',lLstToken[0].EX3_Access_token__c);
            lRequest.setHeader('Content-Type', 'application/json;charset=utf-8');
            lRequest.setBody(aBodies.get(iContinuationMetadata.DeveloperName));

            String lRequestLabel = lContinuation.addHttpRequest(lRequest);
            lMapRequestLabel.put(iContinuationMetadata.DeveloperName, lRequestLabel);
            lLstRequestLabel.add(lRequestLabel);
        }
        lContinuation.continuationMethod = aCallback;
        lContinuation.state = new EX3_ContinuationUtils.StateInfo(lLstRequestLabel, lMapRequestLabel, aDados, aClassName);
        return lContinuation;
    }
}