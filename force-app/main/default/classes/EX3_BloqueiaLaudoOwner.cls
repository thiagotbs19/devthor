/*********************************************************************************
*                                    Itaú - 2020  
*
* Classe Responsável para que o analista não consiga puxar um registro para iniciar
* um atendimento se o mesmo ja possuir um registro de Laudo. 
*
*Object : EX3_Laudo__c
*Apex Class: EX3_BloqueiaLaudoOwner
*  

* Empresa: everis do Brasil
* Autor: Isabella Tannús 
*
********************************************************************************/

public with sharing class EX3_BloqueiaLaudoOwner {
    
    Private static Schema.SObjectType TipoGroup = Schema.getGlobalDescribe().get('Group');
    
	public static void execute() 
    {
    	Set<Id> lSetOwnerLaudo = new Set<Id>();
      	List<EX3_Laudo__c> lLstEX3Laudo = new List<EX3_Laudo__c>();
        
      	for(EX3_Laudo__c iLaudo : (List<EX3_Laudo__c>) Trigger.new)
      	{	
            
            
			if(!EX3_TriggerHelper.changedField(iLaudo, 'OwnerId') && 
			   iLaudo.OwnerId  != UserInfo.getUserId() ||  iLaudo.ownerId.getSObjectType() == TipoGroup) { continue; }
            
        	lSetOwnerLaudo.add(iLaudo.OwnerId);   
        	lLstEX3Laudo.add(iLaudo); 
      	}
      	if(lSetOwnerLaudo == null || lSetOwnerLaudo.isEmpty()) { return ;}
      	laudoOwnerError(lSetOwnerLaudo, lLstEX3Laudo);
    }

    private static void laudoOwnerError(Set<Id> aSetOwnerLaudo, List<EX3_Laudo__c> aLstEX3Laudo){
        
      	if(aSetOwnerLaudo == null || aSetOwnerLaudo.isEmpty() || aLstEX3Laudo == null || aLstEX3Laudo.isEmpty()) { return; } 
      	Map<String, String> lMapProtocolos = new Map<String, String>(); 
      	for(EX3_Laudo__c iLaudo : [SELECT Id, Name, OwnerId FROM EX3_Laudo__c WHERE OwnerId =: aSetOwnerLaudo])
      	{ lMapProtocolos.put(iLaudo.OwnerId, iLaudo.Name); }  
        
      	if(lMapProtocolos == null || lMapProtocolos.isEmpty()) { return; }      
      
      	for(EX3_Laudo__c iLaudo : aLstEX3Laudo)
      	{ 
        	if (!lMapProtocolos.containsKey(iLaudo.OwnerId)) { continue; }
        	iLaudo.adderror(Label.EX3_Pasta_Numero + lMapProtocolos.get(iLaudo.OwnerId) + ' ' + Label.EX3_Tratativa); 
      	}        
    }
}