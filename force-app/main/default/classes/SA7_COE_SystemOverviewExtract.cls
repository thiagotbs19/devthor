public class SA7_COE_SystemOverviewExtract {

/********************************************************************************
 Name:  SA7_COE_SystemOverviewExtract
 Copyright © 2019 COE
=================================================================================
Purpose: The purpose of this class is to extract the System Overview information
from Setup -> System overview screen, this class is necessary because there is 
no way to extract the indicators within system overview screen neither via API
nor SOQL, thus this class extracts the information direclty from html.
=================================================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
  1.0    Vinicius Silva 22/04/19       Created         Creating system overview extract class
*********************************************************************************/
    @future (callout=true)
    public static void systemOverviewExtract(){
		
        //This custom metadata type is designed to register the parameters of integration wioth COE org
        SA7_SystemOverview__mdt SO = [SELECT SA7_indicatorsEndPoint__c, SA7_loginEndPoint__c ,MasterLabel, SA7_cleanContentMock__c, SA7_clientId__c, SA7_clientSecret__c, SA7_password__c, SA7_username__c, SA7_partialURL__c FROM SA7_SystemOverview__mdt WHERE MasterLabel=:'Credentials'];
        
        //Content extraction variables
        string cleanContent;
        string htmlContent;
		

        Pagereference systemOverviewPr = new PageReference(SO.SA7_partialURL__c);
        //When running tests the content will be mocked because test context doesn't support getContent() methods
        if(Test.isRunningTest()) {
            cleanContent = cleanContent=SO.SA7_cleanContentMock__c;
        }else{
            htmlContent = systemOverviewPr.getContent().toString();
            cleanContent = htmlContent.stripHtmlTags();
        }

        //cleaning variable contents
        cleanContent = cleanContent.remove('System Overview ~ Salesforce - Enterprise Edition System Overview Help for this Page View key usage data for your org. Schema ');
        cleanContent = cleanContent.remove('Includes custom settings, which are a special type of custom object. ');
        cleanContent = cleanContent.remove('Includes both visible and hidden custom settings. ');
        cleanContent = cleanContent.remove('Data storage is an approximation of total bytes used. ');
        cleanContent = cleanContent.remove('Code used is the total number of characters used in Apex triggers and Apex classes (excluding comments, test methods, and @isTest annotated classes). ');
        cleanContent = cleanContent.remove(' (Approx.)');
        cleanContent = cleanContent.remove('Visão geral do sistema ~ Salesforce - Enterprise Edition Visão geral do sistema Ajuda para esta página Visualize os principais dados de uso da sua organização. Esquema ');
        cleanContent = cleanContent.remove('Inclui configurações personalizadas, que são um tipo especial de objeto personalizado. ');
        cleanContent = cleanContent.remove('Configurações personalizadas Inclui configurações personalizadas visíveis e ocultas. 0 ');
        cleanContent = cleanContent.remove('O armazenamento de dados é uma aproximação do total de bytes usados. ');
        cleanContent = cleanContent.remove('O código usado é o número total de caracteres usados em acionadores e classes de Apex (excluindo comentários, métodos de teste e classes anotadas de @isTest). ');
        cleanContent = cleanContent.remove('(Aprox.) ');

        
            cleanContent = cleanContent.remove('\r\n');
            cleanContent = cleanContent.remove('\r');
            cleanContent = cleanContent.remove('\n');
            system.debug(cleanContent);
        
        //Making callout to login in COE org
        Httprequest req = new HttpRequest();
        req.setMethod('POST');    
        req.setBody('grant_type=password + &client_id=' + SO.SA7_clientId__c + '&client_secret=' + SO.SA7_clientSecret__c + '&username=' + SO.SA7_username__c +'&password=' + SO.SA7_password__c); 
        req.setEndpoint(SO.SA7_loginEndPoint__c);
        Http http = new Http();
        String Access_Token;
        
        //Getting access token
        try {
            system.debug('reqbody:'+req.getBody());
			HttpResponse res = http.send(req);
            system.debug('body11:'+res.getBody()); 
        	JSONParser parser = JSON.createParser(res.getBody());
        	while (parser.nextToken() != null) {
            	if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                    parser.nextToken();
                    Access_Token = parser.getText();    
                }
            }            
        }catch(system.CalloutException e){            
            System.debug('The following exception has occurred: ' + e.getMessage());
            SA7_Util.logError('SA7', 'SA7_COE_SystemOverviewExtract', 'systemOverviewExtract', 'SA7: retrieving access token from COE Sandbox', e, null);
        }
        
        //Sending overview extraction to COE org
        HttpRequest req1 = new HttpRequest();  
        req1.setEndpoint(SO.SA7_indicatorsEndPoint__c);  
        req1.setMethod('POST');    
        req1.setHeader('Content-Type','application/json');
        req1.setHeader('Authorization','Bearer '+Access_Token);
        req1.setBody('{"cleanContent" : "'+cleanContent+'","OrgId" : "'+system.UserInfo.getOrganizationId()+'","lang" : "'+UserInfo.getLanguage()+'"}');
        Http http1 = new Http();
        try{ 
            HttpResponse res1 = http1.send(req1);   
        }catch(system.CalloutException e){            
            System.debug('The following exception has occurred: ' + e.getMessage());
            SA7_Util.logError('SA7', 'SA7_COE_SystemOverviewExtract', 'systemOverviewExtract', 'Envio de parametros para a Sandbox do CoE', e, null);
        }

    }
}