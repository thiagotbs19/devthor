/*********************************************************************************
*                                    Itaú - 2019
*
* Classe que contém as regras de negócio relacionados ao usuário.
* 
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
public class UserBO {

    private static final UserBO instance = new UserBO();

    private UserBO(){}
    
    public static UserBO getInstance()
    {
        return instance;
    }

    public void alterar(User usuario)
    {
        UserDAO.getInstance().alterar(usuario);
    } 
    
    public void inserir(User usuario)
    {
        UserDAO.getInstance().inserir(usuario);
    } 
    
    public User getUsuarioLogado()
    {
        User usuario = this.buscarPorId(UserInfo.getUserId());

        return usuario;
    }
    
    public User buscarPorId(Id id)
    {
        User usuario = UserDAO.getInstance().buscarPorId(id);

        return usuario;
    }
    
    public User buscarPorFuncional(String funcional)
    {
        User usuario = UserDAO.getInstance().buscarPorFuncional(funcional);

        return usuario;
    }
}