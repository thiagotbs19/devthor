/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por testar a classe EX3_PendenciaParceiroController
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@isTest
public with sharing class EX3_PendenciaParceiroControllerTest
{
    @isTest static void getPageTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devhulk';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devhulk';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        Case lCase = new Case();
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        lCase.EX3_Motivo_situacao__c = '20';
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '1238812'; 
        lCase.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();
        database.insert(lCase);

        Case lCaseTwo = new Case();
        lCaseTwo.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        database.insert(lCaseTwo);

        Map<String,Object> lMapPage = EX3_PendenciaParceiroController.getPage();

        Test.startTest();
        String lColumnsAndSearch = (String) lMapPage.get('columns');
        system.assert(lColumnsAndSearch.contains('Protocolo'));
        system.assert(lColumnsAndSearch.contains('Processo'));
        system.assert(lColumnsAndSearch.contains('Prioridade'));
        system.assert(lColumnsAndSearch.contains('Motivo'));
        system.assert(lColumnsAndSearch.contains('Data de Entrada'));

        List<SObject> lLstCase = (List<SObject>)lMapPage.get('records');
        system.assert(lLstCase.isEmpty());

        String lOptionsMotivoSituacao = (String)lMapPage.get('motivo');
        system.assert(lOptionsMotivoSituacao.contains('Enviado pelo Parceiro'));
        system.assert(lOptionsMotivoSituacao.contains('Enviado pelo Itaú'));
        system.assert(lOptionsMotivoSituacao.contains('Em discussão'));
        system.assert(lOptionsMotivoSituacao.contains('Ciente pelo Parceiro'));
        system.assert(lOptionsMotivoSituacao.contains('Ciente pelo Itaú'));

        String lOptionPriority = (String)lMapPage.get('priority');
        system.assert(lOptionPriority.contains('Crítica'));
        system.assert(lOptionPriority.contains('Urgente'));
        system.assert(lOptionPriority.contains('Alta'));
        system.assert(lOptionPriority.contains('Média'));
        system.assert(lOptionPriority.contains('Baixa'));

        Test.stopTest();

    }
    
    @isTest static void getCasesTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devhulk';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devhulk';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        Case lNewCase = new Case();
        lNewCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado'); 
        lNewCase.EX3_Motivo_situacao__c = '20';
        lNewCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lNewCase.EX3_Numero_do_protocolo__c = '1281283';
        lNewCase.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lNewCase.EX3_Prioridade__c = 'Urgente';
        lNewCase.EX3_Data_de_Entrada__c = system.now();
        database.insert(lNewCase);

        Case lCase = new Case();
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        database.insert(lCase);

        Test.startTest();
        List<SObject> lLstCase = EX3_PendenciaParceiroController.getCases();
        system.assert(lLstCase.isEmpty());
        Test.stopTest();
    }

    @isTest static void sortRecordsTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devhulk';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devhulk';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_situacao__c = '20';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '99128';
        lCase1.EX3_Numero_do_Processo__c = '20190001';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        lCase2.EX3_Motivo_situacao__c = '20';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '9129129';
        lCase2.EX3_Numero_do_Processo__c = '20190002';
        lCase2.EX3_Prioridade__c = 'Urgente';
        lCase2.EX3_Data_de_Entrada__c = system.now();

        database.insert(new List<Case> {lCase1, lCase2});

        Case lCase3 = new Case();
        lCase3.RecordTypeId = lRTCaseRecepcao;

        Case lCase4 = new Case();
        lCase4.RecordTypeId = lRTCaseRecepcao; 
        List<Case> lLstCase = new List<Case>{lCase3, lCase4};
        database.insert(lLstCase);

        String lField = 'EX3_Numero_do_protocolo__c';
        String lSort = 'ASC';
        String lJSONLstCases = JSON.serialize(lLstCase);

        Test.startTest();
        List<Case> lLstCaseSortRecords = EX3_PendenciaParceiroController.sortRecords(lField, lSort, lJSONLstCases);
        system.assert(!lLstCaseSortRecords.isEmpty());
        Test.stopTest();
    }

    @isTest static void filterRecordsTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devhulk';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devhulk';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_situacao__c = '20';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '20190001';
        lCase1.EX3_Numero_do_Processo__c = '20190001';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        lCase2.EX3_Motivo_situacao__c = '20';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '20190002';
        lCase2.EX3_Numero_do_Processo__c = '20190002';
        lCase2.EX3_Prioridade__c = 'Urgente';
        lCase2.EX3_Data_de_Entrada__c = system.now();

        database.insert(new List<Case> {lCase1, lCase2});

        Case lCase3 = new Case();
        lCase3.RecordTypeId = lRTCaseRecepcao;

        Case lCase4 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;

        database.insert(new List<Case>{lCase3, lCase4});

        String lFieldNumProtocolo = 'EX3_Numero_do_Processo__c';
        String lSearchNumProtocolo = 'Num';

        String lFieldDataEntrada = 'EX3_Data_de_Entrada__c';
        DateTime lSearchDataInicio = system.now().addDays(-1);
        DateTime lSearchDataFIM = system.now().addDays(+1);

        Test.startTest();
        List<Case> lLstCase2 = EX3_PendenciaParceiroController.filterRecords(lFieldDataEntrada, null, lSearchDataInicio, lSearchDataFIM);
        Test.stopTest();
    }
    
    @isTest static void atualizaMotivoProtocoloTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];
		String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        
        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devhulk';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devhulk';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        Case lCase = new Case();
        lCase.RecordTypeId = lRTCaseRecepcao;
        lCase.EX3_Motivo_situacao__c = '20';
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '20190001';
        lCase.EX3_Numero_do_Processo__c = '20190001';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();

        database.insert(lCase);

        Case lNewCase = new Case();
        lNewCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        database.insert(lNewCase);

        List<Case> lLstCase = [SELECT Id, EX3_Motivo_situacao__c,
                      RecordTypeId, EX3_Canal_de_Entrada__c, 
                      EX3_Numero_do_Protocolo__c, EX3_Numero_do_Processo__c,
                      EX3_Prioridade__c, EX3_Data_de_Entrada__c
                      FROM Case WHERE Id = :lCase.Id];

        Test.startTest();
        String lStringMapProtocolo = EX3_PendenciaParceiroController.atualizaMotivoProtocolo(JSON.serialize(lLstCase));
        system.assert(String.isNotBlank(lStringMapProtocolo));
        Test.stopTest();
    }

    @isTest static void enviarProtocoloTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devhulk';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devhulk';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        lUser.FuncionalColaborador__c = '111222343';
        database.insert(lUser);
        
        List<EX3_Parametrizacao__c> lLstToken = new List<EX3_Parametrizacao__c>();
        EX3_Parametrizacao__c lParametrizacao = new EX3_Parametrizacao__c();
        lParametrizacao.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token');
        lParametrizacao.EX3_Access_token__c = '12345';
        lParametrizacao.EX3_Token_external__c = 'TokenSTS';
        database.insert(lParametrizacao);

        Case lCase = new Case();
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        lCase.EX3_Motivo_situacao__c = '20';
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '20190001';
        lCase.EX3_Numero_do_Processo__c = '20190001';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();

        database.insert(lCase);

        Case lCaseNew = new Case();
        lCaseNew.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        database.insert(lCaseNew);

        List<Case> lLstCase = [SELECT Id, EX3_Motivo_da_Situacao__c,
                      RecordTypeId, EX3_Canal_de_Entrada__c, 
                      EX3_Numero_do_Protocolo__c, EX3_Numero_do_Processo__c,
                      EX3_Prioridade__c, EX3_Data_de_Entrada__c
                      FROM Case WHERE Id = :lCaseNew.Id];

        String lStringMapProtocolo = EX3_PendenciaParceiroController.atualizaMotivoProtocolo(JSON.serialize(lLstCase));
        
        Map<String,String> lMapParams = new Map<String,String>
        {
            'classe'     => 'EX3_PendenciaParceiroControllerTest',
            'callback'   => 'enviarProtocoloCallback',
            'service'    => 'atualizarProtocolo',
            'protocolos' => lStringMapProtocolo
        };
        Map<String,String> lMapProtocolo = (Map<String,String>) JSON.deserialize(lStringMapProtocolo, Map<String,String>.class);
        Map<String, String> lMapDadosRequest = new Map<String,String>
        {
           'classe' => lMapParams.get('classe'),
           'body' => lMapParams.get('integracaoProtocolo'),
           'callback' => lMapParams.get('callback'),
           'oldProtocolo' => lMapProtocolo.get('oldProtocolo'),
           'newProtocolo' => lMapProtocolo.get('newProtocolo'),
           'integracaoProtocolo' => lMapProtocolo.get('integracaoProtocolo'),
           'service' => 'atualizarProtocolo'
        };
            
        EX3_ObjectFactory.AtualizarProtocolo lAtualizaProtocolo = new EX3_ObjectFactory.AtualizarProtocolo();
		lAtualizaProtocolo.id_protocolo = Decimal.valueOf(lCase.EX3_Numero_do_Processo__c);
        lAtualizaProtocolo.codigo_protocolo_externo = lCase.Id;
        lAtualizaProtocolo.codigo_motivo_situacao = Decimal.valueOf(lCase.EX3_Motivo_Situacao__c);
        lAtualizaProtocolo.operador = lUser.FuncionalColaborador__c;
        lAtualizaProtocolo.codigo_operacao = 3;
        lAtualizaProtocolo.indicador_protocolo = 0;

        Test.startTest();
        Continuation lContinuation = EX3_PendenciaParceiroController.enviarProtocolo(JSON.serialize(lMapParams));
        system.debug('lContinuation tesste ==> ' +lContinuation);
        Map<String, HttpRequest> lRequestSucess = lContinuation.getRequests();
        system.assert(!lRequestSucess.isEmpty());

        String lRequestLabelSuccess = '';
        for (String iLabel : lRequestSucess.keyset())
        {
            lRequestLabelSuccess = iLabel;
        }
	
        Map<String,String> lRequisicaoSucess = new Map<String,String>{'atualizarProtocolo' => lRequestLabelSuccess};
        Object lState = new EX3_ContinuationUtils.StateInfo(lRequestLabelSuccess,lRequisicaoSucess, lMapDadosRequest, 'EX3_PendenciaParceiroControllerTest');
        
        EX3_AtualizarProtocoloMock lMockSucesso = new EX3_AtualizarProtocoloMock(lAtualizaProtocolo, 200);
        HTTPResponse lResponseFromMockSucess = lMockSucesso.respond(lRequestSucess.get('Continuation'));
        Test.setContinuationResponse(lRequestLabelSuccess, lResponseFromMockSucess);
        
        EX3_AtualizarProtocoloMock lMockFalha = new EX3_AtualizarProtocoloMock(lAtualizaProtocolo, 400);
        
        Continuation lContinuationFalha = EX3_PendenciaParceiroController.enviarProtocolo(JSON.serialize(lMapParams));
        Map<String, HttpRequest> lRequestFalha = lContinuationFalha.getRequests();
        String lRequestLabelFalha = '';
        for (String iLabel : lRequestFalha.keySet())
        {
            lRequestLabelFalha = iLabel;
        } 
        Map<String,String> lRequisicaoFalha = new Map<String,String>{'atualizarProtocolo' => lRequestLabelFalha};
        Object lStateFalha = new EX3_ContinuationUtils.StateInfo(lRequestLabelFalha,lRequisicaoFalha, lMapDadosRequest, 'EX3_PendenciaParceiroControllerTest');
        HTTPResponse lResponseFromMockFalha = lMockFalha.respond(lRequestFalha.get('Continuation'));
        Test.setContinuationResponse(lRequestLabelFalha, lResponseFromMockFalha);

        Test.stopTest();
        
        String lResult = (String) EX3_PendenciaParceiroController.enviarProtocoloCallback(lState);
        system.assert(String.isNotBlank(lResult));

        String lResultFalha = (String) EX3_PendenciaParceiroController.enviarProtocoloCallback(lStateFalha);
        Map<String,Object> lResponseFalha = (Map<String,Object>)JSON.deserializeUntyped(lResultFalha);  
        system.assert(!lRequestFalha.isEmpty());
        system.assertEquals('Erro ao enviar o request', lResponseFalha.get('error'));


    /*    Test.startTest();
		EX3_PendenciaParceiroMock mock = new EX3_PendenciaParceiroMock(lProtocolo1.Id);
		Test.setMock(HttpCalloutMock.class, mock);	
        Continuation lContinuation = EX3_PendenciaParceiroController.GetContinuation(lLstParams);
        Map<String, HttpRequest> lRequest = lContinuation.getRequests();
        system.assert(!lRequest.isEmpty());

        Map<String, String> dados = new Map<String,String>();
        Object lState = new EX3_ContinuationUtils.StateInfo(EX3_PendenciaParceiroController.gRequestLabel, null, dados, 'EX3_PendenciaParceiroControllerTest'); 
        HTTPResponse lResponseFromMock = mock.respond(lRequest.get('Continuation-1')); 
        Test.setContinuationResponse(EX3_PendenciaParceiroController.gRequestLabel, lResponseFromMock);
        Map<String,Object> lResult = (Map<String, Object>) EX3_PendenciaParceiroController.Callback(lState);
        String lAttributes = (String) lResult.get('Sucesso');
        Map<String, String> lMapResult = (Map<String, String>) JSON.deserialize(lAttributes,Map<String, String>.class);
        String protocolid = lMapResult.get('id_protocolo');      
        Test.stopTest();
        String lResponseBody = lResponseFromMock.getBody();
        Map<String, String> lMapToGetResponse = (Map<String, String>) JSON.deserialize(lResponseBody, Map<String, String>.class);
        system.assertEquals(true, lMapToGetResponse.get('id_protocolo') == protocolid); */
    }
}