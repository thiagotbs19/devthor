/*********************************************************************************
*                                    Itaú - 2020
*
* 
*
*
*Apex Class: EX3_ProgressIndicatorController
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

public with sharing class EX3_ProgressIndicatorController {
    
    
   @AuraEnabled
   public static Map<String,Object> getCurrentStage(String aRecordId)
   {
       Map<String,Object> dados = new Map<String,Object>();
       
       List<EX3_Cadastro__c> listCadastro = [Select Id from EX3_Cadastro__c where Id =: aRecordId ];
       String lRecordCadastro = (listCadastro == null || listCadastro.isEmpty()) ? '' : listCadastro[0].Id;
       
       List<EX3_Subsidios__c> listSubsidio = [Select Id from EX3_Subsidios__c where Id =: aRecordId ];
       String lRecordSubsidio = (listSubsidio == null || listSubsidio.isEmpty()) ? '' : listSubsidio[0].Id;
       
       List<String> listStageId = new List<String>{
           aRecordId, lRecordCadastro, lRecordSubsidio
       };
           
       //var arrayList = ["step1", "step2", "step3", "step4"]; 
       String lStep = 'step1';
       
       if(String.isNotBlank(lRecordCadastro)){
           lStep = 'step2';
       }if (String.isNotBlank(lRecordSubsidio)){
           lStep = 'step3'; 
       }

           
       dados.put('listStageId', listStageId);
       dados.put('currentStep', lStep);
       
       return dados;
   }

}