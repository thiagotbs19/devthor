public with sharing class EX3_ObjectFactoryResponse
{
    public class ResponseBuscaCartaPrecatoria
    {
        public String nome {get;set;}
        public String numero_processo {get;set;}
        public String cpf {get;set;}
        public String cnpj {get;set;}
        public String pasta{get;set;}
        public String macro_carteira {get;set;}


        public Object get(String aKey)
        {
            Map<String, Object> lMap = new Map<String, Object>
            {
                'nome' => this.nome,
                'numero_processo' => this.numero_processo,
                'cpf' => this.cpf,
                'cnpj' => this.cnpj,
                'pasta' => this.pasta,
                'macro_carteira' => this.macro_carteira
            };

            return (lMap.get(aKey) != null)
                   ? lMap.get(aKey)
                   : 'Valor não existe' ;
        }

        public Map<String,Object> getMap()
        {
            Map<String, Object> lMap = new Map<String, Object>
            {
                'nome' => this.nome,
                'numero_processo' => this.numero_processo,
                'cpf' => this.cpf,
                'cnpj' => this.cnpj,
                'pasta' => this.pasta,
                'macro_carteira' => this.macro_carteira
            };

            return lMap;
        }

        public Object getMapSalesforceAPiByIntegrationAPI(String aKey)
        {
            Map<String, Object> lMap = new Map<String, Object>
            {
                'nome' => 'EX3_Autor__c',
                'numero_processo' => 'EX3_Numero_do_processo__c',
                'cpf' => 'EX3_Cpf_cnpj__c',
                'cnpj' => 'EX3_Cpf_cnpj__c',
                'pasta' => 'EX3_Pasta__c',
                'macro_carteira' => 'EX3_Macro_carteira__c'
            };

            return (lMap.get(aKey) != null)
                   ? lMap.get(aKey)
                   : 'Valor não existe' ;
        }
    }

    public class ResponseBuscaLiminar
    {
        public String nome {get;set;}
        public String numero_processo {get;set;}
        public String cpf {get;set;}
        public String cnpj {get;set;}
        public String pasta{get;set;}
        public String macro_carteira {get;set;}
        public String vara {get;set;}

        public Object get(String aKey)
        {
            Map<String, Object> lMap = new Map<String, Object>
            {
                'nome' => this.nome,
                'numero_processo' => this.numero_processo,
                'cpf' => this.cpf,
                'cnpj' => this.cnpj,
                'pasta' => this.pasta,
                'macro_carteira' => this.macro_carteira,
                'vara' => this.vara
            };

            return (lMap.get(aKey) != null)
                   ? lMap.get(aKey)
                   : 'Valor não existe' ;
        }

        public Map<String,Object> getMap()
        {
            Map<String, Object> lMap = new Map<String, Object>
            {
                'nome' => this.nome,
                'numero_processo' => this.numero_processo,
                'cpf' => this.cpf,
                'cnpj' => this.cnpj,
                'pasta' => this.pasta,
                'macro_carteira' => this.macro_carteira,
                'vara' => this.vara
            };

            return lMap;
        }

        public Object getMapSalesforceAPiByIntegrationAPI(String aKey)
        {
            Map<String, Object> lMap = new Map<String, Object>
            {
                'nome' => 'EX3_Autor__c',
                'numero_processo' => 'EX3_Numero_do_processo__c',
                'cpf' => 'EX3_Cpf_cnpj__c',
                'cnpj' => 'EX3_Cpf_cnpj__c',
                'pasta' => 'EX3_Pasta__c',
                'macro_carteira' => 'EX3_Macro_carteira__c',
                'vara' => 'EX3_Vara__c'
            };

            return (lMap.get(aKey) != null)
                   ? lMap.get(aKey)
                   : 'Valor não existe' ;
        }
    }


    public class ResponseBuscaDuplicidade
    {
        public String autor {get;set;}
        public String numero_processo {get;set;}
        public String pasta {get;set;}
        public String macro_carteira {get;set;}


        public Object get(String aKey)
        {
            Map<String, Object> lMap = new Map<String, Object>
            {
                'autor' => this.autor,
                'numero_processo' => this.numero_processo,
                'pasta' => this.pasta,
                'macro_carteira' => this.macro_carteira
            };

            system.debug('gmap>>> ' +lMap);
            return (lMap.get(aKey) != null)
                   ? lMap.get(aKey)
                   : 'Valor não existe' ;
        }

        public Map<String,Object> getMap()
        {
            Map<String, Object> lMap = new Map<String, Object>
            {
                'autor' => this.autor,
                'numero_processo' => this.numero_processo,
                'pasta' => this.pasta,
                'macro_carteira' => this.macro_carteira
            };

            return lMap;
        }

        public Object getMapSalesforceAPiByIntegrationAPI(String aKey)
        {
            Map<String, Object> lMap = new Map<String, Object>
            {
                'autor' => 'EX3_Autor__c',
                'numero_processo' => 'EX3_Numero_do_processo__c',
                'pasta' => 'EX3_Pasta__c',
                'macro_carteira' => 'EX3_Macro_carteira__c'
            };

            return (lMap.get(aKey) != null)
                   ? lMap.get(aKey)
                   : 'Valor não existe' ;
        }
    }

    public class ResponseUploadFiles {
     
     public String codigo_protocolo_externo;
     public String id_protocolo;

       public ResponseUploadFiles responseUploadFilesParse(String json) {
        return (ResponseUploadFiles) system.JSON.deserialize(json, ResponseUploadFiles.class);
     }
    }

    public class ResponseCaseAssociatedFiles {
     public String cod_retornado;
     public String id_protocolo;

       public ResponseCaseAssociatedFiles responseCaseAssociatedFilesParse(String json) {
        return (ResponseCaseAssociatedFiles) system.JSON.deserialize(json, ResponseCaseAssociatedFiles.class);
     }
    }

   public class ResponseBuscaAtiva
   {
       public String codigo_protocolo_externo; 
       public String id_protocolo;

      public ResponseBuscaAtiva responseBuscaAtivaParse(String json)
      {
       return (ResponseBuscaAtiva) system.JSON.deserialize(json, ResponseBuscaAtiva.class);
    }
   }

}