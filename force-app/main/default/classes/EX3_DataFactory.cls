/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsável por fornecer sObjects instanciados com dados para classes de teste
*
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/

public class EX3_DataFactory {
    
    public static List<Case> getListCase(Integer quantity, String aMotivoSituacao)
  	{
        List<Case> lLstCase = new List<Case>();
        for (Integer i = 0; i < quantity; i++) {
          Case sObjCase = getCase(aMotivoSituacao);
          lLstCase.add(sObjCase); 
        }
        return lLstCase; 
  	}
    
    public static Case getCase(String aMotivoSituacao)
  	{
        Case sObjCase = new Case();
        sObjCase.EX3_Numero_do_Processo__c = 'NumProcesso12';
        sObjCase.EX3_Pasta__c = 'Pasta123';
        sObjCase.EX3_Numero_do_Protocolo__c = '217162';
        sObjCase.EX3_Motivo_situacao__c = aMotivoSituacao;
        sObjCase.EX3_Data_de_Entrada__c = system.now();
        return sObjCase;
  	}
    
    public static ContentVersion getContentVersion(){
        ContentVersion sObjContentVersion = new ContentVersion();
        sObjContentVersion.Title = 'Test';
        sObjContentVersion.PathOnClient = 'Test.jpg';
        sObjContentVersion.VersionData = Blob.valueOf('Test Content Data NumProtocolo1');
        return sObjContentVersion;
    }
    
    public static ContentDocumentLink getContentDocumentLink(){
        ContentDocumentLink sObjContentDocumentLink = new ContentDocumentLink();
        sObjContentDocumentLink.Visibility = 'AllUsers';
        sObjContentDocumentLink.ShareType = 'V' ;
        return sObjContentDocumentLink;
    }
    
    public static User getAdmUser()
  	{
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];
        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devHulk';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devHulk';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        return lUser;
    } 
}