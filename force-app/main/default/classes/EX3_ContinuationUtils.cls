/*********************************************************************************
*                                    Itaú - 2019
*
* Classe para fornecer utilidades na operação com continuation
*
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
public class EX3_ContinuationUtils 
{
    public class StateInfo
    {
        public String state { get; set; }
        public List<String> stateList {get; set;}
        public String nomeClasse {get; set;}
        public Object requisicao  { get; set; }
        public Map<String, String> dados { get; set;}

        public StateInfo(String state, Object requisicao, Map<String, String> dados, string nomeClasse)
        {
            this.state = state;
            this.requisicao = requisicao;
            this.dados = dados;
            this.nomeClasse = nomeClasse;
        }

        public StateInfo(List<String> stateList, Object requisicao, Map<String, String> dados, string nomeClasse)
        {
            this.requisicao = requisicao;
            this.dados = dados;
            this.nomeClasse = nomeClasse;
            this.stateList = stateList;
        }
    }
}