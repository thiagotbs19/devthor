/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável atribuir o status para cálculo Solicitado. 
*
*
*Apex Class: EX3_CreateCalculoFUPTest
*  
*
* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

@isTest
public with sharing class EX3_CreateCalculoFUPTest {
    
    
    @testSetup
    public static void testCreateCalculoFUP(){
        
        String lRecordTypeSolicCalc = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Solicitacao_de_Calculo'); 

        
		User lUser = EX3_BI_DataLoad.getUser();  
        lUser.FuncionalColaborador__c = 'funcional';
        lUser.EX3_Pertence__c = '2'; 
        Database.insert(lUser); 
        
        Case lCase = EX3_BI_DataLoad.getCase();
        Database.insert(lCase);
        List<Group> lLstGroup = [SELECT Id, Name, DeveloperName FROM Group  
                                 WHERE Type='Queue'AND DeveloperName =: EX3_Utils.FILA_CALCULO]; 
        
        EX3_Calculo__c lCalculo = EX3_BI_DataLoad.getCalculo(lCase);
        lCalculo.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_Calculo__c', 'EX3_Calculista');
		lCalculo.OwnerId = lLstGroup[0].Id;
        
        Database.insert(lCalculo);  
    
    }
    
    @isTest static void testCreateADJSolict(){
		
        EX3_Calculo__c lCalculo = [SELECT Id FROM EX3_Calculo__c LIMIT 1];
        
        Database.SaveResult  lResult = Database.insert(lCalculo, false);
		Test.startTest();
        System.assert(!lResult.isSuccess(), 'O Cálculo ja foi inserido');
        Test.stopTest(); 
    }

}