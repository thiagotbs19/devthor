/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de Teste da EX3_CreateFilaEncerramentoADJController
* ação de Busca Ativa
* Empresa: everis do Brasil
* Autor: Thiago Barbosa 
*
********************************************************************************/
@isTest
public class EX3_CreateFilaEncerramentoADJCtrlTest {
    
    
    @TestSetup
    private static void testCreateData(){
        
        User lUser = EX3_BI_DataLoad.getUser();
        
        Database.insert(lUser);
        
        Case lCase = EX3_BI_DataLoad.getCase();
        
        Database.insert(lCase);
        
        EX3_ADJ__c lADJ = EX3_BI_DataLoad.getADJ(lCase);
        lADJ.EX3_Caso__c = lCase.Id;
        Database.insert(lADJ);
    }
    
    @isTest static void testredirectToObject(){
        User lUser = [SELECT Id FROM User LIMIT 1];
        
        
        Case lCase = [SELECT Id FROM Case LIMIT 1];
        
        
        EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];
        
        
        Test.startTest();
        	String lReturn = EX3_CreateFilaEncerramentoADJController.redirectToObject(lADJ.Id);
        Test.stopTest();
        System.assert(lReturn != null, 'O registro de ADJ foi redirecionado para a fila de encerramento');
        
        
        
    }

}