/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável por processar o Registro de ADJ   
* 
*Apex Class: EX3_ProcessarRegistroADJController
*  

* Empresa: everis do Brasil
* Autor: Bruno Sampainho Petiti
*
********************************************************************************/

public with sharing class EX3_ProcessarRegistroADJController {

    private static final String PAGAMENTO = 'Pagamento';
    private static final String OBF = 'OBF';
    private static final String ENCERRAMENTO = 'Encerramento';  
    private static final String SEM_ONUS = 'Sem ônus';
    private static final String RECORD_TYPE_ANALISE_ADJ = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Analise_de_Decisao');  
    private static final String RECORD_TYPE_SOLICIT_ADJ = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Solicitacao_ADJ'); 
    
    @AuraEnabled
    public static Boolean getHasPermissionSet(){
        Boolean lhasPermission = FeatureManagement.checkPermission('EX3_Criar_Tarefa'); 
        
        return lHasPermission;
   } 
    
    @AuraEnabled
    public static Map<string,Object> getRecordDetails(Id recordId) {
        List<String> lLstStrings = new List<String>();
        Map<String, Object> lMapReturn = new Map<String, Object>{
                PAGAMENTO => false,
                OBF => false,
                ENCERRAMENTO => false
        };
        List<EX3_ADJ__c> lLstADJ = [ 
                Select EX3_Checklist_de_Cumprimento__c, RecordTypeId,EX3_Classificacao_de_Decisao__c
                From EX3_ADJ__c   
                Where Id = :recordId];    
        // OBF, PAGAMENTO, Encerramento
        if(lLstADJ.size()>0 && lLstADJ[0].EX3_Checklist_de_Cumprimento__c != null) {
            lLstStrings = lLstADJ[0].EX3_Checklist_de_Cumprimento__c.split(';');
        }
        if(lLstADJ[0].EX3_Classificacao_de_Decisao__c == SEM_ONUS){
            lMapReturn.put(ENCERRAMENTO, true);
        } else {
            lMapReturn.put(PAGAMENTO, true);
            lMapReturn.put(OBF, true);
            lMapReturn.put(ENCERRAMENTO, true);
        }
        return lMapReturn;
    }

    @AuraEnabled
    public static String getRecordTypeADJ(String aRecordId){
        EX3_ADJ__c lADJ;
        if(String.isBlank(aRecordId)){ return '';}
            
            try{  
            
            List<EX3_ADJ__c> lLstADJ = [SELECT EX3_Checklist_de_Cumprimento__c, RecordTypeId,EX3_Classificacao_de_Decisao__c
            FROM EX3_ADJ__c      
            WHERE Id = :aRecordId AND RecordTypeId IN (:RECORD_TYPE_ANALISE_ADJ, :RECORD_TYPE_SOLICIT_ADJ)];  
            if(lLstADJ.isEmpty()){ return '';}      
            lADJ = lLstADJ[0]; 
            
            }
        catch(DmlException ex) 
        {  
            SA7_CustomdebugLog.logError('EX3', 'EX3_CreateCadastroController', 'redirectToObject', 'Erro ao inserir o Cadastro ' + ex.getMessage(), ex, new Map<String, String>()); 
        }       
        catch (Exception ex)
        { 
            SA7_CustomdebugLog.logError('EX3', 'EX3_CreateCadastroController', 'redirectToObject', 'Erro ao inserir o Cadastro '+ ex.getMessage(), ex, null);
        }
            return JSON.serialize(lADJ);      
        } 
}