/*********************************************************************************
*                                    Itaú - 2019
*
* Classe resposavel por fornecer metodos ao OpenID
* 
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
global class OpenIdItauInternalRegHandler implements Auth.RegistrationHandler {

	class RegHandlerException extends Exception { }
       
    global User createUser(Id portalId, Auth.UserData data)
    {
        User usuario = null;
        String funcional = (Test.isRunningTest()) ? data.attributeMap.get('usr') : extractUniqueIdentifier(data);
        
        try 
        {
          usuario = UserBO.getInstance().buscarPorFuncional(funcional);
        }
        catch(QueryException ex)
        {
            throw new RegHandlerException('Usuário não encontrado. Por favor, contate o administrador.');
        }
            
        return usuario;
    }
    
    global void updateUser(Id userId, Id portalId, Auth.UserData data)
    {    

        User usuario = null;
        String lFuncional = (Test.isRunningTest()) ? data.attributeMap.get('usr') : extractUniqueIdentifier(data);
        system.debug('lFuncional..... ' + lFuncional);
        
        try
        {
          	usuario = UserBO.getInstance().buscarPorFuncional(lFuncional);  
            system.debug('usuario..... ' + usuario);
            usuario.FuncionalColaborador__c = usuario.FuncionalColaborador__c;
            system.debug('usuario.FuncionalColaborador__c..... ' + usuario.FuncionalColaborador__c);
            UserBO.getInstance().alterar(usuario);
        }
        catch(QueryException ex)
        {
          throw new RegHandlerException('Usuário não encontrado. Por favor, contate o administrador.');    
        }
        catch(DMLException ex)
        {
          throw new RegHandlerException('Não foi possível atualizar os dados do usuário. Motivo: ' + ex.getMessage());          
        }
    }
       
     global String extractUniqueIdentifier(Auth.UserData data)
     {
		String identifier = '';
        if(data.attributeMap.get('jwt_token') != null)
        { 
			String[] lContent = data.attributeMap.get('jwt_token').split(','); 
            
            for(String iValue : lContent)
            { 
                if(iValue.contains('usr='))
                {
                    identifier = iValue.replace('usr=','').trim(); 
                    break;
                }
            } 
        }
       return identifier;  
    }
       
}