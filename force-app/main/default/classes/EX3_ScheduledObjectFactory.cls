public with sharing class EX3_ScheduledObjectFactory
{
    public class DadosTokenSTS
    {
    public String access_token      {get; set;}
        public String token_type    {get; set;}
        public String expires_in    {get; set;}
        public String refresh_token   {get; set;}
        public String scope       {get; set;}
        public String active      {get; set;}
    }
}