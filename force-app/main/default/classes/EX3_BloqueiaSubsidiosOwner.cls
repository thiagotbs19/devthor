/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável para que o analista não consiga puxar um registro para iniciar
* um atendimento se o mesmo ja possuir um registro de Subsídios.   
*
* 
*Apex Class: EX3_BloqueiaSubsidiosOwner 
* Empresa: everis do Brasil 
* Autor: Thiago Barbosa 
*
********************************************************************************/



public with sharing class EX3_BloqueiaSubsidiosOwner {
    

    public static void execute(){  
        Set<Id> lSetOwnerSubsid = new Set<Id>();
        List<EX3_Subsidios__c> lLstSubsidios = new List<EX3_Subsidios__c>();
        for(EX3_Subsidios__c iSubsidios : (List<EX3_Subsidios__c>) Trigger.new){
            if(EX3_TriggerHelper.changedField(iSubsidios, 'OwnerId') &&
                iSubsidios.OwnerId == UserInfo.getUserId()){
                    lSetOwnerSubsid.add(iSubsidios.OwnerId);  
                    lLstSubsidios.add(iSubsidios);
                } 
        }  
        if(lSetOwnerSubsid.isEmpty() || lSetOwnerSubsid == null){ return; } 
        subsidiosOwnerError(lSetOwnerSubsid, lLstSubsidios); 
    }   



    private static void subsidiosOwnerError(Set<Id> aSetOwnerSubsid, List<EX3_Subsidios__c> aLstSubsidios){

    
      if(aSetOwnerSubsid.isEmpty() || aLstSubsidios.isEmpty() || aLstSubsidios == null){
        return;  
      }
        
      List<User> lLstUserProfile = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() AND Profile.Name NOT IN('System Administrator', 'Administrador do sistema')];
      if (lLstUserProfile.isEmpty()) { return; } 
      String lNumeroProcesso;   
      if(!aLstSubsidios.isEmpty()){ 
          
        for(EX3_Subsidios__c iSubsidios : [SELECT Id, OwnerId, EX3_Status__c, EX3_Numero_do_Processo__c 
                                        FROM EX3_Subsidios__c  WHERE OwnerId =: aSetOwnerSubsid AND EX3_Status_da_Pasta__c NOT IN ('Em tratamento')]){
             
          lNumeroProcesso = iSubsidios.EX3_Numero_do_Processo__c;   
        }    
      }   
      if(lNumeroProcesso == null){  return;}             
      for(EX3_Subsidios__c iSubsidios : aLstSubsidios){            
         iSubsidios.addError(Label.EX3_Pasta_Numero + lNumeroProcesso + ' ' +  Label.EX3_Tratativa);
        }               
}
}