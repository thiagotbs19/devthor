/*********************************************************************************
*                                    Itaú - 2019
*
* Classe que cria ADJ Fila de encerramento
* Empresa: everis do Brasil
* Autor: Sem Autor
*
********************************************************************************/

public class EX3_CreateFilaEncerramentoADJController {

    @AuraEnabled
    public static String redirectToObject(String aRecordId)
    {
       List<EX3_ADJ__c> listENC = [Select Id, EX3_Numero_da_Pasta__c
                                             From EX3_ADJ__c 
                                             Where Id =: aRecordId ];
       
       EX3_ADJ__c lENC2 = listENC[0];
       
       Group queue = [SELECT Id FROM Group WHERE Name = 'EX3 Encerramento' and Type='Queue'];
              
       EX3_ADJ__c lENC = new EX3_ADJ__c(
           EX3_Numero_da_Pasta__c = lENC2.EX3_Numero_da_Pasta__c,
           RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Encerramento'), //Encerramento
           
           OwnerId = queue.Id
       );
       
       Database.insert(lENC);

       return lENC.Id;
   }    
}