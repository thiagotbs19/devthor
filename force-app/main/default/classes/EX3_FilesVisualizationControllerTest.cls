/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de teste da classe  EX3_FilesVisualizationController
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@isTest
public with sharing class EX3_FilesVisualizationControllerTest
{
    @isTest static void getFilesTest()
    {
        User lUser = EX3_DataFactory.getAdmUser(); 
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        
        Case lCase = new Case();  
        lCase.Status = 'New';  

        Database.insert(lCase); 

        ContentVersion lContentVersion = EX3_DataFactory.getContentVersion();
        lContentVersion.Tipo_de_Documento__c = '1';
        database.insert(lContentVersion);

        ContentVersion lContentVersionReturn = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :lContentVersion.Id];
        ContentDocumentLink lContentDocumentLink = EX3_DataFactory.getContentDocumentLink();
        lContentDocumentLink.LinkedEntityId = lCase.Id; 
        lContentDocumentLink.ContentDocumentId = lContentVersionReturn.ContentDocumentId;
        database.insert(lContentDocumentLink);

        Test.startTest();
        String lMapContentDocumentByString = EX3_FilesVisualizationController.getFiles(lCase.Id);
        Test.stopTest();  
        system.assert(String.isNotBlank(lMapContentDocumentByString));
    }
}