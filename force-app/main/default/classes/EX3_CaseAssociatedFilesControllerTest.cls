/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsável por prover métodos ao compomente que lista os arquivos associados
* ao caso e permite conferência/correção para o mesmo, permitindo a visualização e/ou inserção
* de arquivos adicionais
*
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/

@isTest
public class EX3_CaseAssociatedFilesControllerTest {	 
    
    @testSetup
    public static void createData(){
	   
       User lUser = EX3_BI_DataLoad.getUser();
       lUser.FuncionalColaborador__c = '002';
       Database.insert(lUser); 
        
       Case lCase = new Case();
       lCase.EX3_Numero_do_Protocolo__c = '2392934';
       lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

       database.insert(lCase); 
        
       ContentVersion lContentVersion = new ContentVersion();
       lContentVersion.Title = 'Test';
       lContentVersion.Tipo_de_Documento__c = '1';
       lContentVersion.PathOnClient = '/' + lContentVersion.Title + '.jpg';
       lContentVersion.VersionData = Blob.valueOf('Unit Test ContentVersion Body');
       lContentVersion.origin = 'H';
       database.insert(lContentVersion);
        
       EX3_Subsidios__c lSubsidios = new EX3_Subsidios__c();
       lSubsidios.EX3_Caso__c = lCase.Id;   

       Database.insert(lSubsidios); 
        
       EX3_Cadastro__c lCadastro = new EX3_Cadastro__c();
       lCadastro.EX3_Caso__c = lCase.Id;    
       lCadastro.OwnerId = UserInfo.getUserId(); 
        
        Database.insert(lCadastro); 
        
        EX3_Tipo_Documento__c lEX3TipoDocumento = new EX3_Tipo_Documento__c();
        lEX3TipoDocumento.EX3_Documento__c	= 'Teste Doc';   
        lEX3TipoDocumento.EX3_Subsidios__c= lSubsidios.id;  

        Database.insert(lEX3TipoDocumento); 
        
        EX3_Calculo__c lCalculo = EX3_BI_DataLoad.getCalculo(lCase);
        
        Database.insert(lCalculo);
        
        EX3_DefinicaoEstrategia__c lDefEst = EX3_BI_DataLoad.getDefEstrategica(lCase);
        
        Database.insert(lDefEst);
        
        EX3_Centralizador__c lCentralizador = new EX3_Centralizador__c();
        Database.insert(lCentralizador);
        
        List<Group> lLstGroup = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName = 'EX3_Analise_de_decisao'];
        
        EX3_ADJ__c lADJ = EX3_BI_DataLoad.getADJ(lCase);
        lADJ.EX3_Data_da_Publicacao__c = Date.today();
        lADJ.EX3_Tipo_de_Decisao__c = 'Liminar'; 
        lADJ.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF');
        lADJ.OwnerId = lLstGroup[0].Id;
    	lADJ.EX3_Resultado_da_Decisao__c = 'Improcedente';
    	lADJ.EX3_Multa_OBF__c = 'Arbitrada';
        lADJ.EX3_Checklist_de_Cumprimento__c = 'Pagamento';
    	lADJ.EX3_Valor_Multa_OBF__c = 2.0;
        
        Database.insert(lADJ); 
        
        EX3_Laudo__c lLaudo = EX3_BI_DataLoad.getLaudo(lCase);
        
        Database.insert(lLaudo);
        
        EX3_Parametrizacao__c lParametrizacao = new EX3_Parametrizacao__c();
        lParametrizacao.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token');
        lParametrizacao.EX3_Access_token__c = '12345';
        lParametrizacao.EX3_Token_external__c = 'TokenSTS';
        database.insert(lParametrizacao);
    }
    
    @isTest static void getCentralizadorTest()
    {
        Test.startTest();
        String lCentral = EX3_CaseAssociatedFilesController.getCentralizador();
    	system.assert(String.isNotBlank(lCentral));
        Test.stopTest();
    }
        
    @isTest static void getDocumentsTest()
    {
       List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];
        
       User lUser = [SELECT Id FROM User LIMIT 1];
        
       String lCentralStringyfied = EX3_CaseAssociatedFilesController.getCentralizador();
       EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralStringyfied, EX3_Centralizador__c.class);
        
        
       Case lCase = [SELECT Id, EX3_Numero_do_Protocolo__c FROM Case LIMIT 1];
       
        
       Database.update(lCase);  

       ContentVersion lContentVersion = [SELECT Id FROM ContentVersion LIMIT 1];  

       ContentDocumentLink lContentDocumentLink = new ContentDocumentLink();
       lContentDocumentLink.LinkedEntityId = lCase.Id; 
       lContentDocumentLink.ContentDocumentId = [SELECT ID, ContentDocumentId FROM ContentVersion
                                                       WHERE Id =: lContentVersion.Id].ContentDocumentId;
       lContentDocumentLink.ShareType = 'V';
       lContentDocumentLink.LinkedEntityId = lCase.Id;
       database.insert(lContentDocumentLink); 
        
       Test.startTest();
       String lDocuments = EX3_CaseAssociatedFilesController.getDocuments(lCase.Id); 
        
      
        
       system.assert(lDocuments != null ,'O Documento foi anexado'); 
       Test.stopTest();
    }
    
    
    
    @isTest static void getDocumentsSubsidiosTest()
    {
       List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];
        
       User lUser = [SELECT Id FROM User LIMIT 1];
        
       String lCentralStringyfied = EX3_CaseAssociatedFilesController.getCentralizador();
       EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralStringyfied, EX3_Centralizador__c.class);
        
        
       Case lCase = [SELECT Id FROM Case LIMIT 1];

       EX3_Subsidios__c lSubsidios = [SELECT Id FROM EX3_Subsidios__c LIMIT 1];  


       ContentVersion lContentVersion = [SELECT Id FROM ContentVersion LIMIT 1];

       ContentDocumentLink lContentDocumentLink = new ContentDocumentLink();
       lContentDocumentLink.LinkedEntityId = lCase.Id; 
       lContentDocumentLink.ContentDocumentId = [SELECT ID, ContentDocumentId FROM ContentVersion
                                                       WHERE Id =: lContentVersion.Id].ContentDocumentId;
       lContentDocumentLink.ShareType = 'V';
       database.insert(lContentDocumentLink);
        
       Test.startTest();
       String lDocuments = EX3_CaseAssociatedFilesController.getDocuments(lSubsidios.Id); 
       System.assert(lDocuments != null, 'O Documento foi anexado ao caso'); 
       Test.stopTest(); 
    }
    
        @isTest static void getDocumentsCadastroTest()
    {
       List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];
        
       User lUser = [SELECT Id FROM User LIMIT 1];
        
       String lCentralStringyfied = EX3_CaseAssociatedFilesController.getCentralizador();
       EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralStringyfied, EX3_Centralizador__c.class);
        
        
       Case lCase = [SELECT Id FROM Case LIMIT 1];

       EX3_Subsidios__c lSubsidios = [SELECT Id FROM EX3_Subsidios__c LIMIT 1];

       EX3_Cadastro__c lCadastro = [SELECT Id FROM EX3_Cadastro__c LIMIT 1]; 
        
       EX3_Calculo__c lCalculo = [SELECT Id FROM EX3_Calculo__c LIMIT 1];
        
       EX3_DefinicaoEstrategia__c lDefEst = [SELECT Id FROM EX3_DefinicaoEstrategia__c LIMIT 1];

       ContentVersion lContentVersion = [SELECT Id FROM ContentVersion LIMIT 1]; 
        
       EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];
        
       EX3_Laudo__c lLaudo = [SELECT Id FROM EX3_Laudo__c LIMIT 1];

       ContentDocumentLink lContentDocumentLink = new ContentDocumentLink();
       lContentDocumentLink.LinkedEntityId = lCase.Id;  
       lContentDocumentLink.ContentDocumentId = [SELECT ID, ContentDocumentId FROM ContentVersion
                                                       WHERE Id =: lContentVersion.Id].ContentDocumentId;
       lContentDocumentLink.ShareType = 'V'; 
       database.insert(lContentDocumentLink); 
        
       Test.startTest();
       String lDocumentsCadastro = EX3_CaseAssociatedFilesController.getDocuments(lCadastro.Id); 
       String lDocumentsCalculo = EX3_CaseAssociatedFilesController.getDocuments(lCalculo.Id);
       String lDocumentsSubsidios = EX3_CaseAssociatedFilesController.getDocuments(lSubsidios.Id);
       String lDocumentosDefEst = EX3_CaseAssociatedFilesController.getDocuments(lDefEst.Id);
       String lDocumentosADJ = EX3_CaseAssociatedFilesController.getDocuments(lADJ.Id);
       String lDocumentosLaudo = EX3_CaseAssociatedFilesController.getDocuments(lLaudo.Id);
        
       System.assert(lDocumentsCadastro != null);  
       Test.stopTest();
    }
    
    
        @isTest static void getDocumentsTipoDocumentoTest()
    {
       List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];
        
       User lUser = [SELECT Id FROM User LIMIT 1]; 
        
       String lCentralStringyfied = EX3_CaseAssociatedFilesController.getCentralizador();
       EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralStringyfied, EX3_Centralizador__c.class);
        
        
       Case lCase = [SELECT Id FROM Case LIMIT 1]; 

       EX3_Subsidios__c lSubsidios = [SELECT Id FROM EX3_Subsidios__c LIMIT 1]; 


       EX3_Cadastro__c lCadastro = [SELECT Id FROM EX3_Cadastro__c LIMIT 1];
        
       EX3_Calculo__c lCalculo = [SELECT Id FROM EX3_Calculo__c LIMIT 1]; 
        
       EX3_Tipo_Documento__c lTipoDoc = [SELECT Id FROM EX3_Tipo_Documento__c LIMIT 1];
        
      

       ContentVersion lContentVersion = [SELECT Id FROM ContentVersion LIMIT 1];

       ContentDocumentLink lContentDocumentLink = new ContentDocumentLink();
       lContentDocumentLink.LinkedEntityId = lCase.Id;
       lContentDocumentLink.ContentDocumentId = [SELECT ID, ContentDocumentId FROM ContentVersion
                                                       WHERE Id =: lContentVersion.Id].ContentDocumentId;
       lContentDocumentLink.ShareType = 'V';
       database.insert(lContentDocumentLink);   
        
       Test.startTest();
       String lDocuments = EX3_CaseAssociatedFilesController.getDocuments(lTipoDoc.Id);   
        
       System.assert(lDocuments != null);
       Test.stopTest();
    }

    @isTest static void setDocumentsTest() 
    {
       List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

       User lUser =  [SELECT Id FROM User LIMIT 1];
         
       String lCentralStringyfied = EX3_CaseAssociatedFilesController.getCentralizador();  
       EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralStringyfied, EX3_Centralizador__c.class);
        
        
       Case lCase = [SELECT Id FROM Case LIMIT 1];
        
       ContentVersion lContentVersion = [SELECT Id FROM ContentVersion LIMIT 1];

       EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];  
       
       EX3_Tipo_Documento__c lEX3TipoDocumento = [SELECT Id FROM EX3_Tipo_Documento__c LIMIT 1]; 

       ContentDocumentLink lContentDocumentLink = new ContentDocumentLink();
       lContentDocumentLink.LinkedEntityId = lCentral.Id;
       lContentDocumentLink.ContentDocumentId = [SELECT ID, ContentDocumentId FROM ContentVersion
                                                       WHERE Id =: lContentVersion.Id].ContentDocumentId;
       lContentDocumentLink.ShareType = 'V';
       database.insert(lContentDocumentLink); 
       
       Test.startTest(); 
  

       String lGetReturnStringyfied = EX3_CaseAssociatedFilesController.setDocuments(lADJ.Id, lCentralStringyfied);
       String lGetReturnDoc = EX3_CaseAssociatedFilesController.setDocuments(lEX3TipoDocumento.Id, lCentralStringyfied);
       Map<String,Object> lMapToGetReturn = (Map<String,Object>) JSON.deserializeUntyped(lGetReturnStringyfied);
       system.assert(!lMapToGetReturn.isEmpty());   
       Test.stopTest();

    }
    
    @isTest static void associarArquivosTest()
    {    	
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = [SELECT Id FROM User LIMIT 1];
		
        EX3_Centralizador__c lCentralizador = [SELECT Id FROM EX3_Centralizador__c LIMIT 1];
        
        String lCentralStringyfied = testgetCentralizador();
        EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralStringyfied, EX3_Centralizador__c.class);
		
            
        Case lCase = [SELECT Id FROM Case LIMIT 1]; 
        
        ContentVersion lContentVersion = [SELECT Id FROM ContentVersion LIMIT 1];
        
       EX3_Subsidios__c lSubsidios = [SELECT Id FROM EX3_Subsidios__c LIMIT 1];

       EX3_Cadastro__c lCadastro = [SELECT Id FROM EX3_Cadastro__c LIMIT 1]; 
        
       EX3_Calculo__c lCalculo = [SELECT Id FROM EX3_Calculo__c LIMIT 1];
        
       EX3_DefinicaoEstrategia__c lDefEst = [SELECT Id FROM EX3_DefinicaoEstrategia__c LIMIT 1];
        
       EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];
        
       EX3_Laudo__c lLaudo = [SELECT Id FROM EX3_Laudo__c LIMIT 1]; 
        
       EX3_Tipo_Documento__c lDocumento = [SELECT Id FROM EX3_Tipo_Documento__c LIMIT 1];

        ContentDocumentLink lContentDocumentLink = new ContentDocumentLink();
        lContentDocumentLink.LinkedEntityId = lCentral.Id; 
        lContentDocumentLink.ContentDocumentId = [SELECT ID, ContentDocumentId FROM ContentVersion
                                                       WHERE Id =: lContentVersion.Id].ContentDocumentId;
        lContentDocumentLink.ShareType = 'V';
        database.insert(lContentDocumentLink);
        
        Test.startTest(); 
        String lCaseStringyfiedProt = EX3_CaseAssociatedFilesController.associarArquivos(lCentralStringyfied, lCase.Id); 
        String lCaseStringyfiedCadas = EX3_CaseAssociatedFilesController.associarArquivos(lCentralStringyfied, lCadastro.Id);
        String lCaseStringyfiedlCalc = EX3_CaseAssociatedFilesController.associarArquivos(lCentralStringyfied, lCalculo.Id);
        String lCaseStringyfiedDefEst = EX3_CaseAssociatedFilesController.associarArquivos(lCentralStringyfied, lDefEst.Id);
        String lCaseStringyfiedADJ = EX3_CaseAssociatedFilesController.associarArquivos(lCentralStringyfied, lADJ.Id);
        String lCaseStringyfiedlLaudo = EX3_CaseAssociatedFilesController.associarArquivos(lCentralStringyfied, lLaudo.Id);
        String lCaseStringyfiedSubsidios = EX3_CaseAssociatedFilesController.associarArquivos(lCentralStringyfied, lSubsidios.Id);
        String lCaseStringifyfiedDocument = EX3_CaseAssociatedFilesController.associarArquivos(lCentralStringyfied, lDocumento.Id);
        System.assert(lCaseStringyfiedProt != null, 'O Documento foi anexado ao Caso');  
        Test.stopTest();

    } 
    
    @isTest static void enviarArquivosTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = [SELECT Id, FuncionalColaborador__c, EX3_Pertence__c FROM User LIMIT 1];

        String lRTProtocoloRecepcao = EX3_Utils.getRecordTypeIdByDevName('EX3_Protocolo__c', 'EX3_Protocolo_Recepcao');

        String lCentralStringyfied = testgetCentralizador();
        EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralStringyfied, EX3_Centralizador__c.class);

        
        Case lCase =[SELECT Id, EX3_Numero_do_Protocolo__c FROM Case LIMIT 1];

        EX3_Parametrizacao__c lParametrizacao = [SELECT Id FROM EX3_Parametrizacao__c LIMIT 1];
        ContentVersion lContentVersion1 = [SELECT Id FROM ContentVersion LIMIT 1];
        
        ContentVersion lContentVersion2 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :lContentVersion1.Id];

        ContentDocumentLink lContentDocumentLink = new ContentDocumentLink();
        lContentDocumentLink.LinkedEntityId = lCentral.Id;
        lContentDocumentLink.ShareType = 'V';
        lContentDocumentLink.ContentDocumentId = lContentVersion2.ContentDocumentId;
        
        database.insert(lContentDocumentLink);

        EX3_ObjectFactory.IncluirDocumento lIncluirDocumento = new EX3_ObjectFactory.IncluirDocumento();
        lIncluirDocumento.nome_documento = lContentDocumentLink.ContentDocument.Title;
        lIncluirDocumento.codigo_documento_plataforma_externa = lContentDocumentLink.Id;
        lIncluirDocumento.operador_conglomerado = lUser.FuncionalColaborador__c;
        
        List<EX3_ObjectFactory.IncluirDocumento> lLstIncluirDocumento = new List<EX3_ObjectFactory.IncluirDocumento>{lIncluirDocumento};
        
        Map<String, String> lMapToSendParams = new Map<String, String>();
        lMapToSendParams.put('classe', 'EX3_CaseAssociatedFilesControllerTest');
        lMapToSendParams.put('documentos', JSON.serialize(lLstIncluirDocumento));
        lMapToSendParams.put('callback', 'enviarDocumentosCallback');
        lMapToSendParams.put('objeto', JSON.serialize(lCase));  
        lMapToSendParams.put('case', JSON.serialize(lCase));
             
         Map<String, String> lMapDadosRequest = new Map<String,String>
         {
            'case' => lMapToSendParams.get('case'),
            'classe' => lMapToSendParams.get('classe'),
            'objeto'=> lMapToSendParams.get('objeto'),
            'body' => lMapToSendParams.get('documentos'),
            'callback' => 'enviarArquivosCallback',
            'service' => 'incluirDocumento',
            'rollback' => JSON.serialize(new Set<String> {lContentDocumentLink.Id})
         };

        Test.startTest();
        
        EX3_IncluirDocumentoMock lMockSucesso = new EX3_IncluirDocumentoMock(lIncluirDocumento, 200);
        Test.setMock(HttpCalloutMock.class, lMockSucesso);
        Continuation lContinuation = EX3_CaseAssociatedFilesController.enviarArquivos(JSON.serialize(lMapToSendParams));
        Map<String, HttpRequest> lRequestSucess = lContinuation.getRequests();
        system.assert(!lRequestSucess.isEmpty());

        String lRequestLabelSuccess = '';
        for (String iLabel : lRequestSucess.keyset()) 
        {
            lRequestLabelSuccess = iLabel;
        }
	
        Map<String,String> lRequisicaoSucess = new Map<String,String>{'incluirDocumento' => lRequestLabelSuccess};
        Object lState = new EX3_ContinuationUtils.StateInfo(lRequestLabelSuccess,lRequisicaoSucess, lMapDadosRequest, 'EX3_CaseAssociatedFilesControllerTest');
        HTTPResponse lResponseFromMockSucess = lMockSucesso.respond(lRequestSucess.get('Continuation'));
        Test.setContinuationResponse(lRequestLabelSuccess, lResponseFromMockSucess);
        
        EX3_IncluirDocumentoMock lMockFalha = new EX3_IncluirDocumentoMock(lIncluirDocumento, 400); 
        Map<String, HttpRequest> lRequestFalha = new Map<String, HttpRequest>();
        Continuation lContinuationFalha = EX3_UploadFilesController.enviarDocumentos(JSON.serialize(lMapToSendParams));
        lRequestFalha = lContinuationFalha.getRequests();
        String lRequestLabelFalha = '';
        for (String iLabel : lRequestFalha.keySet())
        {
            lRequestLabelFalha = iLabel;
        } 
        Map<String,String> lRequisicaoFalha = new Map<String,String>{'incluirDocumento' => lRequestLabelFalha};
        Object lStateFalha = new EX3_ContinuationUtils.StateInfo(lRequestLabelFalha,lRequisicaoFalha, lMapDadosRequest, 'EX3_CaseAssociatedFilesControllerTest');
        HTTPResponse lResponseFromMockFalha = lMockFalha.respond(lRequestFalha.get('Continuation'));
        Test.setContinuationResponse(lRequestLabelFalha, lResponseFromMockFalha);

        Test.stopTest(); 
	
        String lResultFalha = (String) EX3_CaseAssociatedFilesController.enviarArquivosCallback(lStateFalha);
        Map<String,Object> lResponseFalha = (Map<String,Object>)JSON.deserializeUntyped(lResultFalha);  
        system.assert(!lRequestFalha.isEmpty());
        system.assertEquals('Erro ao enviar o request', lResponseFalha.get('error'));
    }
    
    private static String testgetCentralizador(){ 
        EX3_Centralizador__c lCentralizador = new EX3_Centralizador__c();
        Database.insert(lCentralizador);
        String lCentralizadorText = JSON.serialize(lCentralizador);
        return lCentralizadorText;
    }  
    
    private class AttributesTypesContentDocument
    {
        private Object label {get;set;}
        private Object size {get;set;}
        private Object createdDate {get;set;}
        private Object fileExtension{get;set;}
        private Object url {get;set;}
        private Object icon {get;set;}
    }
}