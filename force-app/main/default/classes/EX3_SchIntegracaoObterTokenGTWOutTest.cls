/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por testar a classe EX3_SchIntegracaoObterTokenGTWOut
*
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
@isTest
private class EX3_SchIntegracaoObterTokenGTWOutTest {

    static testMethod void testSchedule_EX3()
    {

        EX3_SchIntegracaoObterTokenGTWOut sh1 = new EX3_SchIntegracaoObterTokenGTWOut();
        String sch = '0 0 23 * * ?';
        Test.startTest();
          Test.setMock(HttpCalloutMock.class, new EX3_IntegracaoObterTokenGTWOutMock());
          system.schedule('Teste atualização de token 1', sch, sh1);
        Test.stopTest();
    }

    static testMethod void testSchedule_EX3_Postagem()
    {
        Test.startTest();
          EX3_SchIntegracaoObterTokenGTWOut.criarPostagem('Teste dev');
        Test.stopTest();
    }

}