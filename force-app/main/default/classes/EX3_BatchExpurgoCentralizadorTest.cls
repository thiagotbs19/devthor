/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsável por testar a execução do batch de expurgo de registros do 
* objeto centralizador
*
* Batch: EX3_BatchExpurgoCentralizador
* Empresa: everis
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/

@isTest
public class EX3_BatchExpurgoCentralizadorTest {
    
    @isTest
    public static void testCentralizadorDeletion()
    {
        List<EX3_Centralizador__c> lLstCentralizador = new List<EX3_Centralizador__c>();
        for(Integer i=0;i<200;i++)
        {
            EX3_Centralizador__c lCentralizador = new EX3_Centralizador__c();
            lLstCentralizador.add(lCentralizador);
        }
        insert lLstCentralizador;
       
        Test.startTest();
            EX3_BatchExpurgoCentralizador obj = new EX3_BatchExpurgoCentralizador();
            DataBase.executeBatch(obj);
        Test.stopTest();
    }

}