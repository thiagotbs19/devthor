/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por simular a resposta do Serviço do Gateway
*
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
@isTest
public class EX3_IntegracaoObterTokenGTWOutMock implements HttpCalloutMock{

    protected Integer statusCode;

    public EX3_IntegracaoObterTokenGTWOutMock() {
        this.statusCode = 200;
    }

    public EX3_IntegracaoObterTokenGTWOutMock(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public HTTPResponse respond(HTTPRequest req) {

        EX3_ScheduledObjectFactory.DadosTokenSTS body = new EX3_ScheduledObjectFactory.DadosTokenSTS();

        body.access_token = EX3_GuidUtil.newGuid();
        body.active = 'True';
        body.expires_in = '300';
        body.refresh_token = EX3_GuidUtil.newGuid();
        body.scope = 'Bearer';
        body.token_type = 'resource.READ';

        String jsonString = JSON.serialize(body);
        HttpResponse res = new HttpResponse();
        res.setStatusCode(statusCode);
        res.setBody(jsonString);

        return res;
    }
}