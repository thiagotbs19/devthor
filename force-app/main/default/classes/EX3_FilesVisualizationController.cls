/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por exibir os arquivos associados a um protocolo
* AuraComponente: EX3_FilesVisualizationController
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/

public with sharing class EX3_FilesVisualizationController
{
    @AuraEnabled
    public static String getFiles(String aCaseId)
    {
        Case lCase = [SELECT Id FROM Case WHERE Id =: aCaseId];

        List<AttributesTypesContentDocument> lLstAttributesTypes = new List<AttributesTypesContentDocument>();
        for (ContentDocumentLink iContentDocumentLink : [SELECT ContentDocumentId, ContentDocument.FileExtension,
                                                        ContentDocument.Title, ContentDocument.CreatedDate,
                                                        ContentDocument.ContentSize
                                                        FROM ContentDocumentLink WHERE LinkedEntityId =: lCase.Id])
        {
            system.debug('iContentDocumentLink ==> ' +iContentDocumentLink);
            AttributesTypesContentDocument lAttributesTypes = new AttributesTypesContentDocument();
            lAttributesTypes.label = iContentDocumentLink.ContentDocument.Title;
            lAttributesTypes.createdDate = iContentDocumentLink.ContentDocument.CreatedDate.dateGMT();
            lAttributesTypes.size = EX3_Utils.getByteSize(iContentDocumentLink.ContentDocument.ContentSize);
            lAttributesTypes.fileExtension = iContentDocumentLink.ContentDocument.FileExtension ;
            lAttributesTypes.url = URL.getSalesforceBaseUrl().toExternalForm() + '/' + iContentDocumentLink.ContentDocumentId;
            lAttributesTypes.icon = EX3_Utils.getLightningIconByContentDocumentFileExtension(iContentDocumentLink.ContentDocument.FileExtension);
            system.debug('lAttributesTypes ==> ' +lAttributesTypes);
            lLstAttributesTypes.add(lAttributesTypes);
        }
        system.debug('lLstAttributesTypes ==> '+lLstAttributesTypes);
        return JSON.serialize(lLstAttributesTypes);
    }

    private class AttributesTypesContentDocument
    {
        private Object label {get;set;}
        private Object size {get;set;}
        private Object createdDate {get;set;}
        private Object fileExtension{get;set;}
        private Object url {get;set;}
        private Object icon {get;set;}
    }
}