/*********************************************************************************
 *                                    Itaú - 2019
 *
 * Classe utils do projeto EX3
 * Empresa: everis
 * Autor: Rafael Amaral Moreira
 *
 ********************************************************************************/
public with sharing class EX3_Utils
{
	//Valores de picklist para Macro Carteira
	public static final String MACROCARTEIRA_CIVEL = Label.EX3_Macro_Civel;
	public static final String MACROCARTEIRA_TRABALHISTA = Label.EX3_Macro_Carteira_Trabalhista;

	//Valores de picklist para Carteira
	public static final String CARTEIRA_ICC = Label.EX3_Carteira_IC;
	public static final String CARTEIRA_ATACADOPJ;
	public static final String CARTEIRA_COLETIVOS;
	public static final String CARTEIRA_CONTRA_COBRANCA_PF;
	public static final String CARTEIRA_CREDICARD;
	public static final String CARTEIRA_FUNDACOES; //Fundações
	public static final String CARTEIRA_FRAUDES_E_ILICITOS; //Fraudes e ilícitos
	public static final String CARTEIRA_IMOBILIARIO_CONTRA_COBRANCA; //Imobiliário contra cobrança
	public static final String CARTEIRA_MASSIFICADO; //Massificado
	public static final String CARTEIRA_PLANOS_ECONOMICOS; //Planos econômicos
	public static final String CARTEIRA_REDECARD; //Redecard
	public static final String CARTEIRA_SEGURADORA; //Seguradora
	public static final String CARTEIRA_SERVICOS_BANCARIOS; //Serviços bancários
	public static final String CARTEIRA_VAREJO_PJ_COBRANCA; //Varejo PJ contra cobrança
	public static final String CARTEIRA_GRANDES_CAUSAS; //Grandes causas

	//Valores de picklist para Canal de Entrada
	public static final String CANAL_DE_ENTRADA_COCKPIT = Label.EX3_Canal_de_Entrada_COCKPIT;
	public static final String CANAL_DE_ENTRADA_CAT = Label.EX3_Canal_de_Entrada_CAT;
	public static final String CANAL_DE_ENTRADA_CEIC = Label.EX3_Canal_de_Entrada_CEIC;
	public static final String CANAL_DE_ENTRADA_PINHEIROS = Label.EX3_Canal_de_Entrada_PINHEIROS;
	public static final String CANAL_DE_ENTRADA_RECIFE = Label.EX3_Canal_de_Entrada_RECIFE;
	public static final String CANAL_DE_ENTRADA_RJ = Label.EX3_Canal_de_Entrada_RJ;
	public static final String CANAL_DE_ENTRADA_LUIZACRED = Label.EX3_Canal_de_Entrada_LUIZACRED;
	public static final String CANAL_DE_ENTRADA_CITACAO_ELETRONICA = Label.EX3_Canal_de_Entrada_CITACAO_ELETRONICA;
	public static final String CANAL_DE_ENTRADA_BUSCA_ATIVA = Label.EX3_Canal_de_Entrada_BUSCA_ATIVA;
	public static final String CANAL_DE_ENTRADA_BANORTE = Label.EX3_Canal_de_Entrada_BANORTE;
	public static final String CANAL_DE_ENTRADA_PORTO_SEGURO = Label.EX3_Canal_de_Entrada_PORTO_SEGURO;
	public static final String CANAL_DE_ENTRADA_BMG = Label.EX3_Canal_de_Entrada_BMG;
	public static final String CANAL_DE_ENTRADA_ACE = Label.EX3_Canal_de_Entrada_ACE;
	public static final String CANAL_DE_ENTRADA_AIG_SEGUROS = Label.EX3_Canal_de_Entrada_AIG_SEGUROS;
	public static final String CANAL_DE_ENTRADA_ITAUTEC = Label.EX3_Canal_de_Entrada_ITAUTEC;
	public static final String CANAL_DE_ENTRADA_GRUPO_MRS = Label.EX3_Canal_de_Entrada_GRUPO_MRS;
	public static final String CANAL_DE_ENTRADA_CITIBANK = Label.EX3_Canal_de_Entrada_CITIBANK;
	public static final String CANAL_DE_ENTRADA_PRUDENTIAL = Label.EX3_Canal_de_Entrada_PRUDENTIAL;
	public static final String CANAL_DE_ENTRADA_FIRST_DATA = Label.EX3_Canal_de_Entrada_FIRST_DATA;
	public static final String STATUS_SOLICITADO_FORNECEDOR = Label.EX3_Solicitado_Fornecedor;
	public static final String STATUS_ATENDIDO = Label.EX3_Status_Atendido;
	public static final String MATRIZ_MACRO_CARTEIRA_CIVEL = Label.EX3_Matriz_Macro_Carteira_Civel;
	public static final String MATRIZ_CARTEIRA_IC = Label.EX3_Matriz_Carteira_IC;
	public static final String PEDIDO_PROCESSO_NAO_RECONHECIDO = Label.EX3_Contrato_Nao_Reconhecido;
	public static final String FILA_SUBSIDIOS = Label.EX3_Fila_Captura_Documentos;
	public static final String FILA_FORNECEDOR = Label.EX3_Fornecedor_ADJ;
	public static final String FILA_FUP = Label.EX3_Calculo_FUP;
	public static final String CALCULO_SOLICITADO_STATUS = Label.EX3_Calculo_Solicitado;
	public static final String FILA_CALCULO = Label.EX3_Fila_Calculista;
	public static final String FILA_ACEITE = Label.EX3_Fila_Aceite;
	public static final String FINALIZAR_ESTRATEGIA_MACRO = Label.EX3_Finalizar_Estrategia_Macro_Carteira;
	public static final String STATUS_PAG_SOLICITADO = Label.EX3_Status_Pag_Solicitado;
	public static final String STATUS_DEVOLVIDO_ACEITE = Label.EX3_Status_Devol_Aceite;
	public static final String FILA_CUMPRIMENTO_PAGAMENTO = Label.EX3_Fila_Cumprimento_Pagamento;
    public static final String STATUS_CALCULO_INTERNO = Label.EX3_Calculo_Interno; 
    public static final String FILA_CALCULO_TERCEIRIZADO = Label.EX3_Fila_Calculista_Tercerizado;
    public static final String STATUS_CALCULO_EXTERNO = Label.EX3_Calculo_Externo; 

	public static Set<Id> getQueueIdByName(List<String> lLstqueueNames) {
		Set<Id> lSetqueueIds = new Set<Id>();

		for (Group iQueue :[SELECT Id, Name, DeveloperName FROM Group WHERE Type = 'Queue' AND DeveloperName IN :lLstqueueNames]) {
			lSetqueueIds.add(iQueue.Id);
		}

		return lSetqueueIds;
	}

	/*  Retorna o Id do RecordType pelo DeveloperName */
	public static Id getRecordTypeIdByDevName(String pObjName, String pRtDeveloperName)
	{
		if (String.isBlank(pObjName) || String.isBlank(pRtDeveloperName)) { return null; }
		try { return Schema.getGlobalDescribe().get(pObjName).getDescribe().getRecordTypeInfosByDeveloperName().get(pRtDeveloperName).getRecordTypeId(); }
		catch(Exception e) { return null; }
	}

	/* Retorna o FieldSetMember pelo DeveloperName do FieldsSet */
	public static List<Schema.FieldSetMember> getFieldSetMembersByFieldSetDeveloperName(String aObjName, String aFieldSetDeveloperName)
	{
		if (String.isBlank(aObjName) || String.isBlank(aFieldSetDeveloperName)) { return null; }
		try { return Schema.getGlobalDescribe().get(aObjName).getDescribe().FieldSets.getMap().get(aFieldSetDeveloperName).getFields(); }
		catch(Exception e) { return null; }
	}

	/* Retorna a lista de valores pelo API do campo */
	public static List<Schema.PicklistEntry> getPicklistValuesByFieldAPI(String aObjName, String aFieldAPI)
	{
		if (String.isBlank(aObjName) || String.isBlank(aFieldAPI)) { return null; }
		try { return Schema.getGlobalDescribe().get(aObjName).getDescribe().fields.getMap().get(aFieldAPI).getDescribe().getPicklistValues(); }
		catch(Exception e) { return null; }
	}

	/* Retorna o FieldResult pelo API do campo */
	public static List<Schema.DescribeFieldResult> getFieldValuesByObjectAPI(String aObjName, String aFieldAPI)
	{
		if (String.isBlank(aObjName) || String.isBlank(aFieldAPI)) { return null; }

		try
		{
			List<Schema.DescribeFieldResult> lLstDescribeFieldResult = new List<Schema.DescribeFieldResult> ();
			for (Schema.SObjectField iSObjectTypeField : Schema.getGlobalDescribe().get(aObjName).getDescribe().fields.getMap().values())
			{
				lLstDescribeFieldResult.add(iSObjectTypeField.getDescribe());
			}
			return lLstDescribeFieldResult;
		}
		catch(Exception e) { return null; }
	}

	/* Retorna o doctype do lightning icon pelo fileExtension do ContentDocument */
	public static String getLightningIconByContentDocumentFileExtension(String aFileExtension)
	{
		if (String.isBlank(aFileExtension)) { return 'doctype:attachment'; }

		Set<String> lSetImg = new Set<String> { 'png', 'gif', 'jpg', 'jpeg', 'bmp' };
		Set<String> lSetExcel = new Set<String> { 'xls', 'xlsx', 'xlsm' };
		Set<String> lSetWord = new Set<String> { 'doc', 'docx' };
		Set<String> lSetPowerPoint = new Set<String> { 'ppt', 'pptx' };

		if (lSetImg.contains(aFileExtension)) { return 'doctype:image'; }
		else if (aFileExtension == 'mp4') { return 'doctype:' + aFileExtension; }
		else if (lSetExcel.contains(aFileExtension)) { return 'doctype:excel'; }
		else if (lSetWord.contains(aFileExtension)) { return 'doctype:word'; }
		else if (lSetPowerPoint.contains(aFileExtension)) { return 'doctype:ppt'; }
		return 'doctype:' + aFileExtension;
	}

	/* Retorna o tamanho do byte transformando em Byte, kB, MB ou GB */
	public static String getByteSize(Decimal aByteSize)
	{
		if (aByteSize< 1024) { return aByteSize + ' Bytes'; }
		else if (aByteSize >= 1024 && aByteSize<(1024 * 1024)) { return aByteSize.divide(1024, 2) + ' kB'; }
		else if (aByteSize >= (1024 * 1024) && aByteSize<(1024 * 1024 * 1024)) { return aByteSize.divide(1024 * 1024, 2) + ' MB'; }
		else if (aByteSize >= (1024 * 1024 * 1024) && aByteSize<(1024 * 1024 * 1024 * 1024)) { return aByteSize.divide(1024 * 1024 * 1024, 2) + ' GB'; }
		return 'Unknown size or Larger than 1024 GB.' + '\n' + aByteSize + ' Bytes';
	}

	public static String convertApexTypeToLightningType(SCHEMA.DisplayType aApexType)
	{
		switch on aApexType
		{
			when BOOLEAN { return 'checkbox'; }
			when DATE { return 'date'; }
			when DATETIME { return 'datetime'; }
			when INTEGER { return 'number'; }
			when DOUBLE { return 'number'; }
			when URL { return 'url'; }
			when PICKLIST { return 'picklist'; }
		}
		return aApexType.name();
	}
}