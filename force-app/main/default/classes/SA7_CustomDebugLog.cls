/********************************************************************************
Nome:  SA7_CustomDebugLog
Copyright © 2019 COE
=================================================================================
Função:  Esta classe Apex expõe métodos que podem ser utilizados
por outras classes Apex para criar Logs persistentes como alternativa
aos debug logs nativos. 
=================================================================================
Histórico                                                            
-------                                                            
VERSÃO: 1.0                       AUTOR:TCSKRBJ
DATA: 10/06/19
DESCRIÇÃO: Versão Inicial
*********************************************************************************/
/************************************Wishlist************************************
1-TBD
*********************************************************************************/

public class SA7_CustomDebugLog{
    
    //Log com Nível de Informação - Recomendação para uso em Debug
    public static void logInfo(String sigla, String apexClass, String method, String message, Map<String, String> variaveis){    
        //Valida Nível do Log
        if (validaLog('Info')){
            //Criar a variável.
            SA7_Custom_Debug_Logs__c log = setupLog('Info', sigla, apexClass, method, message, variaveis);
            //Depois devemos gravar na base de dados validando os limites de governança.
            log(log);
        }
    }
    
    //Log com Nível de Aviso - Recomendação para uso em falha de processo de negócio. Ex.: Saldo menor que zero. 
    public static void logWarn(String sigla, String apexClass, String method, String message, Map<String, String> variaveis){
        //Valida Nível do Log
        if(validaLog('Warn')){
            //Criar a variável.
            SA7_Custom_Debug_Logs__c log = setupLog('Warn', sigla, apexClass, method, message, variaveis);
            //Depois devemos gravar na base de dados validando os limites de governança.
            log(log);  
        }
    }
    
    //Log com nível de Erro - Recomendação para uso em falha de processamento - Ex.: Callout, AsyncJobs, NullPointer, etc.
    public static void logError(String sigla, String apexClass, String method, String message, Exception ex, Map<String, String> variaveis){
        //Valida Nível do Log
        if(validaLog('Error')){
            //Criar a variável.
            SA7_Custom_Debug_Logs__c log = setupLog('Error', sigla, apexClass, method, message, ex, variaveis);
            //Depois devemos gravar na base de dados validando os limites de governança.
            log(log);  
        } 
    }
    //Log com nível de Erro - Recomendação para uso em falha de processamento de DML.
    public static void logError(String Sigla, String ApexClass, String Method, String Message, DmlException ex, Map<String, String> variaveis){
        //Valida Nível do Log
        if(validaLog('Error')){
            //Criar a variável.
            SA7_Custom_Debug_Logs__c log = setupLog('Error', sigla, apexClass, method, message, ex, variaveis);
            //Depois devemos gravar na base de dados validando os limites de governança.
            log(log);  
        } 
    }
    //Log com nível de Erro - Recomendação para uso em falha de processamento de email.
    public static void logError(String Sigla, String ApexClass, String Method, String Message, EmailException ex, Map<String, String> variaveis){
        //Valida Nível do Log
        if(validaLog('Error')){
            //Criar a variável.
            SA7_Custom_Debug_Logs__c log = setupLog('Error', sigla, apexClass, method, message, ex, variaveis);
            //Depois devemos gravar na base de dados validando os limites de governança.
            log(log);  
        } 
    }
    private static SA7_Custom_Debug_Logs__c setupLog(String type, String sigla, String apexClass, String method, String message, Map<String, String> variaveis)
    {
        return createLog(type, apexClass, method, message, sigla, Userinfo.getUserId(), Userinfo.getUiThemeDisplayed(), 
                         Userinfo.getOrganizationId(), Userinfo.getSessionId(), null, null, null, null, null,
                         null, null, null, null, null, variaveis
        );
    }
    private static SA7_Custom_Debug_Logs__c setupLog(String type, String sigla, String apexClass, String method, String message, Exception ex, Map<String, String> variaveis)
    {
        return createLog(type, apexClass, method, message, sigla, Userinfo.getUserId(), Userinfo.getUiThemeDisplayed(), 
                         Userinfo.getOrganizationId(), Userinfo.getSessionId(), String.valueOf(ex.getLineNumber()), 
                         ex.getMessage(), ex.getStackTraceString(), ex.getTypeName(), null,
                         null, null, null, null, null, variaveis);
    }
    private static SA7_Custom_Debug_Logs__c setupLog(String type, String sigla, String apexClass, String method, String message, DmlException ex, Map<String, String> variaveis)
    {
        return createLog(type, apexClass, method, message, sigla, Userinfo.getUserId(), Userinfo.getUiThemeDisplayed(), 
                         Userinfo.getOrganizationId(), Userinfo.getSessionId(), String.valueOf(ex.getLineNumber()), 
                         ex.getMessage(), ex.getStackTraceString(), ex.getTypeName(), String.join(ex.getDmlFieldNames(0), '; '),
                         String.valueOf(ex.getDmlId(0)), String.valueOf(ex.getDmlIndex(0)), ex.getDmlMessage(0),
                         ex.getDmlType(0).name(), String.valueOf(ex.getNumDml()), variaveis);
    }
    private static SA7_Custom_Debug_Logs__c setupLog(String type, String sigla, String apexClass, String method, String message, EmailException ex, Map<String, String> variaveis)
    {
        return createLog(type, apexClass, method, message, sigla, Userinfo.getUserId(), Userinfo.getUiThemeDisplayed(), 
                         Userinfo.getOrganizationId(), Userinfo.getSessionId(), String.valueOf(ex.getLineNumber()), 
                         ex.getMessage(), ex.getStackTraceString(), ex.getTypeName(), String.join(ex.getDmlFieldNames(0), '; '),
                         String.valueOf(ex.getDmlId(0)), String.valueOf(ex.getDmlIndex(0)), ex.getDmlMessage(0),
                         ex.getDmlType(0).name(), String.valueOf(ex.getNumDml()), variaveis);
    }
    private static SA7_Custom_Debug_Logs__c createLog (String type, String apexClass, String method, String message, String sigla, 
                                                String userId, String theme, String orgId, String sessionId, String linhaDeCodigo, 
                                                String messageException, String stackTrace, String typeName, String dmlFieldNames,
                                               String dmlId, String dmlIndex, String dmlMessage, String dmlType, String qtidadeRegistrosFalha,
                                               Map<String, String> variaveis)
    {
        String textoVariaveis = String.valueOf(variaveis);
        //Validar se o campo é nulo para evitar nullpointer
        apexClass = validaNulo(apexClass);
        method = validaNulo(method);
        message = validaNulo(message);
        sigla = validaNulo(sigla);
        userId = validaNulo(userId);
        theme = validaNulo(theme);
        orgId = validaNulo(orgId);
        sessionId = validaNulo(sessionId);
        linhaDeCodigo = validaNulo(linhaDeCodigo);
        messageException = validaNulo(messageException);
        stackTrace = validaNulo(stackTrace);
        typeName = validaNulo(typeName);
        dmlFieldNames = validaNulo(dmlFieldNames);
        dmlId = validaNulo(dmlId);
        dmlIndex = validaNulo(dmlIndex);
        dmlMessage = validaNulo(dmlMessage);
        dmlType = validaNulo(dmlType);
        qtidadeRegistrosFalha = validaNulo(qtidadeRegistrosFalha);
        textoVariaveis = validaNulo(textoVariaveis);
        
        return new SA7_Custom_Debug_Logs__c(
            SA7_Type__c                              = type,
            SA7_Apex_Class__c                        = apexClass.left(255).trim(),
            SA7_Method__c                            = method.left(255).trim(),
            SA7_Message__c                           = message.left(255).trim(),
            SA7_Sigla__c                             = sigla.left(4).trim(),
            SA7_User_Id__c                           = userId.left(21).trim(),
            SA7_Theme__c                             = theme.left(100).trim(),
            SA7_Org_ID__c                            = orgId.left(21).trim(),
            SA7_Session_Id__c                        = sessionId.left(115).trim(),
            SA7_Linha_de_Codigo__c                   = linhaDeCodigo.left(10).trim(),
            SA7_Message_Exception__c                 = messageException.left(255).trim(),
            SA7_Stack_Trace__c                       = stackTrace.left(255).trim(),
            SA7_Type_Name__c                         = typeName.left(255).trim(),
            SA7_DML_Field_Names__c                   = dmlFieldNames.left(255).trim(),
            SA7_Dml_Id__c                            = dmlId.left(21).trim(),
            SA7_Dml_Index__c                         = dmlIndex.left(10).trim(),
            SA7_Dml_Message__c                       = dmlMessage.left(255).trim(),
            SA7_Dml_Type__c                          = dmlType.left(100).trim(),
            SA7_Quantidade_de_Registros_com_Falha__c = qtidadeRegistrosFalha.left(10).trim(),
            SA7_Variaveis__c                         = textoVariaveis.left(20000).trim());
    }
    
    private static void log(SA7_Custom_Debug_Logs__c log)
    {
        try{
            if((Limits.getDMLRows() < Limits.getLimitDMLRows()) && (Limits.getDMLStatements() < Limits.getLimitDMLStatements()))
            {
                Database.insert(log, FALSE);
            }   
            else
            {
                System.debug('The Governor Limits have already been exhausted and hence failed to create a Log!');
            }
        }
        catch(DMLException ex){
            System.debug('Something fatal has occurred and hence failed to create a Log! Error:' + ex.getMessage());
        }
        catch(Exception ex){
            System.debug('Something fatal has occurred and hence failed to create a Log! Error:' + ex.getMessage());
        }
    }
    private static boolean validaLog (String logLevel)
    {
        //Primeiro tenta obter o nível de log no cache.
        String orgLogLevel;
        if (Cache.Org.contains('OrgLogLevel'))
        {
            // Converte o valor retornado para o tipo correto
            orgLogLevel = (String)Cache.Org.get('OrgLogLevel');
        }
        //Se não existir, pega a variável do Custom Metadata type e também atualiza o cache.
        else
        {
            SA7_Org_Log_Level__mdt SO = [SELECT SA7_LogLevel__c FROM SA7_Org_Log_Level__mdt WHERE MasterLabel=:'LogLevel'];
            orgLogLevel = SO.SA7_LogLevel__c;
            Cache.org.put('OrgLogLevel', orgLogLevel, 60*60*24, cache.Visibility.ALL, false);
        }
        //Depois compara com os valores passado no método e faz a logica de log
        //Error sempre vai logar
        if (logLevel.equalsIgnoreCase('Error')){
            return true;
        } 
        //Se foi passado Warn, só vai logar se for Warn ou Info a nível de Org
        if (logLevel.equalsIgnoreCase('Warn') && (orgLogLevel.equalsIgnoreCase('Warn') || orgLoglevel.equalsIgnoreCase('Info')))
        {
            return true;
        }
        //Info só loga se o nível da Org for info também
        if (logLevel.equalsIgnoreCase('Info') && orgLogLevel.equals('Info'))
        {
            return true;
        }
        return false;
    }
    private static String validaNulo(String texto) 
    {
        return texto==null?'':texto;
    }
    
}