/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel pela implementação do agendamento da rotina de expurgo dos registros no objeto centralizador
*
* Empresa: everis do Brasil
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/

@isTest
public class EX3_SchExpurgoCentralizadorTest {
    
    @isTest
    public static void testScheduller(){
        
        Test.startTest();
        EX3_SchExpurgoCentralizador sh1 = new EX3_SchExpurgoCentralizador();
		String sch = '0 0 23 * * ?'; system.schedule('Test Expurgo Centralizador', sch, sh1);
        Test.stopTest();
        
    }

}