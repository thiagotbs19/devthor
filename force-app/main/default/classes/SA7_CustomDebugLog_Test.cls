@isTest
public class SA7_CustomDebugLog_Test {
    @isTest
    static void customDebugLogTest(){
        SA7_CustomDebugLog.logInfo('sigla', 'apexClass', 'method', 'message', null);
        SA7_CustomDebugLog.logWarn('sigla', 'apexClass', 'method', 'message', null);
        try{
            Account[] accts = new Account[]{new Account(billingcity = 'San Jose')};
                insert accts;
        }
        catch (DmlException ex)
        {
            SA7_CustomDebugLog.logError('sigla', 'apexClass', 'method', 'message', ex, null);   
        }
        try 
        {
            String s;
            s.toLowerCase();
        }catch (Exception ex)
        {
            SA7_CustomDebugLog.logError('sigla', 'apexClass', 'method', 'message', ex, null);
        }
    }
}