/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste  
*
*Apex Trigger : EX3_Calculo  
*Apex Class: EX3_CreateCalculoComplexoTest
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

@isTest
public with sharing class EX3_CreateCalculoComplexoTest {
    
    
    @TestSetup
    public static void createData(){
        
        String RECORDTYPE_OBP = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBP');    
        
        User lUser = EX3_BI_DataLoad.getUser(); 
        lUser.FuncionalColaborador__c = 'funcional';
        lUser.EX3_Pertence__c = '2'; 
        Database.insert(lUser);
        
        Case lCase = EX3_BI_DataLoad.getCase();
        lCase.Status = 'New'; 
        Database.insert(lCase);
        
        List<Group> lLstGroup = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName = 'EX3_Analise_de_decisao'];
        
        
        EX3_ADJ__c lADJ = EX3_BI_DataLoad.getADJ(lCase);
        lADJ.EX3_Data_da_Publicacao__c = Date.today();
        lADJ.EX3_Tipo_de_Decisao__c = 'Acordo'; 
        lADJ.OwnerId = lLstGroup[0].Id; 
        lADJ.EX3_Resultado_da_Decisao__c = 'Improcedente';
        lADJ.EX3_Multa_OBF__c = 'Arbitrada';
        lADJ.EX3_Checklist_de_Cumprimento__c = 'Pagamento';
        lADJ.EX3_Valor_Multa_OBF__c = 2.0;
        lADJ.RecordTypeId = RECORDTYPE_OBP;
        
        Database.insert(lADJ);
        
    }
    
    @isTest static void testCreateADJ(){
        
        User lUser = [SELECT Id, EX3_Pertence__c, FuncionalColaborador__c FROM User LIMIT 1];
        
        Case lCase = [SELECT Id FROM Case LIMIT 1];  
        
        EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];
        
        Test.startTest();  
        Map<String, Object> lMapReturn = EX3_CreateCalculoComplexo.getFieldsADJ(lADJ.Id);   
        
        EX3_ADJ__c lADJFilaAceite = EX3_CreateCalculoComplexo.getUpdateFilaAceite(lADJ.Id);
        
        Test.stopTest();   
        
        System.assert(lMapReturn != null, 'Registro não foi enviado para a fila de Aceite');  
        
    }
    
    @isTest static void testRecordTypeADJ(){
		
        EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];
        
    	Test.startTest();
        	EX3_ADJ__c lADJResult = EX3_CreateCalculoComplexo.getRecordTypeADJ(lADJ.Id); 
        Test.stopTest(); 
        System.assert(lADJResult != null, 'Tipo de Registro não foi atribuído na criação');
    
    }

	@isTest static void testRecordTypeNull(){

		Test.startTest();    
			EX3_ADJ__c lADJResult = EX3_CreateCalculoComplexo.getRecordTypeADJ(''); 
		Test.stopTest(); 
		System.assert(lADJResult == null, 'O Tipo de registro esta nulo');  
    } 
    
    @isTest static void testgetFieldsADJ(){

        Test.startTest();  
        Map<String,Object> lMapFields = EX3_CreateCalculoComplexo.getFieldsADJ('');
        Test.stopTest();    
        System.assert(lMapFields == null, 'Erro ao criar o registro de ADJ'); 
    }

    @isTest static void testgetUpdateFilaAceite(){

        Test.startTest();
            EX3_ADJ__c lADJResult = EX3_CreateCalculoComplexo.getUpdateFilaAceite('');
        Test.stopTest();
        System.assert(lADJResult == null, 'Erro ao atualizar o registro de ADJ');
    }
    
    @isTest static void testHasPermission(){

        Test.startTest();
            Boolean lHasPermission = EX3_CreateCalculoComplexo.getHasPermissionSet();
        Test.stopTest();   
        System.assert(!lHasPermission, 'Não há permissão atribuída');
    }


}