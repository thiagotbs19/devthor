/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável atribuir o Owner para cálculista Tercerizado. 
*
*Apex Trigger : EX3_Calculo__c 
*Apex Class: EX3_ChangeStatusCalculoTerc
*  
*
* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/


public with sharing class EX3_ChangeStatusCalculoTerc {
    
    
    
    public static void execute(){
        Set<String> lSetRecordTypeIdCalc = new Set<String> {EX3_Utils.getRecordTypeIdByDevName('EX3_Calculo__c', 'EX3_Solicitacao_de_Calculo'),
            EX3_Utils.getRecordTypeIdByDevName('EX3_Calculo__c', 'EX3_Calculista')};
                List<EX3_Calculo__c> lLstCalculo = new List<EX3_Calculo__c>();
        for(EX3_Calculo__c iCalculo :(List<EX3_Calculo__c>) Trigger.new){
            if(lSetRecordTypeIdCalc.contains(iCalculo.RecordTypeId) && 
               EX3_TriggerHelper.changedField(iCalculo, 'EX3_Status_do_calculo__c') 
               && iCalculo.EX3_Status_do_calculo__c == Label.EX3_Calculo_Externo){
                   lLstCalculo.add(iCalculo); 
               }
        }
        if(lLstCalculo == null || lLstCalculo.isEmpty()){
            return ;
        }
        updateQueueStatus(lLstCalculo);
    }
    
    private static void updateQueueStatus(List<EX3_Calculo__c> aLstCalculo){
        
        
        if(aLstCalculo == null || aLstCalculo.isEmpty()){ return ;}
        
        List<Group> lLstGroupTerceiro = [SELECT Id, Name, DeveloperName 
                                         FROM Group 
                                         WHERE DeveloperName =: Label.EX3_Fila_Calculista_Tercerizado];
        for(EX3_Calculo__c iCalc : aLstCalculo){
            iCalc.OwnerId =  lLstGroupTerceiro[0].Id;
        }
    }
    
    
    
}