/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por simular a resposta do inclusão de documentos do EX3
* Empresa: everis do Brasil
* Autor: Rafael Amaral Moreira
*
*
********************************************************************************/
public with sharing class EX3_IncluirDocumentoMock implements HttpCalloutMock
{
    private Integer statusCode;
    public String nome_documento;
    public String codigo_documento_plataforma_externa;
    public String operador_conglomerado;

    public EX3_IncluirDocumentoMock(EX3_ObjectFactory.IncluirDocumento aIncluirDocumento, Integer statusCode) 
    {
        this.statusCode = statusCode;
        this.nome_documento = aIncluirDocumento.nome_documento;
        this.codigo_documento_plataforma_externa = aIncluirDocumento.codigo_documento_plataforma_externa;
        this.operador_conglomerado = aIncluirDocumento.operador_conglomerado;
    }

    public HTTPResponse respond(HTTPRequest req) 
    {  
        List<Map<String,Object>> lLstMapBody = new List<Map<String,Object>>();
        Map<String, Object> lMapBody = new Map<String,Object>
        {
            'id_documento' => 2019000001,
            'nome_documento' => this.nome_documento,
            'codigo_documento_plataforma_externa' => this.codigo_documento_plataforma_externa,
            'operador_conglomerado' => this.operador_conglomerado
        };
		lLstMapBody.add(lMapBody);
        String jsonString = (statusCode > 202) 
                          ? '{"Message": "Erro ao enviar o request" }'
                          : '{"data":'+JSON.serialize(lLstMapBody)+'}';
            
        HttpResponse res = new HttpResponse();
        res.setStatusCode(statusCode);
        res.setBody(jsonString);

        return res;
    }
}