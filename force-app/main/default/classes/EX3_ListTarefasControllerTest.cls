/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de Teste do componente de Tarefas. 
*
* Autor: Thiago Barbosa de Souza
* Aura Component Bundle: EX3_ListTarefasController 
* Test Class:   
*
********************************************************************************/

@isTest 
public with sharing class EX3_ListTarefasControllerTest {
    
     

    @TestSetup
    public static void setupCreateData(){
        
     Account lAccount = new Account();
     lAccount.Name = 'Teste Account';
     
     Database.insert(lAccount);
     
     User lUser = EX3_BI_DataLoad.getUser();
     lUser.FuncionalColaborador__c = 'funcional';
     lUser.EX3_Pertence__c = '2';  
     Database.insert(lUser); 
        
      Case lCase = EX3_BI_DataLoad.getCase();
        
      Database.insert(lCase); 

       lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
       Database.update(lCase);   
		
        Test.startTest();
        
        EX3_Cadastro__c lCadastro = EX3_BI_DataLoad.getCadastro(lCase);
        lCadastro.EX3_Status__c = 'Em Aberto'; 
        lCadastro.EX3_Numero_Protocolo__c = '003';
        lCadastro.EX3_Caso__c = lCase.Id;
        lCadastro.OwnerId = UserInfo.getUserId();
        lCadastro.EX3_Escritorio_Credenciado__c = lAccount.Id; 
        lCadastro.EX3_Recebe_Escritorio_Credenciado__c = lCadastro.EX3_Escritorio_Credenciado__c;
	
        Database.insert(lCadastro);
        Test.stopTest();

    }



    @isTest static void testGetFielsCadastro() {    
        
        User lUser = [SELECT Id, FuncionalColaborador__c, EX3_Pertence__c FROM User LIMIT 1];

        Case lCase = [SELECT Id FROM Case LIMIT 1];   

        EX3_Cadastro__c lCadastro = [SELECT Id, OwnerId, EX3_Caso__c,EX3_Status__c, EX3_Numero_Protocolo__c  FROM EX3_Cadastro__c LIMIT 1]; 
		lCadastro.EX3_Caso__c = lCase.Id;
        
        Database.update(lCadastro);
        Test.startTest(); 
            Map<String, Object> lMapCadastro = EX3_ListTarefasController.getFieldsCadastro(lCadastro.Id);
        Test.stopTest(); 
        System.assert(lCadastro != null, 'Cadastro Criado');

    }

    @isTest static void testGetTaskRecordType(){

        Test.startTest();  
            Id lRecordTypeId = EX3_ListTarefasController.getTaskRecordType();
        Test.stopTest();
        System.assert(lRecordTypeId != null, 'O RecordType foi recuperado');    

    }

    @isTest static void testGetFieldsisEmpty(){
        Test.startTest();
            Map<String, Object> lMapCadastro = EX3_ListTarefasController.getFieldsCadastro(null); 
        Test.stopTest();  
            System.assert(lMapCadastro == null, 'Não há Id de Cadastro');    
    }
    
    @isTest static void testDefinicaoEstrategica(){
        
        User lUser = EX3_BI_DataLoad.getUser();
        
        Database.insert(lUser);
        
        Case lCase = EX3_BI_DataLoad.getCase();
        
        Database.insert(lCase);
        
        
        EX3_DefinicaoEstrategia__c lDefinicao = EX3_BI_DataLoad.getDefEstrategica(lCase);
        
        Database.insert(lDefinicao);
        
        Test.startTest();
        	EX3_DefinicaoEstrategia__c lDefReturn = EX3_ListTarefasController.getDefinicaoEstrategia(lDefinicao.Id);
        Test.stopTest();  
        System.assert(lDefReturn != null,'Definição de Estratégia não criada');
    }
    @isTest static void testHasPermission(){

        Test.startTest();
            Boolean lHasPermission = EX3_ListTarefasController.getHasPermissionSet();
        Test.stopTest(); 
        System.assert(!lHasPermission, 'Não há permissão atrbuída a este perfil');
    }
    
}