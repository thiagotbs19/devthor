/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel pela implementação do agendamento da rotina de expurgo dos registros no objeto centralizador
*
* Empresa: everis do Brasil
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/

global class EX3_SchExpurgoCentralizador implements schedulable {

    global void execute(SchedulableContext sc)
    {
       EX3_BatchExpurgoCentralizador lBatch = new EX3_BatchExpurgoCentralizador(); 
       database.executebatch(lBatch, 2000);
    }

}