/*********************************************************************************
*                                    Itaú - 2020
*
* Classe TriggerHandler do objeto EX3_ADJ 
* 
* Autor: Thiago Barbosa de Souza 
  Company: Everis do Brasil
*
********************************************************************************/

public with sharing class EX3_ADJTriggerHandler implements ITrigger{
    

    public void bulkBefore(){}      
    public void bulkAfter(){}
    public void beforeInsert(){
        EX3_BloqueiaADJOwner.execute();  
        //EX3_BloqueiaOwnerADJOBF.execute();
        //EX3_BloqueiaOwnerADJOBF.execute();
    }
    public void beforeUpdate(){
        EX3_BloqueiaADJOwner.execute();
        //EX3_BloqueiaOwnerADJOBF.execute();    
    }
    public void beforeDelete(){}
    
    public void afterInsert()
    {
    }
    
    public void afterUpdate(){
        EX3_ADJFilaFornecedorStatus.execute();  
    } 
    public void afterDelete(){}
    public void andFinally(){} 
}