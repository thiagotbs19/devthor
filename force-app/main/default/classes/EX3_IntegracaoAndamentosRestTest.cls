/*********************************************************************************
*                                    Itaú - 2020
*
* Classe responsavel por testar a classe EX3_IntegracaoAndamentosRest
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@IsTest
public with sharing class EX3_IntegracaoAndamentosRestTest 
{
    @IsTest static void createTest()
    {
        EX3_Andamentos__c lAndamentos = new EX3_Andamentos__c();
        lAndamentos.EX3_Andamentos__c = 'Diligência';
        lAndamentos.EX3_Detalhes_Andamento_Processual__c = 'Detalhe Andamento 1';
        database.insert(lAndamentos);

        Test.setMock(HttpCalloutMock.class, new EX3_IntegracaoAndamentosRestMock()); 
        EX3_IntegracaoAndamentosRest integra = new EX3_IntegracaoAndamentosRest(lAndamentos);
        Test.startTest();
        HttpResponse response = integra.cliente.create();
        System.assertEquals(200, response.getStatusCode());
        Test.stopTest();
    }
}