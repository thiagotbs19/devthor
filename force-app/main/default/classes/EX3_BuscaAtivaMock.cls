/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por simular a resposta para o componente de busca ativa
* Empresa: everis do Brasil
* Autor: Marcelo Seibt de Oliveira
*
*
********************************************************************************/
@isTest
public class EX3_BuscaAtivaMock implements HttpCalloutMock{

    protected Integer statusCode;
    protected Id protocolId;

    public EX3_BuscaAtivaMock(Id aProtocolId) {
        this.statusCode = 200;
        this.protocolId = aProtocolId;
    }
		
    public EX3_BuscaAtivaMock(Integer statusCode) { 
        this.statusCode = statusCode;
    }

    public HTTPResponse respond(HTTPRequest req) {
        EX3_ObjectFactoryResponse.ResponseBuscaAtiva body = new EX3_ObjectFactoryResponse.ResponseBuscaAtiva();

        body.codigo_protocolo_externo = protocolId;  
        body.id_protocolo = protocolId; 

        String jsonString = '{"data":'+JSON.serialize(body)+'}';
        HttpResponse res = new HttpResponse();
        res.setStatusCode(statusCode);
        res.setBody(jsonString);

        return res; 
    }
}