/*********************************************************************************
*                                    Itaú - 2020
*
* Classe responsável por criar registro de ADJ quando Status alterado para 
*Solicitado Fornecedor  
*
* Autor: Thiago Barbosa 
* Apex Class: EX3_ADJFilaFornecedorStatus 
* 
*
********************************************************************************/


public without sharing class EX3_ADJFilaFornecedorStatus {
    
    private static final String lRecordTypeOBF = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF'); 

    public static void execute(){
        List<EX3_ADJ__c> lLstADJ = new List<EX3_ADJ__c>();
        for(EX3_ADJ__c iADJ : (List<EX3_ADJ__c>) Trigger.new){
            if(iADJ.EX3_Status__c == EX3_Utils.STATUS_SOLICITADO_FORNECEDOR 
               && EX3_TriggerHelper.changedField(iADJ, 'EX3_Status__c')
               && iADJ.RecordTypeId == lRecordTypeOBF){ 
                lLstADJ.add(iADJ);   
            }
        }
        if(lLstADJ.isEmpty() || lLstADJ == null){ return ; }
        createFornecedorAndFUP(lLstADJ); 
    } 

    private static void createFornecedorAndFUP(List<EX3_ADJ__c> aLstADJ){

        if(aLstADJ.isEmpty() || aLstADJ == null){
            return ;
        }

        List<Group> lLstGroupFornecedor = [SELECT Id, Name, DeveloperName 
        FROM Group WHERE Type='Queue' AND Name =: EX3_Utils.FILA_FORNECEDOR];

        if(lLstGroupFornecedor.isEmpty()){ return ;}  
        List<Group> lLstGroupFUP = [SELECT Id, Name, DeveloperName 
        FROM Group WHERE Type='Queue' AND DeveloperName =: EX3_Utils.FILA_FUP];
        
        if(lLstGroupFUP.isEmpty()){ return ;}    

        List<EX3_ADJ__c> lLstEX3ADJ = new List<EX3_ADJ__c>();
        for(EX3_ADJ__c iADJ : aLstADJ){
            EX3_ADJ__c lFornecedor = new EX3_ADJ__c();
            lFornecedor.OwnerId = lLstGroupFornecedor[0].Id;
            lFornecedor.EX3_ADJ__c = iADJ.Id;  
            lFornecedor.EX3_Caso__c = iADJ.EX3_Caso__c; 
            lLstEX3ADJ.add(lFornecedor);
            EX3_ADJ__c lFUP = new EX3_ADJ__c();   
            lFUP.OwnerId = lLstGroupFUP[0].Id;
            lFUP.EX3_Numero_do_Processo__c = iADJ.EX3_Numero_do_Processo__c;
            lFUP.EX3_Numero_da_Pasta__c = iADJ.EX3_Numero_da_Pasta__c;
            lFUP.EX3_Tipo_de_Decisao__c = iADJ.EX3_Tipo_de_Decisao__c;
            lFUP.EX3_Checklist_de_Cumprimento__c = iADJ.EX3_Checklist_de_Cumprimento__c;
            lFUP.EX3_Status_do_FUP__c = 'FUP pendente';
            lFUP.EX3_ADJ__c = iADJ.Id;  
            lFUP.EX3_Caso__c = iADJ.EX3_Caso__c;
            lLstEX3ADJ.add(lFUP);  
        } 

        if(!lLstEX3ADJ.isEmpty()){  
            Database.insert(lLstEX3ADJ);  
        }
    }
}