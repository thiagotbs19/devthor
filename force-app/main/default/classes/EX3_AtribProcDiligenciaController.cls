/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsável por atualizar o motivo da diligência
*
* AuraComponent: AtribuicaoProcessoDiligencia
* Empresa: everis
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/

public class EX3_AtribProcDiligenciaController {

   @AuraEnabled
   public static String getMotivos(){
      List<AttributesTypesPicklist> lLstAttributesTypesPicklist = new List<AttributesTypesPicklist>();
      AttributesTypesPicklist lDefaultValue = new AttributesTypesPicklist();
      lDefaultValue.label = '-- Selecione um motivo --';
      lDefaultValue.value = '';
      lLstAttributesTypesPicklist.add(lDefaultValue); 
      Set<String> lSetPickListValues = new Set<String>{'1','15', '17', '18', '19'};
      for(Schema.PicklistEntry iPickList : EX3_Utils.getPicklistValuesByFieldAPI('Case', 'EX3_Motivo_Situacao__c')) {
         if ( lSetPickListValues.contains(iPickList.getValue()) ) {
            AttributesTypesPicklist lAttributeType = new AttributesTypesPicklist();
            lAttributeType.label = iPickList.getLabel();
            lAttributeType.value = iPickList.getValue();
            lLstAttributesTypesPicklist.add(lAttributeType);
         }
      }
      return JSON.serialize(lLstAttributesTypesPicklist);
   }

   @AuraEnabled
   public static String updateMotivos( String aMotivoEscolhido, Id aCaseId ){
      try
      {
         if ( aMotivoEscolhido == '' ){ return 'Error'; }
         List<Case> lLstCase = [SELECT Id, EX3_Motivo_Situacao__c FROM Case WHERE Id = :aCaseId];
         if ( !lLstCase.isEmpty() ) {
            lLstCase[0].EX3_Motivo_Situacao__c = aMotivoEscolhido;
            database.update(lLstCase); 
         }
      }
      catch (DmlException ex)
      {
         SA7_CustomdebugLog.logError('EX3', 'EX3_AtribProcDiligenciaController', 'updateMotivos', 'Erro ao atualizar registro do Protocolo', ex, null);
      }
       
      return 'Sucess';
   }
    
   public class AttributesTypesPicklist
   {
      private String label {get;set;}
      private String value {get;set;}
   }
}