/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste responsável por cobrir e testar a EX3_BloqueiaSubsidiosOwner 
*
* 
*Apex Class: EX3_BloqueiaProtocoloOwnerTest
* Empresa: everis do Brasil 
* Autor: Thiago Barbosa 
*
********************************************************************************/

@isTest 
public with sharing class EX3_BloqueiaSubsidiosOwnerTest {
    
    
    @TestSetup
    public static void testCreateData(){
        String PROF_ANALISTA_OPERACIONAL = 'Analista Operacional';
        User lUserOperacional = EX3_BI_DataLoad.getUser(PROF_ANALISTA_OPERACIONAL);
        Database.insert(lUserOperacional);
        List<Permissionsetassignment> psalist = new List<Permissionsetassignment>();

        Permissionsetassignment psa = new Permissionsetassignment();
        psa.AssigneeId = lUserOperacional.id;
        psa.PermissionSetId = [select id from Permissionset where name = 'EX3_Cadastro' limit 1].id;
        psalist.add(psa);

        insert psalist;
    }
    
    
    @isTest
    public static void testBloqueiaSubsidiosTest(){
        
        User lUserOperacional = [SELECT Id FROM User LIMIT 1];
        
        EX3_Subsidios__c lSubsidios = new EX3_Subsidios__c();
        lSubsidios.OwnerId = UserInfo.getUserId(); 
        lSubsidios.EX3_Status_da_Pasta__c = 'Aguardando tratamento';     
        lSubsidios.EX3_Numero_Protocolo__c = '001';  
        lSubsidios.EX3_Numero_do_Processo__c = '006'; 
        
        Database.insert(lSubsidios); 
        
        
        lSubsidios.OwnerId = lUserOperacional.Id;
        
        Database.update(lSubsidios);
        
        EX3_Subsidios__c lSegundoSubsidios = new EX3_Subsidios__c();
        lSegundoSubsidios.EX3_Status_da_Pasta__c = 'Aguardando tratamento';
        lSegundoSubsidios.EX3_Numero_do_Processo__c = '005'; 
        lSegundoSubsidios.EX3_Numero_Protocolo__c = '002';      
        lSegundoSubsidios.OwnerId = lUserOperacional.Id;      
        
        Test.startTest();     
        
        System.runAs(lUserOperacional){ 
            Database.insert(lSegundoSubsidios, false);
        
            lSegundoSubsidios.EX3_Status_da_Pasta__c = 'Aguardando tratamento';     
            lSegundoSubsidios.EX3_Numero_Protocolo__c = '002'; 
            lSegundoSubsidios.EX3_Numero_do_Processo__c = '005'; 
            lSegundoSubsidios.OwnerId = lUserOperacional.Id;
        }
           
        
        Database.SaveResult lResult = Database.update(lSegundoSubsidios, false);  
        System.assert(!lResult.isSuccess(), 'O usuario ja tem uma pasta em tratativa');  
          Test.stopTest(); 
    } 
}