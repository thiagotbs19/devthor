/*********************************************************************************
*                                    Itaú - 2020
*
* Classe responsavel por realizar a integracao do andamento
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
public with sharing class EX3_IntegracaoAndamentosRest {
    private static final Map<String, String> fieldReplaces = new Map<String, String> 
    {
        'EX3_Andamentos__c' => 'id_subpedido',
        'EX3_Detalhes_Andamento_Processual__c' => 'id_externo_subpedido'
    };
    
    public EX3_RESTIntegracaoEX3Base cliente;
    public EX3_IntegracaoAndamentosRest(EX3_Andamentos__c andamentos) {
        cliente = new EX3_RESTIntegracaoEX3Base(andamentos, fieldReplaces);
    }
}