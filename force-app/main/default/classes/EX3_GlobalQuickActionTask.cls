/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável por dar permissão ao botão Nova Tarefa para os permissions 
* Sets EX3_Advogado_Interno e EX3_Escritorio_Credenciado.
*
* 
*Apex Class: EX3_GlobalQuickActionTask 
* Empresa: everis do Brasil  
* Autor: Thiago Barbosa 
*
********************************************************************************/


public with sharing class EX3_GlobalQuickActionTask { 
    
    
    @AuraEnabled
    public static boolean getHasCustomPermission(){
        
        Boolean lhasPermission = FeatureManagement.checkPermission('EX3_Criar_Tarefa'); 

        return lhasPermission;
    }

}