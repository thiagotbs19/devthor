/********************************************************************************
Nome:  SA7_COEIndicatorsScheduler
Copyright © 2019 COE
=================================================================================
Função: Classe reposnsavel pelo agendamento das execuções dos serviços recorrentes 
de monitoramento do COE
=================================================================================
Histórico                                                            
-------                                                            
VERSÃO: 1.0                       AUTOR:VINICIUS SILVA
DATA: 10/07/19
DESCRIÇÃO: Versão Inicial
*********************************************************************************/

global class SA7_COEIndicatorsScheduler implements Schedulable{
    global void execute(SchedulableContext ctx){
		SA7_auditPermissionSet.audit(SA7_Util.ambienteSandbox());
        SA7_COE_SystemOverviewExtract.systemOverviewExtract(); 
    }
}