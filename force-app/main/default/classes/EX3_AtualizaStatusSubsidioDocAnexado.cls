/*********************************************************************************
*                                    Itaú - 2020
*
* Quando o documento é anexado verifica se todos os registros relacionados ao subsidios
* estão com documento. Se estiver com o campo flegado para todos os registro de Tipo de Documento
* altera o campo Status para Doc Finalizado. 
* 
* Autor: Thiago Barbosa de Souza
Company: Everis do Brasil
*
********************************************************************************/

public with sharing class EX3_AtualizaStatusSubsidioDocAnexado {
    
    
    public static void execute(){
        List<EX3_Tipo_Documento__c> lLstTipoDoc = new List<EX3_Tipo_Documento__c>();
        for(EX3_Tipo_Documento__c iTipo : (List<EX3_Tipo_Documento__c>) Trigger.new){
            if(EX3_TriggerHelper.changedField(iTipo, 'EX3_Arquivo_Anexado__c') &&
               iTipo.EX3_Arquivo_Anexado__c){
                   lLstTipoDoc.add(iTipo); 
               }
        }
        if(lLstTipoDoc.isEmpty() || lLstTipoDoc == null){ return ;}
        updateSubsidios(lLstTipoDoc); 
    }
    
    private static void updateSubsidios(List<EX3_Tipo_Documento__c> lLstTipoDoc){
        
        if(lLstTipoDoc.isEmpty() || lLstTipoDoc == null){ return ;}
        
        
        for(EX3_Tipo_Documento__c iTipo : lLstTipoDoc){   
            iTipo.EX3_Status_do_Documento__c = Label.EX3_StatusDocumento;
            iTipo.EX3_Arquivo_Anexado__c = true;
        } 
    }
}