/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsável por prover fomulário para preenchimento de dados pertinentes a
* execução da Busca Ativa
*
* Aura Component Bundle: EX3_BuscaAtiva
* Apex Class: EX3_BuscaAtivaController
*
* Empresa: everis do Brasil
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/

@isTest
public  with sharing class EX3_BuscaAtivaControllerTest 
{  
    @isTest static void getUploadedFilesTest() 
    {
        EX3_Centralizador__c lCentralizador = EX3_BuscaAtivaController.getCentralizador();
        String lCentralizadorToString = json.serialize(lCentralizador);
        
        ContentVersion lNewContentVersion = new ContentVersion(title = 'title test',
                                                               PathOnClient = 'PathOnClient test', Tipo_de_Documento__c ='1');
        Blob b = Blob.valueOf('This is version data');
        lNewContentVersion.VersionData = Encodingutil.base64Decode('This is version data');
        insert lNewContentVersion;
        
        ContentVersion lContentVersion = [SELECT Id, ContentDocumentId, Tipo_de_Documento__c FROM ContentVersion
                                          WHERE Id =: lNewContentVersion.Id];
        ContentDocumentLink lNewContentDocumentLink = new ContentDocumentLink(LinkedEntityId = lCentralizador.Id,
                                                                              ContentDocumentId = lContentVersion.ContentDocumentId,
                                                                              Visibility = 'AllUsers',
                                                                              ShareType = 'V');
        insert lNewContentDocumentLink;
        
        test.startTest();
        Map<String,Object> getReturnedValues = EX3_BuscaAtivaController.getDocuments(lCentralizadorToString);
        test.stopTest();
        
        EX3_Centralizador__c lUpdatedCentralizador = (EX3_Centralizador__c)getReturnedValues.get('central');
        system.assert(lUpdatedCentralizador.EX3_hasDocuments__c);
        
        List<EX3_WrapperDocument> lLstDocumentParamsToExternalService = (List<EX3_WrapperDocument>)getReturnedValues.get('listDocument');
        system.assert(!lLstDocumentParamsToExternalService.isEmpty());

    }
    
    @isTest static void getProtocoloFieldsTest() 
    {
        test.startTest();
        String lProtocoloFieldsToString = EX3_BuscaAtivaController.getCaseFields();
        test.stopTest();       
        List<Object> lLstAttributesTypes = (List<Object>) JSON.deserializeUntyped(lProtocoloFieldsToString);
        system.assert(!lLstAttributesTypes.isEmpty());     
    }
    
    @isTest static void criarDocumentosTest() 
    {
        EX3_Centralizador__c lCentralizador = EX3_BuscaAtivaController.getCentralizador();
        lCentralizador.EX3_hasDocuments__c = true;
        String lCentralizadorToString = json.serialize(lCentralizador);
        
        Case lCase = EX3_BI_DataLoad.getCase();  
        lCase.EX3_Canal_de_Entrada__c = '9';
        lCase.EX3_Numero_do_protocolo__c = '23842394';
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        
        database.insert(lCase);
        String lProtocoloToString = JSON.serialize(lCase); 
        
        ContentVersion lNewContentVersion = new ContentVersion(title = 'title test',
                                                               PathOnClient = 'PathOnClient test', Tipo_de_Documento__c ='1'
                                                               );
        Blob b = Blob.valueOf('This is version data');
        lNewContentVersion.VersionData = Encodingutil.base64Decode('This is version data');
        insert lNewContentVersion;
        
        ContentVersion lContentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion
                                          WHERE Id =: lNewContentVersion.Id];
        
        ContentDocumentLink lNewContentDocumentLink = new ContentDocumentLink(LinkedEntityId = lCentralizador.Id,
                                                                              ContentDocumentId = lContentVersion.ContentDocumentId,
                                                                              Visibility = 'AllUsers',
                                                                              ShareType = 'V');
        insert lNewContentDocumentLink; 
        
        User lUser = EX3_DataFactory.getAdmUser();
        lUser.FuncionalColaborador__c = 'funcional';
        database.insert(lUser);
        
        String lFuncional = [SELECT FuncionalColaborador__c FROM User WHERE Id =: lUser.Id].FuncionalColaborador__c;
        test.startTest();
        String lDocumentToString = EX3_BuscaAtivaController.criarDocumentos(lProtocoloToString, lCentralizadorToString, lFuncional);
        system.debug('lDocumentToString === ' + lDocumentToString);
        test.stopTest();
        
        
        Map<String,String> lMapToGetDocument = (Map<String,String>) JSON.deserialize(lDocumentToString, Map<String,String>.class);
        
        String lProtocoloIdToString = lMapToGetDocument.get('atualizarProtocolo');
        String lDocumentIdToString = lMapToGetDocument.get('incluirDocumento');
        
        system.assert( String.isNotBlank(lProtocoloIdToString));
        system.assert( String.isNotBlank(lDocumentIdToString));
        
        
    }
    
    @isTest static void criarProtocoloTest() 
    {        
        AttributesTypes lAutor = new AttributesTypes();
        lAutor.label = 'Autor';
        lAutor.value = 'Maria';
        lAutor.required = true;
        lAutor.formatter = null;
        lAutor.maxlength = '25';
        lAutor.api = 'EX3_Autor__c';
        lAutor.min = null;
        lAutor.max = null;
        lAutor.isDisable = false;
        lAutor.fieldType = 'String';
        lAutor.apexType = 'String';
        lAutor.picklistValues = null;
       
        AttributesTypes lDataDeEntrada = new AttributesTypes();
        lDataDeEntrada.label = 'Data de entrada';
        lDataDeEntrada.value = '2019-11-06T13:45:00.000Z';//system.now().format('yyyy-MM-dd HH:mm:ss.SSSZ');
        lDataDeEntrada.required = true;
        lDataDeEntrada.formatter = null;
        lDataDeEntrada.maxlength = '20';
        lDataDeEntrada.api = 'EX3_Data_de_Entrada__c';
        lDataDeEntrada.min = null;
        lDataDeEntrada.max = null;
        lDataDeEntrada.isDisable = false;
        lDataDeEntrada.fieldType = 'DATETIME';
        lDataDeEntrada.apexType = 'DATETIME';
        lDataDeEntrada.picklistValues = null;
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
        lLstAttributesTypes.add(lAutor);
        lLstAttributesTypes.add(lDataDeEntrada);

        String lAT = JSON.serialize(lLstAttributesTypes);
        
        EX3_Centralizador__c lCentralizador = EX3_BuscaAtivaController.getCentralizador();
        String lCentralToStr = json.serialize(lCentralizador);
        
        test.startTest();
        String lProtocoloBody = EX3_BuscaAtivaController.criarCase(lAT, lCentralToStr);
        test.stopTest(); 
        system.assert( String.isNotBlank(lProtocoloBody));
    }
    
    @isTest static void enviarProtocoloTest()
    {                
        Case lCase = EX3_BI_DataLoad.getCase();
        lCase.EX3_Canal_de_Entrada__c = '9';
		lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        lCase.EX3_Numero_do_protocolo__c = '1123991239';
        lCase.EX3_Motivo_Situacao__c = '28';
        database.insert(lCase);
        String lProtocoloToString = JSON.serialize(lCase);
        
        ContentVersion lNewContentVersion = new ContentVersion(title = 'title test',
                                                               PathOnClient = 'PathOnClient test', Tipo_de_Documento__c ='1');
        Blob b = Blob.valueOf('This is version data');
        lNewContentVersion.VersionData = Encodingutil.base64Decode('This is version data');
        insert lNewContentVersion;
        
        ContentVersion lContentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion
                                          WHERE Id =: lNewContentVersion.Id];
        
        EX3_Centralizador__c lCentralizador = EX3_BuscaAtivaController.getCentralizador();
        lCentralizador.EX3_hasDocuments__c = false;
        String lCentralToStr = json.serialize(lCentralizador);
        
        ContentDocumentLink lNewContentDocumentLink = new ContentDocumentLink(LinkedEntityId = lCentralizador.Id,
                                                                              ContentDocumentId = lContentVersion.ContentDocumentId,
                                                                              Visibility = 'AllUsers',
                                                                              ShareType = 'V');
        insert lNewContentDocumentLink; 
        
        User lUser = EX3_DataFactory.getAdmUser();
        lUser.FuncionalColaborador__c = 'funcional';
        database.insert(lUser);
         
        EX3_ObjectFactory.IncluirProtocolo lIncluirProtocolo = new EX3_ObjectFactory.IncluirProtocolo();
        lIncluirProtocolo.codigo_protocolo_externo = lCase.Id;
        lIncluirProtocolo.codigo_unico_processo = lCase.EX3_Numero_do_Processo__c;
        lIncluirProtocolo.data_protocolo_documento = lCase.CreatedDate;
        lIncluirProtocolo.codigo_canal_entrada_documento = Decimal.valueOf(lCase.EX3_Canal_de_Entrada__c);
        lIncluirProtocolo.codigo_motivo_situacao = Decimal.valueOf(lCase.EX3_Motivo_Situacao__c);
        lIncluirProtocolo.data_situacao = system.today(); 
        lIncluirProtocolo.operador = lUser.FuncionalColaborador__c;
        lIncluirProtocolo.codigo_operacao = 3;
        lIncluirProtocolo.indicador_protocolo = 0;
        String lProtocoloBody = JSON.serialize(lIncluirProtocolo);
         
        Map<String, String> lMapToSendParams = new Map<String, String>();
        lMapToSendParams.put('classe', 'EX3_BuscaAtivaController');
        lMapToSendParams.put('callback', 'enviarProtocoloCallback');
        lMapToSendParams.put('central', lCentralToStr);
        lMapToSendParams.put('protocolo', lProtocoloBody); 
        lMapToSendParams.put('service', 'incluirProtocolo');
   
        String lMapToSendParamsToString = JSON.serialize(lMapToSendParams);
        
        List<EX3_Parametrizacao__c> lLstToken = new List<EX3_Parametrizacao__c>();
        EX3_Parametrizacao__c lParametrizacao = new EX3_Parametrizacao__c();
        lParametrizacao.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token');
        lParametrizacao.EX3_Access_token__c = '12345';
        lParametrizacao.EX3_Token_external__c = 'TokenSTS';
        lLstToken.add(lParametrizacao);
        insert lLstToken; 

        test.startTest();
        EX3_IncluirCaseMock lMockSucesso = new EX3_IncluirCaseMock(lIncluirProtocolo, 200);

        Continuation lContinuationSuccess = EX3_BuscaAtivaController.enviarProtocolo(lMapToSendParamsToString);
        Map<String, HttpRequest> lRequestSucess = lContinuationSuccess.getRequests();
        List<String> lLstRequestLabelSucess = new List<String>();
        for (String iLabel : lRequestSucess.keySet()){
            lLstRequestLabelSucess.add(iLabel);
        } 
        Map<String,String> dados = new Map<String,String>();  
        Map<String,String> lRequisicao = new Map<String,String>{'incluirProtocolo' => lLstRequestLabelSucess[0]};
        Object lState = new EX3_ContinuationUtils.StateInfo(lLstRequestLabelSucess[0],lRequisicao, dados, 'EX3_BuscaAtivaControllerTest');
        HTTPResponse lResponseFromMock = lMockSucesso.respond(lRequestSucess.get('Continuation'));
        Test.setContinuationResponse(lLstRequestLabelSucess[0], lResponseFromMock);
        
        EX3_IncluirCaseMock lMockFalha = new EX3_IncluirCaseMock(lIncluirProtocolo, 400);
        
        Continuation lContinuationFalha = EX3_BuscaAtivaController.enviarProtocolo(lMapToSendParamsToString);
        Map<String, HttpRequest> lRequestFalha = lContinuationFalha.getRequests();
        List<String> lLstRequestLabelFalha = new List<String>();
        for (String iLabel : lRequestFalha.keySet()){
            lLstRequestLabelFalha.add(iLabel);
        } 
        Map<String,String> lRequisicaoFalha = new Map<String,String>{'incluirProtocolo' => lLstRequestLabelFalha[0]};
        Object lStateFalha = new EX3_ContinuationUtils.StateInfo(lLstRequestLabelFalha[0],lRequisicaoFalha, dados, 'EX3_BuscaAtivaControllerTest');
        HTTPResponse lResponseFromMockFalha = lMockFalha.respond(lRequestFalha.get('Continuation'));
        Test.setContinuationResponse(lLstRequestLabelFalha[0], lResponseFromMockFalha);
        test.stopTest(); 

        String lResult = (String) EX3_BuscaAtivaController.enviarProtocoloCallback(lState);
        Map<String,Object> lResponse = (Map<String,Object>)JSON.deserializeUntyped(lResult);  
        system.assert(!lRequestSucess.isEmpty());

        String lResultFalha = (String) EX3_BuscaAtivaController.enviarProtocoloCallback(lStateFalha);
        Map<String,Object> lResponseFalha = (Map<String,Object>)JSON.deserializeUntyped(lResultFalha);  
        system.assert(!lRequestFalha.isEmpty());
        system.assertEquals('Erro ao enviar o request', lResponseFalha.get('error'));
                
    }   
    
    @isTest static void enviarArquivosEAtualizarCaseTest() 
    {          
        Case lCase = EX3_BI_DataLoad.getCase(); 
        lCase.EX3_Canal_de_Entrada__c = '9';
        lCase.EX3_Numero_do_protocolo__c = '123919230';
		lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        database.insert(lCase);

        EX3_Centralizador__c lCentralizador = EX3_BuscaAtivaController.getCentralizador();
        lCentralizador.EX3_hasDocuments__c = true;
        String lCentralizadorToString = json.serialize(lCentralizador);

        User lUser = EX3_DataFactory.getAdmUser();
        lUser.FuncionalColaborador__c = 'funcional'; 
        database.insert(lUser);
        String lFuncional = [SELECT FuncionalColaborador__c FROM User WHERE Id =: lUser.Id].FuncionalColaborador__c;

        String lCaseToString = JSON.serialize(lCase);
        String aBody = EX3_BuscaAtivaController.criarDocumentos(lCaseToString, lCentralizadorToString, lFuncional);
        Map<String, String> lMapToSendParams = new Map<String, String>();
        lMapToSendParams.put('case', lCaseToString);
        lMapToSendParams.put('classe', 'EX3_BuscaAtivaController');
        lMapToSendParams.put('aBody', aBody); 
        lMapToSendParams.put('callback', 'enviarArquivosEProtocoloCallback');
        lMapToSendParams.put('service', 'incluirDocumento_AtualizarProtocolo');
   
        String lMapToSendParamsToString = JSON.serialize(lMapToSendParams);
        system.debug('lMapToSendParamsToString === ' + lMapToSendParamsToString);

        List<EX3_Parametrizacao__c> lLstToken = new List<EX3_Parametrizacao__c>();
        EX3_Parametrizacao__c lParametrizacao = new EX3_Parametrizacao__c();
        lParametrizacao.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token');
        lParametrizacao.EX3_Access_token__c = '12345';
        lParametrizacao.EX3_Token_external__c = 'TokenSTS';
        lLstToken.add(lParametrizacao);
        insert lLstToken;  
        EX3_ObjectFactory.IncluirDocumento lDocumento = new EX3_ObjectFactory.IncluirDocumento();
        EX3_ObjectFactory.AtualizarProtocolo lAtualizaProtocolo = new EX3_ObjectFactory.AtualizarProtocolo();
        test.startTest();
         
        Continuation lContinuation = EX3_BuscaAtivaController.enviarArquivosEAtualizarCase(lMapToSendParamsToString);
        Map<String, HttpRequest> lLstRequest = lContinuation.getRequests();
        Map<String,String> lMapRequestLabel = new Map<String,String>();
        for (String iKeyRequest : lLstRequest.keyset())
        {
            if(lLstRequest.get(iKeyRequest).getMethod() == 'PUT')
            {
                lMapRequestLabel.put('atualizarProtocolo',iKeyRequest);
            }
            else 
            {
                lMapRequestLabel.put('incluirDocumento',iKeyRequest);
            }
        }
        Map<String,String> dados = new Map<String,String>();  
        Map<String,String> lRequisicao = new Map<String,String>{
            'incluirDocumento' => lMapRequestLabel.get('incluirDocumento'),
        	'atualizarProtocolo' => lMapRequestLabel.get('atualizarProtocolo')
        };
        Object lState = new EX3_ContinuationUtils.StateInfo(lMapRequestLabel.values(),lRequisicao, dados, 'EX3_BuscaAtivaControllerTest');
        
        EX3_IncluirDocumentoMock lMockDocumento = new EX3_IncluirDocumentoMock(lDocumento, 200);
		EX3_AtualizarProtocoloMock lMockProtocolo = new EX3_AtualizarProtocoloMock(lAtualizaProtocolo, 200); 

        HTTPResponse lResponseFromMockDocumento = lMockDocumento.respond(lLstRequest.get('Continuation'));
        Test.setContinuationResponse(lMapRequestLabel.get('incluirDocumento'), lResponseFromMockDocumento);
        
        HTTPResponse lResponseFromMockProtocolo = lMockProtocolo.respond(lLstRequest.get('Continuation'));
        Test.setContinuationResponse(lMapRequestLabel.get('atualizarProtocolo'), lResponseFromMockProtocolo);
        
        test.stopTest(); 
        
        String lResult = (String) EX3_BuscaAtivaController.enviarArquivosECaseCallback(lState); 
        Map<String,Object> lResponse = (Map<String,Object>)JSON.deserializeUntyped(lResult);
        system.assert(!lMapRequestLabel.isEmpty());
    }   
        
    public class AttributesTypesPicklist
    {
        private String label {get;set;}
        private String value {get;set;}
    }

   public class AttributesTypes
   {
      private String label {get; set;}
      private String value {get; set;}
      private String api {get;set;}
      private String fieldType {get;set;}
      private String apexType {get;set;}
      private boolean isDisable {get;set;}
      private boolean required {get;set;}
      private String formatter {get;set;}
      private String max{get;set;}
      private String min{get;set;}
      private String maxlength {get;set;}
      private String minlength {get;set;}
      private List<AttributesTypesPicklist> picklistValues {get;set;}
   }
}