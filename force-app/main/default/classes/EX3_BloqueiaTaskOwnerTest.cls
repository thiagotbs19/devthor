/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste  
*
* 
*Apex Class: EX3_BloqueiaTaskOwnerTest 
* Empresa: everis do Brasil  
* Autor: Thiago Barbosa 
*
********************************************************************************/

@isTest
public class EX3_BloqueiaTaskOwnerTest {


    @TestSetup
    public static void createData(){ 
        
        String PROF_ANALISTA_OPERACIONAL = 'Analista Operacional';
        User lUserOperacional = EX3_BI_DataLoad.getUser(PROF_ANALISTA_OPERACIONAL);
        Database.insert(lUserOperacional);
        
        List<Group> lLstGroup = [SELECT Id, Name, DeveloperName 
                                 FROM Group WHERE Type='Queue' 
                                 AND DeveloperName='EX3_Definicao_de_pre_estrategia'];
        
        User lUser = EX3_BI_DataLoad.getUser();
        Database.insert(lUser); 
        
        Case lCase = EX3_BI_DataLoad.getCase();	       	
        Database.insert(lCase);
        
        Account lAccount = EX3_BI_DataLoad.getAccount();        
        Database.insert(lAccount); 
        
       	Test.startTest();
        Task lTask = EX3_BI_DataLoad.getTask();
        lTask.OwnerId = lUserOperacional.Id; 
        System.runAs(lUserOperacional){
       		Database.UpsertResult lResult = Database.upsert(lTask, false); 
        }
        Test.stopTest();
       }

    @isTest static void testgetTask(){ 

        

        User lUser = [SELECT Id FROM User LIMIT 1]; 
        
        User lUserOperacional = [SELECT Id FROM User LIMIT 1];

        Case lCase = [SELECT Id FROM Case LIMIT 1];
        
        Task lTask = EX3_BI_DataLoad.getTask();
        lTask.OwnerId = lUserOperacional.Id; 
        
        Test.startTest();
        System.runAs(lUserOperacional){
        	Database.UpsertResult lResult = Database.upsert(lTask, false);
        } 
        Test.stopTest();
        
        Task lTaskTwo = EX3_BI_DataLoad.getTask();
        lTaskTwo.OwnerId = lUserOperacional.Id;
        System.runAs(lUserOperacional){
        	Database.UpsertResult lResult = Database.upsert(lTaskTwo, false);
        }
        
        Task lTaskThree = EX3_BI_DataLoad.getTask();
        lTaskThree.OwnerId = lUserOperacional.Id;
        System.runAs(lUserOperacional){ 
        	Database.UpsertResult lResult = Database.upsert(lTaskThree, false);
        }
        Database.SaveResult lResult = Database.update(lTask, false);
        System.assert(!lResult.isSuccess(), 'A tarefa foi criada');    
    }
}