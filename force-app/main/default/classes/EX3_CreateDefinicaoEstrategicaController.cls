/*********************************************************************************
*                                    Itaú - 2020 
*
* Classe Responsável por atualizar o registro de Subsidios e criar o Laudo
*
*
*Apex Class: EX3_CreateDefinicaoEstrategicaController
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/


public without sharing class EX3_CreateDefinicaoEstrategicaController {    
    
    @AuraEnabled
    public static Boolean getHasPermissionSet(){
        Boolean lhasPermission = FeatureManagement.checkPermission('EX3_Criar_Tarefa'); 
        
        return lHasPermission;
   }
    
    @AuraEnabled 
    public static String redirectToObject(String aRecordId)
    {   
        List<EX3_DefinicaoEstrategia__c> lLstEX3DefEstrategica;
        Exception lEx;  
        if(aRecordId == null){ SA7_CustomdebugLog.logError('EX3', 'EX3_CreateDefinicaoEstrategicaController', 'redirectToObject', 'Erro ao inserir o Laudo '+ lEx.getMessage(), lEx, null); return '';}
        try{
            List<EX3_Subsidios__c> lLstSubsidios = [Select Id, EX3_Caso__c, EX3_Fase__c, OwnerId,EX3_Data_de_Entrada__c,
                                                    EX3_Numero_do_Processo__c, EX3_Numero_Protocolo__c
                                                    FROM EX3_Subsidios__c 
                                                    Where Id =: aRecordId];
            if(lLstSubsidios.isEmpty()){ return '';}   
            
            EX3_Subsidios__c lSubsidios = lLstSubsidios[0]; 
            lSubsidios.OwnerId = UserInfo.getUserId();
            lSubsidios.EX3_Status_da_Pasta__c = Label.EX3_Status_Pasta;
            lSubsidios.EX3_Status__c =  Label.EX3_Doc_Solicitado;        
            Database.update(lSubsidios, false);         

            List<EX3_Subsidios__c> lLstEX3_Subsidios = new List<EX3_Subsidios__c>();
            List<EX3_Tipo_Documento__c> lLstTipoDoc = [SELECT Id, EX3_Subsidios__r.EX3_Caso__c, EX3_Status_do_Documento__c, EX3_Checklist__c,
                                                       EX3_Subsidios__c      
                                                       FROM EX3_Tipo_Documento__c     
                                                       WHERE EX3_Subsidios__c =: lSubsidios.Id 
                                                       AND EX3_Arquivo_Anexado__c = false AND       
                                                       EX3_Status_do_Documento__c  IN ('Documento pendente', 'Documento solicitado')]; 
            if(!lLstTipoDoc.isEmpty()){ 
                return Label.EX3_Finalizar_Captura;     
            }    
            EX3_Subsidios__c iSub = new EX3_Subsidios__c();
            iSub.EX3_Status__c = Label.EX3_Status_Finalizado;   
            iSub.Id = lSubsidios.Id;        
            lLstEX3_Subsidios.add(iSub);        
            
            
            if(!lLstEX3_Subsidios.isEmpty() || lLstEX3_Subsidios != null){
                Database.update(lLstEX3_Subsidios);  
            }
            
            Integer lQuantidadeContratos = 0;
            list<Contract> lLstContrato = [SELECT Id FROM contract WHERE EX3_Captura_de_documentos__c =: aRecordId];
            
            
            List<Group> lLstGroupPreEstrategica = [SELECT Id, Name FROM Group WHERE Type='Queue'   
                                                AND Name =: Label.EX3_Definicao_de_pre_estrategia];
            
            if(lLstGroupPreEstrategica.isEmpty()){ return ''; }  
             
            List<Group> lLstGroupEstrategia = [SELECT Id, Name FROM Group WHERE Type='Queue' AND Name =: Label.EX3_Def_Estrategica]; 
            
            if(lLstGroupEstrategia.isEmpty()){ return '';}    
            
            lLstEX3DefEstrategica = new List<EX3_DefinicaoEstrategia__c>(); 
            List<EX3_Tipo_Documento__c> lLstTipoDocValid = [SELECT Id, EX3_Caso__c, EX3_Status_do_Documento__c, EX3_Checklist__c, EX3_Subsidios__c 
                                                            FROM EX3_Tipo_Documento__c           
                                                            WHERE EX3_Subsidios__c =: lSubsidios.Id    
                                                            AND EX3_Arquivo_Anexado__c = true AND             
                                                            EX3_Status_do_Documento__c IN ('Documento validado','Documento não encontrado')];
            
            if(lLstTipoDocValid.isEmpty()){ 
                return Label.EX3_Empty_Doc;}       
            if(!lLstTipoDocValid.isEmpty()){ 
                EX3_DefinicaoEstrategia__c lDefEstrategica = new EX3_DefinicaoEstrategia__c();  
                if(lSubsidios.EX3_Fase__c == Label.EX3_PreEstrategia){ 
                    lDefEstrategica.OwnerId = lLstGroupPreEstrategica[0].Id;     
                    lDefEstrategica.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_DefinicaoEstrategia__c', 'EX3_Pre_estrategia');
                    lDefEstrategica.EX3_Subsidios__c = lSubsidios.Id;
                    lDefEstrategica.EX3_Estrategia__c = Label.EX3_Estrategia;
                    lDefEstrategica.EX3_Caso__c = lSubsidios.EX3_Caso__c;
                    lLstEX3DefEstrategica.add(lDefEstrategica); 
                }  
                if(lSubsidios.EX3_Fase__c == Label.EX3_ChecklistComplementar){
                    lDefEstrategica.OwnerId = lLstGroupEstrategia[0].Id;          
                    lDefEstrategica.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_DefinicaoEstrategia__c', 'EX3_Estrategia_definitiva');
                    lDefEstrategica.EX3_Subsidios__c = lSubsidios.Id; 
                    lDefEstrategica.EX3_Caso__c = lSubsidios.EX3_Caso__c; 
                    lDefEstrategica.EX3_Estrategia__c = Label.EX3_Estrategia;
                    lLstEX3DefEstrategica.add(lDefEstrategica);  
                } 
            } 
            
            if(!lLstEX3DefEstrategica.isEmpty()){    
                Database.insert(lLstEX3DefEstrategica);  
            }  
            
            List<EX3_Laudo__c> lLstLaudo = new List<EX3_Laudo__c>();
            
            for(Contract iContract : lLstContrato) 
             {
                for(EX3_DefinicaoEstrategia__c iDefEst : lLstEX3DefEstrategica){
                    EX3_Laudo__c lLaudo = new EX3_Laudo__c(
                        EX3_Definicao_de_estrategia__c = iDefEst.id,
                        EX3_Caso__c = iDefEst.EX3_Caso__c,
                        EX3_Numero_protocolo__c = iDefEst.EX3_Codigo_da_pasta__c
                    );  
                    lLstLaudo.add(lLaudo); 
                }  
            } 
            
            if(!lLstLaudo.isEmpty()){   
                Database.insert(lLstLaudo);  
            } 
            
        }
        catch(DmlException ex) {  
            SA7_CustomdebugLog.logError('EX3', 'EX3_CreateDefinicaoEstrategicaController', 'redirectToObject', 'Erro ao inserir o Laudo ou a Definição Estrategica ' +
                                        ex.getMessage(), ex, new Map<String, String>()); 
        }  
        catch (Exception ex)
        { 
            SA7_CustomdebugLog.logError('EX3', 'EX3_CreateDefinicaoEstrategicaController', 'redirectToObject', 'Erro ao inserir o Laudo '+ ex.getMessage(), ex, null);
        } 
        return JSON.serialize(lLstEX3DefEstrategica);        
    }
}