/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por listar protocolos do Forcenedor de Diligencia
* AuraComponente: EX3_PendenciaFornecerDiligencia
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
public with sharing class EX3_PendenciaFornDilController
{
    @AuraEnabled
    public static Map<String, Object> getPage() 
    {
        String lColumnsAndSearchType = getColumnsAndSearchType();
        List<Case> lLstCases = getCases();
        String lOptionsMotivoSituacao = optionsMotivoSituacao();
        String lOptionPriority = optionPriority();

        Map<String, Object> lMapPage = new Map<String,Object>
        {
            'columns' => lColumnsAndSearchType,
            'records' => lLstCases,
            'motivo' => lOptionsMotivoSituacao,
            'priority' => lOptionPriority
        };
        return lMapPage;
    }
	@TestVisible
    private static String getColumnsAndSearchType()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();

        AttributesTypes lNumeroDoProtocolo = new AttributesTypes();
        lNumeroDoProtocolo.label = 'Protocolo';
        lNumeroDoProtocolo.value = 'EX3_Numero_do_Protocolo__c';
        lLstAttributesTypes.add(lNumeroDoProtocolo);

        AttributesTypes lNumeroDoProcesso = new AttributesTypes();
        lNumeroDoProcesso.label = 'Processo';
        lNumeroDoProcesso.value = 'EX3_Numero_do_Processo__c';
        lLstAttributesTypes.add(lNumeroDoProcesso);

        AttributesTypes lPrioridade = new AttributesTypes();
        lPrioridade.label = 'Prioridade';
        lPrioridade.value = 'EX3_Prioridade__c';
        lLstAttributesTypes.add(lPrioridade);

        AttributesTypes lSituacao = new AttributesTypes();
        lSituacao.label = 'Situação';
        lSituacao.value = 'EX3_Motivo_da_Situacao__c';
        lLstAttributesTypes.add(lSituacao);

        AttributesTypes lDataEntrada = new AttributesTypes();
        lDataEntrada.label = 'Data de Entrada';
        lDataEntrada.value = 'EX3_Data_de_Entrada__c';
        lLstAttributesTypes.add(lDataEntrada);

        return JSON.serialize(lLstAttributesTypes);
    }

    @AuraEnabled
    public static List<Case> getCases()
    {
        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Set<String> lSetMotivoFornecedorDiligencia = getMotivoFornecedorDiligencia();

        List<User> lLstUser = [SELECT Id, EX3_Pertence__c FROM User WHERE Id = :UserInfo.getUserId()];
        String lPertence = lLstUser[0].EX3_Pertence__c;

        String lQuery = 'SELECT '+ String.escapeSingleQuotes(returnFields())
                      + ' FROM Case '
                      + ' WHERE EX3_Motivo_Situacao__c = :lSetMotivoFornecedorDiligencia LIMIT 2000';

         List<SObject> lLstCases = (List<SObject>) database.query(String.escapeSingleQuotes(lQuery));
        return lLstCases;
    }
	
    @TestVisible
    private static String optionsMotivoSituacao()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
        Set<String> lSetMotivoFornecedorDiligencia = getMotivoFornecedorDiligencia();
        lSetMotivoFornecedorDiligencia.add('42');
        lSetMotivoFornecedorDiligencia.add('Finalizado');

        for (Schema.PicklistEntry iPicklistLabelField : Case.EX3_Motivo_Situacao__c.getDescribe().getPicklistValues())
        {
            if (!lSetMotivoFornecedorDiligencia.contains(iPicklistLabelField.getValue())) { continue; }
            AttributesTypes lAttributesTypes = new AttributesTypes();
            lAttributesTypes.label = iPicklistLabelField.getLabel();
            lAttributesTypes.value = iPicklistLabelField.getValue();
            lLstAttributesTypes.add(lAttributesTypes);
        }

       return JSON.serialize(lLstAttributesTypes);
    }
	
    @TestVisible
    private static String optionPriority()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
        for (Schema.PicklistEntry iPicklistLabelField : Case.EX3_Prioridade__c.getDescribe().getPicklistValues())
        {
            AttributesTypes lAttributesTypes = new AttributesTypes();
            lAttributesTypes.label = iPicklistLabelField.getLabel();
            lAttributesTypes.value = iPicklistLabelField.getLabel();

         lLstAttributesTypes.add(lAttributesTypes);
        }
        return JSON.serialize(lLstAttributesTypes);
    }

    @AuraEnabled
    public static List<Case> sortRecords(String aSortField, String aSortAsc, String aCaseRecords)
    {

        List<Case> lLstCaseToSort = (List<Case>) JSON.deserialize(aCaseRecords, List<Case>.class);

        String lQuery = 'SELECT '+ String.escapeSingleQuotes(returnFields())
                      + ' FROM Case'
                      + ' WHERE Id = :lLstCaseToSort ';
               lQuery += ' ORDER BY '+String.escapeSingleQuotes(aSortField)+' '+String.escapeSingleQuotes(aSortAsc);

        List<SObject> lLstCasesSorted = (List<SObject>) database.query(String.escapeSingleQuotes(lQuery));

        return lLstCasesSorted;
    }

    @AuraEnabled
    public static List<Case> filterRecords(String aField, String aSearch, Datetime aDataInicio, Datetime aDataFim)
    {
        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        List<User> lLstUser = [SELECT Id, EX3_Pertence__c FROM User WHERE Id = :UserInfo.getUserId()];
        String lPertence = lLstUser[0].EX3_Pertence__c;

        Date lDataInicio = (aField == 'EX3_Data_de_Entrada__c') ? aDataInicio.dateGMT() : null;
        Date lDataFim = (aField == 'EX3_Data_de_Entrada__c') ? aDataFim.dateGMT() : null;

        Set<String> lSetMotivoFornecedorDiligencia = getMotivoFornecedorDiligencia();
        lSetMotivoFornecedorDiligencia.add('42');
        lSetMotivoFornecedorDiligencia.add('Finalizado');

        String lQuery = 'SELECT Status, '+ String.escapeSingleQuotes(returnFields()) + ' FROM Case ';

        String lWhereClause =  ' WHERE RecordTypeId = :lRTCaseRecepcao AND'
                            + ' EX3_Motivo_Situacao__c = :lSetMotivoFornecedorDiligencia ';

        if(aField != 'EX3_Data_de_Entrada__c')
        {
            lWhereClause += ' AND ' +String.escapeSingleQuotes(aField)+ ' LIKE  ' +'\'%'+String.escapeSingleQuotes(aSearch)+'%\'' + ' LIMIT 2000';
        }
        else
        {
            lWhereClause +=  ' AND DAY_ONLY('+String.escapeSingleQuotes(aField) +') >= :lDataInicio';
            lWhereClause += ' AND DAY_ONLY('+String.escapeSingleQuotes(aField) +') <= :lDataFim LIMIT 2000';
        }
        String lEscapeQuery = String.escapeSingleQuotes(lQuery) + lWhereClause;
        List<SObject> lLstCasesFiltered = (List<SObject>) database.query(lEscapeQuery);

        return lLstCasesFiltered;
    }

    @AuraEnabled
    public static String getExportExcelValues(String aLstProtocolo)
    {
        List<Case> lLstCase = (List<Case>) JSON.deserialize(aLstProtocolo, List<Case>.class);

        String fileContent = 'Id'+'\t'+'Numero do Processo'+'\t' +'Numero do Protocolo'+ '\t' + 'Prioridade' +'\t' + 'Motivo' + '\t' + 'Data de entrada' +'\n';
        for(Case iCase : lLstCase)
        {
            fileContent += iCase.Id + '\t' +
                           iCase.EX3_Numero_do_Processo__c + '\t' +
                           iCase.EX3_Numero_do_Protocolo__c + '\t' +
                           iCase.EX3_Prioridade__c + '\t' +
                           iCase.EX3_Motivo_Situacao__c + '\t' +
                           iCase.EX3_Data_de_Entrada__c + '\n';
        }
        return JSON.serialize(blob.valueOf(fileContent));
    }

    private static Set<String> getMotivoFornecedorDiligencia()
    {
        Set<String> lSetMotivoFornecedorDiligencia = new Set<String>
        {
            '9','Solicitação devolvida ao credenciado',
            '14','Solicitação de diligência enviado ao credenciado'
        };
        return lSetMotivoFornecedorDiligencia;
    }

    private static String returnFields()
    {
        Set<String> lSetField = new Set<String>
        {
            'Id', 'RecordtypeId',
            'EX3_Numero_do_Protocolo__c', 'EX3_Numero_do_Processo__c',
            'EX3_Prioridade__c', 'toLabel(EX3_Motivo_Situacao__c)',
            'EX3_Data_de_Entrada__c' 
        };

        String soqlQueryFields = '';

        for (String iField : lSetField)
        {
            soqlQueryFields += iField + ',';
        }

        return soqlQueryFields.removeEnd(',');
    }

    private class AttributesTypes
    {
        private Object label {get; set;}
        private Object value {get; set;}
    } 
}