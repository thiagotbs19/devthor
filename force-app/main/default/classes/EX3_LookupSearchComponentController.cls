public class EX3_LookupSearchComponentController {
    
    @AuraEnabled 
    public static List<SObJectResult> getResults(string StrSql , String ObjectName, String fieldName, String value) {
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        strsql = strsql.replace('@1' , '\'%' + string.escapeSingleQuotes(value) + '%\'');
        system.debug('strsql'+ strsql);
        if(string.isNotBlank(StrSql)){
            for(sObject so : Database.Query(strsql)) {
                String fieldvalue = (String)so.get(fieldName);
                sObjectResultList.add(new SObjectResult(fieldvalue, so.Id));
            }
        }else{
            for(sObject so : Database.Query('Select Id,'+fieldName+' FROM '+ObjectName+' WHERE '+fieldName +' LIKE \'%' + string.escapeSingleQuotes(value) + '%\'')) {
                String fieldvalue = (String)so.get(fieldName);
                sObjectResultList.add(new SObjectResult(fieldvalue, so.Id));
            }
        }
        return sObjectResultList;
    }
    
    public class SObJectResult {
        @AuraEnabled
        public String recName;
        @AuraEnabled
        public Id recId;
        
        public SObJectResult(String recNameTemp, Id recIdTemp) {
            recName = recNameTemp;
            recId = recIdTemp;
        }
    }
}