/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de teste responsável por cobrir e testar a classe EX3_CadastroBeforeInsert
*
*Apex Class: EX3_CadastroBeforeInsertTest
*  

* Empresa: everis do Brasil
* Autor: Isabella Tannús
*
********************************************************************************/

@isTest
public class EX3_CadastroBeforeInsertTest {

    @isTest
    public static void cadastroBeforeInsertTest(){
        
        Account lAccount = new Account();
        lAccount.Name = 'Teste Account';
        Database.insert(lAccount);
        
        Case lCase = EX3_BI_DataLoad.getCase();
        lCase.Status = 'New';
        Database.insert(lCase);
        
        Test.startTest();
        EX3_Cadastro__c lRegister = EX3_BI_DataLoad.getCadastro(lCase);
        lRegister.EX3_Escritorio_Credenciado__c = lAccount.Id;
        lRegister.EX3_Caso__c = lCase.Id;
        

        Database.insert(lRegister);
        
        lRegister.EX3_Recebe_Escritorio_Credenciado__c = lRegister.EX3_Escritorio_Credenciado__c;
        
        Database.update(lRegister);
        Test.stopTest();

		list<Case> lLstCase = [SELECT Id, CreatedDate, EX3_Data_de_Entrada__c FROM Case];      
        list<EX3_Cadastro__c> lstRegister = [SELECT Id, CreatedDate, EX3_Data_da_Entrada__c FROM EX3_Cadastro__c];   
        
        system.assertEquals(lLstCase[0].CreatedDate.date(), lstRegister[0].EX3_Data_da_Entrada__c); 
        
    }
    @isTest
    public static void cadastroBeforeInsertTestcontinue(){
        
        Account lAccount = new Account();
        lAccount.Name = 'Teste Account';
        Database.insert(lAccount);
        
        Case lCase = EX3_BI_DataLoad.getCase(); 
        lCase.Status = 'New';
        Database.insert(lCase);

        
        
        Test.startTest(); 
        EX3_Cadastro__c lRegister = EX3_BI_DataLoad.getCadastro(lCase);
        lRegister.EX3_Escritorio_Credenciado__c = lAccount.Id; 
        lRegister.EX3_Caso__c = lCase.Id;
        lRegister.EX3_Recebe_Escritorio_Credenciado__c = lRegister.EX3_Escritorio_Credenciado__c;
        
        Database.insert(lRegister); 
        
        lRegister.EX3_Recebe_Escritorio_Credenciado__c = lRegister.EX3_Escritorio_Credenciado__c;
        
        Database.update(lRegister);
        Test.stopTest();

		list<Case> lLstCase = [SELECT Id, CreatedDate, EX3_Data_de_Entrada__c FROM Case];      
        list<EX3_Cadastro__c> lstRegister = [SELECT Id, CreatedDate, EX3_Data_da_Entrada__c FROM EX3_Cadastro__c];
        
        system.assertEquals(lLstCase[0].CreatedDate.date(), lstRegister[0].EX3_Data_da_Entrada__c);  
    }
}