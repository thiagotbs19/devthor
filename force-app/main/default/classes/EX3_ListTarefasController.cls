/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por recuperar os Campos de Cadastro. 
*
* Autor: Victor Melhem Deoud Lemos
* Aura Component Bundle: EX3_ListTarefasController 
* Test Class:  
*
********************************************************************************/

public without sharing class EX3_ListTarefasController {
    
    
    @AuraEnabled
    public static Boolean getHasPermissionSet(){
        Boolean lhasPermission = FeatureManagement.checkPermission('EX3_Criar_Tarefa'); 
        
        return lHasPermission;
   } 

   @AuraEnabled  
   public static Map<String, Object> getFieldsCadastro(Id aRecordId)
   {   
       Map<String, Object> lMapReturn;
       try{
       List<EX3_Cadastro__c> lLstCadastro = [Select EX3_Numero_Protocolo__c,  EX3_Carteira__c, EX3_Caso__c, EX3_Numero_do_Processo__c
                                                From EX3_Cadastro__c Where Id =: aRecordId];
       
       EX3_Subsidios__c lSubsidio; 
       EX3_Tipo_Documento__c lTipoDoc;
       EX3_Laudo__c  lLaudo;
       EX3_DefinicaoEstrategia__c lDefEstrategica;
       Group QueueGroup = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = 'EX3_Analise_de_decisao' LIMIT 1];
       Group lQueueGroupCalc = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName =: EX3_Utils.FILA_CALCULO LIMIT 1];
       String lRecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Solicitacao_ADJ');            
       String lRecordTypeCalc = EX3_Utils.getRecordTypeIdByDevName('EX3_Calculo__c', 'EX3_Solicitacao_de_Calculo'); 
       
       List<Group> lLstGroup = [SELECT Id, Name, DeveloperName FROM Group 
                                 WHERE Type='Queue'AND DeveloperName =: EX3_Utils.FILA_FUP];  
       String lfieldsToQuery = 'Id, EX3_Caso__c';                                      
       List<SObject> lLstSobject = querySObjects(aRecordId, lfieldsToQuery);
       Id lIdCase = (Id) lLstSObject[0].get('EX3_Caso__c');
       Schema.sObjectType lSubsidios = aRecordId.getSObjectType();
       if(lSubsidios == EX3_Subsidios__c.sObjectType){ 
      		lSubsidio = [SELECT Id, EX3_Numero_do_Processo__c, EX3_Numero_Protocolo__c, EX3_Cod_Pasta__c FROM EX3_Subsidios__c WHERE Id =: aRecordId];
       } 
       Schema.sObjectType lTipoDocumento = aRecordId.getSObjectType();
       if(lTipoDocumento == EX3_Tipo_Documento__c.sObjectType){ 
      		lTipoDoc = [SELECT Id, EX3_Numero_do_processo__c FROM EX3_Tipo_Documento__c WHERE Id =: aRecordId];
       } 
       Schema.sObjectType lSchemaLaudo = aRecordId.getSObjectType();
       if(lSchemaLaudo == EX3_Laudo__c.sObjectType){ 
      		lLaudo = [SELECT Id, EX3_Codigo_da_pasta__c, EX3_Numero_protocolo__c, EX3_Numero_do_processo__c FROM EX3_Laudo__c WHERE Id =: aRecordId];
       } 
        Schema.sObjectType lSchemaDefEstrategica = aRecordId.getSObjectType();
       if(lSchemaDefEstrategica == EX3_DefinicaoEstrategia__c.sObjectType){ 
      		lDefEstrategica = [SELECT Id, EX3_Codigo_da_pasta__c,  EX3_Numero_do_protocolo__c, EX3_Numero_do_Processo__c FROM EX3_DefinicaoEstrategia__c WHERE Id =: aRecordId];
       }  

           
       if(lLstCadastro.isEmpty()){  
           
           
           lMapReturn = new Map<String,Object>  
        {      
            'recordType' => lRecordTypeId,  
            'recordTypeCalc' => lRecordTypeCalc, 
            'lqueueIdCalcEmpty' => lQueueGroupCalc.Id,
            'IdCase' => lIdCase,
            'cadastro' => '',
            'subsidio' => lSubsidio,
             'documento' => lTipoDoc,
             'laudo' => lLaudo,
             'estrategia' => lDefEstrategica
                
        }; 
        return lMapReturn; 
       
            
       
  }
       EX3_Cadastro__c lCadastro = lLstCadastro[0]; 
       
       
        
        lMapReturn = new Map<String,Object>  
        {   
            
            'cadastro' => lCadastro,
            'recordType' => lRecordTypeId, 
            'recordTypeCalc' => lRecordTypeCalc,
            'queueId' => QueueGroup.Id,
            'queueIdCalc' => lQueueGroupCalc.Id,
            'IdCase' => lIdCase 
           
        };
       }catch (Exception ex)
      { 
         SA7_CustomdebugLog.logError('EX3', 'EX3_ListTarefasController', 'getFieldsCadastro', 'Erro ao criar o registro '+ ex.getMessage(), ex, null);
      }
       return lMapReturn;
   } 
   @AuraEnabled
   public static Id getTaskRecordType(){
       
       Id lRecordType = EX3_Utils.getRecordTypeIdByDevName('Task', 'EX3_Atuacao_Previa');

       return lRecordType;     
   }
    
    @AuraEnabled
    public static EX3_DefinicaoEstrategia__c getDefinicaoEstrategia(Id aRecordId){
        Schema.sObjectType lSObject = aRecordId.getSObjectType(); 
        EX3_DefinicaoEstrategia__c lDefEstrategica;
         if(lSObject == EX3_DefinicaoEstrategia__c.sObjectType){
         lDefEstrategica = [SELECT Id FROM EX3_DefinicaoEstrategia__c WHERE Id =: aRecordId];
    }
        return lDefEstrategica;
    }
    
     private static List<SObject> querySObjects(Id recordId, String fieldsToQuery) {
        string lQuery;
        Schema.sObjectType recordObject = recordId.getSobjectType();
        List<String> fieldNamesList = fieldsToQuery.split(',');
        for (Integer i = 0; i < fieldNamesList.size(); i++) {
            if (Math.mod(i, 2) == 1) {
                fieldNamesList.add(i, ',');
            }
        }
        lQuery = 'SELECT ';
        for (string lcampo : fieldNamesList) {
            lQuery += lcampo + ' ';
        }
        lQuery += ' from ' + recordObject + ' WHERE ' + 'Id' + ' =: recordId';
        List<SObject> lLstSObject = Database.query(String.escapeSingleQuotes(lQuery));
        return lLstSObject;
    }
}