@isTest
public with sharing class EX3_TriggerHelperTest {


  @isTest
  static void notChangedField()  
  {
    Account cAcc1 = EX3_BI_DataLoad.getAccount(), cAcc2 = EX3_BI_DataLoad.getAccount();
    cAcc1.Name = 'notChangedField';
    cAcc1.BillingStreet = 'Rua 1';
    cAcc2.Name = 'notChangedField';
    cAcc2.BillingStreet = 'Rua 1';
    Database.insert(new List<Account> { cAcc1, cAcc2 });
    cAcc1.BillingStreet = 'Rua 2';
    Database.update(new List<Account> { cAcc1, cAcc2 });
    cAcc1 = [SELECT Id FROM Account WHERE Id = :cAcc1.Id];
    system.assertEquals(cAcc1 != null, true);
 }

  @isTest
  static void notChangedFieldList()
  {
    Account cAcc1 = EX3_BI_DataLoad.getAccount(), cAcc2 = EX3_BI_DataLoad.getAccount();
    cAcc1.Name = 'notChangedFieldList';
    cAcc1.BillingStreet = 'Rua 1';
    cAcc2.Name = 'notChangedFieldList';
    cAcc2.BillingStreet = 'Rua 1';
    Database.insert(new List<Account> { cAcc1, cAcc2 });
    cAcc1.BillingStreet = 'Rua 2';
    Database.update(new List<Account> { cAcc1, cAcc2 });
    cAcc1 = [SELECT Id FROM Account WHERE Id = :cAcc1.Id];
    system.assertEquals(cAcc1 != null, true);
  }

  @isTest
  static void notChangedFieldSet()
  {
    Account cAcc1 = EX3_BI_DataLoad.getAccount(), cAcc2 = EX3_BI_DataLoad.getAccount();
    cAcc1.Name = 'notChangedFieldSet';
    cAcc1.BillingStreet = 'Rua 1';
    cAcc2.Name = 'notChangedFieldSet';
    cAcc2.BillingStreet = 'Rua 1';
    Database.insert(new List<Account> { cAcc1, cAcc2 });
    cAcc1.BillingStreet = 'Rua 2';
    Database.update(new List<Account> { cAcc1, cAcc2 });
    cAcc1 = [SELECT Id FROM Account WHERE Id = :cAcc1.Id];
    system.assertEquals(cAcc1 != null, true);
  }

  @isTest
  static void changedField()
  {
    Account cAcc1 = EX3_BI_DataLoad.getAccount(), cAcc2 = EX3_BI_DataLoad.getAccount();
    cAcc1.Name = 'changedField';
    cAcc1.BillingStreet = 'Rua 1';
    cAcc2.Name = 'changedField';
    cAcc2.BillingStreet = 'Rua 1';
    Database.insert(new List<Account> { cAcc1, cAcc2 });
    cAcc1.BillingStreet = 'Rua 2';
    Database.update(new List<Account> { cAcc1, cAcc2 });
    cAcc1 = [SELECT Id  FROM Account WHERE Id = :cAcc1.Id];
    system.assertEquals(cAcc1 != null, true);
  }

  @isTest
  static void changedFieldList()
  {
    Account cAcc1 = EX3_BI_DataLoad.getAccount(), cAcc2 = EX3_BI_DataLoad.getAccount();
    cAcc1.Name = 'changedFieldList';
    cAcc1.BillingStreet = 'Rua 1';
    cAcc2.Name = 'changedFieldList';
    cAcc2.BillingStreet = 'Rua 1';
    Database.insert(new List<Account> { cAcc1, cAcc2 });
    cAcc1.BillingStreet = 'Rua 2';
    Database.update(new List<Account> { cAcc1, cAcc2 });
    cAcc1 = [SELECT Id FROM Account WHERE Id = :cAcc1.Id];
    system.assertEquals(cAcc1 != null, true);
  }

  @isTest
  static void changedFieldSet()
  {
    Account cAcc1 = EX3_BI_DataLoad.getAccount(), cAcc2 = EX3_BI_DataLoad.getAccount();
    cAcc1.Name = 'changedFieldSet';
    cAcc1.BillingStreet = 'Rua 1';
    cAcc2.Name = 'changedFieldSet';
    cAcc2.BillingStreet = 'Rua 1';
    Database.insert(new List<Account> { cAcc1, cAcc2 });
    cAcc1.BillingStreet = 'Rua 2';
    Database.update(new List<Account> { cAcc1, cAcc2 });
    cAcc1 = [SELECT Id FROM Account WHERE Id = :cAcc1.Id];
    system.assertEquals(cAcc1 != null, true);
  }

  @isTest
  static void changedToExpectedValue()
  {
    Account cAcc1 = EX3_BI_DataLoad.getAccount(), cAcc2 = EX3_BI_DataLoad.getAccount();
    cAcc1.Name = 'changedToExpectedValue';
    cAcc1.BillingStreet = 'Rua 1';
    cAcc2.Name = 'changedToExpectedValue';
    cAcc2.BillingStreet = 'Rua 1';
    Database.insert(new List<Account> { cAcc1, cAcc2 });
    cAcc1.BillingStreet = 'Rua 2';
    cAcc2.BillingStreet = 'Rua 4';
    Database.update(new List<Account> { cAcc1, cAcc2 });
    cAcc1 = [SELECT Id  FROM Account WHERE Id = :cAcc1.Id];
    system.assertEquals(cAcc1 != null, true);
  }

  @isTest
  static void changedFrom()
  {
    Account cAcc1 = EX3_BI_DataLoad.getAccount(), cAcc2 = EX3_BI_DataLoad.getAccount();
    cAcc1.Name = 'changedFrom';
    cAcc1.BillingStreet = 'Rua 2';
    cAcc2.Name = 'changedFrom';
    cAcc2.BillingStreet = 'Rua 1';
    Database.insert(new List<Account> { cAcc1, cAcc2 });
    cAcc1.BillingStreet = 'Rua 1';
    Database.update(new List<Account> { cAcc1, cAcc2 });
    cAcc1 = [SELECT Id FROM Account WHERE Id = :cAcc1.Id];
    system.assertEquals(cAcc1 != null, true); 
  }

  @isTest
  static void changedFromTo()
  {
    Account cAcc1 = EX3_BI_DataLoad.getAccount(), cAcc2 = EX3_BI_DataLoad.getAccount();
    cAcc1.Name = 'changedFromTo';
    cAcc1.BillingStreet = 'Rua 1';
    cAcc2.Name = 'changedFromTo';
    cAcc2.BillingStreet = 'Rua 1';
    Database.insert(new List<Account> { cAcc1, cAcc2 });
    cAcc1.BillingStreet = 'Rua 2';
    Database.update(new List<Account> { cAcc1, cAcc2 });
    cAcc1 = [SELECT Id FROM Account WHERE Id = :cAcc1.Id];
    system.assertEquals(cAcc1 != null, true);
  }  
}