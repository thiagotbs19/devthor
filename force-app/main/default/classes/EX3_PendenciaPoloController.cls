/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por listar protocolos dos polos
* AuraComponente: EX3_PendenciaPolo
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
public with sharing class EX3_PendenciaPoloController
{
    @AuraEnabled
    public static Map<String, Object> getPage()
    {
        String lColumnsAndSearchType = getColumnsAndSearchType();
        List<Case> lLstCases = getCases();
        String lOptionsMotivoSituacao = optionsMotivoSituacao();
        String lOptionPriority = optionPriority();

        Map<String, Object> lMapPage = new Map<String,Object>
        {
            'columns' => lColumnsAndSearchType,
            'records' => lLstCases,
            'motivo' => lOptionsMotivoSituacao,
            'priority' => lOptionPriority
        };
        return lMapPage;
    }

    private static String getColumnsAndSearchType()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();

        AttributesTypes lNumeroDoProtocolo = new AttributesTypes();
        lNumeroDoProtocolo.label = 'Protocolo';
        lNumeroDoProtocolo.value = 'EX3_Numero_do_Protocolo__c';
        lLstAttributesTypes.add(lNumeroDoProtocolo);

        AttributesTypes lEtiquetaZ = new AttributesTypes();
        lEtiquetaZ.label = 'Etiqueta Z';
        lEtiquetaZ.value = 'EX3_Etiqueta_Z__c';
        lLstAttributesTypes.add(lEtiquetaZ);

        AttributesTypes lNumeroDoProcesso = new AttributesTypes();
        lNumeroDoProcesso.label = 'Processo';
        lNumeroDoProcesso.value = 'EX3_Numero_do_Processo__c';
        lLstAttributesTypes.add(lNumeroDoProcesso);

        AttributesTypes lPrioridade = new AttributesTypes();
        lPrioridade.label = 'Prioridade';
        lPrioridade.value = 'EX3_Prioridade__c';
        lLstAttributesTypes.add(lPrioridade);

        AttributesTypes lSituacao = new AttributesTypes();
        lSituacao.label = 'Situação';
        lSituacao.value = 'EX3_Motivo_da_Situacao__c';
        lLstAttributesTypes.add(lSituacao);

        AttributesTypes lDataEntrada = new AttributesTypes();
        lDataEntrada.label = 'Data de Entrada';
        lDataEntrada.value = 'EX3_Data_de_Entrada__c';
        lLstAttributesTypes.add(lDataEntrada);

        return JSON.serialize(lLstAttributesTypes); 
    }

    @AuraEnabled
    public static List<Case> getCases()
    {
        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Set<String> lSetMotivoPolo = getMotivosPolo();

        List<User> lLstUser = [SELECT Id, EX3_Pertence__c FROM User WHERE Id = :UserInfo.getUserId()];
        String lPertence = lLstUser[0].EX3_Pertence__c;

        String lQuery = 'SELECT '+ String.escapeSingleQuotes(returnFields())
                      + ' FROM Case '
                      + ' WHERE RecordTypeId = :lRTCaseRecepcao AND' 
                      + ' EX3_Motivo_Situacao__c = :lSetMotivoPolo AND'
                      + ' EX3_Canal_de_Entrada__c = :lPertence LIMIT 2000';

        List<SObject> lLstCases = (List<SObject>) database.query(String.escapeSingleQuotes(lQuery));

        return lLstCases;
    }

    private static String optionsMotivoSituacao()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
        Set<String> lSetMotivoPolo = getMotivosPolo();
        lSetMotivoPolo.add('42');
        lSetMotivoPolo.add('Finalizado');

        for (Schema.PicklistEntry iPicklistLabelField : Case.EX3_Motivo_Situacao__c.getDescribe().getPicklistValues())
        {
            if (!lSetMotivoPolo.contains(iPicklistLabelField.getValue())) { continue; }
            AttributesTypes lAttributesTypes = new AttributesTypes();
            lAttributesTypes.label = iPicklistLabelField.getLabel();
            lAttributesTypes.value = iPicklistLabelField.getValue();
            lLstAttributesTypes.add(lAttributesTypes);
        }

       return JSON.serialize(lLstAttributesTypes);
    }

    private static String optionPriority()
    {
        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
        for (Schema.PicklistEntry iPicklistLabelField : Case.EX3_Prioridade__c.getDescribe().getPicklistValues())
        {
            AttributesTypes lAttributesTypes = new AttributesTypes();
            lAttributesTypes.label = iPicklistLabelField.getLabel();
            lAttributesTypes.value = iPicklistLabelField.getLabel();

         lLstAttributesTypes.add(lAttributesTypes);
        }
        return JSON.serialize(lLstAttributesTypes);
    }

    @AuraEnabled
    public static List<Case> sortRecords(String aSortField, String aSortAsc, String aCaseRecords)
    {

        List<Case> lLstCaseToSort = (List<Case>) JSON.deserialize(aCaseRecords, List<Case>.class);

        String lQuery = 'SELECT ' + String.escapeSingleQuotes(returnFields())
                      + ' FROM Case'
                      + ' WHERE Id = :lLstCaseToSort ';
               lQuery += ' ORDER BY '+String.escapeSingleQuotes(aSortField)+' '+String.escapeSingleQuotes(aSortAsc);

        List<SObject> lLstCasesSorted = (List<SObject>) database.query(String.escapeSingleQuotes(lQuery));

        return lLstCasesSorted;
    }

    @AuraEnabled
    public static List<Case> filterRecords(String aField, String aSearch, Datetime aDataInicio, Datetime aDataFim)
    {
        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        List<User> lLstUser = [SELECT Id, EX3_Pertence__c FROM User WHERE Id = :UserInfo.getUserId()];
        String lPertence = lLstUser[0].EX3_Pertence__c;

        Date lDataInicio = (aField == 'EX3_Data_de_Entrada__c') ? aDataInicio.dateGMT() : null;
        Date lDataFim = (aField == 'EX3_Data_de_Entrada__c') ? aDataFim.dateGMT() : null;

        Set<String> lSetMotivoPolo = getMotivosPolo();
        lSetMotivoPolo.add('42');
        lSetMotivoPolo.add('Finalizado');

        String lQuery = 'SELECT Status, '+ String.escapeSingleQuotes(returnFields()) + ' FROM Case ';

        String lWhereClause =  ' WHERE RecordTypeId = :lRTCaseRecepcao AND'
                            + ' EX3_Motivo_da_Situacao__c = :lSetMotivoPolo AND'
                            + ' EX3_Canal_de_Entrada__c = :lPertence'; 


        if(aField != 'EX3_Data_de_Entrada__c') 
        {
            lWhereClause += ' AND ' +String.escapeSingleQuotes(aField)+ ' LIKE  ' +'\'%'+String.escapeSingleQuotes(aSearch)+'%\'' + ' LIMIT 2000';
        }
        else
        {
            lWhereClause +=  ' AND DAY_ONLY('+String.escapeSingleQuotes(aField) +') >= :lDataInicio';
            lWhereClause += ' AND DAY_ONLY('+String.escapeSingleQuotes(aField) +') <= :lDataFim LIMIT 2000';
        }
        String lEscapeQuery = String.escapeSingleQuotes(lQuery) + lWhereClause;
        List<SObject> lLstCasesFiltered = (List<SObject>) database.query(lEscapeQuery);

        return lLstCasesFiltered;
    }

    private static Set<String> getMotivosPolo()
    {
        Set<String> lSetMotivoPolo = new Set<String>
        {
            '28','Aberto sem documento', 
            '33','Devolvido'
        };
        return lSetMotivoPolo;
    }


    private static String returnFields()
    {
        Set<String> lSetField = new Set<String>
        {
            'Id', 'RecordtypeId',
            'EX3_Numero_do_Protocolo__c', 'EX3_Etiqueta_Z__c',
            'EX3_Numero_do_Processo__c', 'EX3_Prioridade__c', 
            'toLabel(EX3_Motivo_da_Situacao__c)', 'EX3_Data_de_Entrada__c'
        };

        String soqlQueryFields = '';

        for (String iField : lSetField) { soqlQueryFields += iField + ','; }

        return soqlQueryFields.removeEnd(',');
    }

    private class AttributesTypes
    {
        private Object label {get; set;}
        private Object value {get; set;}
    } 
}