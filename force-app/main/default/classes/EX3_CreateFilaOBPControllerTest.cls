/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de Teste EX3_CreateFilaOBPControllerTest
* Empresa: everis do Brasil
* Autor: Sem Autor
*
********************************************************************************/
@isTest
public class EX3_CreateFilaOBPControllerTest {
    
    
    @TestSetup
    private static void testCreateData(){
        
        User lUser = EX3_BI_DataLoad.getUser();
        
        Database.insert(lUser);
        
        Case lCase = EX3_BI_DataLoad.getCase();
        
        Database.insert(lCase);
        
        EX3_ADJ__c lADJ = EX3_BI_DataLoad.getADJ(lCase);
        lADJ.EX3_Caso__c = lCase.Id;
        
        Database.insert(lADJ);
    }
    
    @isTest static void testredirectToObjectOBP(){
        
        User lUser = [SELECT Id FROM User LIMIT 1];
                
        Case lCase = [SELECT Id FROM Case LIMIT 1];
                
        EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];
        
        Test.startTest();
        	String lReturnENC = EX3_CreateFilaOBPController.redirectToObjectENC(lADJ.Id);
        	String lReturnOBP = EX3_CreateFilaOBPController.redirectToObjectOBP(lADJ.Id);
            String lReturnOBF = EX3_CreateFilaOBPController.redirectToObjectOBF(lADJ.Id);
            String lReturnFIM = EX3_CreateFilaOBPController.redirectToObjectFIM(lADJ.Id);     
        Test.stopTest();
        System.assert(lReturnOBP != null);
    }
}