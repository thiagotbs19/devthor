/*********************************************************************************
*                                    Itaú - 2020 
*
* Classe que exibe os registros de Tipo de Documentos.
*
*Apex Class: EX3_TypeDocController
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/



public with sharing class EX3_TypeDocController {
    


    @AuraEnabled
    public static List<EX3_Tipo_Documento__c> getListTypeDocPreEstrategia(String aRecordId){
        
        List<EX3_Tipo_Documento__c> lLstTypeDocPreEst;
        try{
        if(aRecordId == null){ return new List<EX3_Tipo_Documento__c>();} 

        lLstTypeDocPreEst = [SELECT Id, Name, OwnerId, Owner.Name, EX3_Checklist__c, EX3_Documento__c, EX3_Status_do_Documento__c  FROM EX3_Tipo_Documento__c WHERE 
                                    EX3_Checklist__c IN ('Pré Estratégia') AND EX3_Subsidios__c =: aRecordId];
        if(lLstTypeDocPreEst.isEmpty() || lLstTypeDocPreEst == null){ return new List<EX3_Tipo_Documento__c>();}
        }catch (Exception ex)     
      { 
         SA7_CustomdebugLog.logError('EX3', 'EX3_TypeDocController', 'getListTypeDocPreEstrategia', 'Erro ao exibir os dados de pré estrategia '+ ex.getMessage(), ex, null);
      }
      return lLstTypeDocPreEst; 
    }

    @AuraEnabled 
    public static List<EX3_Tipo_Documento__c> getListTypeDocCompl(String aRecordId){
    
        List<EX3_Tipo_Documento__c> lLstTypeDocComp; 
        try{  
        lLstTypeDocComp = [SELECT Id, Name, OwnerId, Owner.Name,  EX3_Checklist__c, EX3_Documento__c, EX3_Status_do_Documento__c  FROM EX3_Tipo_Documento__c WHERE 
                                    EX3_Checklist__c IN ('Complementar') AND EX3_Subsidios__c =: aRecordId];
        
        }
        catch (Exception ex) 
      { 
         SA7_CustomdebugLog.logError('EX3', 'EX3_TypeDocController', 'getListTypeDoc', 'Erro ao exibir os dados de checklist complementar '+ ex.getMessage(), ex, null);
      }
        return lLstTypeDocComp;  
    } 
   
}