/*********************************************************************************
*                                    Itaú - 2020
*
* Classe TriggerHandler do objeto Case   
* 
* Autor: Thiago Barbosa de Souza
  Company: Everis do Brasil
*
********************************************************************************/
public with sharing class CaseTriggerHandler implements ITrigger 
{
	public void bulkBefore(){}
    public void bulkAfter(){}
    public void beforeInsert(){
        EX3_BloqueiaCaseOwner.execute();
    }  
    public void beforeUpdate(){ 
        EX3_BloqueiaCaseOwner.execute();
    }
    public void beforeDelete(){}
    
    public void afterInsert()
    {
    }
    
    public void afterUpdate(){} 
    public void afterDelete(){}
    public void andFinally(){}
}