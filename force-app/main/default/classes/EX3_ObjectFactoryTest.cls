@isTest
private class EX3_ObjectFactoryTest {
    
    @isTest static void IncluirProtocolo()
    {
        test.startTest();
            EX3_ObjectFactory.IncluirProtocolo lIncluirProt = new EX3_ObjectFactory.IncluirProtocolo();        
            lIncluirProt.codigo_protocolo_externo = '';
            lIncluirProt.codigo_unico_processo = '';
            lIncluirProt.data_protocolo_documento = System.now();
            lIncluirProt.codigo_canal_entrada_documento = Decimal.valueOf('10');
            lIncluirProt.codigo_motivo_situacao = Decimal.valueOf('10');
            lIncluirProt.data_situacao = Date.today();
            lIncluirProt.operador = '';
            lIncluirProt.codigo_operacao = Decimal.valueOf('10');
            lIncluirProt.indicador_protocolo = Decimal.valueOf('10');
        test.stopTest();
    }
    
    @isTest static void AtualizarProtocolo()
    {
        test.startTest();
            EX3_ObjectFactory.AtualizarProtocolo lAtualizarProtocolo = new EX3_ObjectFactory.AtualizarProtocolo();          
        	lAtualizarProtocolo.id_protocolo = Decimal.valueOf('10');
            lAtualizarProtocolo.codigo_protocolo_externo = '';
            lAtualizarProtocolo.codigo_motivo_situacao = Decimal.valueOf('10');
            lAtualizarProtocolo.operador = '';
            lAtualizarProtocolo.codigo_operacao = Decimal.valueOf('10');
            lAtualizarProtocolo.indicador_protocolo = Decimal.valueOf('10');        
        test.stopTest();
    }   
    
    @isTest static void IncluirDocumento()
    {
        test.startTest();
        	EX3_ObjectFactory.IncluirDocumento lIncluirDocumento = new EX3_ObjectFactory.IncluirDocumento();
        	lIncluirDocumento.nome_documento = '';
            lIncluirDocumento.codigo_documento_plataforma_externa = '';
            lIncluirDocumento.operador_conglomerado = '';
        test.stopTest();
    }
    
    @isTest static void DadosArquivos()
    {
        test.startTest();
        	EX3_ObjectFactory.DadosArquivos lDadosArquivos = new EX3_ObjectFactory.DadosArquivos();
            lDadosArquivos.id_arquivo = '';
            lDadosArquivos.tipo_arquivo = '';
            lDadosArquivos.tamanho_arquivo = '';
        test.stopTest();        
    }

    @isTest static void RequestEnvioArquivo()
    {
        test.startTest();
        	EX3_ObjectFactory.RequestEnvioArquivo lRequestEnvioArquivo = new EX3_ObjectFactory.RequestEnvioArquivo();
            lRequestEnvioArquivo.origem = '';
            lRequestEnvioArquivo.id_protocolo = '';
            lRequestEnvioArquivo.servico = '';
            lRequestEnvioArquivo.arquivos = new List<EX3_ObjectFactory.DadosArquivos>();        
        test.stopTest();        
    }

    @isTest static void RequestEnvioProtocolo()
    {
        test.startTest();
        	EX3_ObjectFactory.RequestEnvioProtocolo lRequestEnvioProtocolo = new EX3_ObjectFactory.RequestEnvioProtocolo();
            lRequestEnvioProtocolo.num_protocolo = '';
            lRequestEnvioProtocolo.status = '';
            lRequestEnvioProtocolo.estou_ciente = true;
        test.stopTest();        
    }

    @isTest static void RequestAlteracaoStatusProtocolo()
    {
        test.startTest();
        	EX3_ObjectFactory.RequestAlteracaoStatusProtocolo lRequestAlteracaoStatusProtocolo = new EX3_ObjectFactory.RequestAlteracaoStatusProtocolo();
            lRequestAlteracaoStatusProtocolo.id_protocolo = '';
            lRequestAlteracaoStatusProtocolo.motivo_situacao = '';
        test.stopTest();        
    }

    @isTest static void RequestCitacaoEletronicaBuscaDuplicidade()
    {
        test.startTest();
        	EX3_ObjectFactory.RequestCitacaoEletronicaBuscaDuplicidade lRequestCitacaoEletronicaBuscaDuplicidade = new EX3_ObjectFactory.RequestCitacaoEletronicaBuscaDuplicidade();
            lRequestCitacaoEletronicaBuscaDuplicidade.id_protocolo = '';
            lRequestCitacaoEletronicaBuscaDuplicidade.numero_processo = '';
        test.stopTest();        
    }

    @isTest static void RequestBuscaCartaPrecatoria()
    {
        test.startTest();
        	EX3_ObjectFactory.RequestBuscaCartaPrecatoria lRequestBuscaCartaPrecatoria = new EX3_ObjectFactory.RequestBuscaCartaPrecatoria();
            lRequestBuscaCartaPrecatoria.nome = '';
            lRequestBuscaCartaPrecatoria.numero_processo = '';
            lRequestBuscaCartaPrecatoria.cpf = '';
            lRequestBuscaCartaPrecatoria.cnpj = '';
            lRequestBuscaCartaPrecatoria.id_protocolo = '';
            lRequestBuscaCartaPrecatoria.get('nome');
        
        	Map<String, Object> lResponseMap = lRequestBuscaCartaPrecatoria.getMap();
            Map<String,String> lResponseMap2 = lRequestBuscaCartaPrecatoria.getMapBySalesforceApi('EX3_Autor__c');
            String lgetIntegrationApi = lRequestBuscaCartaPrecatoria.getIntegrationApi('EX3_Autor__c');
        
        test.stopTest();        
    }
    
    @isTest static void RequestLiminar()
    {
        test.startTest();
        	EX3_ObjectFactory.RequestLiminar lRequestLiminar = new EX3_ObjectFactory.RequestLiminar();
            lRequestLiminar.numero_processo = '';
            lRequestLiminar.id_protocolo = '';
        test.stopTest();        
    }

    @isTest static void RequestBuscaDuplicidade()
    {
        test.startTest();
        	EX3_ObjectFactory.RequestBuscaDuplicidade lRequestBuscaDuplicidade = new EX3_ObjectFactory.RequestBuscaDuplicidade();
            lRequestBuscaDuplicidade.numero_processo = '';
            lRequestBuscaDuplicidade.id_protocolo = '';
        
            lRequestBuscaDuplicidade.get('nome');
        	Map<String, Object> lResponseDupli = lRequestBuscaDuplicidade.getMap();
            Map<String,String> lResponseDupli2 = lRequestBuscaDuplicidade.getMapBySalesforceApi('EX3_Numero_do_Processo__c');
            String lgetIntegrationApiDupli = lRequestBuscaDuplicidade.getIntegrationApi('EX3_Numero_do_Processo__c');
        
        test.stopTest();        
    }
}