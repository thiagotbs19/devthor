@isTest
private class EX3_ObjectFactoryResponseTest {
    
    @isTest static void ResponseBuscaCartaPrecatoria()
    {
        Test.startTest();
        	EX3_ObjectFactoryResponse.ResponseBuscaCartaPrecatoria lCarta = new EX3_ObjectFactoryResponse.ResponseBuscaCartaPrecatoria();
        	lCarta.nome = 'Testando';
            lCarta.numero_processo = '1231233';
            lCarta.cpf = '1231233';
            lCarta.cnpj = '1231233';
            lCarta.pasta = '1231233';
            lCarta.macro_carteira = '1231233';
        	
        	lCarta.get('nome');
        	Map<String,Object> lResp = lCarta.getMap();
        	lCarta.getMapSalesforceAPiByIntegrationAPI('nome');
        Test.stopTest();
    }
    
    @isTest static void ResponseBuscaLiminar()
    {
        Test.startTest();
        	EX3_ObjectFactoryResponse.ResponseBuscaLiminar lBucLiminar = new EX3_ObjectFactoryResponse.ResponseBuscaLiminar();
        	lBucLiminar.nome = 'Testando';
            lBucLiminar.numero_processo = '1231233';
            lBucLiminar.cpf = '1231233';
            lBucLiminar.cnpj = '1231233';
            lBucLiminar.pasta = '1231233';
            lBucLiminar.macro_carteira = '1231233';
        	lBucLiminar.vara = 'test';
        	
        	lBucLiminar.get('nome');
        	Map<String,Object> lResp = lBucLiminar.getMap();
        	lBucLiminar.getMapSalesforceAPiByIntegrationAPI('nome');
        Test.stopTest();
    }
    
    @isTest static void ResponseBuscaDuplicidade()
    {
        Test.startTest();
        	EX3_ObjectFactoryResponse.ResponseBuscaDuplicidade lBucDup = new EX3_ObjectFactoryResponse.ResponseBuscaDuplicidade();
        	lBucDup.autor = 'Testando';
            lBucDup.numero_processo = '1231233';
            lBucDup.pasta = '1231233';
            lBucDup.macro_carteira = '1231233';
        	
        	lBucDup.get('autor');
        	Map<String,Object> lResp = lBucDup.getMap();
        	lBucDup.getMapSalesforceAPiByIntegrationAPI('autor');
        Test.stopTest();
    }
    
    

}