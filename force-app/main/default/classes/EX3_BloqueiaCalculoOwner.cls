/*********************************************************************************
*                                    Itaú - 2020  
*
* Classe Responsável para que o analista não consiga puxar um registro para iniciar
* um atendimento se o mesmo ja possuir um registro de Cálculo. 
*
*Object : EX3_Calculo__c
*Apex Class: EX3_BloqueiaCalculoOwner
*  

* Empresa: everis do Brasil
* Autor: Isabella Tannús 
*
********************************************************************************/

public without sharing class EX3_BloqueiaCalculoOwner {
    
    Private static Schema.SObjectType TipoGroup = Schema.getGlobalDescribe().get('Group');
    
    public static void execute() 
    {
        Set<Id> lSetOwnerCalculo = new Set<Id>();
        List<EX3_Calculo__c> lLstEX3Calculo = new List<EX3_Calculo__c>();
        for(EX3_Calculo__c iCalculo : (List<EX3_Calculo__c>) Trigger.new)
        {
            if(!EX3_TriggerHelper.changedField(iCalculo, 'OwnerId') || iCalculo.ownerId.getSObjectType() == TipoGroup) { continue; }
            lSetOwnerCalculo.add(iCalculo.OwnerId);
            lLstEX3Calculo.add(iCalculo); 
        }
        if(lSetOwnerCalculo == null || lSetOwnerCalculo.isEmpty()) { return ;}
        calculoOwnerError(lSetOwnerCalculo, lLstEX3Calculo);
    }
    
    private static void calculoOwnerError(Set<Id> aSetOwnerCalculo, List<EX3_Calculo__c> aLstEX3Calculo){

        if(aSetOwnerCalculo == null || aSetOwnerCalculo.isEmpty() || aLstEX3Calculo == null || aLstEX3Calculo.isEmpty()) { return; } 
        
        List<User> lLstUserProfile = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() AND Profile.Name NOT IN('System Administrator', 'Administrador do sistema')];
      	if (lLstUserProfile.isEmpty()) { return; }
        
        Map<String, String> lMapProtocolos = new Map<String, String>(); 
        for(EX3_Calculo__c iCalculo : [SELECT Id, Name, OwnerId FROM EX3_Calculo__c WHERE OwnerId =: aSetOwnerCalculo AND EX3_Status_do_calculo__c NOT IN ('Cálculo Finalizado')])
        { lMapProtocolos.put(iCalculo.OwnerId, iCalculo.Name);}

        if(lMapProtocolos == null || lMapProtocolos.isEmpty()) { return; }      
        
        for(EX3_Calculo__c iCalculo : aLstEX3Calculo)
        { 
            if (!lMapProtocolos.containsKey(iCalculo.OwnerId)) { continue; }
            iCalculo.adderror(Label.EX3_Pasta_Numero + lMapProtocolos.get(iCalculo.OwnerId) + ' ' + Label.EX3_Tratativa); 
        }        
    }
}