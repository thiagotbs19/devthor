/*********************************************************************************
*                                    Itaú - 2019
*
* Classe resposavel por disponibilizar atributos
*
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
public with sharing class EX3_WrapperDocument {

    @AuraEnabled
    public String name {get; set;}

    @AuraEnabled
    public String documentId {get; set;}

}