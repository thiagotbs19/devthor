/*********************************************************************************
*                                    Itaú - 2020
*
* Classe TriggerHandler do objeto Task  
* 
* Autor: Thiago Barbosa de Souza
  Company: Everis do Brasil
*
********************************************************************************/

public with sharing class TaskTriggerHandler implements ITrigger {  
    
    public void bulkBefore(){}     
    public void bulkAfter(){}
    public void beforeInsert(){  
    } 
    public void beforeUpdate(){ 
        EX3_BloqueiaTaskOwner.execute();
    } 
    public void beforeDelete(){}
    
    public void afterInsert(){} 
    
    public void afterUpdate(){} 
    public void afterDelete(){}
    public void andFinally(){}


}