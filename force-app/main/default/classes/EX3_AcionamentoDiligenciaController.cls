/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por acionar a diligencia
* AuraComponente: EX3_AcionamentoDiligencia
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
public with sharing class EX3_AcionamentoDiligenciaController
{
    @AuraEnabled
    public static String getProtocoloValues(String aCaseRecordId)
    {
        Case lCase = [SELECT Id, EX3_Numero_do_Protocolo__c, EX3_Numero_do_Processo__c, EX3_Orgao_Legal__c, Status,
                                      EX3_Escritorio_Credenciado__c, EX3_Data_solicitacao__c, EX3_Data_de_envio__c, EX3_Data_cancelamento__c,
                                      EX3_Data_descontratacao__c, EX3_Solicitante__c, EX3_Envolvido__c, EX3_Reu__c, EX3_Municipio__c,
                                      EX3_UF__c, EX3_Audiencia__c, EX3_Data_da_audiencia__c, 
                                      EX3_Hora_da_audiencia__c, EX3_Prazo__c, EX3_Prazo_dias__c, EX3_Processo_Eletronico__c, EX3_Macro_Carteira__c,
                                      EX3_Observacao__c, EX3_Data_de_Entrada__c, EX3_Tipo_de_Canal_de_Entrada__c, EX3_Canal_de_Entrada__c, EX3_Autor__c,
                                      EX3_Comarca__c, EX3_Empresa_re__c FROM Case WHERE Id = :aCaseRecordId];
        
        User lUser = [SELECT Id, Name,FuncionalColaborador__c FROM User WHERE Id = :UserInfo.getUserId()];
        
        lCase.EX3_Solicitante__c = 'Nome: ' + lUser.name + '\n ' + 'Funcional: ' + lUser.FuncionalColaborador__c + '\n ' + 'Etapa: Recepção';
        lCase.EX3_Data_solicitacao__c = Date.today(); 
        
        Set<String> lSetCamposDesabilitados = getDisabledFields();

        List<AttributesTypes> lLstAttributesTypes = new List<AttributesTypes>();
        for (Schema.FieldSetMember iFieldSetMember : EX3_Utils.getFieldSetMembersByFieldSetDeveloperName('Case','EX3_Acionamento_de_Diligencia'))
        {
            AttributesTypes lAttributesTypes = new AttributesTypes();
            lAttributesTypes.label = iFieldSetMember.getLabel();
            lAttributesTypes.api = iFieldSetMember.getFieldPath();
            lAttributesTypes.fieldType = iFieldSetMember.getType();
            lAttributesTypes.value = lCase.get(iFieldSetMember.getFieldPath());
            lAttributesTypes.isDisable = lSetCamposDesabilitados.contains(iFieldSetMember.getFieldPath());

            if (iFieldSetMember.getType() == Schema.DisplayType.PICKLIST)
            {
                List<AttributesTypesPicklist> lLstAttributesTypesPicklist = new List<AttributesTypesPicklist>();
                for (Schema.PicklistEntry iPickList : EX3_Utils.getPicklistValuesByFieldAPI('Case', iFieldSetMember.getFieldPath()))
                {
                    AttributesTypesPicklist lAttributesTypesPicklist = new AttributesTypesPicklist();
                    lAttributesTypesPicklist.label = iPickList.getLabel();
                    lAttributesTypesPicklist.value = iPickList.getValue();
                    lLstAttributesTypesPicklist.add(lAttributesTypesPicklist);
                }
                lAttributesTypes.picklistValues = lLstAttributesTypesPicklist;
            }
            lLstAttributesTypes.add(lAttributesTypes);
        }
       return JSON.serialize(lLstAttributesTypes);
    }

    @AuraEnabled
    public static String updateProtocolo(String aFieldList)
    {
        if (String.isBlank(aFieldList)) { return 'error'; }
        Set<String> lSetCamposDesabilitados = getDisabledFields();

        List<AttributesTypesReturn> lLstAttributesTypes = (List<AttributesTypesReturn>) JSON.deserialize(aFieldList, List<AttributesTypesReturn>.class);

        Case lCase = new Case();
        for (AttributesTypesReturn iAttributesTypes : lLstAttributesTypes)
        {
            if (iAttributesTypes.value == null || lSetCamposDesabilitados.contains(String.valueOf(iAttributesTypes.api))) { continue; }

            lCase.put(String.valueOf(iAttributesTypes.api), getFieldType(iAttributesTypes));
        }
        if (lCase.EX3_Situacao__c == '21' && lCase.EX3_Motivo_da_Situacao__c == '14') { return 'Esse protocolo já está em diligência!'; }
        try
        {
            lCase.EX3_Situacao__c = '21';
            lCase.EX3_Motivo_Situacao__c = '14';
            database.update(lCase);
        }
        catch (DmlException ex)
        {
            SA7_CustomdebugLog.logError('EX3', 'EX3_AcionamentoDiligenciaController', 'updateProtocolo', 'Erro ao atualizar' +
                            'registro do Protocolo', ex, null);
        }

        return 'success';
    }

    private static Object getFieldType(AttributesTypesReturn aAttributesTypes)
    {
        String lFieldType = aAttributesTypes.fieldType;
        Object lValue;

        if (lFieldType == 'BOOLEAN') { lValue = Boolean.valueOf(aAttributesTypes.value); }
        else if (lFieldType == 'DATETIME')
        {
            lValue = (DateTime)JSON.deserialize('"' + aAttributesTypes.value + '"', DateTime.class);
        }
        else if (lFieldType == 'DATE') { lValue = Date.valueOf(aAttributesTypes.value); }
        else if (lFieldType == 'DOUBLE') { lValue = Integer.valueOf(aAttributesTypes.value); }
        else if (lFieldType == 'INTEGER') { lValue = Double.valueOf(aAttributesTypes.value); }
        else { lValue = aAttributesTypes.value; }
        return lValue;
    }

    private static Set<String> getDisabledFields()
    {
        Set<String> lSetCamposDesabilitados = new Set<String>();
        for (Schema.FieldSetMember iFieldSetMember : EX3_Utils.getFieldSetMembersByFieldSetDeveloperName('Case','EX3_Acionamento_de_Diligencia_Inativos'))
        {
            lSetCamposDesabilitados.add(iFieldSetMember.getFieldPath());
        }
        return lSetCamposDesabilitados;
    }

    public class AttributesTypesPicklist
    {
        public String label {get;set;}
        public String value {get;set;}
    }

    public class AttributesTypes
    {
        public Object label {get; set;}
        public Object value {get; set;}
        public Object api {get;set;}
        public Object fieldType {get;set;}
        public boolean isDisable {get;set;}
        public List<AttributesTypesPicklist> picklistValues {get;set;}
    }

    private class AttributesTypesReturn 
    {
        private String label {get; set;}
        private String value {get; set;}
        private String api {get;set;}
        private String fieldType {get;set;}
        private boolean isDisable {get;set;}
        private List<AttributesTypesPicklist> picklistValues {get;set;}
    }
}