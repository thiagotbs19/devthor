/**
 * @author Thiago Ribeiro Silva
 * Funcionalidade: Tem como objetivo deletar o registro informando o ID para a função DelReg.
 * Data: 09/04/2020
 **/

public with sharing class EX3_RecordPageController {
    
        @AuraEnabled
   		 public static void DelReg (Id idNumber){
             
                 try {

                    SObject so = idNumber.getSObjectType().newSObject(idNumber);
                    delete so;              

                     
                 } catch (DmlException e) {          
                    
                 }
               }
	     
    

}