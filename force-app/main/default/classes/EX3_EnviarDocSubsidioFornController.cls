/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável por mudar o Owner(Proprietário) do Tipo de Documento 
*
*Apex Class: EX3_EnviarDocSubsidioFornController
*  
* 
* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

public with sharing class EX3_EnviarDocSubsidioFornController {
    
    @AuraEnabled
    public static String mudarProprietarioRegistro(String aRecordId){
        Exception lEx;
        if(aRecordId == null){return '';}
        try{
            
            
            EX3_Tipo_Documento__c lTipoDocumento  = [SELECT Id, OwnerId,
                                                     EX3_Subsidios__c  
                                                     FROM EX3_Tipo_Documento__c     
                                                     WHERE Id =:aRecordId];
            
            lTipoDocumento.OwnerId = Label.EX3_Fornecedor; 
            if(lTipoDocumento != null){
                Database.update(lTipoDocumento);
            }
            
        }catch (Exception ex)  
        { 
            SA7_CustomdebugLog.logError('EX3', 'EX3_EnviarDocSubsidioFornController', 'mudarProprietarioRegistro', 'Erro ao alterar o Proprietário '+ ex.getMessage(), ex, null);
        }
        return aRecordId;   
    } 
    
}