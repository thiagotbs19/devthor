/*********************************************************************************
*                                    Itaú - 2020
*
* Classe responsavel por criar o mock a classe EX3_IntegracaoAndamentosRestTest
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@IsTest
global with sharing class EX3_IntegracaoAndamentosRestMock implements HttpCalloutMock{
    global HTTPResponse respond(HttpRequest request)
    {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setStatusCode(200);
        return response;
    }
}