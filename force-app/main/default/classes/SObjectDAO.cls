/*********************************************************************************
*                                    Itaú - 2019
*
* Classe que define métodos de operações DML comuns a todos objetos Salesforce.
* 
* Autor: Victor Melhem Deoud Lemos 
*
********************************************************************************/
public abstract class SObjectDAO {

    /**
    * Insere um objeto na base de dados.
    * @param sObj O objeto a ser inserido. Este objeto é qualquer tipo filho de SObject, sendo possível passar um objeto padrão ou customizado.
    * @throws DMLException Se a inserção do objeto falhar.
    */
    public void inserir(SObject sObj)
    {
       Database.insert(sObj);
    }
    
    /**
    * Altera um objeto na base de dados.
    * @param sObj O objeto a ser alterado. Este objeto é qualquer tipo filho de SObject, sendo possível passar um objeto padrão ou customizado.
    * @throws DMLException Se a inserção do objeto falhar.
    */
    public void alterar(SObject sObj)
    {
       Database.update(sObj);
    }
    
}