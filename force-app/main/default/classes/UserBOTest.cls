/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de teste das classes UserBO - UserDAO - SObjectDAO
* 
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
@isTest
private class UserBOTest {
    
   private static final String FUNCIONAL_USUARIO = 'F1234TEST';
    
   @testSetup
    static void setup() {
			
        User lUserLogado = UserBO.getInstance().getUsuarioLogado();
        
        Test.startTest();
        	User usuario = new User();
            usuario.FuncionalColaborador__c = FUNCIONAL_USUARIO; 
            usuario.FirstName = lUserLogado.FirstName;
            usuario.Username = 'usuario.classeteste@teste.com.br';
            usuario.LastName = lUserLogado.LastName;
            usuario.Email = 'usuarioclasseteste@teste.com.br';
            usuario.Alias = 'uTeste';
            usuario.CommunityNickname = 'teste';
            usuario.TimeZoneSidKey = 'America/Sao_Paulo';
            usuario.LocaleSidKey = 'pt_BR';
            usuario.EmailEncodingKey = 'UTF-8';
            usuario.LanguageLocaleKey = 'PT_BR';
        	usuario.ProfileId =     Userinfo.getProfileId();
            usuario.IsActive = true;
        	UserBO.getInstance().inserir(usuario);
        Test.stopTest();
    }
    
    @isTest
    static void testFuncional() 
    {
		Test.startTest();
        	User lUser = UserBO.getInstance().buscarPorFuncional(FUNCIONAL_USUARIO);
        	UserBO.getInstance().alterar(lUser);
        Test.stopTest();
    }

    
}