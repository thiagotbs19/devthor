/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de teste para garantir assertividade do funcionamento do componente de Upload de arquivos
*
* Aura Component Bundle: EX3_UploadFiles
* Apex Class: EX3_UploadFilesController
* Empresa: everis
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/

@isTest 
public with sharing class EX3_UploadFilesControllerTest 
{   
    @isTest static void getDocumentsTest()  
    {        
        ContentVersion lNewContentVersion = new ContentVersion(title = 'title test',
                                                               PathOnClient = 'PathOnClient test');
        Blob b = Blob.valueOf('This is version data');
        lNewContentVersion.VersionData = Encodingutil.base64Decode('This is version data');
        lNewContentVersion.Tipo_de_Documento__c = '1';
        insert lNewContentVersion;
        
        ContentVersion lContentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion
                                          WHERE Id =: lNewContentVersion.Id];
        
        String lCentralizadorToString = EX3_UploadFilesController.getCentralizador();
        EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralizadorToString,EX3_Centralizador__c.class);
        
        ContentDocumentLink lNewContentDocumentLink = new ContentDocumentLink(LinkedEntityId = lCentral.Id,
                                                                              ContentDocumentId = lContentVersion.ContentDocumentId,
                                                                              Visibility = 'AllUsers',
                                                                              ShareType = 'V');
        insert lNewContentDocumentLink;
        
        test.startTest();
        String getDocuments = EX3_UploadFilesController.getDocuments(lCentralizadorToString);
        test.stopTest();
        
        Map<String,Object> lMapToReturnValues = (Map<String,Object>) JSON.deserializeUntyped(getDocuments);
        system.assert(lMapToReturnValues.size()>0, 'Não há documentos');  

    }
    
    @isTest static void criarProtocoloTest()
    {           
        ContentVersion lNewContentVersion = new ContentVersion(title = 'title test',
                                                               PathOnClient = 'PathOnClient test');
        Blob b = Blob.valueOf('This is version data');
        lNewContentVersion.VersionData = Encodingutil.base64Decode('This is version data');
        lNewContentVersion.Tipo_de_Documento__c = '1';
        insert lNewContentVersion;
        
        ContentVersion lContentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion
                                          WHERE Id =: lNewContentVersion.Id]; 
        
        String lCentralizadorToString = EX3_UploadFilesController.getCentralizador();
        EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralizadorToString,EX3_Centralizador__c.class);
        
        ContentDocumentLink lNewContentDocumentLink = new ContentDocumentLink(LinkedEntityId = lCentral.Id,
                                                                              ContentDocumentId = lContentVersion.ContentDocumentId,
                                                                              Visibility = 'AllUsers',
                                                                              ShareType = 'V');
        insert lNewContentDocumentLink; 
        
        User lUser = EX3_DataFactory.getAdmUser();
        lUser.FuncionalColaborador__c = 'funcional';
        lUser.EX3_Pertence__c = '3';
        //database.insert(lUser);
        String lgetProtocolo;
        System.runAs(lUser) 
        {
            Test.startTest();  
                lgetProtocolo = EX3_UploadFilesController.criarProtocolo('123123', lCentralizadorToString);
            Test.stopTest();     
        }
        System.assert(lgetProtocolo != null, 'O Caso foi criado');  
    }
    
    @isTest static void enviarCaseTest() 
    {        
        Case lCase = EX3_DataFactory.getCase('20');
        lCase.EX3_Canal_de_Entrada__c = '9';
		lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
        lCase.EX3_Motivo_Situacao__c = '28';
        database.insert(lCase);
        String lCaseToString = JSON.serialize(lCase);  
        
        ContentVersion lNewContentVersion = new ContentVersion(title = 'title test',
                                                               PathOnClient = 'PathOnClient test');
        Blob b = Blob.valueOf('This is version data');
        lNewContentVersion.VersionData = Encodingutil.base64Decode('This is version data');
        lNewContentVersion.Tipo_de_Documento__c = '1';
        insert lNewContentVersion;
        
        ContentVersion lContentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion
                                          WHERE Id =: lNewContentVersion.Id];
        
        String lCentralizadorToString = EX3_UploadFilesController.getCentralizador();
        EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(lCentralizadorToString,EX3_Centralizador__c.class);
        
        ContentDocumentLink lNewContentDocumentLink = new ContentDocumentLink(LinkedEntityId = lCase.Id,
                                                                              ContentDocumentId = lContentVersion.ContentDocumentId,
                                                                              Visibility = 'AllUsers',
                                                                              ShareType = 'V');
        insert lNewContentDocumentLink; 
        
        User lUser = EX3_DataFactory.getAdmUser();
        lUser.FuncionalColaborador__c = 'funcional';
        database.insert(lUser);
        
        EX3_ObjectFactory.IncluirProtocolo lIncluirProtocolo = new EX3_ObjectFactory.IncluirProtocolo();
        lIncluirProtocolo.codigo_protocolo_externo = lCase.Id;
        lIncluirProtocolo.codigo_unico_processo = lCase.EX3_Numero_do_Processo__c;
        lIncluirProtocolo.data_protocolo_documento = lCase.CreatedDate;
        lIncluirProtocolo.codigo_canal_entrada_documento = Decimal.valueOf(lCase.EX3_Canal_de_Entrada__c);
        lIncluirProtocolo.codigo_motivo_situacao = Decimal.valueOf(lCase.EX3_Motivo_Situacao__c);
        lIncluirProtocolo.data_situacao = system.today(); 
        lIncluirProtocolo.operador = lUser.FuncionalColaborador__c;
        lIncluirProtocolo.codigo_operacao = 3;
        lIncluirProtocolo.indicador_protocolo = 0;
        String lProtocoloBody = JSON.serialize(lIncluirProtocolo);
         
        Map<String, String> lMapToSendParams = new Map<String, String>();
        lMapToSendParams.put('classe', 'EX3_UploadFilesController');
        lMapToSendParams.put('callback', 'enviarProtocoloCallback');
        lMapToSendParams.put('protocolo', lProtocoloBody);
   
        String lMapToSendParamsToString = JSON.serialize(lMapToSendParams);
        
        List<EX3_Parametrizacao__c> lLstToken = new List<EX3_Parametrizacao__c>();
        EX3_Parametrizacao__c lParametrizacao = new EX3_Parametrizacao__c();
        lParametrizacao.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token');
        lParametrizacao.EX3_Access_token__c = '12345';
        lParametrizacao.EX3_Token_external__c = 'TokenSTS';
        lLstToken.add(lParametrizacao);
        insert lLstToken; 

        test.startTest(); 
        EX3_IncluirCaseMock lMockSucesso = new EX3_IncluirCaseMock(lIncluirProtocolo, 200);

        Continuation lContinuationSuccess = EX3_UploadFilesController.enviarProtocolo(lMapToSendParamsToString);
        Map<String, HttpRequest> lRequestSucess = lContinuationSuccess.getRequests();
        List<String> lLstRequestLabelSucess = new List<String>();
        for (String iLabel : lRequestSucess.keySet()){
            lLstRequestLabelSucess.add(iLabel);
        } 
        Map<String,String> dados = new Map<String,String>();  
        Map<String,String> lRequisicao = new Map<String,String>{'incluirProtocolo' => lLstRequestLabelSucess[0]};
        Object lState = new EX3_ContinuationUtils.StateInfo(lLstRequestLabelSucess[0],lRequisicao, dados, 'EX3_UploadFilesControllerTest');
        HTTPResponse lResponseFromMock = lMockSucesso.respond(lRequestSucess.get('Continuation'));
        Test.setContinuationResponse(lLstRequestLabelSucess[0], lResponseFromMock);
         
        EX3_IncluirCaseMock lMockFalha = new EX3_IncluirCaseMock(lIncluirProtocolo, 400);
        
        Continuation lContinuationFalha = EX3_UploadFilesController.enviarProtocolo(lMapToSendParamsToString);
        Map<String, HttpRequest> lRequestFalha = lContinuationFalha.getRequests();
        List<String> lLstRequestLabelFalha = new List<String>();
        for (String iLabel : lRequestFalha.keySet()){
            lLstRequestLabelFalha.add(iLabel);
        } 
        Map<String,String> lRequisicaoFalha = new Map<String,String>{'incluirProtocolo' => lLstRequestLabelFalha[0]};
        Object lStateFalha = new EX3_ContinuationUtils.StateInfo(lLstRequestLabelFalha[0],lRequisicaoFalha, dados, 'EX3_UploadFilesControllerTest');
        HTTPResponse lResponseFromMockFalha = lMockFalha.respond(lRequestFalha.get('Continuation'));
        Test.setContinuationResponse(lLstRequestLabelFalha[0], lResponseFromMockFalha);

        
        test.stopTest(); 
        
        String lResult = (String) EX3_UploadFilesController.enviarProtocoloCallback(lState);
        Map<String,Object> lResponse = (Map<String,Object>)JSON.deserializeUntyped(lResult);  
        system.assert(!lRequestSucess.isEmpty(), 'Request Enviado com sucesso');
        Map<String,Object> lProtocoloFromCallback = (Map<String,Object>) JSON.deserializeUntyped((String) lResponse.get('protocolo'));
		Id idCase =(String) lProtocoloFromCallback.get('Id');       
        system.assert(IdCase == lCase.Id, 'O caso foi inserido'); 
 
        String lResultFalha = (String) EX3_UploadFilesController.enviarProtocoloCallback(lStateFalha);
        Map<String,Object> lResponseFalha = (Map<String,Object>)JSON.deserializeUntyped(lResultFalha);  
        system.assert(!lRequestFalha.isEmpty(), 'Chamada não foi enviada para o Protocolo');
                
    }
    
    @isTest static void enviarDocumentosTest() {
        
        ContentVersion lNewContentVersion = new ContentVersion(title = 'title test',
                                                               PathOnClient = 'PathOnClient test');
        Blob b = Blob.valueOf('This is version data');
        lNewContentVersion.VersionData = Encodingutil.base64Decode('This is version data');
        lNewContentVersion.Tipo_de_Documento__c = '1';
        insert lNewContentVersion;
        
        ContentVersion lContentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion
                                          WHERE Id =: lNewContentVersion.Id];
        
        EX3_Centralizador__c lCentralizador = EX3_BuscaAtivaController.getCentralizador();
        String lCentralizadorToString = json.serialize(lCentralizador); 
        
        ContentDocumentLink lNewContentDocumentLink = new ContentDocumentLink(LinkedEntityId = lCentralizador.Id,
                                                                              ContentDocumentId = lContentVersion.ContentDocumentId,
                                                                              Visibility = 'AllUsers',
                                                                              ShareType = 'V');
        insert lNewContentDocumentLink;
        
        EX3_ObjectFactory.IncluirDocumento lDocumento = new EX3_ObjectFactory.IncluirDocumento();
        lDocumento.nome_documento = lNewContentDocumentLink.ContentDocument.Title;
        lDocumento.codigo_documento_plataforma_externa = lNewContentDocumentLink.Id;
        lDocumento.operador_conglomerado = '111222333';
        List<EX3_ObjectFactory.IncluirDocumento> lLstIncluirDocumento = new List<EX3_ObjectFactory.IncluirDocumento>{lDocumento};
        
        String getDocuments = EX3_UploadFilesController.getDocuments(lCentralizadorToString);
        
        Case lCase = EX3_BI_DataLoad.getCase();   
        lCase.EX3_Canal_de_Entrada__c = '9'; 
		lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
        lCase.EX3_Numero_do_Protocolo__c = '12345678';
        database.insert(lCase);
        String lCaseToString = JSON.serialize(lCase); 
        
        Map<String, String> lMapToSendParams = new Map<String, String>();
        lMapToSendParams.put('classe', 'EX3_UploadFilesController');
        lMapToSendParams.put('documentos', JSON.serialize(lLstIncluirDocumento));
        lMapToSendParams.put('callback', 'enviarDocumentosCallback');
        lMapToSendParams.put('case', lCaseToString);   
   
        String lMapToSendParamsToString = JSON.serialize(lMapToSendParams);
        
        List<EX3_Parametrizacao__c> lLstToken = new List<EX3_Parametrizacao__c>();
        EX3_Parametrizacao__c lParametrizacao = new EX3_Parametrizacao__c();
        lParametrizacao.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token');
        lParametrizacao.EX3_Access_token__c = '12345';
        lParametrizacao.EX3_Token_external__c = 'TokenSTS';
        lLstToken.add(lParametrizacao);
        insert lLstToken; 
        
        
        test.startTest();
        
        EX3_IncluirDocumentoMock lMockSucesso = new EX3_IncluirDocumentoMock(lDocumento, 200);

        Continuation lContinuationSuccess = EX3_UploadFilesController.enviarDocumentos(lMapToSendParamsToString);
        Map<String, HttpRequest> lRequestSucess = lContinuationSuccess.getRequests();
        List<String> lLstRequestLabelSucess = new List<String>();
        for (String iLabel : lRequestSucess.keySet()){
            lLstRequestLabelSucess.add(iLabel);
        } 
        Map<String,String> dados = new Map<String,String>();  
        Map<String,String> lRequisicao = new Map<String,String>{'incluirDocumento' => lLstRequestLabelSucess[0]};
        Object lState = new EX3_ContinuationUtils.StateInfo(lLstRequestLabelSucess[0],lRequisicao, dados, 'EX3_UploadFilesControllerTest');
        HTTPResponse lResponseFromMock = lMockSucesso.respond(lRequestSucess.get('Continuation'));
        Test.setContinuationResponse(lLstRequestLabelSucess[0], lResponseFromMock);
        
        EX3_IncluirDocumentoMock lMockFalha = new EX3_IncluirDocumentoMock(lDocumento, 400);
        
        Continuation lContinuationFalha = EX3_UploadFilesController.enviarDocumentos(lMapToSendParamsToString);
        Map<String, HttpRequest> lRequestFalha = lContinuationFalha.getRequests();
        List<String> lLstRequestLabelFalha = new List<String>();
        for (String iLabel : lRequestFalha.keySet()){
            lLstRequestLabelFalha.add(iLabel);
        } 
        Map<String,String> lRequisicaoFalha = new Map<String,String>{'incluirDocumento' => lLstRequestLabelFalha[0]};
        Object lStateFalha = new EX3_ContinuationUtils.StateInfo(lLstRequestLabelFalha[0],lRequisicaoFalha, dados, 'EX3_UploadFilesControllerTest');
        HTTPResponse lResponseFromMockFalha = lMockFalha.respond(lRequestFalha.get('Continuation'));
        Test.setContinuationResponse(lLstRequestLabelFalha[0], lResponseFromMockFalha);

        
        test.stopTest(); 
        
        String lResult = (String) EX3_UploadFilesController.enviarDocumentosCallback(lState);
        Map<String,Object> lResponse = (Map<String,Object>)JSON.deserializeUntyped(lResult); 
        system.assert(!lRequestSucess.isEmpty(), 'Request não foi enviado'); 
  
        String lResultFalha = (String) EX3_UploadFilesController.enviarDocumentosCallback(lStateFalha);
        Map<String,Object> lResponseFalha = (Map<String,Object>)JSON.deserializeUntyped(lResultFalha);  
        system.assert(!lRequestFalha.isEmpty(), 'Houve falha na resposta do JSON');
                

    }
}