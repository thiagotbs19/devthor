/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste
*
*Apex Class: EX3_EnviarDocSubsidioFornControllerTest
*  
* 
* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/
@isTest
public class EX3_EnviarDocSubsidioFornControllerTest {
    
    
    @TestSetup
    public static void testmudarProprietarioRegistro(){
		
        User lUser = EX3_BI_DataLoad.getUser();
        
        Database.insert(lUser);
        
        Case lCase = EX3_BI_DataLoad.getCase();
        
        Database.insert(lCase);
        
        EX3_Subsidios__c lSubsidios = EX3_BI_DataLoad.getCapturaDocumentos(lCase);
        
        Database.insert(lSubsidios);
        
        EX3_Tipo_Documento__c lTipoDoc = EX3_BI_DataLoad.createDocument(lCase, lSubsidios);
        
        Database.insert(lTipoDoc);
    
    }
    
    @isTest static void testEnviarSubsidioForn(){
        
        
        User lUser = [SELECT Id FROM User LIMIT 1];
        
        Case lCase = [SELECT Id FROM Case];
        
        EX3_Subsidios__c lSubsidios = [SELECT Id FROM EX3_Subsidios__c LIMIT 1];
        
        EX3_Tipo_Documento__c lTipoDoc = [SELECT Id FROM EX3_Tipo_Documento__c LIMIT 1];
        
        Test.startTest();
        	String lReturn = EX3_EnviarDocSubsidioFornController.mudarProprietarioRegistro(lTipoDoc.Id);
        Test.stopTest();
        	System.assert(lReturn != null, 'O Proprietário do Tipo de Documento foi alterado');
        
    } 

}