/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável por mudar o Status quando o Owner for alterado. 
*
*Apex Trigger : EX3_Calculo__c 
*Apex Class: EX3_ChangeOwnerCalculo
*  
*
* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/



public without sharing class EX3_ChangeOwnerCalculo {
    
    
    public static final Set<Id> lSetQueueIds = EX3_Utils.getQueueIdByName(new List<String> { EX3_Utils.FILA_CALCULO });
    
    public static void execute(){
        
        List<EX3_Calculo__c> lLstCalculo = new List<EX3_Calculo__c>();
        EX3_Calculo__c lOldCalc;
        for(EX3_Calculo__c iCalculo :(List<EX3_Calculo__c>) Trigger.new){
            lOldCalc = (EX3_Calculo__c) Trigger.oldMap.get(iCalculo.Id);
            if(EX3_TriggerHelper.changedField(iCalculo, 'OwnerId')){
                lLstCalculo.add(iCalculo);   
            }
        }
        if(lLstCalculo.isEmpty() || lLstCalculo == null){ return ;}
        changeStatus(lLstCalculo, lOldCalc);
    }
    
    private static void changeStatus(List<EX3_Calculo__c> aLstCalculo, EX3_Calculo__c lOldCalc){
        
        if(aLstCalculo.isEmpty() || aLstCalculo == null){
            return ;
        }
        
        List<Group> lLstGroupCalc = [SELECT Id, Name, DeveloperName 
                                     FROM Group WHERE Type='Queue' AND DeveloperName =: EX3_Utils.FILA_CALCULO];
        if(lLstGroupCalc.isEmpty() || lLstGroupCalc == null){ return ;}
        
        for(EX3_Calculo__c iCalc : aLstCalculo){
            if(lOldCalc.OwnerId == lLstGroupCalc[0].Id
               && iCalc.OwnerId == UserInfo.getUserId()){
                iCalc.EX3_Status_do_calculo__c =  EX3_Utils.STATUS_CALCULO_INTERNO;
            } 
        }
        
        List<Group> lLstGroupTerc = [SELECT Id, Name, DeveloperName 
                                     FROM Group WHERE Type='Queue' AND DeveloperName =: EX3_Utils.FILA_CALCULO_TERCEIRIZADO];
        if(lLstGroupTerc.isEmpty() || lLstGroupTerc == null){ return ;}
		
        
        for(EX3_Calculo__c iCalc : aLstCalculo){  
            if(lOldCalc.OwnerId == lLstGroupTerc[0].Id
               && iCalc.OwnerId == UserInfo.getUserId()
               && iCalc.EX3_Controle_Flow__c == null){
                iCalc.EX3_Status_do_calculo__c = EX3_Utils.STATUS_CALCULO_EXTERNO;
            	}  
            }
        }
        
     
    }