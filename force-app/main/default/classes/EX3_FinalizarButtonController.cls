/*********************************************************************************
*                                    Itaú - 2020
*
*
* Autor: Bruno Sampainho Petiti
* Aura Component Bundle: EX3_FinalizarCalculo
*
********************************************************************************/

public without sharing class EX3_FinalizarButtonController {
    
    
    @AuraEnabled
    public static Boolean getHasPermissionSet(){
        Boolean lhasPermission = FeatureManagement.checkPermission('EX3_Criar_Tarefa'); 
        
        return lHasPermission;
   } 
    
    @AuraEnabled
    public static Boolean getAttachment(Id recordId, String fieldsToQuery) {
        List<SObject> lLstSObject = querySObjects(recordId, fieldsToQuery);
        Id lIdLinked = (Id) lLstSObject[0].get('EX3_Caso__c');
        String lName = (String) lLstSObject[0].get('Name'); 
        List<ContentDocumentLink> iContentDocumentLink = [SELECT Id, ContentDocument.Description FROM ContentDocumentLink WHERE LinkedEntityId = :lIdLinked];
        return iContentDocumentLink.size() > 0;
    }
    
    @AuraEnabled
    public static SObject getQueue(Id aRecordId){
        
        String lfieldsToQuery = 'Id, Owner.Name'; 
        List<SObject> lLstObjects = querySObjects(aRecordId, lfieldsToQuery);
        SObject lObject = lLstObjects[0]; 
        return lObject;
    }

    @AuraEnabled
    public static List<String> validateFields(Id recordId, String fieldsToValidate) {
        List<SObject> lLstSObject = querySObjects(recordId, fieldsToValidate);
 
        Map<String, String> fieldApiToLabelMap = getFieldLabel(recordId, fieldsToValidate);
        String fieldsToLowercase = fieldsToValidate.toLowerCase();
        List<String> fieldNamesList = fieldsToLowercase.split(',');
        Map<String, Boolean> fieldNameToMessageMap = new Map<String, Boolean>();
        for (String field : fieldNamesList) {
            if (lLstSObject[0].get(field) == null) {
                fieldNameToMessageMap.put(fieldApiToLabelMap.get(field), false);
            }
        }
        if (!fieldNameToMessageMap.values().contains(false)) {
            fieldNameToMessageMap.put('Sucesso', true);
        }
        List<String> listToReturn = new List<String>(fieldNameToMessageMap.keySet());
        return listToReturn;
    }

    private static List<SObject> querySObjects(Id recordId, String fieldsToQuery) {
        string lQuery;
        Schema.sObjectType recordObject = recordId.getSobjectType();
        List<String> fieldNamesList = fieldsToQuery.split(',');
        for (Integer i = 0; i < fieldNamesList.size(); i++) {
            if (Math.mod(i, 2) == 1) {
                fieldNamesList.add(i, ',');
            }
        }
        lQuery = 'SELECT ';
        for (string lcampo : fieldNamesList) {
            lQuery += lcampo + ' ';
        }
        lQuery += ' from ' + recordObject + ' WHERE ' + 'Id' + ' =: recordId';
        List<SObject> lLstSObject = Database.query(String.escapeSingleQuotes(lQuery));
        return lLstSObject;
    }
    
    @AuraEnabled
    public static Boolean getRecordTypeOBF(Id aRecordId){

        Schema.SObjectType lSchemaADJ = aRecordId.getSObjectType();
        Boolean isTrueADJOBF = false;
        EX3_ADJ__c lADJOBF;
        if(lSchemaADJ == EX3_ADJ__c.sObjectType){
            lADJOBF = [SELECT Id FROM EX3_ADJ__c WHERE Id =: aRecordId 
                       AND RecordTypeId =: EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF')];
            if(lADJOBF != null){
           		isTrueADJOBF = true;
            }
        }
        
        return isTrueADJOBF;    
    }

    private static Map<String, String> getFieldLabel(Id recordId, String fieldsToQuery) {

        Map<String, String> fieldApiToLabelMap = new Map<String, String>();
        String fieldsToQueries = fieldsToQuery.toLowerCase();
        List<String> fieldNamesList = fieldsToQueries.split(',');
        String recordObject = String.valueOf(recordId.getSobjectType());

        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjectSchema = schemaMap.get(recordObject);
        Map<String, Schema.SObjectField> fieldMap = sObjectSchema.getDescribe().fields.getMap();
        for (String fieldName : fieldMap.keySet()) {
            if (fieldNamesList.contains(fieldName)) {
                fieldApiToLabelMap.put(fieldName, String.valueOf(fieldMap.get(fieldName).getDescribe().getLabel()));
            }
        }  
        return fieldApiToLabelMap;
    }
    
    
    
}