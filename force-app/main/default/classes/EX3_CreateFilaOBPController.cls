/*********************************************************************************
*                                    Itaú - 2019
*
* Classe que cria ADJ Fila de OBP
* Empresa: everis do Brasil
* Autor: Sem Autor
*
********************************************************************************/

public class EX3_CreateFilaOBPController {
 
   @AuraEnabled
   
   public static String redirectToObjectOBP(String aRecordId)
   {
       List<EX3_ADJ__c> listOBP = [Select Id, EX3_Numero_da_Pasta__c
                                             From EX3_ADJ__c 
                                             Where Id =: aRecordId ];
       
       EX3_ADJ__c lOBP2 = listOBP[0];
       
       Group queue = [SELECT Id FROM Group WHERE Name = 'EX3 Cumprimento de pagamento' and Type='Queue'];
              
       EX3_ADJ__c lOBP = new EX3_ADJ__c(
           EX3_Numero_da_Pasta__c = lOBP2.EX3_Numero_da_Pasta__c,
           RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBP'),
           
           OwnerId = queue.Id
       );
       
       Database.insert(lOBP);

       return lOBP.Id;
   }
      
 	   @AuraEnabled   
       public static String redirectToObjectOBF(String aRecordId)
   {
       List<EX3_ADJ__c> listOBF = [Select Id, EX3_Data_de_Solicitacao__c, EX3_Status__c, EX3_Observacao__c,  
                                              EX3_Numero_da_Pasta__c, EX3_Status_Fornecedor__c,
                                              EX3_Checklist_de_Cumprimento__c
                                              From EX3_ADJ__c 
                                              Where Id =: aRecordId ];
       
       EX3_ADJ__c lOBF2 = listOBF[0];
       
       Group queue = [SELECT Id FROM Group WHERE Name = 'EX3 Cumprimento de OBF' and Type='Queue'];
              
       EX3_ADJ__c lOBF = new EX3_ADJ__c(
           EX3_Data_de_Solicitacao__c = lOBF2.EX3_Data_de_Solicitacao__c,
           EX3_Status__c = lOBF2.EX3_Status__c,
           EX3_Observacao__c = lOBF2.EX3_Observacao__c,
           EX3_Numero_da_Pasta__c = lOBF2.EX3_Numero_da_Pasta__c,
           EX3_Status_Fornecedor__c = lOBF2.EX3_Status_Fornecedor__c,
           EX3_Checklist_de_Cumprimento__c = lOBF2.EX3_Checklist_de_Cumprimento__c, 
           RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF'), //Cumprimento OBF
           
           OwnerId = queue.Id
       );
       
       Database.insert(lOBF);

       return lOBF.Id;
   }
    
       @AuraEnabled
       public static String redirectToObjectENC(String aRecordId)
   {
       List<EX3_ADJ__c> listENC = [Select Id, EX3_Tipo_de_Decisao__c, EX3_Classificacao_de_Decisao__c, 
                                              EX3_Resultado_da_decisao__c, EX3_Multa_OBF__c, EX3_Valor_Multa_OBF__c,
                                              EX3_Data_da_Publicacao__c, EX3_Checklist_de_Cumprimento__c, 
                                              EX3_Checklist_de_Encerramento__c, EX3_Checklist_de_Pagamento__c,
                                              EX3_Prazos_Judiciais_OBF__c, EX3_Prazos_Judiciais_PGTO__c,
                                              EX3_Pagamento_de_Dano_Moral__c, EX3_Numero_da_Pasta__c
                                             From EX3_ADJ__c 
                                             Where Id =: aRecordId ];
       
       EX3_ADJ__c lENC2 = listENC[0];
       
       Group queue = [SELECT Id FROM Group WHERE Name = 'EX3 Encerramento' and Type='Queue'];
              
       EX3_ADJ__c lENC = new EX3_ADJ__c(
            EX3_Tipo_de_Decisao__c = lENC2.EX3_Tipo_de_Decisao__c,
            EX3_Classificacao_de_Decisao__c = lENC2.EX3_Classificacao_de_Decisao__c,
            EX3_Resultado_da_decisao__c = lENC2.EX3_Resultado_da_decisao__c,
            EX3_Multa_OBF__c = lENC2.EX3_Multa_OBF__c,
            EX3_Valor_Multa_OBF__c = lENC2.EX3_Valor_Multa_OBF__c,
            EX3_Data_da_Publicacao__c = lENC2.EX3_Data_da_Publicacao__c,
            EX3_Checklist_de_Cumprimento__c = lENC2.EX3_Checklist_de_Cumprimento__c,
            EX3_Checklist_de_Encerramento__c = lENC2.EX3_Checklist_de_Encerramento__c,
            EX3_Checklist_de_Pagamento__c = lENC2.EX3_Checklist_de_Pagamento__c,
            EX3_Prazos_Judiciais_OBF__c = lENC2.EX3_Prazos_Judiciais_OBF__c,
            EX3_Prazos_Judiciais_PGTO__c = lENC2.EX3_Prazos_Judiciais_PGTO__c,
            EX3_Pagamento_de_Dano_Moral__c = lENC2.EX3_Pagamento_de_Dano_Moral__c,           
            EX3_Numero_da_Pasta__c = lENC2.EX3_Numero_da_Pasta__c,
            EX3_Status__c = 'Encerrado',
            RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_ADJ'), //ADJ      
           
           OwnerId = queue.Id
       );
       
       Database.insert(lENC);

       return lENC.Id;
   }
       
    	@AuraEnabled
       public static String redirectToObjectFIM(String aRecordId)
   {
       List<EX3_ADJ__c> listFIM = [Select Id, EX3_Tipo_de_Decisao__c, EX3_Classificacao_de_Decisao__c, 
                                              EX3_Resultado_da_decisao__c, EX3_Multa_OBF__c, EX3_Valor_Multa_OBF__c,
                                              EX3_Data_da_Publicacao__c, EX3_Checklist_de_Cumprimento__c, 
                                              EX3_Checklist_de_Encerramento__c, EX3_Checklist_de_Pagamento__c,
                                              EX3_Prazos_Judiciais_OBF__c, EX3_Prazos_Judiciais_PGTO__c,
                                              EX3_Pagamento_de_Dano_Moral__c, EX3_Numero_da_Pasta__c
                                             From EX3_ADJ__c 
                                             Where Id =: aRecordId ];
       
       EX3_ADJ__c lFIM2 = listFIM[0];
       
       Group queue = [SELECT Id FROM Group WHERE Name = 'EX3 Fechamento' and Type='Queue'];
              
       EX3_ADJ__c lFIM = new EX3_ADJ__c(
            EX3_Tipo_de_Decisao__c = lFIM2.EX3_Tipo_de_Decisao__c,
            EX3_Classificacao_de_Decisao__c = lFIM2.EX3_Classificacao_de_Decisao__c,
            EX3_Resultado_da_decisao__c = lFIM2.EX3_Resultado_da_decisao__c,
            EX3_Multa_OBF__c = lFIM2.EX3_Multa_OBF__c,
            EX3_Valor_Multa_OBF__c = lFIM2.EX3_Valor_Multa_OBF__c,
            EX3_Data_da_Publicacao__c = lFIM2.EX3_Data_da_Publicacao__c,
            EX3_Checklist_de_Cumprimento__c = lFIM2.EX3_Checklist_de_Cumprimento__c,
            EX3_Checklist_de_Encerramento__c = lFIM2.EX3_Checklist_de_Encerramento__c,
            EX3_Checklist_de_Pagamento__c = lFIM2.EX3_Checklist_de_Pagamento__c,
            EX3_Prazos_Judiciais_OBF__c = lFIM2.EX3_Prazos_Judiciais_OBF__c,
            EX3_Prazos_Judiciais_PGTO__c = lFIM2.EX3_Prazos_Judiciais_PGTO__c,
            EX3_Pagamento_de_Dano_Moral__c = lFIM2.EX3_Pagamento_de_Dano_Moral__c,                      
            EX3_Numero_da_Pasta__c = lFIM2.EX3_Numero_da_Pasta__c,
            EX3_Status__c = 'Atendido',
            RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_ADJ'), //ADJ            
           	OwnerId = queue.Id
       );
       
       Database.insert(lFIM);

       return lFIM.Id;
   }    
}