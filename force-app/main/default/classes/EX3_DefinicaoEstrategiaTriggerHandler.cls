/*********************************************************************************
*                                    Itaú - 2020
*
* Classe TriggerHandler do objeto EX3_Definicao_de_estrategia  
* 
* Autor: Thiago Barbosa de Souza 
  Company: Everis do Brasil
*
********************************************************************************/


public with sharing class EX3_DefinicaoEstrategiaTriggerHandler implements ITrigger {
    
    public void bulkBefore(){}    
    public void bulkAfter(){}
    public void beforeInsert(){
        EX3_BloqueiaEstrategiaOwner.execute(); 
    }
    public void beforeUpdate(){
        EX3_BloqueiaEstrategiaOwner.execute();  
    }
    public void beforeDelete(){}
    
    public void afterInsert()
    {
    }
    
    public void afterUpdate(){
        EX3_UpdateDefinicaoEstrategica.execute();   
    } 
    
    public void afterDelete(){}
    public void andFinally(){}
}