/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de teste da classe  EX3_PendenciaCentralController
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@isTest
public with sharing class EX3_PendenciaCentralControllerTest
{ 
   @isTest static void getPageTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User(); 
        lUser.Username = 'teste@teste.testeclass.devhulk';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devhulk';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        Case lCase = new Case();
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado'); 
        lCase.EX3_Motivo_situacao__c = '20';
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '128128';
        lCase.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();
        database.insert(lCase);

        Case lCase2 = new Case();
        lCase2.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        database.insert(lCase2);

        Test.startTest();
        Map<String,Object> lMapPage = EX3_PendenciaCentralController.getPage();

        String lColumnsAndSearch = (String) lMapPage.get('columns');
        system.assert(lColumnsAndSearch.contains('Protocolo'));
        system.assert(lColumnsAndSearch.contains('Processo'));
        system.assert(lColumnsAndSearch.contains('Prioridade'));
        system.assert(lColumnsAndSearch.contains('Motivo'));
        system.assert(lColumnsAndSearch.contains('Data de Entrada'));
        system.assert(lColumnsAndSearch.contains('Canal de entrada'));
        system.assert(lColumnsAndSearch.contains('Prazo judicial'));

        List<SObject> lLstCase = (List<SObject>)lMapPage.get('records');

        String lOptionsMotivoSituacao = (String)lMapPage.get('motivo');
        system.assert(lOptionsMotivoSituacao.contains('Previsto de diligência'));
        system.assert(lOptionsMotivoSituacao.contains('Aguardando análise de grandes causas'));
        system.assert(lOptionsMotivoSituacao.contains('Solicitação devolvida ao credenciado'));
        system.assert(lOptionsMotivoSituacao.contains('Solicitação de diligência enviado ao credenciado'));
        system.assert(lOptionsMotivoSituacao.contains('Enviado pelo Itaú'));
        system.assert(lOptionsMotivoSituacao.contains('Em discussão'));
        system.assert(lOptionsMotivoSituacao.contains('Aberto'));
        system.assert(lOptionsMotivoSituacao.contains('Aberto sem documento'));
        system.assert(lOptionsMotivoSituacao.contains('Devolvido'));
        system.assert(lOptionsMotivoSituacao.contains('Finalizado'));

        String lOptionPriority = (String)lMapPage.get('priority');
        system.assert(lOptionPriority.contains('Crítica'));
        system.assert(lOptionPriority.contains('Urgente'));
        system.assert(lOptionPriority.contains('Alta'));
        system.assert(lOptionPriority.contains('Média'));
        system.assert(lOptionPriority.contains('Baixa'));

        String lOptionCanalDeEntrada = (String)lMapPage.get('canaldeentrada');
        system.assert(lOptionCanalDeEntrada.contains('COCKPIT'));
        system.assert(lOptionCanalDeEntrada.contains('CAT'));
        system.assert(lOptionCanalDeEntrada.contains('CEIC'));
        Test.stopTest();

    }

    @isTest static void getCasesTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User(); 
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase = new Case();
        lCase.RecordTypeId = lRTCaseRecepcao;
        lCase.EX3_Motivo_Situacao__c = '21';
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '129129';
        lCase.EX3_Numero_do_Processo__c = '20190001';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();
        database.insert(lCase);

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        database.insert(lCase2);

        Test.startTest();
        List<Case> lLstCase = EX3_PendenciaCentralController.getCases();
        system.assert(!lLstCase.isEmpty());
        Test.stopTest();
    }

    @isTest static void sortRecordsTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_situacao__c = '20';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '20190001';
        lCase1.EX3_Numero_do_Processo__c = '20190001';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        lCase2.EX3_Motivo_situacao__c = '20';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '128127';
        lCase2.EX3_Numero_do_Processo__c = '20190002';
        lCase2.EX3_Prioridade__c = 'Urgente';
        lCase2.EX3_Data_de_Entrada__c = system.now();

        database.insert(new List<Case> {lCase1, lCase2});

        Case lCase3 = new Case();
        lCase3.RecordTypeId = lRTCaseRecepcao;

        Case lCase4 = new Case();
        lCase4.RecordTypeId = lRTCaseRecepcao;
        List<Case> lLstCase = new List<Case>{lCase3, lCase4};
        database.insert(lLstCase);

        String lField = 'EX3_Numero_do_Protocolo__c';
        String lSort = 'ASC';
        String lJSONLstCases = JSON.serialize(lLstCase);

        Test.startTest();
        List<Case> lLstCaseSortRecords = EX3_PendenciaCentralController.sortRecords(lField, lSort, lJSONLstCases);
        system.assert(!lLstCaseSortRecords.isEmpty());
        Test.stopTest();
    } 

    @isTest static void filterRecordsTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_situacao__c = '21';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '812871';
        lCase1.EX3_Numero_do_Processo__c = '20190001';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        lCase2.EX3_Motivo_situacao__c = '33';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '20190002';
        lCase2.EX3_Numero_do_Processo__c = '20190002';
        lCase2.EX3_Prioridade__c = 'Urgente';
        lCase2.EX3_Data_de_Entrada__c = system.now();
        
        Case lCase3 = new Case();
        lCase3.RecordTypeId = lRTCaseRecepcao;
        lCase3.EX3_Motivo_situacao__c = '33';
        lCase3.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase3.EX3_Numero_do_Protocolo__c = '20190003';
        lCase3.EX3_Numero_do_Processo__c = '20190003';
        lCase3.EX3_Prioridade__c = 'Urgente';
        lCase3.EX3_Data_de_Entrada__c = system.now();
        lCase3.EX3_Prazo_Judicial__c = system.today(); 

        database.insert(new List<Case> {lCase1, lCase2, lCase3});

        Case lCase4= new Case();
        lCase4.RecordTypeId = lRTCaseRecepcao;

        Case lCase5 = new Case();
        lCase5.RecordTypeId = lRTCaseRecepcao;
        
        Case lCase6 = new Case();
        lCase6.RecordTypeId = lRTCaseRecepcao; 

        database.insert(new List<Case>{lCase4, lCase5, lCase6});

        String lFieldNumProtocolo = 'EX3_Numero_do_Processo__c';
        String lSearchNumProtocolo = '20190001';

        String lFieldDataEntrada = 'EX3_Data_de_Entrada__c';
        DateTime lSearchDataInicio = system.now().addDays(-1);
    	DateTime lSearchDataFIM = system.now().addDays(+1);
        
        String lFieldPrazoJudicial = 'EX3_Prazo_Judicial__c';
        Date lSearchDataInicioPrazoJudicial = system.today().addDays(-1);
        Date lSearchDataFimPrazoJudicial = system.today().addDays(+1);

        Test.startTest();
        List<Case> lLstCase1 = EX3_PendenciaCentralController.filterRecords(lFieldNumProtocolo, lSearchNumProtocolo, null, null);
        List<Case> lLstCase2 = EX3_PendenciaCentralController.filterRecords(lFieldDataEntrada, null, lSearchDataInicio, lSearchDataFIM);
        List<Case> lLstCase3 = EX3_PendenciaCentralController.filterRecords(lFieldPrazoJudicial, null, lSearchDataInicioPrazoJudicial, lSearchDataFimPrazoJudicial);
        system.assert(!lLstCase1.isEmpty());
        system.assert(!lLstCase2.isEmpty());
        system.assert(!lLstCase3.isEmpty());
        Test.stopTest();
    }

    @isTest static void atualizaMotivoProtocoloTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);
		
        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Motivo_situacao__c = '20';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '20190001';
        lCase1.EX3_Numero_do_Processo__c = '20190001';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();
        
        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao; 
        lCase2.EX3_Motivo_Situacao__c = '21';
        lCase2.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase2.EX3_Numero_do_Protocolo__c = '20190002';
        lCase2.EX3_Numero_do_Processo__c = '20190002';
        lCase2.EX3_Prioridade__c = 'Urgente';
        lCase2.EX3_Data_de_Entrada__c = system.now(); 
        database.insert(new List<Case> {lCase1, lCase2});

        String lRTCase = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        Case lCase3 = new Case();
        lCase3.RecordTypeId = lRTCase;
        lCase3.EX3_Numero_do_protocolo__c = '82391239';
        lCase3.EX3_Motivo_Situacao__c = '28';
        
        Case lCase4 = new Case();
        lCase4.RecordTypeId = lRTCase;
        lCase4.EX3_Numero_do_Protocolo__c = '12381823';
        lCase4.EX3_Motivo_Situacao__c = '28';
        List<Case> lLstCase1 = new List<Case>{lCase3, lCase4};
        database.insert(lLstCase1);
        
        List<Case> lLstCase2 = [SELECT Id, RecordTypeId,
                               EX3_Canal_de_Entrada__c, EX3_Motivo_Situacao__c,
                               EX3_Numero_do_Protocolo__c, EX3_Numero_do_Processo__c,
                               EX3_Prioridade__c, EX3_Data_de_Entrada__c
                               FROM Case WHERE Id = :lLstCase1];

        Test.startTest();
        String lMapProtocolo = EX3_PendenciaCentralController.atualizaMotivoProtocolo(JSON.serialize(lLstCase2));
        system.assert(String.isNotBlank(lMapProtocolo));
        Test.stopTest();
    }
    
    @isTest static void enviarProtocoloTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);
        
        List<EX3_Parametrizacao__c> lLstToken = new List<EX3_Parametrizacao__c>();
        EX3_Parametrizacao__c lParametrizacao = new EX3_Parametrizacao__c();
        lParametrizacao.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_Parametrizacao__c', 'EX3_Token');
        lParametrizacao.EX3_Access_token__c = '12345';
        lParametrizacao.EX3_Token_external__c = 'TokenSTS';
        database.insert(lParametrizacao);

        Case lCase = new Case();
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        lCase.EX3_Motivo_situacao__c = '20';
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '20190001';
        lCase.EX3_Numero_do_Processo__c = '20190001';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();
        database.insert(lCase);

        Case lNewCase = new Case();
        lNewCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        database.insert(lNewCase);
        
        lNewCase.EX3_Motivo_situacao__c = '28';
        
        Database.update(lNewCase);

        List<Case> lLstCase = [SELECT Id, RecordTypeId,
                               EX3_Canal_de_Entrada__c, EX3_Motivo_Situacao__c,
                               EX3_Numero_do_Protocolo__c, EX3_Numero_do_Processo__c,
                               EX3_Prioridade__c, EX3_Data_de_Entrada__c
                               FROM Case WHERE Id = :lCase.Id];
        
        String lProtocolos = EX3_PendenciaCentralController.atualizaMotivoProtocolo(JSON.serialize(lLstCase));

        Map<String,String> lMapParams = new Map<String,String>{
        	'classe' => 'EX3_PendenciaCentralControllerTest',
         	'callback' => 'enviarProtocoloCallback',
            'service' => 'atualizarProtocolo',
            'protocolos' => lProtocolos 
        };
        EX3_ObjectFactory.AtualizarProtocolo lAtualizaProtocolo = new EX3_ObjectFactory.AtualizarProtocolo();
		lAtualizaProtocolo.id_protocolo = Decimal.valueOf(lCase.EX3_Numero_do_Processo__c);
        lAtualizaProtocolo.codigo_protocolo_externo = lCase.Id;
        lAtualizaProtocolo.codigo_motivo_situacao = Decimal.valueOf(lCase.EX3_Motivo_Situacao__c);
        lAtualizaProtocolo.operador = lUser.FuncionalColaborador__c;
        lAtualizaProtocolo.codigo_operacao = 3;
        lAtualizaProtocolo.indicador_protocolo = 0;
        
        Map<String,String> lMapProtocolo = (Map<String,String>) JSON.deserialize(lMapParams.get('protocolos'), Map<String,String>.class);
        
        Map<String, String> lMapDadosRequest = new Map<String,String>
        {                
           'classe' => lMapParams.get('classe'),
           'body' => lMapParams.get('integracaoProtocolo'),
           'callback' => lMapParams.get('callback'),
           'oldProtocolo' => lMapProtocolo.get('oldProtocolo'),
           'newProtocolo' => lMapProtocolo.get('newProtocolo'),
           'integracaoProtocolo' => lMapProtocolo.get('integracaoProtocolo'),
           'service' => 'atualizarProtocolo'
        };
        
        Test.startTest();
        Continuation lContinuation = EX3_PendenciaCentralController.enviarProtocolo(JSON.serialize(lMapParams));
        Map<String, HttpRequest> lRequestSucess = lContinuation.getRequests();
        system.assert(!lRequestSucess.isEmpty());

        String lRequestLabelSuccess = '';
        for (String iLabel : lRequestSucess.keyset())
        {
            lRequestLabelSuccess = iLabel;
        }
	
        Map<String,String> lRequisicaoSucess = new Map<String,String>{'atualizarProtocolo' => lRequestLabelSuccess};
        Object lState = new EX3_ContinuationUtils.StateInfo(lRequestLabelSuccess,lRequisicaoSucess, lMapDadosRequest, 'EX3_PendenciaCentralControllerTest');
        
        EX3_AtualizarProtocoloMock lMockSucesso = new EX3_AtualizarProtocoloMock(lAtualizaProtocolo, 200);
        HTTPResponse lResponseFromMockSucess = lMockSucesso.respond(lRequestSucess.get('Continuation'));
        Test.setContinuationResponse(lRequestLabelSuccess, lResponseFromMockSucess);
        
        EX3_AtualizarProtocoloMock lMockFalha = new EX3_AtualizarProtocoloMock(lAtualizaProtocolo, 400);
        
        Continuation lContinuationFalha = EX3_PendenciaCentralController.enviarProtocolo(JSON.serialize(lMapParams));
        Map<String, HttpRequest> lRequestFalha = lContinuationFalha.getRequests();
        String lRequestLabelFalha = '';
        for (String iLabel : lRequestFalha.keySet())
        {
            lRequestLabelFalha = iLabel;
        } 
        Map<String,String> lRequisicaoFalha = new Map<String,String>{'atualizarProtocolo' => lRequestLabelFalha};
        Object lStateFalha = new EX3_ContinuationUtils.StateInfo(lRequestLabelFalha,lRequisicaoFalha, lMapDadosRequest, 'EX3_PendenciaCentralControllerTest');
        HTTPResponse lResponseFromMockFalha = lMockFalha.respond(lRequestFalha.get('Continuation'));
        Test.setContinuationResponse(lRequestLabelFalha, lResponseFromMockFalha);

        Test.stopTest();
        
        String lResult = (String) EX3_PendenciaCentralController.enviarProtocoloCallback(lState);
        system.assert(String.isNotBlank(lResult));

        String lResultFalha = (String) EX3_PendenciaCentralController.enviarProtocoloCallback(lStateFalha);
        Map<String,Object> lResponseFalha = (Map<String,Object>)JSON.deserializeUntyped(lResultFalha);  
        system.assert(!lRequestFalha.isEmpty());
        system.assertEquals('Erro ao enviar o request', lResponseFalha.get('error'));

    } 
}