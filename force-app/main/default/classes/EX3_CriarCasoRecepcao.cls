/*********************************************************************************
*                                    Itaú - 2019
*
* Classe que cria um caso e associa ao protocolo
* Empresa: everis do Brasil
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
public without sharing class EX3_CriarCasoRecepcao
{
    public static void execute()
    {
        if (!Trigger.isExecuting) { return; }

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        List<Case> lLstCase = new List<Case>();
        for (Case iCase : (List<Case>) Trigger.new)
        {
            Case lCase = new Case();
            lCase.RecordTypeId = lRTCaseRecepcao;
            lCase.EX3_Motivo_situacao__c = iCase.EX3_Motivo_Situacao__c;
            lCase.EX3_Macro_carteira__c = iCase.EX3_Macro_carteira__c;
            lCase.Origin = iCase.EX3_Canal_de_Entrada__c;

            lLstCase.add(lCase);
        }
        if (lLstCase.isEmpty()) { return; }


        criarCaso(JSON.serialize(lLstCase));
    }

    @future
    private static void criarCaso(String aLstCase) 
    {
        List<Case> lLstCase = (List<Case>) JSON.deserialize(aLstCase, List<Case>.class);
        database.insert(lLstCase);
    }
}