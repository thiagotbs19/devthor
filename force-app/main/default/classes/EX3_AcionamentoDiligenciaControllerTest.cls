/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por testar a classe EX3_AcionamentoDiligenciaController
*
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
@isTest
public with sharing class EX3_AcionamentoDiligenciaControllerTest
{ 
    @isTest static void getProtocoloValuesTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Situacao__c = '21';
        lCase1.EX3_Motivo_situacao__c = '28';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '19129';
        lCase1.EX3_Numero_do_Processo__c ='012912'; //NumProcesso1
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();

        database.insert(lCase1);

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        database.insert(lCase2);

        Test.startTest();
        String lAttributesTypes = EX3_AcionamentoDiligenciaController.getProtocoloValues(lCase1.Id);
        system.assert(String.isNotBlank(lAttributesTypes));
        Test.stopTest();
    }

    @isTest static void updateCaseTest()
    {
        List<Profile> lLstProfile = [SELECT Id FROM Profile WHERE Name IN ('system Administrator', 'Administrador do sistema') LIMIT 1];

        User lUser = new User();
        lUser.Username = 'teste@teste.testeclass.devurano';
        lUser.FirstName = 'Teste';
        lUser.LastName = 'User';
        lUser.Email = 'teste@teste.com.devurano';
        lUser.EX3_Pertence__c = '2';
        lUser.Alias = 'testuser';
        lUser.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser.LocaleSidKey = 'pt_BR';
        lUser.EmailEncodingKey  = 'ISO-8859-1';
        lUser.ProfileId = lLstProfile[0].Id;
        lUser.LanguageLocaleKey = 'en_US';
        database.insert(lUser);

        String lRTCaseRecepcao = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');

        Case lCase1 = new Case();
        lCase1.RecordTypeId = lRTCaseRecepcao;
        lCase1.EX3_Situacao__c = '21';
        lCase1.EX3_Motivo_situacao__c = '28';
        lCase1.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase1.EX3_Numero_do_Protocolo__c = '123812'; //NumProtocolo1
        lCase1.EX3_Numero_do_Processo__c = 'NumProcesso1';
        lCase1.EX3_Prioridade__c = 'Urgente';
        lCase1.EX3_Data_de_Entrada__c = system.now();

        database.insert(lCase1);

        Case lCase2 = new Case();
        lCase2.RecordTypeId = lRTCaseRecepcao;
        database.insert(lCase2);
        String lAttributeTypes = getAttributesTypes(lCase1);

        Test.startTest();
        String lSuccess = EX3_AcionamentoDiligenciaController.updateProtocolo(lAttributeTypes);
        system.assert(String.isNotBlank(lSuccess));
        system.assertEquals(lSuccess, 'success');
        Test.stopTest();
    }

    private static String getAttributesTypes(Case aCase)
    {
        List<EX3_AcionamentoDiligenciaController.AttributesTypes> lLstAttributesTypes = new List<EX3_AcionamentoDiligenciaController.AttributesTypes>();
        for (Schema.FieldSetMember iFieldSetMember : EX3_Utils.getFieldSetMembersByFieldSetDeveloperName('Case','EX3_Acionamento_de_Diligencia'))
        {
            EX3_AcionamentoDiligenciaController.AttributesTypes lAttributesTypes = new EX3_AcionamentoDiligenciaController.AttributesTypes();
            lAttributesTypes.label = iFieldSetMember.getLabel();
            lAttributesTypes.api = iFieldSetMember.getFieldPath();
            lAttributesTypes.fieldType = iFieldSetMember.getType();
            lAttributesTypes.value = aCase.get(iFieldSetMember.getFieldPath());
            lAttributesTypes.isDisable  = false;

            if (iFieldSetMember.getType() == Schema.DisplayType.PICKLIST)
            {
                List<EX3_AcionamentoDiligenciaController.AttributesTypesPicklist> lLstAttributesTypesPicklist = new List<EX3_AcionamentoDiligenciaController.AttributesTypesPicklist>();
                for (Schema.PicklistEntry iPickList : EX3_Utils.getPicklistValuesByFieldAPI('Case', iFieldSetMember.getFieldPath()))
                {
                    EX3_AcionamentoDiligenciaController.AttributesTypesPicklist lAttributesTypesPicklist = new EX3_AcionamentoDiligenciaController.AttributesTypesPicklist();
                    lAttributesTypesPicklist.label = iPickList.getLabel();
                    lAttributesTypesPicklist.value = iPickList.getValue();
                    lLstAttributesTypesPicklist.add(lAttributesTypesPicklist);
                }
                lAttributesTypes.picklistValues = lLstAttributesTypesPicklist;
            }
            lLstAttributesTypes.add(lAttributesTypes);
        }
       return JSON.serialize(lLstAttributesTypes);
    } 
}