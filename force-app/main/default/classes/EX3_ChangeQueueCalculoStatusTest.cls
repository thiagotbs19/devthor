/*********************************************************************************
*                                    Itaú - 2020
*
* Classe EX3_ChangeQueueCalculoStatus
* Descrição : Altera o Status para Cálculo Externo em Execução quando o Owner é alterado 
* para Fila de Cálculo Tercerizado.
* Empresa: everis do Brasil
* Autor: Thiago Barbosa 
*
********************************************************************************/

@isTest
public without sharing class EX3_ChangeQueueCalculoStatusTest {
    
    
    @TestSetup
    public static void testQueueCalculo(){
		User lUser = EX3_BI_DataLoad.getUser();
        
        Database.insert(lUser);
        
        Case lCase = EX3_BI_DataLoad.getCase();
        Database.insert(lCase);
        
        EX3_Calculo__c lCalculo = EX3_BI_DataLoad.getCalculo(lCase);
        lCalculo.EX3_Caso__c = lCase.Id;
        lCalculo.EX3_Status_do_calculo__c = 'Cálculo externo em execução';
        Database.insert(lCalculo);  
    
    
    }
    
    @isTest static void testOwnerQueueCalculo(){

    	List<Group> lLstGroupTerceiro = [SELECT Id, Name, DeveloperName 
                                         FROM Group 
                                         WHERE DeveloperName =: Label.EX3_Fila_Calculista_Tercerizado];
        EX3_Calculo__c lCalculo = [SELECT Id FROM EX3_Calculo__c LIMIT 1];
        lCalculo.OwnerId = lLstGroupTerceiro[0].Id;
        
        
        Test.startTest();
        	Database.SaveResult lResult = Database.update(lCalculo);     
        Test.stopTest();
        System.assert(lResult.isSuccess(), 'O registro foi atualizado com sucesso');
    
    }

}