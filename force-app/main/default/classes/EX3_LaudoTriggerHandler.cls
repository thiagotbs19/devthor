/*********************************************************************************
*                                    Itaú - 2020
*
* Classe TriggerHandler do objeto EX3_Laudo__c  
* 
* Empresa: everis do Brasil
* Autor: Isabella Tannús 
*
********************************************************************************/

public with sharing class EX3_LaudoTriggerHandler implements ITrigger{

        public void bulkBefore(){} 

        public void bulkAfter(){}

        public void beforeInsert(){
            EX3_BloqueiaLaudoOwner.execute(); 
        }

        public void beforeUpdate(){
            EX3_BloqueiaLaudoOwner.execute();
        }

        public void beforeDelete(){}
        
        public void afterInsert(){}
        
        public void afterUpdate(){}

        public void afterDelete(){}
        
        public void andFinally(){}
}