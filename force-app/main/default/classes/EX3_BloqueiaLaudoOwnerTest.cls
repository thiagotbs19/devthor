/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste responsável por cobrir e testar a EX3_BloqueiaLaudoOwner 
*
* 
*Apex Class: EX3_BloqueiaLaudoOwnerTest
* Empresa: everis do Brasil 
* Autor: Isabella Tannús 
*
********************************************************************************/

@isTest
public with sharing class EX3_BloqueiaLaudoOwnerTest {
	@isTest static void testLaudoOwnerTest(){

        User lUser = EX3_BI_DataLoad.getUser();
        Database.insert(lUser); 
        
        EX3_Laudo__c lLaudo = new EX3_Laudo__c();
        lLaudo.OwnerId = UserInfo.getUserId();
        
        Database.insert(lLaudo);   
        
        EX3_Laudo__c lSegLaudo = new EX3_Laudo__c(); 
        lSegLaudo.OwnerId = lUser.Id;
        
        Test.startTest();  

        Database.insert(lSegLaudo);  
        
        lSegLaudo.OwnerId = UserInfo.getUserId();    

        Test.stopTest();                            
        Database.SaveResult lResult = Database.update(lSegLaudo, false);      
        System.assert(!lResult.isSuccess(), 'Registro não foi atualizado'); 
    } 
}