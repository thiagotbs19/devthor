/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de teste da classe EX3_CriaLinkArquivos
* 
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
@isTest
private class EX3_CriaLinkArquivosTest {
    
    @isTest static void criarDoc() 
    {
        EX3_Centralizador__c lCentralizador = EX3_BuscaAtivaController.getCentralizador();
        lCentralizador.EX3_hasDocuments__c = true;
        String lCentralizadorToString = json.serialize(lCentralizador);
        
        Case lCase = EX3_BI_DataLoad.getCase(); 
        lCase.EX3_Canal_de_Entrada__c = '9'; 
		lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
        
        database.insert(lCase);
        String lProtocoloToString = JSON.serialize(lCase);
        
        ContentVersion lContentVersion1 = new ContentVersion();
        lContentVersion1.Title = 'Test';
        lContentVersion1.Tipo_de_Documento__c = '1';
        lContentVersion1.PathOnClient = '/' + lContentVersion1.Title + '.jpg';
        lContentVersion1.VersionData = Blob.valueOf('Unit Test ContentVersion Body');
        lContentVersion1.origin = 'H';   
        lContentVersion1.Description = 'Teste Description'; 
        
        database.insert(lContentVersion1);
        
        lCase.EX3_Numero_do_Protocolo__c = lContentVersion1.Description;
        
        Database.update(lCase);
        
        ContentVersion lContentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion
                                          WHERE Id =: lContentVersion1.Id]; 
        
        ContentDocumentLink lContentDocumentLink = new ContentDocumentLink();
        lContentDocumentLink.LinkedEntityId = lCase.Id; 
        lContentDocumentLink.ShareType = 'V';
        lContentDocumentLink.ContentDocumentId = lContentVersion.ContentDocumentId;
        
        database.insert(lContentDocumentLink); 
    } 

}