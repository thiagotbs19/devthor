/*********************************************************************************
*                                    Itaú - 2020
*
* Classe EX3_ChangeQueueCalculoStatus
* Descrição : Altera o Status para Cálculo Externo em Execução quando o Owner é alterado 
* para Fila de Cálculo Tercerizado.
* Empresa: everis do Brasil
* Autor: Thiago Barbosa 
*
********************************************************************************/

public without sharing class EX3_ChangeQueueCalculoStatus {
    	
    	private static Schema.SObjectType lTipoGroup = Schema.getGlobalDescribe().get('Group');
        private static final String lRecordTypeSolicit = EX3_Utils.getRecordTypeIdByDevName('EX3_Calculo__c', 'EX3_Calculista');
    	public static void execute() 
    {	
    	Set<Id> lSetOwnerCalculo = new Set<Id>();
      	List<EX3_Calculo__c> lLstEX3Calculo = new List<EX3_Calculo__c>();
      	for(EX3_Calculo__c iCalculo : (List<EX3_Calculo__c>) Trigger.new)
      	{
        	if(EX3_TriggerHelper.changedField(iCalculo, 'OwnerId') && iCalculo.ownerId.getSObjectType() == lTipoGroup) {
        	lSetOwnerCalculo.add(iCalculo.OwnerId);
        	lLstEX3Calculo.add(iCalculo); 
            }
      	}
        if(lLstEX3Calculo.isEmpty() || lLstEX3Calculo == null){ return ;}
        changeOwnerQueue(lLstEX3Calculo, lSetOwnerCalculo);
    }
    
    private static void changeOwnerQueue(List<EX3_Calculo__c> aLstCalculo, Set<Id> aSetOwnerCalculo){
        if(aLstCalculo.isEmpty() || aLstCalculo == null){ return ;}
        
        List<Group> lLstGroupTerceiro = [SELECT Id, Name, DeveloperName 
                                         FROM Group 
                                         WHERE DeveloperName =: Label.EX3_Fila_Calculista_Tercerizado];
       	List<EX3_Calculo__c> lLstUpdateCalculo = new List<EX3_Calculo__c>();
        for(EX3_Calculo__c iCalculo : [SELECT Id, OwnerId, EX3_Controle_Flow__c, EX3_Status_do_calculo__c
                                       FROM EX3_Calculo__c 
                                       WHERE OwnerId =: lLstGroupTerceiro[0].Id
                                       AND EX3_Controle_Flow__c = null]){ 
                      EX3_Calculo__c lCalculo = new EX3_Calculo__c();
                      lCalculo.Id = iCalculo.Id;
                      lCalculo.RecordTypeId = lRecordTypeSolicit;
                      lCalculo.EX3_Status_do_calculo__c = Label.EX3_Calculo_Externo;
                      lCalculo.EX3_Controle_Flow__c = Label.EX3_Calculo_Tercerizado;
                      lLstUpdateCalculo.add(lCalculo);                    
          }
        
        if(lLstUpdateCalculo.isEmpty()){ return ; }
        Database.update(lLstUpdateCalculo);
        
       
    }

}