/*********************************************************************************
*                                    Itaú - 2019
*
* Classe desenvolvida para prover conjunto de motivos de diligencia ao componente 
* e atualizar o motivo da situação de acordo com a diligência selecionada pelo usuário
*
* AuraComponent: AtribuicaoProcessoDiligencia
* Empresa: everis
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/
@isTest
public class EX3_AtribProcDiligenciaControllerTest {
    
    @isTest
    public static void testGetMotivo() {
        test.startTest();
        String lMotivosThatMatchCriteria = EX3_AtribProcDiligenciaController.getMotivos();
        test.stopTest();
        List<AttributesTypesPicklist> lLstMotivosThatMatchCriteria = (List<AttributesTypesPicklist>) JSON.deserialize(lMotivosThatMatchCriteria, List<AttributesTypesPicklist>.class); 
        system.assertEquals(true, (lLstMotivosThatMatchCriteria[0].value).equals(''));
    }
    
    @isTest
    public static void testUpdateMotivos() {
        
        test.startTest();
          Case lCase = new Case();
          insert lCase;
          String aTest = EX3_AtribProcDiligenciaController.updateMotivos('11', lCase.Id);
        	
        test.stopTest();

   }
    
   public class AttributesTypesPicklist
   {
      private String label {get;set;}
      private String value {get;set;}
   }

}