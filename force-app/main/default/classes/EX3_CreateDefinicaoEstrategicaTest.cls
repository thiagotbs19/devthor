/*********************************************************************************
*                                    Itaú - 2020  
*
* Classe de Teste
*
*Apex Class: EX3_CreateDefinicaoEstrategicaTest
*  

* Empresa: everis do Brasil
* Autor: Bruno Sampainho Petiti
*
********************************************************************************/

@isTest
public with sharing class EX3_CreateDefinicaoEstrategicaTest {
    @testSetup
    public static void createData(){

        Case lCaso = EX3_BI_DataLoad.createCase();
        EX3_Subsidios__c lSubsidio = EX3_BI_DataLoad.getCapturaDocumentos(lCaso);
        Database.insert(lSubsidio); 
        Database.insert(lCaso);
    }

    @isTest
    public static void returnSubsidioTest(){
        EX3_Subsidios__c lSubsidio = [SELECT Id, EX3_Fase__c FROM EX3_Subsidios__c LIMIT 1];
        Test.startTest();
        String label = EX3_CreateDefinicaoEstrategicaController.redirectToObject(lSubsidio.Id);
        Test.stopTest();
        String labelReturn = 'Não há Tipos de Documentos.';
        System.assert(label != null, labelReturn);     
    }
    @isTest
    public static void createDocumentoPreEstrategiaTest(){
        EX3_Subsidios__c lSubsidio = [SELECT Id, EX3_Caso__c FROM EX3_Subsidios__c LIMIT 1];
        lSubsidio.EX3_Fase__c = Label.EX3_PreEstrategia;
        Database.update(lSubsidio);
        Case lCaso = [SELECT Id FROM Case LIMIT 1];
        EX3_Tipo_Documento__c lDocumento = EX3_BI_DataLoad.createDocument(lCaso,lSubsidio);
        Database.insert(lDocumento); 
        Test.startTest();
        String label = EX3_CreateDefinicaoEstrategicaController.redirectToObject(lSubsidio.Id);
        Test.stopTest();
        List<EX3_DefinicaoEstrategia__c> definicaoEstrategia = [SELECT Id FROM EX3_DefinicaoEstrategia__c];
        System.assertEquals(1,definicaoEstrategia.size());
    }

    @isTest
    public static void createDocumentoChecklistTest(){
        EX3_Subsidios__c lSubsidio = [SELECT Id, EX3_Caso__c FROM EX3_Subsidios__c LIMIT 1];
        lSubsidio.EX3_Fase__c = Label.EX3_ChecklistComplementar;
        Database.update(lSubsidio);
        
        Case lCaso = [SELECT Id FROM Case LIMIT 1];
        EX3_Tipo_Documento__c documento = EX3_BI_DataLoad.createDocument(lCaso,lSubsidio);
        Database.insert(documento);
        
        Test.startTest();
        String label = EX3_CreateDefinicaoEstrategicaController.redirectToObject(lSubsidio.Id);
        Test.stopTest(); 
        List<EX3_DefinicaoEstrategia__c> lLstdefinicaoEstrategia = [SELECT Id FROM EX3_DefinicaoEstrategia__c];
        System.assertEquals(1,lLstdefinicaoEstrategia.size(), 'Não foi criado registro de Definição Estratégica'); 
    }

    @isTest
    public static void createContract(){
        EX3_Subsidios__c lSubsidio = [SELECT Id, EX3_Caso__c FROM EX3_Subsidios__c LIMIT 1];
        lSubsidio.EX3_Fase__c = Label.EX3_ChecklistComplementar;
        
        Database.update(lSubsidio);
        
        Case lCaso = [SELECT Id FROM Case LIMIT 1];
        EX3_Tipo_Documento__c lDocumento = EX3_BI_DataLoad.createDocument(lCaso,lSubsidio);
        Database.insert(lDocumento);
        
        Account lEnvolvido = EX3_BI_DataLoad.createEnvolvido();
        Database.insert(lEnvolvido);
        
        Contract lContrato = EX3_BI_DataLoad.createContrato(lEnvolvido,lSubsidio);

        Database.insert(lContrato);
        Test.startTest();
        String label = EX3_CreateDefinicaoEstrategicaController.redirectToObject(lSubsidio.Id);
        Test.stopTest();
        List<EX3_DefinicaoEstrategia__c> lLstDefinicaoEstrategia = [SELECT Id FROM EX3_DefinicaoEstrategia__c];
        System.assert(!lLstDefinicaoEstrategia.isEmpty(), 'Foi Criada a Definição Estratégica');
    }

	@isTest static void testTipoDocPendente(){
		
		EX3_Subsidios__c lSubsidio = [SELECT Id, EX3_Caso__c FROM EX3_Subsidios__c LIMIT 1];
        lSubsidio.EX3_Fase__c = Label.EX3_ChecklistComplementar;
        Database.update(lSubsidio);
        
        Case lCaso = [SELECT Id FROM Case LIMIT 1];
        EX3_Tipo_Documento__c lDocumento = EX3_BI_DataLoad.createDocument(lCaso,lSubsidio);
		lDocumento.EX3_Arquivo_Anexado__c = false;
		lDocumento.EX3_Status_do_Documento__c = 'Documento pendente'; 
		lDocumento.EX3_Subsidios__c = lSubsidio.Id; 
        Database.insert(lDocumento);

		Test.startTest();
			 String label = EX3_CreateDefinicaoEstrategicaController.redirectToObject(lDocumento.Id);
		Test.stopTest();  
		System.assert(lDocumento != null, 'Anexe mais documentos');  
		}
    
    @isTest static void testRedirectToObjectNull(){
        
        Test.startTest();
        	String lLabel = EX3_CreateDefinicaoEstrategicaController.redirectToObject('');
        Test.stopTest();
        System.assert(lLabel != null, 'O registro de Definição Estrategica não foi criado');
    }

    @isTest static void testHasPermission(){

        Test.startTest();
            Boolean lHasPermission = EX3_CreateDefinicaoEstrategicaController.getHasPermissionSet();
        Test.stopTest(); 
        System.assert(!lHasPermission, 'Não há permissão atribuida a este perfil');
    }
}