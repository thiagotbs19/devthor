/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por cancelar ou abrir um modal da diligencia
* AuraComponent: EX3_AcaoDiligencia
* Empresa: everis
* Autor: Rafael Amaral Moreira
*
********************************************************************************/
public with sharing class EX3_AcaoDiligenciaController
{
    @AuraEnabled
    public static Case cancelarCaseDiligencia(String aCaseId)
    {
        Case lCase;
        try
        {
            lCase = [SELECT Id, EX3_Situacao__c, EX3_Motivo_situacao__c FROM Case WHERE Id = :aCaseId];
 
            if (lCase.EX3_Situacao__c != '21') { return null;}

            lCase.EX3_Motivo_situacao__c = '25'; 
            database.update(lCase); 
        }
        catch (DmlException ex)
        {
            SA7_CustomdebugLog.logError('EX3', 'EX3_AcaoDiligenciaController', 'updateProtocoloDiligencia', 'Erro ao atualizar' +
                'registro do Caso', ex, null);
        }
        return lCase;
    }

    @AuraEnabled
    public static Case isNotDiligencia(String aCaseId)
    {
        Case lCase = [SELECT Id, EX3_Situacao__c FROM Case WHERE Id = :aCaseId];

        return lCase; 
    }
}