/*********************************************************************************
*                                    Itaú - 2019
*
* Classe que cria ADJ Fila de OBF 
* Empresa: everis do Brasil
* Autor: Sem Autor
*
********************************************************************************/

public class EX3_CreateFilaOBFController {
   
   @AuraEnabled
   public static String redirectToObject(String aRecordId)
   {
       List<EX3_ADJ__c> listOBF = [Select Id, EX3_Data_de_Solicitacao__c, EX3_Status__c, EX3_Observacao__c,  
                                              EX3_Numero_da_Pasta__c, EX3_Status_Fornecedor__c,
                                              EX3_Checklist_de_Cumprimento__c
                                             From EX3_ADJ__c 
                                             Where Id =: aRecordId ];
       
       EX3_ADJ__c lOBF2 = listOBF[0]; 
       
       Group queue = [SELECT Id FROM Group WHERE Name = 'EX3 Fornecedor' and Type='Queue'];
              
       EX3_ADJ__c lOBF = new EX3_ADJ__c(
           EX3_Data_de_Solicitacao__c = lOBF2.EX3_Data_de_Solicitacao__c,
           EX3_Status__c = lOBF2.EX3_Status__c,
           EX3_Observacao__c = lOBF2.EX3_Observacao__c,
           EX3_Numero_da_Pasta__c = lOBF2.EX3_Numero_da_Pasta__c,
           EX3_Status_Fornecedor__c = lOBF2.EX3_Status_Fornecedor__c,
           EX3_Checklist_de_Cumprimento__c = lOBF2.EX3_Checklist_de_Cumprimento__c,
           RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF'), //OBF
           
           OwnerId = queue.Id
       );
       
       Database.insert(lOBF);

       return lOBF.Id;
   }    

   @AuraEnabled
   public static String redirectToObject2(String aRecordId)
   {
       List<EX3_ADJ__c> listOBF = [Select Id, EX3_Data_de_Solicitacao__c, EX3_Status__c, EX3_Observacao__c,  
                                              EX3_Numero_da_Pasta__c, EX3_Status_Fornecedor__c,
                                              EX3_Checklist_de_Cumprimento__c
                                             From EX3_ADJ__c 
                                             Where Id =: aRecordId ];
       
       EX3_ADJ__c lOBF2 = listOBF[0];
       
       Group queue = [SELECT Id FROM Group WHERE Name = 'EX3 Fechamento' and Type='Queue'];
              
       EX3_ADJ__c lOBF = new EX3_ADJ__c(
           EX3_Data_de_Solicitacao__c = lOBF2.EX3_Data_de_Solicitacao__c,
           EX3_Status__c = lOBF2.EX3_Status__c,
           EX3_Observacao__c = lOBF2.EX3_Observacao__c,
           EX3_Numero_da_Pasta__c = lOBF2.EX3_Numero_da_Pasta__c,
           EX3_Status_Fornecedor__c = lOBF2.EX3_Status_Fornecedor__c,
           EX3_Checklist_de_Cumprimento__c = lOBF2.EX3_Checklist_de_Cumprimento__c,
           RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF'), //OBF
           
           OwnerId = queue.Id
       );
       
       Database.insert(lOBF);

       return lOBF.Id;
   }        
}