/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável pelo Fluxo de Cálculo(Criar registro de Solicitação de Calculo)
* e Botão Enviar Aceite. 
*
*Apex Trigger : EX3_Calculo 
*Apex Class: EX3_CreateCalculoComplexo
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/


public without sharing class EX3_CreateCalculoComplexo {
    
    
    public static final Set<String> lSetRecordTypeId = new Set<String> { EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBP'),
        EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'Tipo_de_verba_DT_OBP')};  
    
            
    @AuraEnabled
    public static Boolean getHasPermissionSet(){
        Boolean lhasPermission = FeatureManagement.checkPermission('EX3_Criar_Tarefa'); 
        
        return lHasPermission;
   } 
    
    @AuraEnabled
    public static EX3_ADJ__c getRecordTypeADJ(String aRecordId){
        
        EX3_ADJ__c lADJ; 
        try{ 
            
            if(aRecordId == null){
                return lADJ;     
            }
            lADJ = [SELECT Id, RecordTypeId FROM  EX3_ADJ__c   
                    WHERE Id =: aRecordId 
                    AND RecordTypeId =: lSetRecordTypeId]; 
        } catch (Exception ex)   
        {   
            SA7_CustomdebugLog.logError('EX3', 'EX3_CreateCalculoComplexo', 'getRecordTypeADJ', 'Erro ao retornar o registro cujo tipo de registro é OBP '+ ex.getMessage(), ex, null);
        }  
        
        return lADJ; 
        
    }
    
    @AuraEnabled 
    public static Map<String, Object> getFieldsADJ(String aRecordId)
    {        
        Map<String, Object> lMapReturn;  
        try{
            List<EX3_ADJ__c> lLstADJ = [SELECT Id, EX3_Caso__c, EX3_Numero_do_processo__c, EX3_Numero_da_Pasta__c, EX3_Status__c, RecordTypeId
                                        From EX3_ADJ__c WHERE Id =: aRecordId   
                                        AND RecordTypeId =: lSetRecordTypeId];    
            Group lQueueGroup = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName =: EX3_Utils.FILA_FUP]; 
            Group lQueueGroupCalc = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName =: EX3_Utils.FILA_CALCULO ]; 
            String lRecordTypeCalc = EX3_Utils.getRecordTypeIdByDevName('EX3_Calculo__c', 'EX3_Solicitacao_de_Calculo'); 
            
            EX3_ADJ__c lADJ = lLstADJ[0];  
            lMapReturn = new Map<String,Object>  
            {   
                	'adj' => lADJ, 
                    'recordTypeCalc' => lRecordTypeCalc, 
                    'queueId' => lQueueGroup.Id, 
                    'queueCalc' =>  lQueueGroupCalc.Id
                    
                    };
                        
            
        }catch (Exception ex)
        { 
            SA7_CustomdebugLog.logError('EX3', 'EX3_CreateCalculoComplexo', 'getFieldsCadastro', 'Erro ao criar o registro '+ ex.getMessage(), ex, null);
        }
        return lMapReturn;
    }
    
    @AuraEnabled
    public static EX3_ADJ__c getUpdateFilaAceite(String aRecordId){
        
        if(aRecordId == null){ return new EX3_ADJ__c();}
        EX3_ADJ__c lEX3ADJ; 
        
        String lRecordTypeAceite = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Aceite_DOP');  
        try{  
            
            lEX3ADJ = [SELECT Id, RecordTypeId, OwnerId FROM EX3_ADJ__c WHERE Id =: aRecordId]; 
            
            List<Group> lLstGroupFilaAceite = [SELECT Id, Name, DeveloperName FROM Group WHERE Type='Queue' AND DeveloperName =: EX3_Utils.FILA_ACEITE ];
            
            
            lEX3ADJ.RecordTypeId = lRecordTypeAceite;
            lEX3ADJ.OwnerId = lLstGroupFilaAceite[0].Id;
            lEX3ADJ.EX3_Controle_Flow__c = true;
            if(lEX3ADJ != null){    
                Database.update(lEX3ADJ);  
            } 
        }
        
        catch(DmlException ex) {  
            SA7_CustomdebugLog.logError('EX3', 'EX3_CreateCalculoComplexo', 'getUpdateFilaAceite', 'Erro ao inserir o Cadastro ' + ex.getMessage(), ex, new Map<String, String>()); 
        } 
        catch (Exception ex)  
        { 
            SA7_CustomdebugLog.logError('EX3', 'EX3_CreateCalculoComplexo', 'getUpdateFilaAceite', 'Erro ao atualizar o registro '+ ex.getMessage(), ex, null);
        }
        return lEX3ADJ;     
    } 
}