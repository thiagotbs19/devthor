/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por simular a resposta do inclusão de protocolo do EX3
* Empresa: everis do Brasil
* Autor: Rafael Amaral Moreira
*
*
********************************************************************************/
public with sharing class EX3_IncluirCaseMock  implements HttpCalloutMock
{
    private Integer statusCode;
    private String codigo_protocolo_externo;
    private String codigo_unico_processo;
    private DateTime data_protocolo_documento;
    private Decimal codigo_canal_entrada_documento;
    private Decimal codigo_motivo_situacao;
    private Date data_situacao;
    private String operador;
    private Decimal codigo_operacao;
    private Decimal indicador_protocolo; 

    public EX3_IncluirCaseMock(EX3_ObjectFactory.IncluirProtocolo aIncluirCase, Integer statusCode) 
    {
        this.statusCode = statusCode;
        this.codigo_protocolo_externo = aIncluirCase.codigo_protocolo_externo;
        this.codigo_unico_processo = aIncluirCase.codigo_unico_processo;
        this.data_protocolo_documento = aIncluirCase.data_protocolo_documento;
        this.codigo_canal_entrada_documento = aIncluirCase.codigo_canal_entrada_documento;
        this.codigo_motivo_situacao = aIncluirCase.codigo_motivo_situacao;
        this.data_situacao = aIncluirCase.data_situacao;
        this.operador = aIncluirCase.operador;
        this.codigo_operacao = 3;
        this.indicador_protocolo = 0; 
    }

    public HTTPResponse respond(HTTPRequest req) 
    {  
        Map<String, Object> lMapBody = new Map<String,Object>
        {
            'id_protocolo' => 2019000001,
            'codigo_protocolo_externo' => this.codigo_protocolo_externo,
            'codigo_unico_processo' => this.codigo_unico_processo,
            'data_protocolo_documento' => this.data_protocolo_documento,
            'codigo_canal_entrada_documento' => this.codigo_canal_entrada_documento,
            'codigo_motivo_situacao' => this.codigo_motivo_situacao,
            'data_situacao' => this.data_situacao,
            'operador' => this.operador,
            'codigo_operacao' => this.codigo_operacao,
            'indicador_protocolo' => this.indicador_protocolo
        };
		
        String jsonString = (statusCode > 202) 
                          ? '{"Message": "Erro ao enviar o request" }'
                          : '{"data":'+JSON.serialize(lMapBody)+'}';
            
        HttpResponse res = new HttpResponse();
        res.setStatusCode(statusCode);
        res.setBody(jsonString);

        return res;
    }
}