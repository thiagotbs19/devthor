/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por gerar codigo GUID/UUID para integrar via Gateway
*
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
public class EX3_GuidUtil {

    public static String newGuid()
    {
        Blob lBlobCrypto = Crypto.GenerateAESKey(128);
        String lhexaEncod = EncodingUtil.ConvertTohex(lBlobCrypto);

        return lhexaEncod.SubString(0,8)+ '-' + lhexaEncod.SubString(8,12) + '-' + lhexaEncod.SubString(12,16) + '-' + lhexaEncod.SubString(16,20) + '-' + lhexaEncod.substring(20);
    }

}