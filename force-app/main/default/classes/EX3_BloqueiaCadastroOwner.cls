/*********************************************************************************
*                                    Itaú - 2019
*
* Classe Responsável para que o analista não consiga puxar um registro para iniciar
* um atendimento se o mesmo ja possuir um registro de Cadastro. 
*
*Apex Trigger : EX3_Cadastro__c
*Apex Class: EX3_BloqueiaCadastroOwner
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

public without sharing class EX3_BloqueiaCadastroOwner {
    
   
    public static void execute(){ 
         
        Set<Id> lSetOwnerCadastro = new Set<Id>();
        List<EX3_Cadastro__c> lLstEX3Cadastro = new List<EX3_Cadastro__c>();
        for(EX3_Cadastro__c iCadastro : (List<EX3_Cadastro__c>) Trigger.new){
            if(EX3_TriggerHelper.changedField(iCadastro, 'OwnerId') &&
               iCadastro.OwnerId == UserInfo.getUserId()) {
                   lSetOwnerCadastro.add(iCadastro.OwnerId);
                   lLstEX3Cadastro.add(iCadastro);
               }    
        }
        if(lSetOwnerCadastro.isEmpty() || lSetOwnerCadastro == null){ return ;}
        cadastroOwnerError(lSetOwnerCadastro, lLstEX3Cadastro);
    } 
    
    private static void cadastroOwnerError(Set<Id> aSetOwnerCadastro, List<EX3_Cadastro__c> aLstCadastro){
        
        
        if(aSetOwnerCadastro.isEmpty() || aSetOwnerCadastro == null || aLstCadastro.isEmpty() || aLstCadastro == null){
            return;
        }
        
      List<User> lLstUserProfile = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() AND Profile.Name NOT IN('System Administrator', 'Administrador do sistema')];
      if (lLstUserProfile.isEmpty()) { return; } 
        String lNumeroProtocolo; 
        if(!aLstCadastro.isEmpty()){
        for(EX3_Cadastro__c iCadastro : [SELECT Id, OwnerId, EX3_Numero_Protocolo__c, EX3_Status__c  FROM EX3_Cadastro__c
                                             WHERE OwnerId =: aSetOwnerCadastro AND EX3_Status__c NOT IN ('Cadastrado')]){
                                                 
             lNumeroProtocolo = String.valueOf(iCadastro.EX3_Numero_Protocolo__c);
                                             }  
        } 
        if(lNumeroProtocolo == null){  return;}      
        for(EX3_Cadastro__c iCadastro : aLstCadastro){    
            iCadastro.adderror(Label.EX3_Pasta_Numero + lNumeroProtocolo + Label.EX3_Tratativa);
        }       
        
    } 
    
}