/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por chamar a integração de 3 em 3 minutos
*
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
global without sharing class EX3_SchIntegracaoObterTokenGTWOut implements Schedulable{

    /* Chamada via Anonymous
       EX3_SchIntegracaoObterTokenGTWOut cls = new EX3_SchIntegracaoObterTokenGTWOut();
     cls.execute(null);
     */

    public static final Integer INTERVAL_MINUTES = 03;
    public static final String AG_NAME = 'EX3_SchIntegracaoObterTokenGTWOut_';
    public static String lRecorTypeToken = Schema.SObjectType.EX3_Parametrizacao__c.getRecordTypeInfosByName().get('Token').getRecordTypeId();

     global void execute(SchedulableContext sc) {
         atualizarTokenGatewayEX3();
    }


    @future(callout=true)
    private static void atualizarTokenGatewayEX3() {
        String lException = '';
        Map<String, String> MapVariaveisIntegracao = new Map<String, String>();
        EX3_Parametrizacao__c lValue;
         try {
              EX3_obter_token_STS__mdt lConfigToken = [select Endpoint__c, Method__c, Timeout__c,
                                                         Appid__c, Client_id__c, Client_secret__c, Grant_type__c
                                                         FROM EX3_obter_token_STS__mdt
                                                         WHERE DeveloperName =: 'TokenEX3STS_Dev'];

              	Http http = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint(lConfigToken.Endpoint__c);
                req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                req.setTimeout(Integer.valueOf(lConfigToken.Timeout__c));
                req.setmethod(lConfigToken.Method__c);

                String payload = lConfigToken.Client_id__c +'&'+ lConfigToken.Client_secret__c +'&'+ lConfigToken.Grant_type__c +'&'+ lConfigToken.Appid__c;

                req.setBody(payload);

                HttpResponse response = http.send(req);

                if(response.getStatusCode() == 200)
                {

                   EX3_ScheduledObjectFactory.DadosTokenSTS lResponse = new EX3_ScheduledObjectFactory.DadosTokenSTS();

                   lResponse = (EX3_ScheduledObjectFactory.DadosTokenSTS)JSON.deserialize(response.getBody(), EX3_ScheduledObjectFactory.DadosTokenSTS.class);

                   lValue = obterConfiguracaoToken(lRecorTypeToken, 'TokenSTS');

                   lValue.EX3_Access_token__c = lResponse.access_token;
                   lValue.EX3_Active__c = lResponse.active;
                   lValue.EX3_Expires_in__c = lResponse.expires_in;
                   lValue.EX3_Refresh_token__c = lResponse.refresh_token;
                   lValue.EX3_Scope__c = lResponse.scope;
                   lValue.EX3_Token_type__c = lResponse.token_type;

                   MapVariaveisIntegracao.put('Classe - Metodo ', +'EX3_SchIntegracaoObterTokenGTWOut - atualizarTokenGatewayEX3'+'\n');
                   MapVariaveisIntegracao.put('Endpoint ', lConfigToken.Endpoint__c+'\n');
                   MapVariaveisIntegracao.put('Method ', lConfigToken.Method__c+'\n');
                   MapVariaveisIntegracao.put('Request ', +' '+payload +'\n');
                   MapVariaveisIntegracao.put('Response StatusCode ', +' '+response.getStatusCode()+'\n');
                   MapVariaveisIntegracao.put('Response Status ', +' '+response.getStatus());

                   upsert lValue;
                }
                else
                {
                    criarPostagem('Erro ao obter token STS Token SFDC->GTW \n' + response.getStatusCode() + ' - ' + response.getBody());
                }

         }catch(DmlException ex) {
            lException = ex.getMessage();
            SA7_CustomdebugLog.logError('EX3', 'EX3_IntegracaoObterTokenGTWOut', 'iniciaIntegracao', 'Erro ao integrar ' +
                                            lException, ex, MapVariaveisIntegracao);
         }catch(Exception ex) {
            lException = ex.getMessage();
            SA7_CustomdebugLog.logError('EX3', 'EX3_IntegracaoObterTokenGTWOut', 'iniciaIntegracao', 'Erro ao integrar ' +
                                            lException, ex, MapVariaveisIntegracao);
         }finally{
              SA7_CustomdebugLog.logWarn('EX3', 'EX3_IntegracaoObterTokenGTWOut', 'iniciaIntegracao', (String.isNotBlank(lException)) ? 'Erro ao integrar ' + lException : 'Sucesso ao integrar', MapVariaveisIntegracao);
              agendarProximaExecucao();  
         }
    }

    private static void agendarProximaExecucao()
    {
        String lException = '';
        Map<String, String> MapVariaveisIntegracao = new Map<String, String>();
        String cronStr = '';
        try{
            Datetime dtNow = system.now().addMinutes(EX3_SchIntegracaoObterTokenGTWOut.INTERVAL_MINUTES);

            cronStr = dtNow.second() + ' ' + dtNow.minute() + ' ' + dtNow.hour() + ' ' + dtNow.day() +
                ' ' + dtNow.month() + ' ? ' + dtNow.year();

            for(CronTrigger jobAgendado : [SELECT Id FROM CronTrigger Where CronJobDetail.Name = : EX3_SchIntegracaoObterTokenGTWOut.AG_NAME])
                system.abortJob(jobAgendado.id);

             MapVariaveisIntegracao.put('Classe - Metodo ', +'EX3_SchIntegracaoObterTokenGTWOut - agendarProximaExecucao'+'\n');
             MapVariaveisIntegracao.put('Cronograma ', + cronStr);

            system.schedule(EX3_SchIntegracaoObterTokenGTWOut.AG_NAME, cronStr, new EX3_SchIntegracaoObterTokenGTWOut());
        }catch(Exception ex) {
            lException = ex.getMessage();
            MapVariaveisIntegracao.put('Erro ao agendar',ex.getMessage());
            SA7_CustomdebugLog.logError('EX3', 'EX3_IntegracaoObterTokenGTWOut', 'agendarProximaExecucao', 'Erro ao agendar ' +
                                        lException, ex, MapVariaveisIntegracao);
        }finally{
            if(String.isBlank(lException)){
                MapVariaveisIntegracao.put('Sucesso ao agendar',EX3_SchIntegracaoObterTokenGTWOut.AG_NAME);
                SA7_CustomdebugLog.logWarn('EX3', 'EX3_IntegracaoObterTokenGTWOut', 'agendarProximaExecucao', 'Sucesso ao agendar', MapVariaveisIntegracao);
            }
        }
    }

    private static EX3_Parametrizacao__c obterConfiguracaoToken(String pConfigRecordId, String pKeyToken)
    {
        EX3_Parametrizacao__c returnValue;
        list<EX3_Parametrizacao__c> listaConfiguracoes = new list<EX3_Parametrizacao__c>();

        listaConfiguracoes = [Select id, EX3_Access_token__c, EX3_Active__c, EX3_Expires_in__c,
                              EX3_Refresh_token__c, EX3_Scope__c, EX3_Token_type__c,EX3_Token_external__c
                              From EX3_Parametrizacao__c
                              Where RecordtypeId  =: pConfigRecordId
                              AND EX3_Token_external__c =: pKeyToken];

        if(!listaConfiguracoes.isEmpty()) returnValue = listaConfiguracoes[0];
        else returnValue = new EX3_Parametrizacao__c(RecordtypeId = pConfigRecordId,EX3_Token_external__c = pKeyToken);

        return returnValue;
    }

    @TestVisible private static void criarPostagem(string p_mensagem)
    {
        id idParent;
        list<CollaborationGroup> listaGrupos = new list<CollaborationGroup>();
        listaGrupos = [SELECT Id FROM CollaborationGroup Where Name = 'Token STS' Limit 1];
        idParent = (listaGrupos.isEmpty()?UserInfo.getUserId():listaGrupos[0].Id);

        FeedItem post = new FeedItem();
        post.ParentId = idParent;
        post.Body = p_mensagem;
        insert post;
    }

}