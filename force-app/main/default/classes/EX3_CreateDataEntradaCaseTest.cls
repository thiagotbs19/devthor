/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de teste responsável por cobrir e testar o fluxo que seta o valor do campo
* Data de Entrada igual ao valor da Data de Criação
*
*  

* Empresa: everis do Brasil
* Autor: Isabella Tannús
*
********************************************************************************/

@isTest
public class EX3_CreateDataEntradaCaseTest {

    @isTest
    public static void caseBeforeInsertTest(){

        
        Case lCase = new Case();
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
        lCase.EX3_Motivo_Situacao__c = '14';   
        lCase.EX3_Numero_do_Protocolo__c = '2019001';
        lCase.EX3_Numero_do_Processo__c = '2019001';
        lCase.EX3_Prioridade__c = 'Urgente';

        Test.startTest();  
        Database.insert(lCase);
        Test.stopTest();

		List<Case> lLstCase = [SELECT Id, CreatedDate, EX3_Data_de_Entrada__c FROM Case];     
        system.assert(!lLstCase.isEmpty(), 'Não foram encontrados registros de Protocolo.'); 
        lCase = lLstCase[0]; 
        system.assertEquals(lCase.CreatedDate, lCase.EX3_Data_de_Entrada__c);
        
    }
}