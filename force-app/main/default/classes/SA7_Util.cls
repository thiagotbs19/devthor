/********************************************************************************
Name:  SA7_Util
Copyright © 2019 COE
=================================================================================
Propósito: Esta classe tem como objetivo centralizar os métodos disponibilizados pelo 
Framework do CoE.
Funcionalidades disponíveis: 
Log
Captura de endpoint por ambiente
Identificação de Sandbox

=================================================================================
Histórico                                                            
-------                                                            
VERSÃO: 1.0                       AUTOR:TCSKRBJ
DATA: 10/06/19
DESCRIÇÃO: Versão Inicial
*********************************************************************************/
public class SA7_Util {
    
    public enum TipoOperacao {CALLOUT, DML, EMAIL, FUTURE, QUERY, QUEUEABLE, AGGREGATEQUERY}
    //Método para retornar o endpoint de acordo com o ambiente.
    /*public static String retornaEndpoint(String integracao)
{
//Logica para usar Cache
SA7_EndpointConfiguration__mdt configEndpoint;
String endpoint;
if((Limits.getDMLRows() < Limits.getLimitDMLRows()) && (Limits.getDMLStatements() < Limits.getLimitDMLStatements()))
{
configEndpoint  = [SELECT SA7_Endpoint_Desenvolvimento__c, SA7_Endpoint_Homologacao__c, SA7_Endpoint_Producao__c FROM SA7_EndpointConfiguration__mdt WHERE MasterLabel= :integracao];
}

//Se não for Sandbox, é produção.
if (!ambienteSandbox())
{
endpoint = configEndpoint.SA7_Endpoint_Producao__c;
}
else
{
Map<String,System.OrgLimit> limitsMap = OrgLimits.getMap();
System.OrgLimit dataStorageMB = limitsMap.get('DataStorageMB');
switch on dataStorageMB.getLimit()
{
//Sandbox Parcial - 5GB
when 5120 {
endpoint = configEndpoint.SA7_Endpoint_Homologacao__c;
}
//Sandbox Devpro - 1GB
when 1024 {
endpoint = configEndpoint.SA7_Endpoint_Homologacao__c;
}
//Sandbox Dev - 200MB
when 200 {
endpoint = configEndpoint.SA7_Endpoint_Desenvolvimento__c;
}
//Sandbox Full
when else {
endpoint = configEndpoint.SA7_Endpoint_Homologacao__c;
}
}
}
//Se estiver rodando para testes, vai devolver o endpoint de desenvolvimento
if (System.Test.isRunningTest())
{
endpoint = configEndpoint.SA7_Endpoint_Desenvolvimento__c;
}
return endpoint;        
}*/
    
    //Retorna o RecordTypeID a partir do nome
    @InvocableMethod(label='Atualiza RecordType ID' description='Deve-se usar fórmula para mandar o valor da variável separado por ";"')  
    public static void getRecordTypeIdForObject(List<String> parametros) {
        
        //Primeira posicao deve ser o nome do objeto
        //Segunda posição deve ser o nome do recordtype
        //Terceira posicao deve ser o recordid
        List<String> valores = parametros.get(0).split(';');
        
        //Obter o RecordTypeID do registro
        Id recordtypeID = ((SObject)Type.forName(valores.get(0)).newInstance())
            .getSObjectType()
            .getDescribe()
            .getRecordTypeInfosByDeveloperName()
            .get(valores.get(1))
            .getRecordTypeId();
        
        /* Chama descrição global para obter o mapa de objetos. */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        
        /* Obtem o valor do token para o Sobject baseado no type. */
        Schema.SObjectType st = gd.get(valores.get(0));
        
        /* Instancia o Sobject baseado no valor do token. */
        Sobject s = st.newSobject();
        
        s.id = valores.get(2);
        s.put('RecordTypeId', recordtypeID);
        
        update s;
        
    }
    
    //Determina se o ambiente onde está sendo executado é produtivo
    public static boolean ambienteSandbox(){
        //usar Cache para evitar selects toda hora.
        if((Limits.getDMLRows() < Limits.getLimitDMLRows()) && (Limits.getDMLStatements() < Limits.getLimitDMLStatements()))
        {
            Organization org = [SELECT Id, InstanceName, IsSandbox, Name, OrganizationType FROM Organization LIMIT 1];
            return org.IsSandbox;
        }
        return true;
    }
    
    public static boolean validaLimites(TipoOperacao tipoOperacao, Integer qtidadeRegistros ) {
        switch on tipoOperacao 
        {
            when CALLOUT
            {
                if (Limits.getCallouts() < Limits.getLimitCallouts())
                {
                    return true;
                }
                return false;
            }
            when FUTURE
            {
                if (Limits.getFutureCalls()<Limits.getLimitFutureCalls())
                {
                    return true;
                }
                return false;
            }
            when DML
            {
                if (qtidadeRegistros == null)
                {
                    qtidadeRegistros = 1;
                }
                if ((Limits.getDMLRows() + qtidadeRegistros <= Limits.getLimitDMLRows()) && (Limits.getDMLStatements() < Limits.getLimitDMLStatements())){
                    return true;
                }
                return false;
            }
            when EMAIL
            {
                if (Limits.getEmailInvocations() < Limits.getLimitEmailInvocations())
                {
                    return true;
                }
                return false;
            }  
            when QUEUEABLE
            {
                if (Limits.getQueueableJobs() < Limits.getLimitQueueableJobs())
                {
                    return true;
                }
                return false;
            }
            when QUERY
            {
                if (Limits.getQueries() < Limits.getLimitQueries())
                {
                    return true;
                }
                return false;
            }
            when AGGREGATEQUERY
            {
                if (Limits.getAggregateQueries() < Limits.getLimitAggregateQueries())
                {
                    return true;
                }
                return false;
            }
            when else
            {
                return true;   
            }
        }
    }
    
    //Log com Nível de Informação - Recomendação para uso em Debug
    public static void logInfo(String sigla, String apexClass, String method, String message, Map<String, String> variaveis){    
        SA7_CustomDebugLog.logInfo(sigla, apexClass, method, message, variaveis);
    }
    
    //Log com Nível de Aviso - Recomendação para uso em falha de processo de negócio. Ex.: Saldo menor que zero. 
    public static void logWarn(String sigla, String apexClass, String method, String message, Map<String, String> variaveis){
        SA7_CustomDebugLog.logWarn(sigla, apexClass, method, message, variaveis);
    }
    
    //Log com nível de Erro - Recomendação para uso em falha de processamento - Ex.: Callout, AsyncJobs, NullPointer, etc.
    public static void logError(String sigla, String apexClass, String method, String message, Exception ex, Map<String, String> variaveis){
        SA7_CustomDebugLog.logError(sigla, apexClass, method, message, ex, variaveis); 
    }
    
    //Log com nível de Erro - Recomendação para uso em falha de processamento de DML.
    public static void logError(String Sigla, String ApexClass, String Method, String Message, DmlException ex, Map<String, String> variaveis){
        SA7_CustomDebugLog.logError(sigla, apexClass, method, message, ex, variaveis);
    }
    
    //Log com nível de Erro - Recomendação para uso em falha de processamento de email.
    public static void logError(String Sigla, String ApexClass, String Method, String Message, EmailException ex, Map<String, String> variaveis){
        SA7_CustomDebugLog.logError(sigla, apexClass, method, message, ex, variaveis);
    } 
    
}