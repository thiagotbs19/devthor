/*********************************************************************************
*                                    Itaú - 2019
*
* Classe resposavel por disponibilizar atributos para integracoes
*
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
global with sharing class EX3_ObjectFactory 
{
    public class IncluirProtocolo
    {
        public String codigo_protocolo_externo {get; set;}
        public String codigo_unico_processo {get;set;}
        public DateTime data_protocolo_documento {get;set;}
        public Decimal codigo_canal_entrada_documento {get;set;}
        public Decimal codigo_motivo_situacao {get;set;}
        public Date data_situacao {get;set;}
        public String operador {get;set;}
        public Decimal codigo_operacao {get;set;}
        public Decimal indicador_protocolo {get;set;} 
    }

    public class AtualizarProtocolo
    {
        public Decimal id_protocolo {get;set;}
        public String codigo_protocolo_externo {get;set;}
        public Decimal codigo_motivo_situacao {get;set;}
        public String operador {get;set;}
        public Decimal codigo_operacao {get;set;}
        public Decimal indicador_protocolo {get;set;}  
    }

    public class IncluirDocumento 
    {
        public String nome_documento {get;set;}
        public String codigo_documento_plataforma_externa {get;set;}
        public String operador_conglomerado {get;set;}
    }

    public RequestEnvioArquivo request_envio_arquivo {get; set;}
    public DadosArquivos dados_arquivo {get; set;}

    public class DadosArquivos
    {
        public String id_arquivo          {get; set;}
        public String tipo_arquivo        {get; set;}
        public String tamanho_arquivo       {get; set;}
    }

    public class RequestEnvioArquivo
    {
        public String origem            {get; set;}
        public String id_protocolo        {get; set;}
        public String servico         {get; set;}
        public List<DadosArquivos> arquivos   {get; set;}
    }


    public class RequestEnvioProtocolo
    {
        public String num_protocolo       {get; set;}
        public String status          {get; set;}
        public Boolean estou_ciente       {get; set;}
    }

    public class RequestAlteracaoStatusProtocolo
    {
        public String id_protocolo              {get; set;}
        public String motivo_situacao           {get; set;}
    }

    public class RequestCitacaoEletronicaBuscaDuplicidade
    {
        public String id_protocolo              {get; set;}
        public String numero_processo           {get; set;}
    }

    public class RequestBuscaCartaPrecatoria
    {
        public String nome {get;set;}
        public String numero_processo {get;set;}
        public String cpf {get;set;}
        public String cnpj {get;set;}
        public String id_protocolo{get;set;}

        public Object get(String aField)
        {
            Map<String, Object> lMap = new Map<String,Object>
            {
                'nome' => this.nome,
                'numero_processo' => this.numero_processo,
                'cpf' => this.cpf,
                'cnpj' => this.cnpj,
                'id_protocolo' => this.id_protocolo
            };
            return (lMap.get(aField) != null)
                   ? lMap.get(aField)
                   : 'Valor não existe' ;
        }

        public Map<String, Object> getMap()
        {
            return new Map<String,Object>
            {
                'nome' => this.nome,
                'numero_processo' => this.numero_processo,
                'cpf' => this.cpf,
                'cnpj' => this.cnpj,
                'id_protocolo' => this.id_protocolo
            };
        }

        public Map<String,String> getMapBySalesforceApi(String aSalesforceApi)
        {
            return new Map<String,String>
            {
                'EX3_Autor__c' => 'nome',
                'EX3_Numero_do_Processo__c' => 'numero_processo',
                'EX3_CPF_CNPJ__c' => 'cpf_cnpj',
                'Id' => 'id_protocolo'
            };
        }

        public String getIntegrationApi(String aSalesforceApi)
        {
            Map<String, String> lMap = new Map<String,String>
            {
                'EX3_Autor__c' => 'nome',
                'EX3_Numero_do_Processo__c' => 'numero_processo',
                'EX3_CPF_CNPJ__c' => 'cpf_cnpj',
                'Id' => 'id_protocolo'
            };

            return (lMap.get(aSalesforceApi) != null)
                   ? lMap.get(aSalesforceApi)
                   : 'Valor não existe' ;
        }
    }

    public class RequestLiminar
    {
        public String numero_processo {get;set;}
        public String id_protocolo{get;set;}
    }

   public class RequestBuscaDuplicidade
    {
       public String numero_processo {get;set;}
       public String id_protocolo{get;set;}

       public Object get(String aField)
        {
            Map<String, Object> lMap = new Map<String,Object>
            {
                'numero_processo' => this.numero_processo,
                'id_protocolo' => this.id_protocolo
            };
            return (lMap.get(aField) != null)
                   ? lMap.get(aField)
                   : 'Valor não existe' ;
        }

        public Map<String, Object> getMap()
        {
            return new Map<String,Object>
            {
                'numero_processo' => this.numero_processo,
                'id_protocolo' => this.id_protocolo
            };
        }

        public Map<String,String> getMapBySalesforceApi(String aSalesforceApi)
        {
            return new Map<String,String>
            {
                'EX3_Numero_do_Processo__c' => 'numero_processo',
                'Id' => 'id_protocolo'
            };
        }

        public String getIntegrationApi(String aSalesforceApi)
        {
            Map<String, String> lMap = new Map<String,String>
            {
                'EX3_Numero_do_Processo__c' => 'numero_processo',
                'Id' => 'id_protocolo'
            };

            return (lMap.get(aSalesforceApi) != null)
                   ? lMap.get(aSalesforceApi)
                   : 'Valor não existe' ;
        }
    }
}