/*********************************************************************************
*                                    Itaú - 2019
*
* Classe que auxilia o Desenvolvimento de Classes de Teste.
* Empresa: everis do Brasil
* Autor: Thiago Barbosa de Souza
*
********************************************************************************/

public with sharing class EX3_BI_DataLoad { 

    public static final String PROF_ADMIN = 'Administrador do sistema';
    public static final map<String, String> MAP_PERFIS;
    public static final String PROF_ANALISTA_OP = 'Analista Operacional';

    static {
        // perfis de interesse
        map<String, String> lMapPerfis = new map<String, String>();
        set<String> lSetProfile = new set<String>{
                PROF_ADMIN, PROF_ANALISTA_OP
        };
        for (Profile iProf : [
                select Id, Name, PermissionsAuthorApex
                from Profile
                where ((Name = :lSetProfile) or (Name like '%admin%' and PermissionsAuthorApex = true))
        ]) {
            String lName = (iProf.Name.toLowerCase().contains('admin') && iProf.PermissionsAuthorApex) ? PROF_ADMIN : iProf.Name;
            lMapPerfis.put(lName, iProf.Id);
        }
        MAP_PERFIS = lMapPerfis;
        for (String lProfName : lSetProfile) {
            system.assert(lMapPerfis.get(lProfName) != null, 'Perfil ' + lProfName + ' nao foi encontrado!');
        }
    }

    @TestVisible
    private static User getDefUser(String aProfId) {
        User sObjUser = new User(Alias = 'sfa', Email = 'sfa@sfa.com.br', EmailEncodingKey = 'UTF-8',
                LastName = 'UserTest', LanguageLocaleKey = 'pt_BR', LocaleSidKey = 'pt_BR',
                TimeZoneSidKey = 'America/Sao_Paulo', UserName = ('sfa' + (Math.random() * 8999999 + 1000000).round() + '@sfa.com.br')
        );
        if (String.isNotBlank(aProfId)) sObjUser.ProfileId = aProfId;

        return sObjUser;
    }

    // RLdO: fabrica o proprio usuario que executa os testes; para uso com system.runAs
    public static User getThisUser() {
        User lUsr = getDefUser(null);
        lUsr.Id = UserInfo.getUserId();
        return lUsr;
    }

    public static User getUser(String aProfileName) {
        String lProfileId = MAP_PERFIS.get(aProfileName);
        return getDefUser(lProfileId);
    }

    public static User getUser() {
        return getUser(PROF_ADMIN);
    }

    public static Account getAccount() {
        Account lAccount = new Account();
        lAccount.Name = 'Teste Account';
        return lAccount;
    }

    public static User getUserAnalistaOperacional() {
        return getUser(PROF_ANALISTA_OP);
    }

    public static Case getCase() {
        Case lCase = new Case();
        lCase.Status = 'New';

        return lCase;
    }

    public static EX3_Cadastro__c getCadastro(Case aCase) {

        EX3_Cadastro__c lCadastro = new EX3_Cadastro__c();
        lCadastro.OwnerId = UserInfo.getUserId();
        lCadastro.EX3_Caso__c = aCase.Id;
        return lCadastro;
    }

    public static EX3_Subsidios__c getCapturaDocumentos(Case aCase) {

        EX3_Subsidios__c lSubsidios = new EX3_Subsidios__c();
        lSubsidios.EX3_Status__c = 'Doc Pendente';
        lSubsidios.EX3_Caso__c = aCase.Id;
        lSubsidios.EX3_Numero_do_Processo__c = '004';
        lSubsidios.EX3_Numero_Protocolo__c = '002';

        return lSubsidios;
    }

    public static EX3_ADJ__c getADJ(Case aCase) {
        EX3_ADJ__c lADJ = new EX3_ADJ__c();
        lADJ.EX3_Numero_da_Pasta__c = '003';
        lADJ.OwnerId = UserInfo.getUserId();
        lADJ.EX3_Caso__c = aCase.Id;

        return lADJ;
    }

    public static Case createCase() {
        Case caso = new Case();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('EX3 Consignado').getRecordTypeId();
        caso.Status = 'Novo';
        caso.Origin = 'E-mail';
        return caso;
    }

    public static EX3_Calculo__c getCalculo(Case aCase) {

        EX3_Calculo__c lCalculo = new EX3_Calculo__c();
        lCalculo.EX3_Caso__c = aCase.Id;

        return lCalculo;
    }
    public static EX3_Tipo_Documento__c createDocument(Case aCase, EX3_Subsidios__c aSubsidio) {
        EX3_Tipo_Documento__c documento = new EX3_Tipo_Documento__c();
        documento.EX3_Caso__c = aCase.Id;
        documento.EX3_Status_do_Documento__c = 'Documento validado';
        documento.EX3_Subsidios__c = aSubsidio.Id;
        documento.EX3_Arquivo_Anexado__c = true;
        return documento;
    }

  public static Contract createContrato(Account aAccount, EX3_Subsidios__c aSubsidio) {
    Contract contrato = new Contract(Status = 'Draft');
    contrato.EX3_Captura_de_documentos__c = aSubsidio.Id;
    contrato.AccountId = aAccount.Id;
    return contrato;

  }
    
    public static EX3_DefinicaoEstrategia__c getDefEstrategica(Case lCase){
        EX3_DefinicaoEstrategia__c lDefEstrategia = new EX3_DefinicaoEstrategia__c();
        lDefEstrategia.EX3_StatusEstrategia__c = 'Documento Pendente'; 
        lDefEstrategia.OwnerId = UserInfo.getUserId();
        
        return lDefEstrategia;
    }
    
    public static EX3_Laudo__c getLaudo(Case lCase){
        EX3_Laudo__c lLaudo = new EX3_Laudo__c();
        lLaudo.EX3_Caso__c = lCase.Id;
        
        return lLaudo;
    }

  public static Account createEnvolvido(){
    Account lAccount = new Account();
    lAccount.Name = 'Teste Account';
    return lAccount;
  }

  public static Task getTask(){
 
      Task lObjTask = new Task();
      lObjTask.Description = 'Description';
      lObjTask.Subject = 'Ligar';
      lObjTask.Status = 'Open';  
      lObjTask.Priority = 'Normal';
      
      return lObjTask;

  }
    
  
}