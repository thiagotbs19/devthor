/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável para que a Data de Entrada receba a Data de Criação do 
* Protocolo
*
*Apex Trigger : EX3_Cadastro__c
*Apex Class: EX3_CadastroBeforeInsert
*  

* Empresa: everis do Brasil
* Autor: Isabella Tannús
*
********************************************************************************/

public with sharing class EX3_CadastroBeforeInsert {
    public static void atualizaDataEntrada(){
        
        list<EX3_Cadastro__c> lstCadastro = (list<EX3_Cadastro__c>) Trigger.new;
        Set<Id> lSetIdsProtocolos = new Set<Id>(); 
        for(EX3_Cadastro__c c : lstCadastro){ 
            lSetIdsProtocolos.add(c.EX3_Caso__c); 
        }
        if (lSetIdsProtocolos.isEmpty()) { return; } 
        
        Map<Id, Case> lMapCases = new Map<Id, Case>([SELECT Id, CreatedDate 
        FROM Case WHERE Id IN :lSetIdsProtocolos]); 
        for(EX3_Cadastro__c iCadastro : lstCadastro){ 
            if (!lMapCases.containsKey(iCadastro.EX3_Caso__c)) { continue; }
            Case lProtocolo = lMapCases.get(iCadastro.EX3_Caso__c);
            iCadastro.EX3_Data_da_Entrada__c = lProtocolo.CreatedDate.date();
        }
    }
}