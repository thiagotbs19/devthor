/*********************************************************************************
*                                    Itaú - 2019
*
* Classe que cria um Link dos arquivos inseridos pelo Integrador EX3
* 
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
public with sharing class EX3_CriaLinkArquivos {
 
    public static void execute()
    {
        if (!Trigger.isExecuting) { return; }
        
		Integration_User__mdt lIntegrationUser = [select ID_Usuario__c
                                                  FROM Integration_User__mdt
                                                  WHERE DeveloperName =: 'EX3_Integracao'];
        
        if(!Test.isRunningTest() && lIntegrationUser.ID_Usuario__c != UserInfo.getUserId()) { return; }
        
        List<ContentVersion> lLstContentVersion = new List<ContentVersion>();
        Set<String> lSetExternalkey = new Set<String>();
        List<String> lLstConversion = new List<String>();
        for(ContentVersion iConVersion : (List<ContentVersion>) Trigger.new)
        {
            if(String.isNotBlank(iConVersion.Description)) lSetExternalkey.add(iConVersion.Description); 
            lLstContentVersion.add(iConVersion);
        }
        
		if(lLstContentVersion.isEmpty() || lSetExternalkey.isEmpty()) return; 

        map<String,String> mapIdChaveExterna = new map<String,String>();
        
        for(Case iCase : [Select Id, EX3_Numero_do_protocolo__c 
                                      From Case
                                      Where EX3_Numero_do_protocolo__c in: lSetExternalkey])
        {
            mapIdChaveExterna.put(iCase.EX3_Numero_do_protocolo__c, iCase.Id);
        }
                
        list<ContentDocumentLink> listContendDocLinkToInsert = new list<ContentDocumentLink>();
        
        for(ContentVersion iConVersion : lLstContentVersion)
        {
            String lIdProtocolo = mapIdChaveExterna.get(iConVersion.Description);
            ContentDocumentLink lContentLink = new ContentDocumentLink(); 
            lContentLink.ContentDocumentId = iConVersion.ContentDocumentId;
            lContentLink.LinkedEntityId = lIdProtocolo;
            lContentLink.ShareType = 'I';
            lContentLink.Visibility = 'AllUsers';
            
            listContendDocLinkToInsert.add(lContentLink);
        }

       if(!listContendDocLinkToInsert.isEmpty()) Database.insert(listContendDocLinkToInsert);
    }    
}