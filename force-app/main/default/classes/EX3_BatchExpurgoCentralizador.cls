/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel pela implementação da rotina para expurgo de registros no objeto centralizador
* 
* Empresa: everis
* Autor: Marcelo Seibt de Oliveira
*
********************************************************************************/


global class EX3_BatchExpurgoCentralizador implements Database.Batchable<sObject> {

   private static final String query = 'SELECT Id FROM EX3_Centralizador__c';

   global Database.QueryLocator start( Database.BatchableContext bc ) {
      return Database.getQueryLocator(query);
   }

   global void execute( Database.BatchableContext bc, List<EX3_Centralizador__c> scope ){
      delete scope;
   }

   global void finish( Database.BatchableContext bc ) {}

}