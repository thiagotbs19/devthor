/*********************************************************************************
*                                    Itaú - 2020
*
* Classe responsável por atualizar o registro de Fluxo DOP/Retornar para Fila de
*Cumprimento de Pagamento.  
*
* Autor: Thiago Barbosa 
* Aura Component Bundle: EX3_ADJAceitePagamento 
* Test Class:  
*
********************************************************************************/

public with sharing class EX3_ADJAceitePagamento {
   


	@AuraEnabled
    public static Boolean getHasPermissionSet(){
        Boolean lhasPermission = FeatureManagement.checkPermission('EX3_Criar_Tarefa'); 
        
        return lHasPermission;
   } 
    
    @AuraEnabled
    public static string updateDOPPagSolicitado(String aRecordId){

        EX3_ADJ__c lADJ;
        try{
        if(aRecordId == null){
            return '';  
        }

        lADJ = [SELECT Id, EX3_Status__c FROM EX3_ADJ__c WHERE Id =: aRecordId];
  
        lADJ.EX3_Status__c = EX3_Utils.STATUS_PAG_SOLICITADO; 

        Database.update(lADJ);   
        
    }
    catch(DmlException ex) { 
        SA7_CustomdebugLog.logError('EX3', 'EX3_ADJAceitePagamento', 'updateDOPPagSolicitado', 'Erro ao atualizar ADJ ' +
                                    ex.getMessage(), ex, new Map<String, String>()); 
    }
    catch (Exception ex) 
    {  
        SA7_CustomdebugLog.logError('EX3', 'EX3_ADJAceitePagamento', 'updateDOPPagSolicitado', 'Erro ao atualizar ADJ '+ ex.getMessage(), ex, null);
    }  
    return JSON.serialize(lADJ); 
}


    @AuraEnabled
    public static string getQueueCumprimentoPag(String aRecordId){
        
        EX3_ADJ__c lADJ;  
        try{ 
        if(aRecordId == null){
            return ''; 
        } 

        List<Group> lLstGroup = [SELECT Id, Name, DeveloperName FROM Group 
            WHERE Type='Queue'  
            AND DeveloperName =: EX3_Utils.FILA_CUMPRIMENTO_PAGAMENTO];
  
        lADJ = [SELECT Id, OwnerId, RecordTypeId, EX3_Status__c FROM EX3_ADJ__c WHERE Id =: aRecordId];
  
        lADJ.EX3_Status__c = EX3_Utils.STATUS_DEVOLVIDO_ACEITE;  
        lADJ.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBP');
        lADJ.OwnerId = lLstGroup[0].Id;   

        Database.update(lADJ);    
        
    }catch(DmlException ex) { 
            SA7_CustomdebugLog.logError('EX3', 'EX3_ADJAceitePagamento', 'getQueueCumprimentoPag', 'Erro ao atualizar ADJ ' +
                                        ex.getMessage(), ex, new Map<String, String>()); 
        }
        catch (Exception ex) 
        {  
            SA7_CustomdebugLog.logError('EX3', 'EX3_ADJAceitePagamento', 'getQueueCumprimentoPag', 'Erro ao atualizar ADJ '+ ex.getMessage(), ex, null);
        }   
        return JSON.serialize(lADJ);  

    }
}