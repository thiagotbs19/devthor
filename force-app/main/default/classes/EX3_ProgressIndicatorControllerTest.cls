/*********************************************************************************
*                                    Itaú - 2020
*
* 
*
*
*Apex Class: EX3_ProgressIndicatorController
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/
@isTest
public without sharing class EX3_ProgressIndicatorControllerTest {
    
    
    @testSetup
    private static void testCreateData(){
		
		User lUser = EX3_BI_DataLoad.getUser();

		Database.insert(lUser);

		Case lCase = EX3_BI_DataLoad.getCase();

		Database.insert(lCase);

		EX3_Cadastro__c lCadastro = EX3_BI_DataLoad.getCadastro(lCase);
		lCadastro.EX3_Caso__c = lCase.Id;
		Database.insert(lCadastro);        

    }
    
    @isTest static void testgetCurrentStage(){
        User lUser = [SELECT Id FROM User LIMIT 1];


		Case lCase =[SELECT Id FROM Case LIMIT 1];


		EX3_Cadastro__c lCadastro = [SELECT Id FROM EX3_Cadastro__c LIMIT 1];
        
        
        Test.startTest();
        	Map<String,Object> lMapReturn = EX3_ProgressIndicatorController.getCurrentStage(lCadastro.Id);
        Test.stopTest();
        System.assert(!lMapReturn.isEmpty());
        
    }

}