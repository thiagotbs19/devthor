/**
 * Created by jmariane on 2020-04-08.
 */

@IsTest
global with sharing class EX3_IntegracaoCadastroRestMock implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setStatusCode(200);
        return response;
    }
}