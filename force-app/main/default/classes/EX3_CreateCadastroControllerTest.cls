/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável por atualizar o registro de Protocolo e criar Cadastro. 
*
*Apex Class: EX3_CreateCadastroControllerTest
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/
@isTest
public with sharing class EX3_CreateCadastroControllerTest {
    

    @TestSetup
    public static void createCadastroData(){
        
        User lUser = EX3_BI_DataLoad.getUser();
        lUser.FuncionalColaborador__c =  'funcional';
        lUser.EX3_Pertence__c = '2'; 
        Database.insert(lUser);

        Case lCase = new Case();
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
        lCase.EX3_Motivo_situacao__c = '14'; 
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Protocolo__c = '2019001';
        lCase.EX3_Numero_do_Processo__c = '2019001';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();
        Database.insert(lCase);



    }

    @isTest static void testgetFieldsProtocol(){

         User lUser = [SELECT Id, FuncionalColaborador__c, EX3_Pertence__c FROM User LIMIT 1];  
        
         Case lCase = [SELECT Id FROM Case LIMIT 1];


        Test.startTest();
           Map<String, Object> lMapReturn = EX3_CreateCadastroController.getFieldsCase(lCase.Id); 
        Test.stopTest();
        System.assert(lMapReturn != null, 'O Protocolo foi atualizado');
    } 
 
      @isTest static void testRedirectToObject(){

        User lUser = [SELECT Id, FuncionalColaborador__c, EX3_Pertence__c FROM User LIMIT 1];  
         
        Case lCase = [SELECT Id FROM Case LIMIT 1];


        Test.startTest();  
            String lResult = EX3_CreateCadastroController.redirectToObject(lCase.Id); 
        Test.stopTest();
        System.assert(lResult != null, 'O Cadastro foi criado'); 
    }

    @isTest static void testRedirectCadastroNull(){

         Test.startTest();
         Map<String, Object> lMapReturn = EX3_CreateCadastroController.getFieldsCase(null);
         Test.stopTest();  
         System.assert(lMapReturn == null, 'O campo Situação não foi atualizado');
    } 
}