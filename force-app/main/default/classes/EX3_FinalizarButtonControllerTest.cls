/*********************************************************************************
*                                    Itaú - 2020
*
* Autor: Bruno Sampainho Petiti
* Aura Component Bundle: EX3_FinalizarCalculo
*
********************************************************************************/

@isTest
private with sharing class EX3_FinalizarButtonControllerTest {
    @testSetup
    static void dataSetup(){
        Id lCaseConsignadoId =
                Schema.SObjectType.Case.getRecordTypeInfosByName()
                        .get('EX3 Consignado').getRecordTypeId();
        Case lCase = new Case(RecordTypeId = lCaseConsignadoId, Origin = 'E-mail');
        insert lCase;
        EX3_Calculo__c lCalculo = new EX3_Calculo__c(EX3_Caso__c = lCase.Id);
        insert lCalculo;

    }
    @isTest
    static void noAttachmentsTest(){  
        EX3_Calculo__c lCalculo = [SELECT Id,EX3_Caso__c,Name FROM EX3_Calculo__c LIMIT 1];
        String fieldToQuery = 'Id' + ', ' + 'EX3_Caso__c' + ', ' + 'Name';
        Test.startTest();  
        Boolean valueReturned = EX3_FinalizarButtonController.getAttachment(lCalculo.Id,fieldToQuery);
        Test.stopTest(); 
        System.assertEquals(false,valueReturned, 'There is no attachments to this record');
    }
    @isTest
    static void hasAttachmentsTest(){
        EX3_Calculo__c lCalculo = [SELECT Id, Name, EX3_Caso__c FROM EX3_Calculo__c];
        String fieldToQuery = 'Id' + ', ' + 'EX3_Caso__c' + ', ' + 'Name';
        ContentVersion content = new ContentVersion();
        content.Title='Header_Picture1'; 
        content.Tipo_de_Documento__c = '1';
        content.PathOnClient='/' + content.Title + '.jpg';
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
        content.VersionData=bodyBlob;
        //content.LinkedEntityId=sub.id;
        content.origin = 'H';
        insert content;
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=lCalculo.EX3_Caso__c;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
        contentlink.ShareType = 'I';
        contentlink.Visibility = 'AllUsers';
        insert contentlink;
        ContentDocument contentDoc = new ContentDocument(id = contentlink.ContentDocumentId);
        contentDoc.Description = (String)lCalculo.Name;
        upsert contentDoc;
        Test.startTest();
        Boolean valueReturned = EX3_FinalizarButtonController.getAttachment(lCalculo.Id,fieldToQuery);
        Test.stopTest();  
        System.assertEquals(true,valueReturned, 'There is an attachments to this record');
    }


    @isTest static void testvalidateFields(){      
        EX3_Calculo__c lCalculo = [SELECT Id,EX3_Caso__c, EX3_Carteira__c,Name FROM EX3_Calculo__c LIMIT 1];
        String lfieldsToValidate = 'Id' + ',' + 'Name';    

        Test.startTest();  
            List<String> lLstValidateFields = EX3_FinalizarButtonController.validateFields(lCalculo.Id, lfieldsToValidate);  
        Test.stopTest();    
        System.assert(!lLstValidateFields.isEmpty(), 'Não foi realizado a validação de campos.'); 

    }

	@isTest static void testGetQueue(){

		EX3_Calculo__c lCalculo = [SELECT Id FROM EX3_Calculo__c LIMIT 1];

		Test.startTest();
			SObject lObject = EX3_FinalizarButtonController.getQueue(lCalculo.Id);  
		Test.stopTest();
		System.assert(lObject != null, 'A mensagem foi exibida'); 
        }
        
    @isTest static void testHasPermission(){

        Test.startTest();
            Boolean lHasPermission = EX3_FinalizarButtonController.getHasPermissionSet();
        Test.stopTest(); 
        System.assert(!lHasPermission, 'Não há permissão atribuída a este perfil');
    }

    @isTest static void testgetRecordTypeOBF(){
        Case lCase = EX3_BI_DataLoad.getCase();

        Database.insert(lCase);

        EX3_ADJ__c lADJ = EX3_BI_DataLoad.getADJ(lCase);
        lADJ.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF');

        Database.insert(lADJ); 
        Test.startTest();
            Boolean lRecordType = EX3_FinalizarButtonController.getRecordTypeOBF(lADJ.Id);
        Test.stopTest(); 
        System.assert(lRecordType, 'Tipo de Registro atribuído ao registro');
    }
}