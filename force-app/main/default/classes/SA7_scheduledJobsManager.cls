public class SA7_scheduledJobsManager {
/********************************************************************************
 Name:  SA7_scheduledJobsManager
 Copyright © 2019 COE
=================================================================================
Purpose: The purpose oif this class is to monitor the scheduled jobs running in order
to identify if a job fails to run.
=================================================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
  1.0    Vinicius Silva 13/05/19       Created         Creating job monitor
*********************************************************************************/

    public static void verifyJobStatus(){
        
        Map<String, String> week_days = new Map<String, String>();
        week_days.put('Monday','Segunda');
        week_days.put('Tuesday','Terça');
        week_days.put('Wednesday','Quarta');
        week_days.put('Thursday','Quinta');
        week_days.put('Friday','Sexta');
        week_days.put('Saturday','Sábado');
        week_days.put('Sunday','Domingo');
        Datetime dt = System.now();
        
        List<CronTrigger> scheduledJobs = [SELECT CronJobDetail.Name,TimesTriggered 
                                             FROM CronTrigger 
                                            WHERE CronJobDetail.JobType = '7'];
		
        List<SA7_Scheduled_Jobs_Monitor__c> jobsToMonitor = [SELECT Name, 
                                                                SA7_Number_of_checks__c, 
                                                         		SA7_Actual_Result__c,
                                                             	SA7_Dias_de_Execucao__c
                                                           FROM SA7_Scheduled_Jobs_Monitor__c];
        integer actualResult = 0;
        
        for(SA7_Scheduled_Jobs_Monitor__c job: jobsToMonitor){

            if(job.SA7_Dias_de_Execucao__c.split(';',0).contains(week_days.get(dt.format('EEEE')))){
                
            
            
                for(CronTrigger cronJob: scheduledJobs){
                    if(cronJob.CronJobDetail.Name.startsWith(job.Name)){
                        actualResult = actualResult+cronJob.TimesTriggered;
                    }
                }
                job.SA7_Actual_Result__c = actualResult;
                job.SA7_Number_of_Checks__c++;
                actualResult=0;
			}
        }
        update jobsToMonitor;
    }
}