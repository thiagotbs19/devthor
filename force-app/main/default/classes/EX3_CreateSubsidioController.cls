/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável por realizar a atualização do registro de Cadastro para Cadastrado,
* criar registro de subsidio e Tipo de Documento. 

*Sprint 1 - Retirado a validação do contrato. 
* 
* Autor: Thiago Barbosa de Souza
  Company: Everis do Brasil 
*
********************************************************************************/
 

public without sharing class EX3_CreateSubsidioController {
    
   
   @AuraEnabled
    public static Boolean getHasPermissionSet(){
        Boolean lhasPermission = FeatureManagement.checkPermission('EX3_Criar_Tarefa'); 
        
        return lHasPermission;
   }
    
   @AuraEnabled 
   public static String redirectToObject(String aRecordId)
   {  Exception lEx; 
    EX3_Subsidios__c lSubsidio;
     if (String.isBlank(aRecordId) || String.isBlank(Label.EX3_Fila_Subsidios)) { SA7_CustomdebugLog.logError('EX3', 'EX3_CreateSubsidioController', 'redirectToObject', 'Erro ao inserir o Tipo de Documento '+ lEx.getMessage(), lEx, null); return ''; }
     try {
     List<EX3_Cadastro__c> lLstCadastro = [SELECT Id, EX3_Caso__c,  EX3_Carteira__c, EX3_Assuntos__c,  EX3_Assunto__c, EX3_Numero_do_Processo__c, 
                                           EX3_Numero_Protocolo__c, EX3_Macro_Carteira__c, EX3_Data_da_Entrada__c 
                                           FROM EX3_Cadastro__c  
                                           WHERE Id =: aRecordId AND EX3_Caso__c != null];
     if (lLstCadastro.isEmpty()) { SA7_CustomdebugLog.logError('EX3', 'EX3_CreateSubsidioController', 'redirectToObject', 'Erro ao inserir o Tipo de Documento ' + lEx.getMessage(), lEx, null); return ''; }  
     
     EX3_Cadastro__c lCadastro = lLstCadastro[0]; 
       
     if(lCadastro.EX3_Carteira__c != EX3_Utils.CARTEIRA_ICC        
     || lCadastro.EX3_Macro_Carteira__c != EX3_Utils.MACROCARTEIRA_CIVEL){
       return Label.EX3_Matriz_Tipo_Documento;  
     }    

    
     lCadastro.EX3_Status__c = Label.EX3_Status;  
     Database.SaveResult iResCadastro = Database.update(lCadastro, false);    

     List<Group> lLstQueueSubsidios = [SELECT Id, Name FROM Group WHERE Type='Queue' AND Name =: Label.EX3_Fila_Captura_Documentos];
      

     lSubsidio = new EX3_Subsidios__c(  
           EX3_Numero_do_Processo__c = lCadastro.EX3_Numero_do_Processo__c,
           EX3_Numero_Protocolo__c = lCadastro.EX3_Numero_Protocolo__c,
           EX3_Cod_Pasta__c = String.valueOf(lCadastro.EX3_Numero_Protocolo__c), 
           OwnerId = lLstQueueSubsidios[0].Id, 
           EX3_Fase__c = Label.EX3_Fase_Pre_Estrategia,    
           EX3_Cadastro__c = lCadastro.Id,      
           EX3_Macro_Carteira__c = lCadastro.EX3_Macro_Carteira__c,  
           EX3_Assunto__c = lCadastro.EX3_Assuntos__c,    
           EX3_Caso__c = lCadastro.EX3_Caso__c,   
           EX3_Carteira__c = lCadastro.EX3_Carteira__c         
       );    
       Database.saveResult iResSubsidio = Database.insert(lSubsidio);    
       if(!iResSubsidio.isSuccess()) { SA7_CustomdebugLog.logError('EX3', 'EX3_CreateSubsidioController', 'redirectToObject', 'Erro ao inserir o Tipo de Documento ' + lEx.getMessage(), lEx, null); return ''; }
    
       List<EX3_Tipo_Documento__c> lLstTiposDoc = new List<EX3_Tipo_Documento__c>();

        List<EX3_pedidos__c> lLstPedidos = [SELECT Id, EX3_Caso__c, EX3_Complemento_pedido__c, EX3_Pedido__c 
                                      FROM EX3_Pedidos__c 
                                      WHERE EX3_Caso__c =: lCadastro.EX3_Caso__c AND    
                                      EX3_Pedido__c =: EX3_Utils.PEDIDO_PROCESSO_NAO_RECONHECIDO AND       
                                      EX3_Complemento_pedido__c IN ('EX3_Proposta_excluída', 'DOC/TED')];
  
        if(lLstPedidos.isEmpty()){ 
          return Label.EX3_Pedido ;  
          }       
        
       for(EX3_Matriz_Tipo_Documento__c iMatriz : [SELECT EX3_Macro_Carteira__c, EX3_Matriz_Tipo_Documento__c.EX3_Documento__c, 
       EX3_Matriz_Tipo_Documento__c.EX3_Tipo_Estrategia__c, EX3_Carteira__c, EX3_Assunto__c, EX3_Complemento_pedido__c   
       FROM EX3_Matriz_Tipo_Documento__c WHERE EX3_Macro_Carteira__c IN (:EX3_Utils.MATRIZ_MACRO_CARTEIRA_CIVEL)
       AND EX3_Carteira__c IN(:EX3_Utils.MATRIZ_CARTEIRA_IC) AND                    
       EX3_Pedidos_do_Processo__c IN (:EX3_Utils.PEDIDO_PROCESSO_NAO_RECONHECIDO) AND  
       EX3_Tipo_Estrategia__c IN ('Pré Estratégia') AND EX3_Documento__c != null])  
            
            
       {   
             
          if(iMatriz.EX3_Complemento_pedido__c == lLstPedidos[0].EX3_Complemento_pedido__c){
          	lLstTiposDoc.addAll(getListTipoDocumento( iMatriz.EX3_Documento__c ,lSubsidio, lSubsidio.EX3_Caso__c)); 
               
          } 
       }  
                        
      if(!lLstTiposDoc.isEmpty()){ 
              
          Database.insert(lLstTiposDoc);
      }    
      }catch(DmlException ex) { 
        SA7_CustomdebugLog.logError('EX3', 'EX3_CreateSubsidioController', 'redirectToObject', 'Erro ao inserir o subsidio ' +
                                    ex.getMessage(), ex, new Map<String, String>()); 
    }  
    catch (Exception ex) 
    {  
        SA7_CustomdebugLog.logError('EX3', 'EX3_CreateSubsidioController', 'redirectToObject', 'Erro ao inserir o subsidio '+ ex.getMessage(), ex, null);
    } 
      return lSubsidio.Id;      
   }

   private static List<EX3_Tipo_Documento__c> getListTipoDocumento(String aNomeDocumento, EX3_Subsidios__c aSubsidio, String aCaseId)
   {
     List<EX3_Tipo_Documento__c> lLstTiposDoc = new List<EX3_Tipo_Documento__c>();
     if (aSubsidio == null) { return lLstTiposDoc; } 

     lLstTiposDoc.add(getTipoDocumento(aNomeDocumento, aCaseId,  aSubsidio));
     return lLstTiposDoc;      
   }       
 
   private static EX3_Tipo_Documento__c getTipoDocumento(String aNomeDocumento, String aCaseId,  EX3_Subsidios__c aSubsidio) 
   { return new EX3_Tipo_Documento__c(EX3_Documento__c	=  aNomeDocumento, EX3_Caso__c= aCaseId, EX3_Subsidios__c= aSubsidio.id, EX3_Checklist__c = Label.EX3_PreEstrategia, EX3_Status_do_Documento__c = Label.EX3_Documento_Pendente, EX3_Dados_para_Busca__c = Label.EX3_CPF_do_Autor); }
 
}