/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de TriggerHandler do objecto ContentVersion
* 
* Autor: Victor Melhem Deoud Lemos
*
********************************************************************************/
public with sharing class ContentVersionTriggerHandler implements ITrigger 
{
	public void bulkBefore(){}
    public void bulkAfter(){}
    public void beforeInsert(){}
    public void beforeUpdate(){}
    public void beforeDelete(){}
    
    public void afterInsert()
    {
        EX3_CriaLinkArquivos.execute();
    }
    
    public void afterUpdate(){}
    public void afterDelete(){}
    public void andFinally(){}
}