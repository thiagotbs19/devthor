global class SA7_jobManagerScheduler implements Schedulable{
    global void execute(SchedulableContext ctx){
        SA7_scheduledJobsManager.verifyJobStatus(); 
    }
}