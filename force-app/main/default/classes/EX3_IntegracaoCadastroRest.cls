/**
 * Created by jmariane on 2020-04-03.
 */

public with sharing class EX3_IntegracaoCadastroRest {
    private static final Map<String, String> fieldReplaces = new Map<String, String> {
            'EX3_Numero_Protocolo__c' => 'codigo_protocolo_externo'

    };
    private static final Map<String, String> fixedFields = new Map<String, String> {
            'indicador_medida_prevencao' => 'N'
    };
    public EX3_RESTIntegracaoEX3Base cliente;
    public EX3_IntegracaoCadastroRest(EX3_Cadastro__c cadastro) {
        cliente = new EX3_RESTIntegracaoEX3Base(cadastro, fieldReplaces, fixedFields);
    }
}