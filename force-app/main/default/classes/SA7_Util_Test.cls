@isTest
public class SA7_Util_Test {
    @isTest
    static void getRecordTypeIdForObjectTest(){
        //Metodo para inserir ao menos um registro no objeto de log
        try 
        {
            String s;
            s.toLowerCase();
        }catch (Exception ex)
        {
            SA7_Util.logError('sigla', 'apexClass', 'method', 'message', ex, null);
        }
        
        SA7_Custom_Debug_Logs__c idteste = [Select Id from SA7_Custom_Debug_Logs__c limit 1];
            
        String valorteste = 'SA7_Custom_Debug_Logs__c;SA7_Custom_Debug_Log;' + idteste.Id;
        List<String> parametros = new List<String>();
        parametros.add(valorteste);
        SA7_Util.getRecordTypeIdForObject(parametros);
    }
    @isTest
    static void customDebugLogTest(){
        SA7_Util.logInfo('sigla', 'apexClass', 'method', 'message', null);
        SA7_Util.logWarn('sigla', 'apexClass', 'method', 'message', null);
        try{
            Account[] accts = new Account[]{new Account(billingcity = 'San Jose')};
                insert accts;
        }
        catch (DmlException ex)
        {
            SA7_Util.logError('sigla', 'apexClass', 'method', 'message', ex, null);   
        }
        try 
        {
            String s;
            s.toLowerCase();
        }catch (Exception ex)
        {
            SA7_Util.logError('sigla', 'apexClass', 'method', 'message', ex, null);
        }
        try
        {
            Messaging.SingleEmailMessage msg= new Messaging.SingleEmailMessage();
            msg.setToAddresses(new String[]{'Cust.Email_Address__c'});
            msg.setSubject('info about session');
            msg.setPlainTextBody('cust.comment__c');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{msg});
            
        }catch (EmailException ex){
            SA7_Util.logError('sigla', 'apexClass', 'method', 'message', ex, null);
        }
    }
    @isTest
    static void ambienteSandboxTest()
    {
        Organization org = [SELECT Id, InstanceName, IsSandbox, Name, OrganizationType FROM Organization LIMIT 1];
        System.assert(org.IsSandbox==SA7_Util.ambienteSandbox());
    }
    
    @isTest
    static void validaLimitesTest()
    {
        System.assert(true == SA7_Util.validaLimites(SA7_Util.TipoOperacao.DML, null));
        System.assert(true == SA7_Util.validaLimites(SA7_Util.TipoOperacao.FUTURE, null));
        System.assert(true == SA7_Util.validaLimites(SA7_Util.TipoOperacao.CALLOUT, null));
        System.assert(true == SA7_Util.validaLimites(SA7_Util.TipoOperacao.EMAIL, null));
        System.assert(true == SA7_Util.validaLimites(SA7_Util.TipoOperacao.QUEUEABLE, null));
        System.assert(true == SA7_Util.validaLimites(SA7_Util.TipoOperacao.QUERY, null));
        System.assert(true == SA7_Util.validaLimites(SA7_Util.TipoOperacao.AGGREGATEQUERY, null));
    }
    
    /*@isTest
    static void retornaEndpointTest()
    {
        System.assert(SA7_Util.retornaEndpoint('CoEAuditIndicators')=='https://itaudigitalworkplace--CoE.cs60.my.salesforce.com/services/apexrest/COEAuditIndicators/');
    }*/
}