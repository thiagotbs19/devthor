/*********************************************************************************
*                                    Itaú - 2019
*
* Classe de Teste responsável por cobrir e testar a EX3_BloqueiaCadastroOwner
*
* 
*Apex Class: EX3_BloqueiaCadastroOwnerTest
* Empresa: everis do Brasil
* Autor: Thiago Barbosa 
*
********************************************************************************/
@isTest
public class EX3_BloqueiaCadastroOwnerTest {


    @TestSetup
    static void createData(){

        String PROF_ANALISTA_OPERACIONAL = 'Analista Operacional';
        User lUserOperacional = EX3_BI_DataLoad.getUser(PROF_ANALISTA_OPERACIONAL);
        Database.insert(lUserOperacional); 

		Case lCase = EX3_BI_DataLoad.getCase();

        Database.insert(lCase);
        
        EX3_Cadastro__c lCadastro = new EX3_Cadastro__c();
        lCadastro.EX3_Status__c = 'Em Aberto';  
        lCadastro.EX3_Numero_Protocolo__c = '003'; 
        lCadastro.OwnerId = lUserOperacional.Id;
        
        System.runAs(lUserOperacional){
        	Database.insert(lCadastro); 
        }
        
    }
    
    
    @isTest  
    private static void testBloqueiaCadastroOwner(){
        
        User lUserOperacional = [SELECT Id FROM User LIMIT 1];

        Case lCase = [SELECT Id FROM Case LIMIT 1];
        
        EX3_Cadastro__c lCadastro = [SELECT Id FROM EX3_Cadastro__c LIMIT 1];
        
        EX3_Cadastro__c lSegundoCadastro = new EX3_Cadastro__c();
        lSegundoCadastro.EX3_Numero_Protocolo__c = '002'; 
        lSegundoCadastro.OwnerId = lUserOperacional.Id;
        lSegundoCadastro.EX3_Status__c = 'Em Tratativa';
        
        Test.startTest(); 
        System.runAs(lUserOperacional){ 
        	Database.upsert(lSegundoCadastro, false);  
        }  
        
        lSegundoCadastro.OwnerId = UserInfo.getUserId();  
        lSegundoCadastro.EX3_Numero_Protocolo__c = '004';
        lSegundoCadastro.EX3_Status__c = 'Em Tratativa';    
             
        Database.SaveResult result = Database.update(lSegundoCadastro, false);   
        Test.stopTest();    
        System.assert(result.isSuccess(),'Erro ao inserir o Cadastro');  
    }
}