/*********************************************************************************
*                                    Itaú - 2020
*
* Classe Responsável por atualizar o registro do Caso e criar Cadastro. 
*
*Apex Trigger : EX3_Cadastro__c 
*Apex Class: EX3_CreateCadastroController
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

public with sharing class EX3_CreateCadastroController {

    
   @AuraEnabled
   public static Map<String, Object> getFieldsCase(String aRecordId)
   {   
       Map<String, Object> lMapReturn;
       try{
       List<Case> lLstCase = [SELECT Id, OwnerId, EX3_Canal_de_Entrada__c 
                                              FROM Case WHERE Id =: aRecordId];
       
       
       Case lCase = lLstCase[0]; 

       lCase.EX3_Situacao__c = Label.EX3_Situacao_Finalizado;
       lCase.OwnerId = UserInfo.getUserId(); 
       lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
       if(lCase.EX3_Situacao__c != null){ 
            Database.update(lCase);    
       }  
 
              
        lMapReturn = new Map<String,Object>
        {
            'case' => lCase 
           
        };
            
       }catch (Exception ex)
      {
         SA7_CustomdebugLog.logError('EX3', 'EX3_CreateCadastroController', 'getFieldsProtocol', 'Erro ao atualizar o Protocolo '+ ex.getMessage(), ex, null);
      }
      return lMapReturn; 
   } 
    
   @AuraEnabled
   public static String redirectToObject(String aRecordId)
   {    
       EX3_Cadastro__c lCadastro; 
       Exception lEx; 
       try{
       List<Case> lLstCase = [SELECT Id,  EX3_Orgao_Legal__c,  EX3_Canal_de_Entrada__c,EX3_Numero_do_protocolo__c,
                                              EX3_Numero_do_Processo__c, EX3_Data_de_Entrada__c,
                                              EX3_Macro_carteira__c, EX3_UF__c, EX3_Data_ajuizamento__c,
                                              EX3_Observacao__c, EX3_Valor_da_causa__c, EX3_Etiqueta_Z__c,
                                              EX3_Senha__c, EX3_Carteira__c,
                                              EX3_Tipo_audiencia__c
                                              FROM Case 
                                              WHERE Id =: aRecordId];   

       Case lCase = lLstCase[0];     
		
       lCase.EX3_Motivo_situacao__c = Label.EX3_Motivo_Situacao;
       lCase.OwnerId = UserInfo.getUserId();  
 
       Database.update(lCase); 
       
       
       List<Group> lLstQueueCadastro = [SELECT Id, Name FROM Group WHERE Type='Queue' AND Name =: Label.EX3_Cadastro];

        
            	  
            lCadastro = new EX3_Cadastro__c(    
       		EX3_Canal_de_Entrada__c = lCase.EX3_Canal_de_Entrada__c,
            EX3_Numero_Protocolo__c = lCase.EX3_Numero_do_protocolo__c, 
            EX3_Numero_do_Processo__c = lCase.EX3_Numero_do_Processo__c,
            EX3_Data_da_Entrada__c = Date.valueOf(lCase.EX3_Data_de_Entrada__c),
            EX3_Macro_Carteira__c = lCase.EX3_Macro_carteira__c,
            EX3_UF__c = lCase.EX3_UF__c,   
            EX3_Data_de_Ajuizamento__c = lCase.EX3_Data_ajuizamento__c,
            EX3_Observacao__c = lCase.EX3_Observacao__c,
            EX3_Valor_da_Causa__c = lCase.EX3_Valor_da_causa__c,
            EX3_Etiqueta__c = lCase.EX3_Etiqueta_Z__c,
            EX3_Senha_doc__c = lCase.EX3_Senha__c,
            EX3_Tipo_de_Audiencia__c = lCase.EX3_Tipo_audiencia__c,
            OwnerId = lLstQueueCadastro[0].Id,  
            EX3_Caso__c = lCase.Id,  
            EX3_Carteira__c = lCase.EX3_Carteira__c,
            EX3_Orgao_Legal__c = lCase.EX3_Orgao_Legal__c   
            
      );    
           
        Database.upsert(lCadastro);  
          
           
       
       } 
       catch(DmlException ex) {  
        SA7_CustomdebugLog.logError('EX3', 'EX3_CreateCadastroController', 'redirectToObject', 'Erro ao inserir o Cadastro ' + ex.getMessage(), ex, new Map<String, String>()); 
    	}      
       catch (Exception ex)
      { 
         SA7_CustomdebugLog.logError('EX3', 'EX3_CreateCadastroController', 'redirectToObject', 'Erro ao inserir o Cadastro '+ ex.getMessage(), ex, null);
      }
      return lCadastro.Id; 
   }
    
}