/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por criar protocolo, case e atrelar as files.
*
* Autor: Victor Melhem Deoud Lemos
* Aura Component Bundle: EX3_UploadFiles
* Test Class: EX3_UploadFilesControllerTest
*
********************************************************************************/
public without sharing class EX3_UploadFilesController
{
   @AuraEnabled
   public static String getCentralizador() 
   {
      String lException = '';
      EX3_Centralizador__c lCentral = new EX3_Centralizador__c();
      try
      {
         insert lCentral;
      }
      catch(DmlException ex)
    {
         lException = ex.getMessage();
         SA7_CustomdebugLog.logError('EX3', 'EX3_UploadFilesController', 'getCentralizador', 'Erro ao inserir registro do objeto que associa os arquivos ao Salesforce'+lException, ex, null);
      }
      catch(Exception ex)
      {
         lException = ex.getMessage();
         SA7_CustomdebugLog.logError('EX3', 'EX3_UploadFilesController', 'getCentralizador', 'Erro ao inserir registro do objeto que associa os arquivos ao Salesforce'+lException, ex, null);
      }
      finally
      {
         if(String.isBlank(lException))
         {
            SA7_CustomdebugLog.logWarn('EX3', 'EX3_UploadFilesController', 'getCentralizador', 'Sucesso ao inserir registro do objeto que associa os arquivos ao Salesforce', null);
         }
      }
      return JSON.serialize(lCentral); 
   }

   @AuraEnabled
   public static String getDocuments(String aCentral)
   {
      EX3_Centralizador__c lCentral = (EX3_Centralizador__c)JSON.deserialize(aCentral, EX3_Centralizador__c.class);
      lCentral.EX3_hasDocuments__c = true;

      Set<String> lSetDocumentIds = new Set<String>();
      for(ContentDocumentLink iContentDocumentLink : [SELECT ContentDocumentId, LinkedEntityId
                                                      FROM ContentDocumentLink
                                                      WHERE LinkedEntityId = :lCentral.Id])
      {
         lSetDocumentIds.add(iContentDocumentLink.ContentDocumentId);
      } 
            
      List<EX3_WrapperDocument> lLstWrapperDocument = new List<EX3_WrapperDocument>();
      for(ContentVersion iContentVersion : [SELECT ContentDocumentId, PathOnClient 
                                            FROM ContentVersion
                                            WHERE ContentDocumentId IN: lSetDocumentIds])
      {
         EX3_WrapperDocument lWrapper = new EX3_WrapperDocument();
         lWrapper.name = iContentVersion.PathOnClient;
         lWrapper.documentId = iContentVersion.ContentDocumentId;
         lLstWrapperDocument.add(lWrapper);
      }

      Map<String,Object> lMapToReturnValues = new Map<String,Object>();
      lMapToReturnValues.put('listDocument', lLstWrapperDocument);
      lMapToReturnValues.put('central', lCentral);

      return JSON.serialize(lMapToReturnValues);
   }

   @AuraEnabled
   public static String criarProtocolo(String aNumProcesso, String aCentral)
   {
       String lException = '';
       String lProtocoloBody = '';
       try
       {
           User lUser = [SELECT Id, EX3_Pertence__c, FuncionalColaborador__c FROM User WHERE Id = :UserInfo.getUserId()];
           Case lCase = new Case();
           lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado');
           lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
           lCase.EX3_Sigla__c = 'EX3';
           lCase.EX3_Motivo_Situacao__c = '20'; 
           lCase.EX3_Numero_do_Processo__c = aNumProcesso;

           database.insert(lCase);

           EX3_Centralizador__c lCentral = (EX3_Centralizador__c) JSON.deserialize(aCentral, EX3_Centralizador__c.class);
           List<ContentDocumentLink> lLstContentDocumentLink = new List<ContentDocumentLink>();
           for(ContentDocumentLink iContentDocumentLink : [SELECT ContentDocumentId, ContentDocument.Title, ShareType, Visibility, 
                                                          LinkedEntityId FROM ContentDocumentLink
                                                          WHERE LinkedEntityId = :lCentral.Id])
           {
              ContentDocumentLink lContentDocumentLink = iContentDocumentLink.clone();
              lContentDocumentLink.LinkedEntityId = lCase.Id;
              lLstContentDocumentLink.add(lContentDocumentLink);
           }
           database.insert(lLstContentDocumentLink);
           EX3_ObjectFactory.IncluirProtocolo lIncluirProtocolo = new EX3_ObjectFactory.IncluirProtocolo();
           lIncluirProtocolo.codigo_protocolo_externo = lCase.Id;
           lIncluirProtocolo.codigo_unico_processo = lCase.EX3_Numero_do_Processo__c;
           lIncluirProtocolo.data_protocolo_documento = lCase.CreatedDate;
           lIncluirProtocolo.codigo_canal_entrada_documento = Decimal.valueOf(lCase.EX3_Canal_de_Entrada__c);
           lIncluirProtocolo.codigo_motivo_situacao = Decimal.valueOf(lCase.EX3_Motivo_da_Situacao__c);
           lIncluirProtocolo.data_situacao = system.today();
           lIncluirProtocolo.operador = lUser.FuncionalColaborador__c;
           lIncluirProtocolo.codigo_operacao = 3;
           lIncluirProtocolo.indicador_protocolo = 0;
           lProtocoloBody = JSON.serialize(lIncluirProtocolo);
       } 
       catch(DmlException ex)  
       {
         lException = ex.getMessage();
         SA7_CustomdebugLog.logError('EX3', 'EX3_UploadFilesController', 'criarProtocolo', 'Erro ao inserir registro do Protocolo '+lException, ex, null);
       }
       catch(Exception ex) 
       {
         lException = ex.getMessage();
         SA7_CustomdebugLog.logError('EX3', 'EX3_UploadFilesController', 'criarProtocolo', 'Erro ao inserir registro do Protocolo '+lException, ex, null);
       }
       finally
       {
         if(String.isBlank(lException))
         {
            SA7_CustomdebugLog.logWarn('EX3', 'EX3_UploadFilesController', 'criarProtocolo', 'Sucesso ao inserir registro do protocolo', null);
         }
      }
      return lProtocoloBody;
   }

   @AuraEnabled(continuation=true)
   public static Continuation enviarProtocolo(String aParams)
   {
      Map<String,String> lMapParams = (Map<String,String>) JSON.deserialize(aParams, Map<String,String>.class);
       
      Map<String, String> lMapDadosRequest = new Map<String,String>
      {
         'classe' => lMapParams.get('classe'),
         'body' => lMapParams.get('protocolo'),
         'callback' => lMapParams.get('callback'),
         'service' => 'incluirProtocolo'
      };
      system.debug('lMapDadosRequest ==> ' +lMapDadosRequest);
      Continuation lContinuation;
      try 
      {
         lContinuation = EX3_ServiceFactory.incluirProtocolo(lMapParams.get('protocolo'), lMapParams.get('classe'), lMapParams.get('callback'), lMapDadosRequest);
      }
      catch (Exception ex)
      {
         SA7_CustomdebugLog.logError('EX3', 'EX3_UploadFilesController', 'enviarProtocolo', 'Erro ao criar o Request '+ex.getMessage(), ex, null);
      }

      return lContinuation;
   }

   @AuraEnabled
   public static object enviarProtocoloCallback(Object state)
   {
      EX3_ContinuationUtils.StateInfo lRetorno = (EX3_ContinuationUtils.StateInfo)state;

      HttpResponse lResponse = Continuation.getResponse(lRetorno.state);
      if (lResponse.getStatusCode() > 202)
      {
         SA7_CustomdebugLog.logWarn('EX3', 'EX3_UploadFilesController', 'enviarProtocoloCallback','Status: ' + lResponse.getStatusCode() + '\n' + 
                                      lResponse.getBody(), null);
         Map<String,String> lResponseMsg = (Map<String, String>) JSON.deserialize(lResponse.getBody(), Map<String,String>.class);

         String lMensagem = 'Unkown Error';
         if (String.isNotBlank(lResponseMsg.get('message')))
         {
            lMensagem = lResponseMsg.get('message');
         }
         else if (String.isNotBlank(lResponseMsg.get('Message')))
         {
            lMensagem = lResponseMsg.get('Message');
         }

         return JSON.serialize(new Map<String, Object> 
         {
            'error' => lMensagem,
            'status' => lResponse.getStatusCode()
         });
      }
      Map<String,Object> lMapDataResponse = (Map<String,Object>)JSON.deserializeUntyped(lResponse.getBody());
      Map<String,Object> lMapBodyResponse = (Map<String,Object>)lMapDataResponse.get('data');


      Case lCase = fillCaseNumber(lMapBodyResponse);
      User lUser = [SELECT Id, FuncionalColaborador__c FROM User WHERE Id = :UserInfo.getUserId()];
      List<EX3_ObjectFactory.IncluirDocumento> lLstDocumento = new List<EX3_ObjectFactory.IncluirDocumento>();
      for(ContentDocumentLink iContentDocumentLink : [SELECT Id, ContentDocument.Title FROM ContentDocumentLink
                                                      WHERE LinkedEntityId = :lCase.Id])
      {
         EX3_ObjectFactory.IncluirDocumento lDocumento = new EX3_ObjectFactory.IncluirDocumento();
         lDocumento.nome_documento = iContentDocumentLink.ContentDocument.Title;
         lDocumento.codigo_documento_plataforma_externa = iContentDocumentLink.Id;
         lDocumento.operador_conglomerado = lUser.FuncionalColaborador__c;
         lLstDocumento.add(lDocumento);
      }

      lMapBodyResponse.put('protocolo', JSON.serialize(lCase));
      lMapBodyResponse.put('documentos', JSON.serialize(lLstDocumento));

      return JSON.serialize(lMapBodyResponse);
    } 

   private static Case fillCaseNumber(Map<String,Object> aMapBodyResponse)
   {
      String lNumeroDoProtocolo = JSON.serialize(aMapBodyResponse.get('id_protocolo'));
      Case lCase = new Case();
      lCase.Id = (Id)aMapBodyResponse.get('codigo_protocolo_externo');
      lCase.EX3_Numero_do_Processo__c = (String)aMapBodyResponse.get('codigo_unico_processo');
      database.update(lCase);

      return lCase;
   }

   @AuraEnabled(Continuation=true)
   public static Continuation enviarDocumentos(String aParams)
   {
      Map<String,String> lMapParams = (Map<String,String>) JSON.deserialize(aParams, Map<String,String>.class);
      Case lCase = (Case) JSON.deserialize(lMapParams.get('case'),Case.class);
      Map<String, String> lMapDadosRequest = new Map<String,String>
      {
         'classe' => lMapParams.get('classe'),
         'body' => lMapParams.get('documentos'),
         'callback' => lMapParams.get('callback'), 
         'case' => lMapParams.get('case'),
         'service' => 'incluirDocumento'
      };
      Continuation lContinuation;
      try 
      {
         lContinuation = EX3_ServiceFactory.incluirDocumento(lMapParams.get('documentos'), lMapParams.get('classe'), lMapParams.get('callback'), lMapDadosRequest, String.valueOf(lCase.EX3_Numero_do_Protocolo__c));
      }
      catch (Exception ex)
      {
         SA7_CustomdebugLog.logError('EX3', 'EX3_UploadFilesController', 'enviarDocumentos', 'Erro ao criar o Request '+ex.getMessage(), ex, null);
      }
      return lContinuation;      
   }

   @AuraEnabled
   public static String enviarDocumentosCallback(Object state)
   {
      EX3_ContinuationUtils.StateInfo lRetorno = (EX3_ContinuationUtils.StateInfo)state;
      HttpResponse lResponse = Continuation.getResponse(lRetorno.state);
      if (lResponse.getStatusCode() > 202)
      {
         SA7_CustomdebugLog.logWarn('EX3', 'EX3_UploadFilesController', 'enviarDocumentosCallback','Status: ' + lResponse.getStatusCode() + '\n' + 
                                      lResponse.getBody(), null);
         Map<String,String> lResponseMsg = (Map<String, String>) JSON.deserialize(lResponse.getBody(), Map<String,String>.class);

         String lMensagem = 'Unkown Error';
         if (String.isNotBlank(lResponseMsg.get('message')))
         {
            lMensagem = lResponseMsg.get('message');
         }
         else if (String.isNotBlank(lResponseMsg.get('Message')))
         {
            lMensagem = lResponseMsg.get('Message');
         }

         return JSON.serialize(new Map<String, Object> 
         {
            'error' => lMensagem,
            'status' => lResponse.getStatusCode()
         });
      }
      Map<String,Object> lMapDataResponse = (Map<String,Object>)JSON.deserializeUntyped(lResponse.getBody());
      Map<String,Object> lMapBodyResponse;
      for (Object iObj : (List<Object>)lMapDataResponse.get('data'))
      {
         lMapBodyResponse = (Map<String,Object>) iObj;
      }
      Map<String,String> lMapDados = (Map<String,String>)lRetorno.dados;
      
      lMapBodyResponse.put('protocolo', lMapDados.get('protocolo'));
      return JSON.serialize(lMapBodyResponse);
   }
}