/*********************************************************************************
*                                    Itaú - 2020  
*
* Classe Responsável para que o analista não consiga puxar um registro para iniciar
* um atendimento se o mesmo ja possuir um registro de Estratégia. 
*
*Apex Trigger : EX3_DefinicaoEstrategia__c
*Apex Class: EX3_BloqueiaEstrategiaOwner
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

public with sharing class EX3_BloqueiaEstrategiaOwner {
    
    public static void execute(){ 
        Set<Id> lSetOwnerDefEstrategia = new Set<Id>();
        List<EX3_DefinicaoEstrategia__c> lLstEX3DefEstrategia = new List<EX3_DefinicaoEstrategia__c>();
        for(EX3_DefinicaoEstrategia__c iEstrategia : (List<EX3_DefinicaoEstrategia__c>) Trigger.new){
           if(EX3_TriggerHelper.changedField(iEstrategia, 'OwnerId') &&
                iEstrategia.OwnerId == UserInfo.getUserId()) {
                  lSetOwnerDefEstrategia.add(iEstrategia.OwnerId);
                  lLstEX3DefEstrategia.add(iEstrategia);
              }    
        }
        if(lSetOwnerDefEstrategia.isEmpty() || lSetOwnerDefEstrategia == null){ return ;}
        estrategiaOwnerError(lSetOwnerDefEstrategia, lLstEX3DefEstrategia);
    } 

    private static void estrategiaOwnerError(Set<Id> aSetOwnerDefEstrategia, List<EX3_DefinicaoEstrategia__c> aLstEX3DefEstrategia){

    
      if(aSetOwnerDefEstrategia.isEmpty() || aSetOwnerDefEstrategia == null || aLstEX3DefEstrategia.isEmpty() || aLstEX3DefEstrategia == null){
        return; 
      } 
      String lNumeroProtocolo; 
      if(!aLstEX3DefEstrategia.isEmpty()){ 
      for(EX3_DefinicaoEstrategia__c iEstrategia : [SELECT Id, Name, OwnerId , EX3_StatusEstrategia__c FROM EX3_DefinicaoEstrategia__c
                                       WHERE OwnerId =: aSetOwnerDefEstrategia AND EX3_StatusEstrategia__c NOT IN ('Documento Solicitado')]){
              
          lNumeroProtocolo = iEstrategia.Name;
        }  
      }  
      if(lNumeroProtocolo == null){  return;}      
      for(EX3_DefinicaoEstrategia__c iEstrategia : aLstEX3DefEstrategia){     
          iEstrategia.adderror(Label.EX3_Pasta_Numero + lNumeroProtocolo + ' ' + Label.EX3_Tratativa);
        }        
       
}

}