/*********************************************************************************
*                                    Itaú - 2020 
*
* Classe Responsável por atualizar o registro de Definição Estratégica
*
*
*Apex Class: EX3_UpdateDefinicaoEstrategica
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/


public with sharing class EX3_UpdateDefinicaoEstrategica {
   
    public static void execute(){

        Set<String> lSetValuesDef = new Set<String> {Label.EX3_Estrategia_Acordo, Label.EX3_Estrategia_Defesa};      
        String lRecordTypePreEstr = EX3_Utils.getRecordTypeIdByDevName('EX3_DefinicaoEstrategia__c', 'EX3_Pre_estrategia');
        List<EX3_DefinicaoEstrategia__c> lLstDefEstr = new List<EX3_DefinicaoEstrategia__c>();
        Set<Id> lSetCreatedById = new Set<Id>();
        for(EX3_DefinicaoEstrategia__c iDef : (List<EX3_DefinicaoEstrategia__c>) Trigger.new){ 
            if(iDef.RecordTypeId == lRecordTypePreEstr && 
               lSetValuesDef.contains(iDef.EX3_Estrategia__c)){   
                lLstDefEstr.add(iDef);
                lSetCreatedById.add(iDef.CreatedById);  
            }
        }
        if(lLstDefEstr.isEmpty() || lLstDefEstr == null){return ;} 
        updateDefinicaoEstrategica(lLstDefEstr, lSetCreatedById);    
    } 

    private static void updateDefinicaoEstrategica(List<EX3_DefinicaoEstrategia__c> aLstDefEstr, Set<Id> aSetCreatedById){
        
        String lRecorTypeDefEstr = EX3_Utils.getRecordTypeIdByDevName('EX3_DefinicaoEstrategia__c', 'EX3_Estrategia_definitiva');
        if(aLstDefEstr.isEmpty() || aLstDefEstr == null){ return ;}  
        EX3_DefinicaoEstrategia__c lDefPreEstrat = aLstDefEstr[0];    
        
        List<EX3_DefinicaoEstrategia__c> lLstDefLastCreated = new List<EX3_DefinicaoEstrategia__c>();
        for(EX3_DefinicaoEstrategia__c iDef : [SELECT Id, OwnerId, EX3_DefinicaoEstrategia__c.CreatedById 
                                              FROM EX3_DefinicaoEstrategia__c   
                                              WHERE CreatedById =: aSetCreatedById AND
                                              RecordTypeId =: lRecorTypeDefEstr 
                                              ORDER BY CreatedDate DESC LIMIT 1]){   
                    EX3_DefinicaoEstrategia__c lDefEst = new EX3_DefinicaoEstrategia__c();
                    lDefEst.Id = iDef.Id;  
                    lDefEst.EX3_Analise_por_JEC_semelhanca__c = lDefPreEstrat.EX3_Analise_por_JEC_semelhanca__c;
                    lDefEst.EX3_Atuacao_previa__c = lDefPreEstrat.EX3_Atuacao_previa__c;
                    lDefEst.EX3_Autor_realizou_ctt_administrativo__c = lDefPreEstrat.EX3_Autor_realizou_ctt_administrativo__c;
                    lDefEst.EX3_Caso__c = lDefPreEstrat.EX3_Caso__c;
                    lDefEst.EX3_Causa_Raiz__c = lDefPreEstrat.EX3_Causa_Raiz__c;
                    lDefEst.EX3_Consta_analise_da_inspetoria__c = lDefPreEstrat.EX3_Consta_analise_da_inspetoria__c;
                    lDefEst.EX3_Data_da_baixa__c = lDefPreEstrat.EX3_Data_da_baixa__c; 
                    lDefEst.EX3_Data_da_regularizacao__c = lDefPreEstrat.EX3_Data_da_regularizacao__c;
                    lDefEst.EX3_Data_do_contato__c = lDefPreEstrat.EX3_Data_do_contato__c;
                    lDefEst.EX3_Data_do_ressarcimento__c = lDefPreEstrat.EX3_Data_do_ressarcimento__c;
                    lDefEst.EX3_Endereco_da_agencia_de_saque_da_OP__c = lDefPreEstrat.EX3_Endereco_da_agencia_de_saque_da_OP__c;
                    lDefEst.EX3_Houve_desconto_no_beneficio__c = lDefPreEstrat.EX3_Houve_desconto_no_beneficio__c;
                    lDefEst.EX3_Justificativa__c = lDefPreEstrat.EX3_Justificativa__c;
                    lDefEst.EX3_Marcacao_da_materia__c = lDefPreEstrat.EX3_Marcacao_da_materia__c;
                    lDefEst.EX3_Motivo_de_baixa__c = lDefPreEstrat.EX3_Motivo_de_baixa__c;
                    lDefEst.EX3_Providencia__c = lDefPreEstrat.EX3_Providencia__c;
                    lDefEst.EX3_Qual_o_canal_de_atendimento__c = lDefPreEstrat.EX3_Qual_o_canal_de_atendimento__c;
                    lDefEst.EX3_Qual_o_retorno_dado_para_o_cliente__c = lDefPreEstrat.EX3_Qual_o_retorno_dado_para_o_cliente__c;
                    lDefEst.EX3_Quando_ocorreu_a_regularizacao__c = lDefPreEstrat.EX3_Quando_ocorreu_a_regularizacao__c;
                    lDefEst.EX3_Realizada_atuacao_regularizacao_prev__c = lDefPreEstrat.EX3_Realizada_atuacao_regularizacao_prev__c;
                    lDefEst.EX3_Solicitada_a_baixa_do_contrato__c = lDefPreEstrat.EX3_Solicitada_a_baixa_do_contrato__c;
                    lDefEst.EX3_StatusEstrategia__c = lDefPreEstrat.EX3_StatusEstrategia__c;
                    lDefEst.EX3_Status_do_cheque__c = lDefPreEstrat.EX3_Status_do_cheque__c;
                    lDefEst.EX3_Status_do_contrato_antes_do_ajuizam__c = lDefPreEstrat.EX3_Status_do_contrato_antes_do_ajuizam__c;
                    lDefEst.EX3_Tese__c = lDefPreEstrat.EX3_Tese__c;
                    lDefEst.EX3_Tipo_da_baixa__c = lDefPreEstrat.EX3_Tipo_da_baixa__c;  
                    lDefEst.EX3_Valor_do_ressarcimento__c = lDefPreEstrat.EX3_Valor_do_ressarcimento__c;
                    lLstDefLastCreated.add(lDefEst);   
                }
                if(lLstDefLastCreated.isEmpty()){ return ;}  
                Database.update(lLstDefLastCreated);        
    }    
}