@isTest
public with sharing class EX3_CreateCalculoSolicitadoTest {
    

    @TestSetup
    public static void createData(){
    
            User lUser = EX3_BI_DataLoad.getUser();
            lUser.FuncionalColaborador__c = 'funcional';
            lUser.EX3_Pertence__c = '2'; 
            Database.insert(lUser);
    
            Case lCase = EX3_BI_DataLoad.getCase();
            lCase.Status = 'New'; 
            Database.insert(lCase);  

            EX3_Calculo__c lCalculo = EX3_BI_DataLoad.getCalculo(lCase);
            lCalculo.EX3_Status_do_calculo__c = 'Cálculo Solicitado';  
            Database.insert(lCalculo);  
    }

    @isTest static void testupdateStatusFilterGroup(){

        User lUser = [SELECT Id FROM User LIMIT 1]; 

        Case lCase = [SELECT Id FROM Case LIMIT 1];

        Test.startTest();
        EX3_Calculo__c lCalculo = [SELECT Id, EX3_Status_do_calculo__c FROM EX3_Calculo__c LIMIT 1];
        Test.stopTest();  

        System.assert(lCalculo.EX3_Status_do_calculo__c != null, 'O Status do calculo não foi preenchido');
        
    }
}