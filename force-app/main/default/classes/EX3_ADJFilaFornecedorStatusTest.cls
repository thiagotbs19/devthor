/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste  
*
* Autor: Thiago Barbosa 
* Apex Class: EX3_ADJFilaFornecedorStatusTest 
* 
*
********************************************************************************/
@isTest 
public with sharing class EX3_ADJFilaFornecedorStatusTest {
    

    @TestSetup
    static void createData(){

        User lUser = EX3_BI_DataLoad.getUser();
        lUser.EX3_Pertence__c = '2';
        lUser.FuncionalColaborador__c = 'funcional';

        Database.insert(lUser);

        Case lCase = EX3_BI_DataLoad.getCase();

        Database.insert(lCase);

        EX3_ADJ__c lADJ = EX3_BI_DataLoad.getADJ(lCase);
        lADJ.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF');
        lADJ.EX3_Status__c = 'Aguardando comprovante'; 
        lADJ.EX3_Data_da_Publicacao__c = Date.today();
        lADJ.EX3_Tipo_de_Decisao__c = 'Liminar';
        lADJ.EX3_Resultado_da_Decisao__c = 'Improcedente';
        lADJ.EX3_Multa_OBF__c = 'Arbitrada';
        lADJ.EX3_Valor_Multa_OBF__c = 2.0;
        lADJ.EX3_Checklist_de_Cumprimento__c = 'Pagamento';
        Database.insert(lADJ);  
        

    
        
    }

    @isTest
    public static void testCreateADJ(){
		
        List<Group> lLstGroupFornecedor = [SELECT Id, Name, DeveloperName FROM Group WHERE Type='Queue' AND DeveloperName = 'EX3_Fornecedor'];

        User lUser = [SELECT Id, EX3_Pertence__c, FuncionalColaborador__c FROM User LIMIT 1];
        
        Case lCase = [SELECT Id FROM Case LIMIT 1];

        EX3_ADJ__c lADJ = [SELECT Id, RecordTypeId, EX3_Data_da_Publicacao__c, 
                           EX3_Tipo_de_Decisao__c, EX3_Resultado_da_Decisao__c,
                           EX3_Multa_OBF__c, EX3_Valor_Multa_OBF__c,
                           EX3_Checklist_de_Cumprimento__c FROM EX3_ADJ__c LIMIT 1];
          
        lADJ.EX3_Status__c = 'Solicitado ao fornecedor';
        lADJ.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF');
        lADJ.EX3_Data_da_Publicacao__c = Date.today();
        lADJ.EX3_Tipo_de_Decisao__c = 'Liminar';
        lADJ.OwnerId = lLstGroupFornecedor[0].Id;
        lADJ.EX3_Resultado_da_Decisao__c = 'Improcedente';
        lADJ.EX3_Multa_OBF__c = 'Arbitrada';
        lADJ.EX3_Valor_Multa_OBF__c = 2.0;
        lADJ.EX3_Checklist_de_Cumprimento__c = 'Pagamento';
        
        Test.startTest(); 
        Database.SaveResult lResult = Database.update(lADJ, false);   
		Test.stopTest();  
        System.assert(lResult.isSuccess(), 'Registro de ADJ não criado');
    
    }
}