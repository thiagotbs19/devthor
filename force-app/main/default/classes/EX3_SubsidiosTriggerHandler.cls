/*********************************************************************************
*                                    Itaú - 2020
*
* Classe TriggerHandler do objeto EX3_Subsidios 
* 
* Autor: Thiago Barbosa de Souza
Company: Everis do Brasil
*
********************************************************************************/

public with sharing class EX3_SubsidiosTriggerHandler implements ITrigger {
    
    
    public void bulkBefore(){}  
    public void bulkAfter(){}
    public void beforeInsert(){ 
        EX3_BloqueiaSubsidiosOwner.execute();
        
    }
    public void beforeUpdate(){ 
        EX3_BloqueiaSubsidiosOwner.execute();
    } 
    public void beforeDelete(){}
    
    public void afterInsert(){} 
    
    public void afterUpdate(){} 
    public void afterDelete(){}
    public void andFinally(){}
    
}