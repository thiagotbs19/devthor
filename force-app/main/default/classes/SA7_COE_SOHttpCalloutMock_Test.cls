@isTest
global class SA7_COE_SOHttpCalloutMock_Test implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"Status": "Access Granted"}');
        response.setStatusCode(200);
        return response; 
    }
}