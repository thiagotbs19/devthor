/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsavel por simular a resposta de atualizaçã do protocolo do EX3
* Empresa: everis do Brasil
* Autor: Rafael Amaral Moreira
*
*
********************************************************************************/
public with sharing class EX3_AtualizarProtocoloMock 
{
    private Integer statusCode;
    private Decimal id_protocolo;
    private String codigo_protocolo_externo;
    private Decimal codigo_motivo_situacao;
    private String operador;
    private Decimal codigo_operacao;
    private Decimal indicador_protocolo;  

    public EX3_AtualizarProtocoloMock(EX3_ObjectFactory.AtualizarProtocolo aAtualizarProtocolo, Integer statusCode) 
    {
        this.statusCode = statusCode;
        this.id_protocolo = aAtualizarProtocolo.id_protocolo;
        this.codigo_protocolo_externo = aAtualizarProtocolo.codigo_protocolo_externo;
        this.codigo_motivo_situacao = aAtualizarProtocolo.codigo_motivo_situacao;
        this.operador = aAtualizarProtocolo.operador;
        this.codigo_operacao = 3;
        this.indicador_protocolo = 0; 
    }

    public HTTPResponse respond(HTTPRequest req) 
    {  
        List<Map<String,Object>> lLstMapBody = new List<Map<String,Object>>();

        Map<String, Object> lMapBody = new Map<String,Object>
        {
            'id_protocolo' => 2019000001, 
            'codigo_protocolo_externo' => this.codigo_protocolo_externo,
            'codigo_motivo_situacao' => this.codigo_motivo_situacao,
            'operador' => this.operador,
            'codigo_operacao' => this.codigo_operacao,
            'indicador_protocolo' => this.indicador_protocolo
        };
        lLstMapBody.add(lMapBody);
		
        String jsonString = (statusCode > 202) 
                          ? '{"Message": "Erro ao enviar o request" }'
                          : '{"data":'+JSON.serialize(lLstMapBody)+'}';
            
        HttpResponse res = new HttpResponse();
        res.setStatusCode(statusCode);
        res.setBody(jsonString);

        return res;
    }
}