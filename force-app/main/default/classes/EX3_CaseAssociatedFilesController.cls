/*********************************************************************************
*                                    Itaú - 2019
*
* Classe responsável por listar os arquivos atrelados ao protocolo/caso
* para revisão e/ou adição de novos arquivos
*
* Autor: Marcelo Seibt de Oliveira
* Aura Component Bundle: EX3_CaseAssociatedFiles
*
********************************************************************************/
public without sharing class EX3_CaseAssociatedFilesController 
{  
    
    
    private static final Set<string> CAMPOS_CASE = new set<string>{'Id'};   
        private static final Set<String> CAMPOS_SUBSIDIOS = new Set<String>{Label.EX3_Campos_Subsidios};
            private static final Set<String> CAMPOS_CADASTRO = new Set<String>{Label.EX3_Campos_Cadastros};
                private static final Set<String> CAMPOS_TIPODOC = new Set<String>{Label.EX3_Campos_Tipo_de_Documento};
                    private static final Set<String> CAMPOS_CALCULO = new Set<String>{Label.EX3_Campos_Calculo};
                        private static final Set<String> CAMPOS_DEFINICAO_ESTRATEGICA = new Set<String>{Label.EX3_Campos_Estrategia};
                            private static final Set<String> CAMPOS_ADJ = new Set<String>{Label.EX3_Campos_ADJ};
                                private static final Set<String> CAMPOS_LAUDO = new Set<String>{Label.EX3_Campos_Laudo};
                                    
                                    @AuraEnabled   
                                    public static String getCentralizador()
                                {
                                    EX3_Centralizador__c lCentralizador; 
                                    String lException;  
                                    try
                                    { 
                                        lCentralizador = new EX3_Centralizador__c();
                                        database.insert(lCentralizador);
                                    }
                                    catch(DmlException ex)  
                                    { 
                                        lException = ex.getMessage();
                                        SA7_CustomdebugLog.logError('EX3', 'EX3_CaseAssociatedFilesController', 'getCentralizador', 
                                                                    'Erro ao inserir registro do objeto que associa os arquivos ao Salesforce'+lException, ex, null);
                                    }
                                    catch(Exception ex)
                                    {
                                        lException = ex.getMessage();
                                        SA7_CustomdebugLog.logError('EX3', 'EX3_CaseAssociatedFilesController', 'getCentralizador', 
                                                                    'Erro ao inserir registro do objeto que associa os arquivos ao Salesforce'+lException, ex, null);
                                    }
                                    finally
                                    {
                                        if(String.isBlank(lException))
                                        { 
                                            SA7_CustomdebugLog.logWarn('EX3', 'EX3_CaseAssociatedFilesController', 'getCentralizador', 
                                                                       'Sucesso ao inserir registro do objeto que associa os arquivos ao Salesforce', 
                                                                       new Map<String,String>{'EX3_Centralizador__c' => JSON.serialize(lCentralizador)}); 
                                        }
                                    } 
                                    return JSON.serialize(lCentralizador);    
                                }
    
    @AuraEnabled
    public static String getDocuments(Id aCaseId) 
    {
        List<SObject> lLstSObject; 
        String lException; 
        List<AttributesTypesContentDocument> lLstAttributesTypesContentDocument = new List<AttributesTypesContentDocument>();
        string lQuery;  
        try 
        {  
            Schema.sObjectType lTypeObject = aCaseId.getSObjectType();  
        if(lTypeObject == Case.sObjectType){
            lLstSObject = [SELECT Id FROM Case WHERE Id =: aCaseId];  
        }     
           else if(lTypeObject == EX3_Subsidios__c.sObjectType){
                lQuery = 'SELECT ';
                for(string lcampo : CAMPOS_SUBSIDIOS){
                    lQuery += ' ' + lcampo + ' ';
                }
                lQuery += ' FROM EX3_Subsidios__c WHERE ' + 'Id' + ' =: aCaseId';
            }
            else if(lTypeObject == EX3_Cadastro__c.sObjectType){
                lQuery = 'SELECT ';
                for(string lcampo : CAMPOS_CADASTRO){ 
                    lQuery += ' ' + lcampo + ' ';  
                }
                lQuery += ' FROM EX3_Cadastro__c WHERE ' + 'Id' + ' =: aCaseId';
            }
            else if(lTypeObject == EX3_Tipo_Documento__c.sObjectType){
                lQuery = 'SELECT ';
                for(string lcampo : CAMPOS_TIPODOC){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_Tipo_Documento__c WHERE ' + 'Id' + '=: aCaseId';
            } 
            else if(lTypeObject == EX3_Calculo__c.sObjectType){
                lQuery = 'SELECT ';
                for(string lcampo : CAMPOS_CALCULO){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_Calculo__c WHERE ' + 'Id' + '=: aCaseId';
            } 
            else if(lTypeObject == EX3_DefinicaoEstrategia__c.sObjectType){
                lQuery = 'SELECT '; 
                for(string lcampo : CAMPOS_DEFINICAO_ESTRATEGICA){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_DefinicaoEstrategia__c WHERE ' + 'Id' + '=: aCaseId';
            }
            else if(lTypeObject == EX3_ADJ__c.sObjectType){
                lQuery = 'SELECT '; 
                for(string lcampo : CAMPOS_ADJ){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_ADJ__c WHERE ' + 'Id' + '=: aCaseId';
            }
            else if(lTypeObject == EX3_Laudo__c.sObjectType){
                lQuery = 'SELECT '; 
                for(string lcampo : CAMPOS_LAUDO){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_Laudo__c WHERE ' + 'Id' + '=: aCaseId';
            }
            String lIdLinked;
            
            if(lTypeObject != Case.sObjectType){
                lLstSObject = Database.query(String.escapeSingleQuotes(lQuery));
                lIdLinked = (String)lLstSObject[0].get('EX3_Caso__c'); 
            }   
            String lIdCase = lLstSObject[0].Id;  

            if(String.isNotBlank(lIdLinked)){
                lIdLinked = (Id)lLstSObject[0].get('EX3_Caso__c');   
            }  
            else if(String.isNotBlank(lIdCase)){
                lIdLinked = (Id)lLstSObject[0].Id;   
            }  
                     
            for (ContentDocumentLink iContentDocumentLink : [SELECT ContentDocumentId, ContentDocument.FileExtension,
                                                             ContentDocument.Title, ContentDocument.CreatedDate, 
                                                             ContentDocument.ContentSize   
                                                             FROM ContentDocumentLink               
                                                             WHERE LinkedEntityId = : lIdLinked])
            {   
                AttributesTypesContentDocument lAttributesTypes = new AttributesTypesContentDocument();
                lAttributesTypes.label = iContentDocumentLink.ContentDocument.Title;
                lAttributesTypes.createdDate = iContentDocumentLink.ContentDocument.CreatedDate.dateGMT().format();
                lAttributesTypes.size = EX3_Utils.getByteSize(iContentDocumentLink.ContentDocument.ContentSize);
                lAttributesTypes.fileExtension = iContentDocumentLink.ContentDocument.FileExtension ;
                lAttributesTypes.url = URL.getSalesforceBaseUrl().toExternalForm() + '/' + iContentDocumentLink.ContentDocumentId;
                lAttributesTypes.icon = EX3_Utils.getLightningIconByContentDocumentFileExtension(iContentDocumentLink.ContentDocument.FileExtension);
                
                lLstAttributesTypesContentDocument.add(lAttributesTypes);
            }
        } 
        catch(Exception ex)
        {
            SA7_CustomdebugLog.logError('EX3', 'EX3_CaseAssociatedFilesController', 'getDocuments', 
                                        'Erro buscar os arquivos associados '+lException, ex, null);
        }
        finally
        {
            if(String.isBlank(lException))
            {
                SA7_CustomdebugLog.logWarn('EX3', 'EX3_CaseAssociatedFilesController', 'getDocuments', 
                                           'Sucesso ao inserir registro do objeto que associa os arquivos ao Salesforce', null);
            } 
        }
        return JSON.serialize(lLstAttributesTypesContentDocument);
    }
    
    @AuraEnabled
    public static String setDocuments(Id aCaseId, String aParamCentral)
    {        
        EX3_Centralizador__c lCentral = (EX3_Centralizador__c)JSON.deserialize(aParamCentral, EX3_Centralizador__c.class);
        lCentral.EX3_hasDocuments__c = true;
        EX3_Tipo_Documento__c lTipoDocumento;
        List<EX3_ADJ__c> lLstADJObject;
        
        Schema.sObjectType lTipoDoc = aCaseId.getSObjectType(); 
        if(lTipoDoc == EX3_Tipo_Documento__c.sObjectType){ 
            lTipoDocumento = [SELECT Id, EX3_Subsidios__c, EX3_Arquivo_Anexado__c, EX3_Status_do_Documento__c FROM EX3_Tipo_Documento__c WHERE Id =: aCaseId];
        }
        
        if(lTipoDocumento != null){ 
                lTipoDocumento.EX3_Arquivo_Anexado__c = true; 
                lTipoDocumento.EX3_Status_do_Documento__c  = 'Documento Validado'; 
                Database.update(lTipoDocumento);   
         } 
        
        Set<String> lSetRecordTypesADJ = new Set<String>{EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Fechamento'),
            EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF')};
                
        Schema.sObjectType lADJ = aCaseId.getSObjectType(); 
        if(lADJ == EX3_ADJ__c.sObjectType){
            
            lLstADJObject = [SELECT Id, EX3_Status__c, RecordTypeId FROM EX3_ADJ__c WHERE Id =: aCaseId 
                             AND RecordTypeId =: lSetRecordTypesADJ];
            
            
            if(!lLstADJObject.isEmpty()){
                if(lLstADJObject[0].RecordTypeId == EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Fechamento') ){
                    lLstADJObject[0].EX3_Status__c = 'Concluido';
                    lLstADJObject[0].EX3_BypassADJ__c = true;
                }
                else if(lLstADJObject[0].RecordTypeId == EX3_Utils.getRecordTypeIdByDevName('EX3_ADJ__c', 'EX3_Cumprimento_OBF')){
                    lLstADJObject[0].EX3_Status_do_FUP__c = 'FUP concluído';
                }
                Database.update(lLstADJObject);
            }
        }
        List<ContentDocumentLink> lLstContendDocumentLink = [SELECT ContentDocumentId, LinkedEntityId, ContentDocument.CreatedDate
                                                             FROM ContentDocumentLink
                                                             WHERE LinkedEntityId =: lCentral.Id];
        Set<String> lSetDocumentId = new Set<String>();
        for(ContentDocumentLink iContentDocumentLink : lLstContendDocumentLink)
        { 
            lSetDocumentId.add(iContentDocumentLink.ContentDocumentId);
        } 
        List<ContentVersion> lLstContentVersion = new List<ContentVersion>();
        lLstContentVersion = [SELECT ContentDocumentId, PathOnClient
                              FROM ContentVersion
                              WHERE ContentDocumentId IN :lSetDocumentId];
        List<EX3_WrapperDocument> lLstWrapperDocument = new List<EX3_WrapperDocument>();
        for(ContentVersion iContentVersion : lLstContentVersion)
        { 
            EX3_WrapperDocument lWrapperDocument = new EX3_WrapperDocument();
            lWrapperDocument.name = iContentVersion.PathOnClient;
            lWrapperDocument.documentId = iContentVersion.ContentDocumentId; 
            lLstWrapperDocument.add(lWrapperDocument); 
        }
        Map<String,Object> lMapCentralAndDocuments = new Map<String,Object>();
        lMapCentralAndDocuments.put('listDocument', lLstWrapperDocument);
        lMapCentralAndDocuments.put('central', lCentral);
        
        return JSON.serialize(lMapCentralAndDocuments); 
    }
    
    @AuraEnabled
    public static String associarArquivos(String aCentral, Id aCaseId)
    {  
        String lException = '';  
        List<EX3_ObjectFactory.IncluirDocumento> lLstIncluirDocumento;
        List<sObject> lLstObject;
        String lQuery; 
        Set<String> lSetContentDocumentId;
        List<AttributesTypesContentDocument> lLstAttributesTypesContentDocument = new List<AttributesTypesContentDocument>();
        try  
        {    
            Schema.sObjectType lTypeObject = aCaseId.getSObjectType();
            if(lTypeObject == Case.sObjectType){
                lLstObject = [SELECT Id FROM Case WHERE Id =: aCaseId]; 
            }   
            else if(lTypeObject == EX3_Subsidios__c.sObjectType){
                lQuery = 'SELECT ';
                for(string lcampo : CAMPOS_SUBSIDIOS){ 
                    lQuery += ' ' + lcampo + ' ';  
                }
                lQuery += ' FROM EX3_Subsidios__c WHERE ' + 'Id' + '=: aCaseId';
            }  
            else if(lTypeObject == EX3_Cadastro__c.sObjectType){
                lQuery = 'SELECT ';
                for(string lcampo : CAMPOS_CADASTRO){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_Cadastro__c WHERE ' + 'Id' + '=: aCaseId';
            }
            else if(lTypeObject == EX3_Tipo_Documento__c.sObjectType){
                lQuery = 'SELECT ';
                for(string lcampo : CAMPOS_TIPODOC){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_Tipo_Documento__c WHERE ' + 'Id' + '=: aCaseId';
            }
            else if(lTypeObject == EX3_Calculo__c.sObjectType){
                lQuery = 'SELECT ';
                for(string lcampo : CAMPOS_CALCULO){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_Calculo__c WHERE ' + 'Id' + '=: aCaseId';
            } 
            else if(lTypeObject == EX3_DefinicaoEstrategia__c.sObjectType){
                lQuery = 'SELECT '; 
                for(string lcampo : CAMPOS_DEFINICAO_ESTRATEGICA){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_DefinicaoEstrategia__c WHERE ' + 'Id' + '=: aCaseId';
            } 
            else if(lTypeObject == EX3_ADJ__c.sObjectType){
                lQuery = 'SELECT '; 
                for(string lcampo : CAMPOS_ADJ){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_ADJ__c WHERE ' + 'Id' + '=: aCaseId';
            }
            else if(lTypeObject == EX3_Laudo__c.sObjectType){
                lQuery = 'SELECT '; 
                for(string lcampo : CAMPOS_LAUDO){ 
                    lQuery += ' ' + lcampo + ' '; 
                }
                lQuery += ' FROM EX3_Laudo__c WHERE ' + 'Id' + '=: aCaseId';
            }
            
            
            EX3_Centralizador__c lCentral = (EX3_Centralizador__c)JSON.deserialize(aCentral, EX3_Centralizador__c.class);
            String lIdLinked; 
            if(lTypeObject != Case.sObjectType){  
                lLstObject = Database.query(String.escapeSingleQuotes(lQuery));
                lIdLinked = (String)lLstObject[0].get('EX3_Caso__c'); 
            } //aqui seria a query dinâmica para recuperar de qualquer objeto
            String lIdCase = lLstObject[0].Id;  
          
            List<ContentDocumentLink> lLstContentDocumentLink = new List<ContentDocumentLink>();
            lSetContentDocumentId = new Set<String>(); 
            for(ContentDocumentLink iContentDocumentLink : [SELECT Id, ContentDocumentId, LinkedEntityId, 
                                                            ContentDocument.CreatedDate, ShareType, 
                                                            Visibility, ContentDocument.Title 
                                                            FROM ContentDocumentLink          
                                                            WHERE LinkedEntityId = : lCentral.Id     
                                                            AND ContentDocument.CreatedDate = TODAY])
            { 
                lSetContentDocumentId.add(iContentDocumentLink.ContentDocumentId);
                ContentDocumentLink lContentDocumentLink = iContentDocumentLink.clone(); 
                if(String.isNotBlank(lIdLinked)){ 
                    lContentDocumentLink.LinkedEntityId = (String)lLstObject[0].get('EX3_Caso__c');  
                }else if(String.isNotBlank(lIdCase)){ 
                    lContentDocumentLink.LinkedEntityId = lLstObject[0].Id;
                }         
                lLstContentDocumentLink.add(lContentDocumentLink);  
            }  
            Database.insert(lLstContentDocumentLink);
            lLstIncluirDocumento = new List<EX3_ObjectFactory.IncluirDocumento>();
            User lUser = [SELECT FuncionalColaborador__c FROM User WHERE Id = :UserInfo.getUserId()];
            for (ContentDocumentLink iContentDocumentLink : lLstContentDocumentLink)
            {
                EX3_ObjectFactory.IncluirDocumento lIncluirDocumento = new EX3_ObjectFactory.IncluirDocumento();
                
                lIncluirDocumento.nome_documento = iContentDocumentLink.ContentDocument.Title;
                lIncluirDocumento.codigo_documento_plataforma_externa = iContentDocumentLink.Id;
                lIncluirDocumento.operador_conglomerado = lUser.FuncionalColaborador__c;
                
                lLstIncluirDocumento.add(lIncluirDocumento);
            }
            
        }
        catch(DmlException ex) 
        {
            lException = ex.getMessage();
            SA7_CustomdebugLog.logError('EX3', 'EX3_CaseAssociatedFilesController', 'associarArquivos', 'Erro ao atualizar o registro do Caso '+lException, ex, null);
        }
        catch (Exception ex)
        {
            lException = ex.getMessage();
            SA7_CustomdebugLog.logError('EX3', 'EX3_CaseAssociatedFilesController', 'associarArquivos', 'Erro ao atualizar o registro '+ex.getMessage(), ex, null);
        }
        finally
        {
            if(String.isBlank(lException))
            {
                SA7_CustomdebugLog.logWarn('EX3', 'EX3_CaseAssociatedFilesController', 'associarArquivos', 'Sucesso ao atualizar registro do Caso', null);
            }
        }
        
        return JSON.serialize(new Map<String,String> {
            'documentos' => JSON.serialize(lLstIncluirDocumento),
                'objeto' => JSON.serialize(lLstObject),
                'documentsIds' => JSON.serialize(lSetContentDocumentId)
                });
    }
    
    @AuraEnabled(Continuation=true)
    public static Continuation enviarArquivos(String aParams)
    {   
        Map<String, String> lMapParams = (Map<String,String>) JSON.deserialize(aParams, Map<String,String>.class);
        Map<String, String> lMapDadosRequest = new Map<String,String>
        {  
            'objeto' => lMapParams.get('objeto'),  
                'classe' => lMapParams.get('classe'),
                'body' => lMapParams.get('documentos'),
                'callback' => 'enviarArquivosCallback',
                'service' => 'incluirDocumento',
                'rollback' => lMapParams.get('documentsIds') 
                }; 
                    Continuation lContinuation;   
                    
 
        SObject lObject = (SObject) JSON.deserialize(lMapParams.get('objeto'), SObject.class);      
        try              
        {        
            lContinuation = EX3_ServiceFactory.incluirDocumento(lMapParams.get('documentos'), 'EX3_BuscaAtivaController', 'enviarArquivosCallback', lMapDadosRequest, JSON.serialize(lObject));
        }
        catch (Exception ex)
        {
            SA7_CustomdebugLog.logError('EX3', 'EX3_BuscaAtivaController', 'enviarArquivos', 'Erro ao criar o Request '+ ex.getMessage(), ex, null);
        }
        return lContinuation;
        
    }
    
    @AuraEnabled
    public static String enviarArquivosCallback(Object state)
    {
        EX3_ContinuationUtils.StateInfo lRetorno = (EX3_ContinuationUtils.StateInfo)state;
        
        HttpResponse lResponse = Continuation.getResponse(lRetorno.state);
        
        Map<String,String> lMapDados = lRetorno.dados;
        
        if (lResponse.getStatusCode() > 202)
        {
            SA7_CustomdebugLog.logWarn('EX3', 'Ex3_CaseAssociatedFilesController', 'enviarArquivosCallback','Status: ' + lResponse.getStatusCode() + '\n' + 
                                       lResponse.getBody(), null);
            Map<String,String> lResponseMsg = (Map<String, String>) JSON.deserialize(lResponse.getBody(), Map<String,String>.class);
            
            String lMensagem = 'Unkown Error';
            if (String.isNotBlank(lResponseMsg.get('message')))
            {
                lMensagem = lResponseMsg.get('message');
            }
            else if (String.isNotBlank(lResponseMsg.get('Message')))
            {
                lMensagem = lResponseMsg.get('Message');
            }
            
            Set<String> lSetContentDocumentId = (Set<String>)JSON.deserialize(lMapDados.get('rollback'), Set<String>.class);
            
            List<ContentDocument> lLstContentDocument = [SELECT Id FROM ContentDocument WHERE Id = :lSetContentDocumentId];
            database.delete(lLstContentDocument);
            
            return JSON.serialize(new Map<String, Object> 
                                  {
                                      'error' => lMensagem,
                                          'status' => lResponse.getStatusCode()
                                          });
        }
        
        //adicionado caso igual a fechado - Ponto de dúvida  
        Case lCase = (Case) JSON.deserialize(lMapDados.get('Case'), Case.class); 
        lCase = (String.isNotBlank(lCase.Id)) ? [SELECT Id, Status FROM Case WHERE Id = : lCase.Id] : lCase;  
        if(lCase.Status == 'Closed')
        {
            lCase.Status = 'New'; 
            database.update(lCase) ; 
        }
        //adicionado caso motivo igual a devolvido 
        
        return JSON.serialize(lResponse.getBody());
    } 
    
    private class AttributesTypesContentDocument
    {
        private Object label {get;set;}
        private Object size {get;set;}
        private Object createdDate {get;set;}
        private Object fileExtension{get;set;}
        private Object url {get;set;}
        private Object icon {get;set;}
    } 
    
}