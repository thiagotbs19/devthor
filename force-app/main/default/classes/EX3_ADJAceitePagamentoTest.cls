/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de teste : EX3_ADJAceitePagamentoTest  
*
* Autor: Thiago Barbosa 
* Apex Class Test: EX3_ADJAceitePagamentoTest 
* Test Class:  
*
********************************************************************************/


@isTest
public with sharing class EX3_ADJAceitePagamentoTest {
    


    @TestSetup
    public static void createDataADJ(){

        Case lCase = EX3_BI_DataLoad.getCase(); 

        Database.insert(lCase);
		
        List<Group> lLstGroup = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName = 'EX3_Analise_de_decisao'];
        EX3_ADJ__c lADJ = EX3_BI_DataLoad.getADJ(lCase); 
        lADJ.EX3_Data_da_Publicacao__c = Date.today();
        lADJ.EX3_Tipo_de_Decisao__c = 'Liminar';
        lADJ.OwnerId = lLstGroup[0].Id;
        lADJ.EX3_Resultado_da_Decisao__c = 'Improcedente';
        lADJ.EX3_Multa_OBF__c = 'Arbitrada';
        lADJ.EX3_Valor_Multa_OBF__c = 2.0;
        lADJ.EX3_Checklist_de_Cumprimento__c = 'Pagamento';

        Database.upsert(lADJ);  
        
    }

    @isTest
    public static void testAceitePagamento(){
        Case lCase = [SELECT Id FROM Case LIMIT 1];

        EX3_ADJ__c lADJ = [SELECT Id FROM EX3_ADJ__c LIMIT 1];

        Test.startTest();
            String lReturn1 = EX3_ADJAceitePagamento.getQueueCumprimentoPag(lADJ.Id);
            String lReturn2 = EX3_ADJAceitePagamento.updateDOPPagSolicitado(lADJ.Id);  
            String lReturn3 = EX3_ADJAceitePagamento.updateDOPPagSolicitado(null);  
            String lReturn4 = EX3_ADJAceitePagamento.getQueueCumprimentoPag(null);   
        Test.stopTest();  
        System.assert(lReturn1 != null, 'Registro de Cumprimento Pagamento não foi enviado para a fila');
    }

    @isTest
    public static void catchExceptionTest(){
        String fakeRecordId = 'auheuaheuahe';
        Test.startTest();
        String lReturn1 = EX3_ADJAceitePagamento.updateDOPPagSolicitado(fakeRecordId);
        String lReturn2 = EX3_ADJAceitePagamento.getQueueCumprimentoPag(fakeRecordId);
        test.stopTest(); 
        System.assert(lReturn1 != null, 'Registro não foi atualizado para Pagamento Solicitado');

    }

    @isTest
    public static void testHasPermission(){
 
        Test.startTest(); 
            Boolean lHasPermission = EX3_ADJAceitePagamento.getHasPermissionSet(); 
        Test.stopTest();   
        System.assert(!lHasPermission, 'Permissão não habilitada para este permission Set');
    }  
}