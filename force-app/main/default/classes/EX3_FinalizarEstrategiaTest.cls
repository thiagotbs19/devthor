/*********************************************************************************
*                                    Itaú - 2020
*
* Autor: Thiago Barbosa de Souza
* Classe de Teste: EX3_FinalizarEstrategiaTest 
*
********************************************************************************/
@isTest
public with sharing class EX3_FinalizarEstrategiaTest {


        @TestSetup
        public static void createData(){ 
            
            User lUser = EX3_BI_DataLoad.getUser();

            Database.insert(lUser); 

            Case lCase = EX3_BI_DataLoad.getCase();

            Database.insert(lCase);
            
            EX3_Cadastro__c lCadastro = EX3_BI_DataLoad.getCadastro(lCase);
            lCadastro.EX3_Carteira__c = '18';
            lCadastro.EX3_Macro_Carteira__c = '1'; 

            Database.insert(lCadastro); 

            EX3_Subsidios__c lSubsidios = EX3_BI_DataLoad.getCapturaDocumentos(lCase);
            lSubsidios.EX3_Cadastro__c = lCadastro.Id;
            Database.insert(lSubsidios);
            
            EX3_Pedidos__c lPedidos = new EX3_Pedidos__c();
            lPedidos.EX3_Caso__c = lCase.Id;
            lPedidos.EX3_Pedido__c = 'Contrato Não Reconhecido';
            lPedidos.EX3_Complemento_pedido__c = 'EX3_Proposta_excluída';
            
            Database.insert(lPedidos); 
            
            EX3_Matriz_Tipo_Documento__c lMatrizTipoDoc = new EX3_Matriz_Tipo_Documento__c();
            lMatrizTipoDoc.EX3_Carteira__c = 'IC';
            lMatrizTipoDoc.EX3_Macro_Carteira__c = 'Cível';
            lMatrizTipoDoc.EX3_Tipo_Estrategia__c = 'Complementar';
            lMatrizTipoDoc.EX3_Pedidos_do_Processo__c = 'Contrato Não Reconhecido'; 
			lMatrizTipoDoc.EX3_Documento__c = 'Contrato IC';
            lMatrizTipoDoc.EX3_Complemento_pedido__c = 'EX3_Proposta_excluída';
            Database.insert(lMatrizTipoDoc);
            
            EX3_DefinicaoEstrategia__c lDefEst = EX3_BI_DataLoad.getDefEstrategica(lCase);
            lDefEst.EX3_Subsidios__c = lSubsidios.Id;
            lDefEst.EX3_Estrategia__c = 'Não Definido'; 
            Database.insert(lDefEst);

        }

        @isTest static void testfinalizarEstrategia(){

            User lUser = [SELECT Id FROM User LIMIT 1];

            Case lCase = [SELECT Id FROM Case LIMIT 1];
            
			EX3_Pedidos__c lPedidos = [SELECT Id FROM EX3_Pedidos__c LIMIT 1];            

            EX3_DefinicaoEstrategia__c lDefEst = [SELECT Id FROM EX3_DefinicaoEstrategia__c LIMIT 1];

            Test.startTest();
            String lResult = EX3_FinalizarEstrategia.getflowExecuteEstrategia(lDefEst.Id);
            Test.stopTest();
            System.assert(lResult != null, 'Flow executado'); 
        }
    
     @isTest static void testfinalizarEstrategiaAcordo(){

            User lUser = [SELECT Id FROM User LIMIT 1];

            Case lCase = [SELECT Id FROM Case LIMIT 1];
            
			EX3_Pedidos__c lPedidos = [SELECT Id FROM EX3_Pedidos__c LIMIT 1];            

            EX3_DefinicaoEstrategia__c lDefEst = [SELECT Id FROM EX3_DefinicaoEstrategia__c LIMIT 1];
			lDefEst.EX3_Estrategia__c = 'Acordo';
         
         	Database.update(lDefEst);
         
            Test.startTest();
            String lResult = EX3_FinalizarEstrategia.getflowExecuteEstrategia(lDefEst.Id);
            Test.stopTest(); 
            System.assert(lResult != null, 'Flow executado');
        }
    
    @isTest static void testfinalizarEstrategiaNull(){
        
        Test.startTest();
        	String lResult = EX3_FinalizarEstrategia.getflowExecuteEstrategia(null);
        Test.stopTest();
        System.assert(lResult == null, 'Flow não executado');
    }

    @isTest static void testHaspermission(){

        Test.startTest();
            Boolean lHasPermission = EX3_FinalizarEstrategia.getHasPermissionSet(); 
        Test.stopTest();  
        System.assert(lHasPermission != null, 'Permissão atribuida'); 
    }
}