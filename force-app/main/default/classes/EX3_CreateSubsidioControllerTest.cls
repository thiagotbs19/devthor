/*********************************************************************************
*                                    Itaú - 2020  
*
*Descrição: Classe de Teste da EX3_CreateSubsidioController
*
* 
*Apex Class: EX3_CreateSubsidioControllerTest   
*   
*
* Empresa: everis do Brasil
* Autor: Thiago Barbosa 
*
********************************************************************************/

@isTest
public class EX3_CreateSubsidioControllerTest {



    @TestSetup
    public static void testSetupCreateData(){ 
		
       
        Account lAccount = new Account();
        lAccount.Name = 'Teste Account'; 

        Database.insert(lAccount); 

        User lUser = EX3_BI_DataLoad.getUser();
        lUser.FuncionalColaborador__c = '002';  
        lUser.EX3_Pertence__c = '2'; 
        Database.insert(lUser); 

        Case lCase = new Case();
        lCase.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Recepcao');
        lCase.EX3_Motivo_Situacao__c = '14'; 
        lCase.EX3_Canal_de_Entrada__c = lUser.EX3_Pertence__c;
        lCase.EX3_Numero_do_Processo__c = '2019001';
        lCase.EX3_Prioridade__c = 'Urgente';
        lCase.EX3_Data_de_Entrada__c = system.now();
        database.insert(lCase);
         
        EX3_Cadastro__c lCadastro = new EX3_Cadastro__c();
        lCadastro.EX3_Caso__c = lCase.Id;    
        lCadastro.OwnerId = UserInfo.getUserId(); 
        lCadastro.EX3_Macro_Carteira__c = '1';
        lCadastro.EX3_Carteira__c = '18';  
       Test.startTest();
	   Database.insert(lCadastro); 
       Test.stopTest();
        
       Contract lContract = new Contract();
       lContract.Status = 'Draft';   
       lContract.EX3_Caso__c = lCase.Id; 
       lContract.AccountId = lAccount.Id;   

       Database.insert(lContract); 

       EX3_Subsidios__c lSubsidio = new EX3_Subsidios__c(); 
       lSubsidio.EX3_Numero_do_Processo__c = lCadastro.EX3_Numero_do_Processo__c; 

       Database.insert(lSubsidio); 

       EX3_Tipo_Documento__c lEX3TipoDocumento = new EX3_Tipo_Documento__c();
       lEX3TipoDocumento.EX3_Documento__c	= 'Teste Doc';   
       lEX3TipoDocumento.EX3_Subsidios__c= lSubsidio.id;   

        Database.insert(lEX3TipoDocumento);

        EX3_Matriz_Tipo_Documento__c lEX3ParmDocsNR = new EX3_Matriz_Tipo_Documento__c();
        lEX3ParmDocsNR.EX3_Carteira__c = 'IC'; 
        lEX3ParmDocsNR.EX3_Macro_Carteira__c = 'Cível';   
        lEX3ParmDocsNR.EX3_Pedidos_do_Processo__c = Label.EX3_Pedidos_do_Processo;
        lEX3ParmDocsNR.EX3_Tipo_Estrategia__c = 'Pré Estratégia'; 
        lEX3ParmDocsNR.EX3_Documento__c = 'SERASA';
        lEX3ParmDocsNR.EX3_Complemento_pedido__c = 'EX3_Proposta_excluída';  
        Database.insert(lEX3ParmDocsNR);  

        EX3_pedidos__c lPedidos = new EX3_pedidos__c();
        lPedidos.EX3_Complemento_pedido__c = 'EX3_Proposta_excluída';  
        lPedidos.EX3_Pedido__c = 'Contrato não reconhecido'; 
        lPedidos.EX3_Caso__c = lCase.Id;  
  
        Database.insert(lPedidos); 
       
    }
    

    @isTest 
    public static void testredirectToObject(){
         
        User lUser = [SELECT Id FROM User LIMIT 1];   


        Case lProtocolo = [SELECT Id FROM Case LIMIT 1];
            
   
        EX3_Cadastro__c lCadastro = [SELECT Id, EX3_Macro_Carteira__c, EX3_Carteira__c FROM EX3_Cadastro__c LIMIT 1];
        
        lCadastro.EX3_Status__c = 'Cadastrado'; 
        
        Database.update(lCadastro);      

        Contract lContract = [SELECT Id, Status FROM Contract LIMIT 1];

        lContract.Status = 'Activated';
        Database.update(lContract);
          
        EX3_Subsidios__c lSubsidios = [SELECT Id FROM EX3_Subsidios__c LIMIT 1];

        EX3_Tipo_Documento__c lTipoDocumento = [SELECT Id FROM EX3_Tipo_Documento__c LIMIT 1];
          
        EX3_pedidos__c lPedidos = [SELECT Id FROM EX3_pedidos__c LIMIT 1];  
        
        EX3_Matriz_Tipo_Documento__c lEX3ParmDocsNR = [SELECT Id FROM EX3_Matriz_Tipo_Documento__c LIMIT 1];
        Integer lQuantidadeContratos = 0;
        lQuantidadeContratos = [Select COUNT() from contract where EX3_Cadastro__c =: lCadastro.Id];  

        Test.startTest(); 
            String lLCadastro = EX3_CreateSubsidioController.redirectToObject(lCadastro.Id);
        Test.stopTest();  
        System.assert(lLCadastro != null, 'O Cadastro foi Finalizado');  

    }
    
    @isTest static void testHasPermissionSet(){

        Test.startTest();
            Boolean lHasPermission = EX3_CreateSubsidioController.getHasPermissionSet();
        Test.stopTest(); 
        System.assert(!lHasPermission, 'Não há Permissão atribuída');
    } 
}