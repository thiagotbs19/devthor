/********************************************************************************
Nome:  SA7_auditPermissionSet
Copyright © 2019 COE
=================================================================================
Função: Processo de auditoria para garantir que os perfis e conjuntos de permissões
estão de acordo com o definido pela governança.
=================================================================================
Histórico                                                            
-------                                                            
VERSÃO: 1.0                       AUTOR:TCSKRBJ
DATA: 02/05/19
DESCRIÇÃO: Versão Inicial
*********************************************************************************/
/************************************Wishlist************************************
1- Fazer a lista de exceções considerar permissões por perfil/conjunto de permissão.
Por exemplo: O perfil COE tem permissão para publicar Classes Apex, mas não tem permissão de gerir Sandboxes.
Então, a restrição do APEX não apareceria como um alerta, mas a de Sandbox, sim.
*********************************************************************************/

public class SA7_auditPermissionSet {
    @future (callout=true)
    public static void audit(Boolean isSandbox){
        List<PermissionSet> permissions = new List<PermissionSet>();
        Map<Id, Profile> profiles = new Map<Id, Profile>();
        Map<Id, String> whitelistMap = new Map<Id, String>();
        String postChatter = '';
        
        //Obtem a lista de PermissionSet
        permissions = getPermissions(isSandbox);        
        
        //Obter o nome dos perfis para facilitar a identificação     
        profiles = getProfiles(permissions);
        
        //Obter lista de exceção
        whitelistMap = getAuditExceptions();
        
        //Fazer a publicação no chatter com a validação se é sandbox para a permissão de managesandbox
        for (PermissionSet permission : permissions){
            
            postChatter = postChatter + postBuilder(permission, profiles, whitelistMap, isSandbox);
        }
        //Criar método para publicar na Sandbox do CoE.
        coePublisher(permissions, profiles, whitelistMap);

        postPublisher(postChatter);

    }
    
    private static String postBuilder(PermissionSet permission, Map<Id, Profile> profiles, Map<Id, String> exceptions, boolean isSandbox){
        String post = '';
        if(permission.PermissionsAuthorApex)
        {
            post = post + ' PermissionsAuthorApex'; 
        }
        if(permission.PermissionsBulkApiHardDelete)
        {
            post = post + ' PermissionsBulkApiHardDelete'; 
        }
        if(permission.PermissionsCustomizeApplication)
        {
            post = post + ' PermissionsCustomizeApplication'; 
        }
        if(permission.PermissionsManageCertificates)
        {
            post = post + ' PermissionsManageCertificates'; 
        }
        if(permission.PermissionsManageCustomPermissions)
        {
            post = post + ' PermissionsManageCustomPermissions'; 
        }
        if(permission.PermissionsManageInternalUsers)
        {
            post = post + ' PermissionsManageInternalUsers'; 
        }
        if(permission.PermissionsManageIpAddresses)
        {
            post = post + ' PermissionsManageIpAddresses'; 
        }
        if(permission.PermissionsManageLoginAccessPolicies)
        {
            post = post + ' PermissionsManageLoginAccessPolicies'; 
        }
        if(permission.PermissionsManagePasswordPolicies)
        {
            post = post + ' PermissionsManagePasswordPolicies'; 
        }
        if(permission.PermissionsManageProfilesPermissionsets)
        {
            post = post + ' PermissionsManageProfilesPermissionsets'; 
        }
        if(permission.PermissionsManageRoles)
        {
            post = post + ' PermissionsManageRoles'; 
        }
        if(permission.PermissionsManageSharing)
        {
            post = post + ' PermissionsManageSharing'; 
        }
        if(permission.PermissionsManageUsers)
        {
            post = post + ' PermissionsManageUsers'; 
        }
        if(permission.PermissionsModifyAllData)
        {
            post = post + ' PermissionsModifyAllData'; 
        }
        if(permission.PermissionsModifyMetadata)
        {
            post = post + ' PermissionsModifyMetadata'; 
        }
        if(permission.PermissionsPasswordNeverExpires)
        {
            post = post + ' PermissionsPasswordNeverExpires'; 
        }
        if(permission.PermissionsResetPasswords)
        {
            post = post + ' PermissionsResetPasswords'; 
        }
        /* if(permission.PermissionsScheduleJob)
        {
            post = post + ' PermissionsScheduleJob'; 
        } */
        if(permission.PermissionsViewAllData)
        {
            post = post + ' PermissionsViewAllData'; 
        }
        if(permission.PermissionsViewAllUsers)
        {
            post = post + ' PermissionsViewAllUsers'; 
        }
        if(permission.PermissionsViewEncryptedData)
        {
            post = post + ' PermissionsViewEncryptedData'; 
        }
        
        if (!isSandbox)
        {
            if((boolean)permission.get('PermissionsManageSandboxes'))
            {
                post = post + ' PermissionsManageSandboxes'; 
            }
        }
        
        if (post != '')
        {
            if (exceptions.containsKey(permission.ProfileId))
            {
                post = 'O perfil com o ID ' + permission.ProfileId + ' é uma exceção. Justificativa: ' + exceptions.get(permission.ProfileId) + '\n';
            } 
            else 
            {
                if (exceptions.containsKey(permission.Id))
                {
                    post = 'O conjunto de permissões com o ID ' + permission.Id + ' é uma exceção. Justificativa: ' + exceptions.get(permission.Id) + '\n';
                }
                else{
                    if (permission.IsOwnedByProfile){
                        //existem perfis que são de comunidades. Eles sempre tem 2 permissões ativadas, portanto devem ser desconsiderados.
                        if (profiles.get(permission.ProfileId) != null){
                            post = 'O perfil ' + profiles.get(permission.ProfileId).Name + ', com o ID: ' + permission.ProfileId + ' possui as seguintes permissões:' + post + '.\n';
                        }
                        else{
                            //Limpar a variável para não postar no chatter
                            post = '';
                        }
                    }
                    else{
                        post = 'O conjunto de permissões ' + permission.Label + ', com o ID: ' + permission.Id + ' possui as seguintes permissões:' + post + '.\n';
                    }
                }
            }
        }
        return post;        
    }
    private static void postPublisher(string p_post)
    {
        id idParent;
        list<CollaborationGroup> groupList = new list<CollaborationGroup>();
        
        groupList = [SELECT Id FROM CollaborationGroup Where Name = 'Monitor de Auditoria' Limit 1];
        
        idParent = (groupList.isEmpty()?UserInfo.getUserId():groupList[0].Id);
        
        FeedItem post = new FeedItem();
        
        post.ParentId = idParent;
        
        post.Body = p_post;
        
        insert post;
    }
    
    private static List<PermissionSet> getPermissions(Boolean isSandbox){
        
        String sQuery;
        
        if (isSandbox){
            sQuery = 'Select Id, Description, Label, Name, ProfileId, PermissionsAuthorApex, PermissionsBulkApiHardDelete, PermissionsCustomizeApplication, PermissionsManageCertificates, PermissionsManageCustomPermissions, PermissionsManageInternalUsers, PermissionsManageIpAddresses, PermissionsManageLoginAccessPolicies, PermissionsManagePasswordPolicies, PermissionsManageProfilesPermissionsets, PermissionsManageRoles, PermissionsManageSharing, PermissionsManageUsers, PermissionsModifyAllData, PermissionsModifyMetadata, PermissionsPasswordNeverExpires, PermissionsResetPasswords, PermissionsScheduleJob, PermissionsViewAllData, PermissionsViewAllUsers, PermissionsViewEncryptedData, IsOwnedByProfile FROM PermissionSet WHERE PermissionsAuthorApex = true OR PermissionsBulkApiHardDelete = true OR PermissionsCustomizeApplication = true OR PermissionsManageCertificates = true OR PermissionsManageCustomPermissions = true OR PermissionsManageInternalUsers = true OR PermissionsManageIpAddresses = true OR PermissionsManageLoginAccessPolicies = true OR PermissionsManagePasswordPolicies = true OR PermissionsManageProfilesPermissionsets = true OR PermissionsManageRoles = true OR PermissionsManageSharing = true OR PermissionsManageUsers = true OR PermissionsModifyAllData = true OR PermissionsModifyMetadata = true OR PermissionsPasswordNeverExpires = true OR PermissionsResetPasswords = true OR PermissionsScheduleJob = true OR PermissionsViewAllData = true OR PermissionsViewAllUsers = true OR PermissionsViewEncryptedData = true';
        }
        else{
            sQuery = 'Select Id, Description, Label, Name, ProfileId, PermissionsAuthorApex, PermissionsBulkApiHardDelete, PermissionsCustomizeApplication, PermissionsManageCertificates, PermissionsManageCustomPermissions, PermissionsManageInternalUsers, PermissionsManageIpAddresses, PermissionsManageLoginAccessPolicies, PermissionsManagePasswordPolicies, PermissionsManageProfilesPermissionsets, PermissionsManageRoles, PermissionsManageSharing, PermissionsManageUsers, PermissionsModifyAllData, PermissionsModifyMetadata, PermissionsPasswordNeverExpires, PermissionsResetPasswords, PermissionsScheduleJob, PermissionsViewAllData, PermissionsViewAllUsers, PermissionsViewEncryptedData, PermissionsManageSandboxes, IsOwnedByProfile FROM PermissionSet WHERE PermissionsAuthorApex = true OR PermissionsBulkApiHardDelete = true OR PermissionsCustomizeApplication = true OR PermissionsManageCertificates = true OR PermissionsManageCustomPermissions = true OR PermissionsManageInternalUsers = true OR PermissionsManageIpAddresses = true OR PermissionsManageLoginAccessPolicies = true OR PermissionsManagePasswordPolicies = true OR PermissionsManageProfilesPermissionsets = true OR PermissionsManageRoles = true OR PermissionsManageSharing = true OR PermissionsManageUsers = true OR PermissionsModifyAllData = true OR PermissionsModifyMetadata = true OR PermissionsPasswordNeverExpires = true OR PermissionsResetPasswords = true OR PermissionsScheduleJob = true OR PermissionsViewAllData = true OR PermissionsViewAllUsers = true OR PermissionsViewEncryptedData = true OR PermissionsManageSandboxes = true';
        }
        return Database.query(sQuery);
    }
    
    private static Map<Id, Profile> getProfiles(List<PermissionSet> permissions){
        
        Set<Id> profileIds = new Set<Id>();
        for (PermissionSet permission : permissions){
            profileIds.add(permission.ProfileId);
        }        
        return new Map<Id, Profile>([select id, name from Profile where id in :profileIds and Usertype <> 'Guest']);
    }
    
    private static Map<Id, String> getAuditExceptions(){
        
        Map<Id, String> whitelistMap = new Map<Id, String>();
        
        //Obter a lista de permissões e transformar em um Map
        List<SA7_SystemOverview__mdt> exceptions = [SELECT MasterLabel, SA7_reason__c FROM SA7_SystemOverview__mdt WHERE SA7_Indicatortype__c =:'PermissionException'];
        for (SA7_SystemOverview__mdt exceptionReason : exceptions)
        {
            whitelistMap.put(exceptionReason.MasterLabel, exceptionReason.SA7_reason__c);
        }
        return whitelistMap;
    }
    private static void coePublisher(List<PermissionSet> permissions, Map<Id, Profile> profiles, Map<Id, String> whitelist){
        //Enviar a lista de Permissões fora do padrão
        //A lista de perfis
        //A lista de exceção.
        
        //Converter a lista de Permissoes para Mapa.
        List<SA7_PermissionSet> permissoesList = converteParaSA7_PermissionSet(permissions);
        
        //This custom metadata type is designed to register the parameters of integration with COE org
        SA7_SystemOverview__mdt SO = [SELECT SA7_indicatorsEndPoint__c, SA7_loginEndPoint__c ,MasterLabel, SA7_cleanContentMock__c, SA7_clientId__c, SA7_clientSecret__c, SA7_password__c, SA7_username__c, SA7_partialURL__c FROM SA7_SystemOverview__mdt WHERE MasterLabel=:'CredentialsAudit'];

        //Making callout to login in COE org
        Httprequest req = new HttpRequest();
        req.setMethod('POST');    
        req.setBody('grant_type=password + &client_id=' + SO.SA7_clientId__c + '&client_secret=' + SO.SA7_clientSecret__c + '&username=' + SO.SA7_username__c +'&password=' + SO.SA7_password__c); 
        req.setEndpoint(SO.SA7_loginEndPoint__c);
        Http http = new Http();
        String Access_Token;
        
        //Getting access token
        try {
			HttpResponse res = http.send(req);                
        	JSONParser parser = JSON.createParser(res.getBody());
            system.debug(res.getBody());
        	while (parser.nextToken() != null) {
            	if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                    parser.nextToken();
                    Access_Token = parser.getText();    
                }
            }            
        }catch(system.CalloutException e){            
            System.debug('The following exception has occurred: ' + e.getMessage());
            SA7_Util.logError('SA7', 'SA7_auditProfileCustomObject', 'coePublisher', 'SA7: retrieving access token from COE Sandbox', e, null);
        }
        System.debug(Access_Token);
        //Sending overview extraction to COE org
        HttpRequest req1 = new HttpRequest();  
        req1.setEndpoint(SO.SA7_indicatorsEndPoint__c);  
        req1.setMethod('POST');    
        req1.setHeader('Content-Type','application/json');
        req1.setHeader('Authorization','Bearer '+Access_Token);
        //Para auditoria de Perfil, função é igual a 1.
        String requestBody = '{"functionID" : "1","OrgId" : "' +system.UserInfo.getOrganizationId()+ '"';
        requestBody = requestBody + ',"lang" : "'+UserInfo.getLanguage()+ '"';
        requestBody = requestBody + ',"permissionSetList" : ' +JSOn.serialize(permissoesList);
        requestBody = requestBody + ',"profileList" : ' + JSon.serialize(profiles.values());
        requestBody = requestBody + ',"whitelist" : ' + JSON.serialize(whitelist.keySet());
        requestBody = requestBody + '}';
        
        req1.setBody(requestBody);
        Http http1 = new Http();
        try{
            HttpResponse res1 = http1.send(req1);
            system.debug(res1.getBody());
        }catch(system.CalloutException e){            
            System.debug('The following exception has occurred: ' + e.getMessage());
            SA7_Util.logError('SA7', 'SA7_auditProfileCustomObject', 'coePublisher', 'Envio de parametros para a Sandbox do CoE', e, null);
        }
    }
    
    private static List<SA7_PermissionSet> converteParaSA7_PermissionSet (List<PermissionSet> permissionsetList)
    {
        List<SA7_PermissionSet> SA7_PermissionSetList = new List<SA7_PermissionSet>();
        
        for (PermissionSet permission : permissionsetList)
        {
            SA7_PermissionSet item = new SA7_PermissionSet();
            item.ProfileId = permission.ProfileId;
            item.PermissionsAuthorApex = permission.PermissionsAuthorApex;
            item.PermissionsBulkApiHardDelete = permission.PermissionsBulkApiHardDelete;
            item.PermissionsCustomizeApplication = permission.PermissionsCustomizeApplication;
            item.PermissionsManageCertificates = permission.PermissionsManageCertificates;
            item.PermissionsManageCustomPermissions = permission.PermissionsManageCustomPermissions;
            item.PermissionsManageInternalUsers = permission.PermissionsManageInternalUsers;
            item.PermissionsManageIpAddresses = permission.PermissionsManageIpAddresses;
            item.PermissionsManageLoginAccessPolicies = permission.PermissionsManageLoginAccessPolicies;
            item.PermissionsManagePasswordPolicies = permission.PermissionsManagePasswordPolicies;
            item.PermissionsManageProfilesPermissionsets = permission.PermissionsManageProfilesPermissionsets;
            item.PermissionsManageRoles = permission.PermissionsManageRoles;
            item.PermissionsManageSharing = permission.PermissionsManageSharing;
            item.PermissionsManageUsers = permission.PermissionsManageUsers;
            item.PermissionsModifyAllData = permission.PermissionsModifyAllData;
            item.PermissionsModifyMetadata = permission.PermissionsModifyMetadata;
            item.PermissionsPasswordNeverExpires = permission.PermissionsPasswordNeverExpires;
            item.PermissionsResetPasswords = permission.PermissionsResetPasswords;
            item.PermissionsViewAllData = permission.PermissionsViewAllData;
            item.PermissionsViewAllUsers = permission.PermissionsViewAllUsers;
            item.PermissionsViewEncryptedData = permission.PermissionsViewEncryptedData;
            if (!SA7_Util.ambienteSandbox())
            {
                item.PermissionsManageSandboxes = (Boolean) permission.get('PermissionsManageSandboxes');
            }
            item.permissionSetId = permission.Id;
            SA7_PermissionSetList.add(item);
        }
        return SA7_PermissionSetList;
    }
    
    private class SA7_PermissionSet {
        public ID permissionSetId;
        public ID ProfileID;
        public Boolean PermissionsAuthorApex;
        public Boolean PermissionsBulkApiHardDelete;
        public Boolean PermissionsCustomizeApplication;
        public Boolean PermissionsManageCertificates;
        public Boolean PermissionsManageCustomPermissions;
        public Boolean PermissionsManageInternalUsers;
        public Boolean PermissionsManageIpAddresses;
        public Boolean PermissionsManageLoginAccessPolicies;
        public Boolean PermissionsManagePasswordPolicies;
        public Boolean PermissionsManageProfilesPermissionsets;
        public Boolean PermissionsManageRoles;
        public Boolean PermissionsManageSharing;
        public Boolean PermissionsManageUsers;
        public Boolean PermissionsModifyAllData;
        public Boolean PermissionsModifyMetadata;
        public Boolean PermissionsPasswordNeverExpires;
        public Boolean PermissionsResetPasswords;
        public Boolean PermissionsViewAllData;
        public Boolean PermissionsViewAllUsers;
        public Boolean PermissionsViewEncryptedData;
        public Boolean PermissionsManageSandboxes;
    }
}