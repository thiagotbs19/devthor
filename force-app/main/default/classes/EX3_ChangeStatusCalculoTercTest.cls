/*********************************************************************************
*                                    Itaú - 2020
*
* Classe de Teste
*
*Apex Trigger : EX3_Calculo__c 
*Apex Class: EX3_ChangeStatusCalculoTercTest
*  
*
* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

@isTest
public class EX3_ChangeStatusCalculoTercTest {
    
    @TestSetup
    private static void createData(){
       	 
            User lUser = EX3_BI_DataLoad.getUser();
            lUser.FuncionalColaborador__c = 'funcional';
            lUser.EX3_Pertence__c = '2'; 
            Database.insert(lUser);
    
            Case lCase = EX3_BI_DataLoad.getCase();
            lCase.Status = 'New'; 
            Database.insert(lCase);  

            EX3_Calculo__c lCalculo = EX3_BI_DataLoad.getCalculo(lCase);
            lCalculo.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_Calculo__c', 'EX3_Calculista');
            lCalculo.EX3_Status_do_calculo__c = Label.EX3_Calculo_Externo;   

            Test.startTest();   
                Database.insert(lCalculo);
            Test.stopTest(); 

    }
    
    @isTest static void testCalculoStatus(){
        
        User lUser = [SELECT Id FROM User LIMIT 1];
        
        Case lCase  = [SELECT Id FROM Case LIMIT 1];   

        EX3_Calculo__c lCalculo = [SELECT Id FROM EX3_Calculo__c LIMIT 1];

        Database.SaveResult lResult = Database.update(lCalculo, false);
        System.assert(lResult.isSuccess(), 'O Owner foi alterado para Calculista Tercerizado'); 
    }
    
    

}