/*********************************************************************************
*                                    Itaú - 2019
*
* Classe Responsável para que o analista não consiga puxar um registro para iniciar
* um atendimento se o mesmo ja possuir um registro de Case.  
*
* 
*Apex Class: EX3_BloqueiaCaseOwner 
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/

public with sharing class EX3_BloqueiaCaseOwner { 


    public static void execute(){     
        Set<Id> lSetOwnerCase = new Set<Id>();
        Set<String> lSetNumeroProtocolo = new Set<String>();
        List<Case> lLstCase = new List<Case>();
        for(Case iCase : (List<Case>) Trigger.new){
            if(EX3_TriggerHelper.changedField(iCase, 'OwnerId') &&
                iCase.OwnerId == UserInfo.getUserId() && 
                iCase.RecordTypeId == EX3_Utils.getRecordTypeIdByDevName('Case', 'EX3_Consignado')){
                  lSetNumeroProtocolo.add(iCase.EX3_Numero_do_protocolo__c);  
                  lSetOwnerCase.add(iCase.OwnerId);  
                    lLstCase.add(iCase);
                }
        }  
        if(lSetOwnerCase.isEmpty() || lSetOwnerCase == null){ return; } 
        caseOwnerError(lSetOwnerCase, lLstCase, lSetNumeroProtocolo);  
    } 



    private static void caseOwnerError(Set<Id> aSetOwnerCase, List<Case> aLstCase, Set<String> aSetNumeroProtocolo){

    
      if(aSetOwnerCase.isEmpty() || aLstCase.isEmpty() || aLstCase == null){
        return; 
      }
         
      List<User> lLstUserProfile = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() AND Profile.Name NOT IN('System Administrator', 'Administrador do sistema')];
      if (lLstUserProfile.isEmpty()) { return; }   

      Map<String, String> lMapCase = new Map<String, String>();   

      for(Case iCase : [SELECT Id, OwnerId, EX3_Numero_do_Protocolo__c, EX3_Motivo_da_Situacao__c, EX3_Situacao__c 
                                        FROM Case WHERE OwnerId =: aSetOwnerCase AND EX3_Situacao__c NOT IN ('11') AND 
                                        EX3_Numero_do_protocolo__c =: aSetNumeroProtocolo]){
             
          lMapCase.put(iCase.OwnerId , iCase.EX3_Numero_do_protocolo__c);        
      }  
      if(lMapCase == null || lMapCase.isEmpty()) { return; }       
        
        for(Case iCase : aLstCase) 
        { 
            if (!lMapCase.containsKey(iCase.OwnerId)) { continue; } 
            iCase.addError(Label.EX3_Pasta_Numero + lMapCase.get(iCase.OwnerId) + ' ' + Label.EX3_Tratativa);
        }           
       
  
  } 
}