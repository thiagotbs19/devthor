/*********************************************************************************
*                                    Itaú - 2020 
*
* Classe de teste da EX3_UpdateDefinicaoEstrategica
*
*
*Apex Class: EX3_UpdateDefinicaoEstrategicaTest
*  

* Empresa: everis do Brasil
* Autor: Thiago Barbosa
*
********************************************************************************/
@isTest
public with sharing class EX3_UpdateDefinicaoEstrategicaTest {
    

    @TestSetup
    private static void testCreateData(){

        User lUser = EX3_BI_DataLoad.getUser();
        Database.insert(lUser);

        User lUserOperacional = EX3_BI_DataLoad.getUserAnalistaOperacional();
        Database.insert(lUserOperacional);

        Case lCase = EX3_BI_DataLoad.getCase();
        Database.insert(lCase);
        
        EX3_DefinicaoEstrategia__c lDefEst = EX3_BI_DataLoad.getDefEstrategica(lCase);
        lDefEst.OwnerId = lUserOperacional.Id;
        Database.insert(lDefEst, false); 

        String lRecorTypeDefEstr = EX3_Utils.getRecordTypeIdByDevName('EX3_DefinicaoEstrategia__c', 'EX3_Estrategia_definitiva');

        EX3_DefinicaoEstrategia__c lEstDef = EX3_BI_DataLoad.getDefEstrategica(lCase);
        lEstDef.RecordTypeId = lRecorTypeDefEstr;
        lEstDef.OwnerId = lUser.Id;
        Database.insert(lEstDef, false);        

    }

    @isTest static void testUpdateDefEstrategica(){ 

        User lUser = [SELECT Id FROM User LIMIT 1];

        Case lCase = [SELECT Id FROM Case LIMIT 1];

        EX3_DefinicaoEstrategia__c lDefEst = [SELECT Id FROM EX3_DefinicaoEstrategia__c LIMIT 1];

        Test.startTest();
        lDefEst.EX3_Estrategia__c = 'Acordo';
        lDefEst.RecordTypeId = EX3_Utils.getRecordTypeIdByDevName('EX3_DefinicaoEstrategia__c', 'EX3_Pre_estrategia');
        lDefEst.OwnerId = UserInfo.getUserId();

        Database.SaveResult lResult = Database.update(lDefEst, false);  
        Test.stopTest();    
        System.assert(lResult.isSuccess(), 'A Definição Estrategica foi atualizada');

    }
}