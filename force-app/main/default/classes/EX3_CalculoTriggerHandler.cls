/*********************************************************************************
*                                    Itaú - 2020
*
* Classe TriggerHandler do objeto EX3_Calculo__c  
* 
* Empresa: everis do Brasil
* Autor: Isabella Tannús 
*
********************************************************************************/

public without sharing class EX3_CalculoTriggerHandler implements ITrigger{

        public void bulkBefore(){} 

        public void bulkAfter(){}

        public void beforeInsert(){
            EX3_BloqueiaCalculoOwner.execute();
            EX3_CreateCalculoSolicitado.execute(); 
            EX3_ChangeStatusCalculoTerc.execute();
            
        }

        public void beforeUpdate(){
            EX3_BloqueiaCalculoOwner.execute(); 
            EX3_ChangeOwnerCalculo.execute();
        }

        public void beforeDelete(){}
        
        public void afterInsert(){
            EX3_CreateCalculoFUP.execute();
        } 
        
        public void afterUpdate(){
            EX3_ChangeQueueCalculoStatus.execute();
        }

        public void afterDelete(){}
        
        public void andFinally(){}
}