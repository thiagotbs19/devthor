({	
    doInit : function(component, event, helper){
        helper.doInit(component, event, helper); 
    },
    
    finalizarEstrategia : function(component, event, helper) {
       helper.finalizarEstrategia(component, event, helper);
    },

    acionarTarefas : function(component, event, helper){    
        helper.acionarTarefas(component, event, helper); 
    }    
})