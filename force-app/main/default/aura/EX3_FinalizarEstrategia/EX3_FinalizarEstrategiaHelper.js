({	
    doInit : function(component, event, helper){
        
        var action = component.get("c.getHasPermissionSet"); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                
                var lReturnValue = response.getReturnValue(); 
                component.set("v.isDisabledJuridico", !lReturnValue);  
                
            }  
            else if (state === "INCOMPLETE" || state === "ERROR") {
                var erro = $A.get("$Label.c.EX3_Erro");  
                helper.showToast("Erro", erro ,"error");
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {  
                        helper.showToast("Erro", errors[0].message ,"error");   
                    }
                } else {    
                    helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
                }
            } 
        });
        
        $A.enqueueAction(action);
    },
    
    finalizarEstrategia : function(component, event, helper) {
        var recordId = component.get("v.recordId");

        var action = component.get("c.getflowExecuteEstrategia");

        action.setParams({
            'aRecordId' : recordId 
        }); 
         
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
              var lReturnValue = response.getReturnValue();
              console.log('THBS ---- lReturnValue' + lReturnValue);
              if(lReturnValue == 'Laudo Cadastrado'){        
                helper.showToast("Sucesso",$A.get("$Label.c.EX3_Laudo_Cadastrado"),"success");
              	return;
              }
              if(lReturnValue == null || lReturnValue == ''){ 
                helper.showToast("Alerta",$A.get("$Label.c.EX3_Preeencher_Captura"),"warning");
              	return;
              }
              if(lReturnValue == $A.get("$Label.c.EX3_Finalizar_Estrategia_Macro_Carteira")){  
                helper.showToast("Alerta", $A.get("$Label.c.EX3_Finalizar_Estrategia_Macro_Carteira"), "warning"); 
              	return;
              }
                if(lReturnValue== $A.get("$Label.c.EX3_Pedido")){ 
				helper.showToast("Alerta", $A.get("$Label.c.EX3_Pedido"), "warning");
                return;
                } 
                helper.showToast("Sucesso",$A.get("$Label.c.EX3_Subsidio_Criado"),"success");     
        }    
        else if (state === "INCOMPLETE" || state === "ERROR") {
            var erro = $A.get("$Label.c.EX3_Erro");  
            helper.showToast("Erro", erro ,"error");
     
            var errors = response.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {  
                    helper.showToast("Erro", errors[0].message ,"error");   
                }
            } else {    
                helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
            }
        }
          });
      
          $A.enqueueAction(action);
    },


    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            "duration": 3000,
            "title": title,
            "message": message,
            "type": type // THE TOAST TYPE, WHICH CAN BE ERROR, WARNING, SUCCESS, OR INFO
        });

        toastEvent.fire();
  },

  acionarTarefas : function(component, event, helper){
    component.set('v.viewComponent', !component.get('v.viewComponent'));  
  } 
})