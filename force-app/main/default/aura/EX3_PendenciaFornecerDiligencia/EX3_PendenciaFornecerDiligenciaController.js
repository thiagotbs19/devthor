({
    doInit: function(component, event, helper) 
    {
        component.set('v.isMobile', $A.get("$Browser.formFactor") !== 'DESKTOP');
        component.set('v.loading', true);
        helper.getRendererPage(component);
    },

    previousPage : function(component, event, helper)
    {
        component.set("v.page", Math.max(component.get("v.page")-1, 1));
        helper.renderPage(component);
    },

    nextPage : function(component, event, helper) 
    {
        component.set("v.page", Math.min(component.get("v.page")+1, component.get("v.pages")));
        helper.renderPage(component);
    },

    toCase : function (component, event, helper) 
    {
        component.set('v.loading', true);
        helper.toCase(component, event);
    },

    updateColumnSorting: function (component, event, helper) 
    {
        var lSortField = event.getSource().get("v.name");
        var lSortAsc = event.getSource().get("v.value");
        component.set('v.loading', true);

        helper.sortByField(component, lSortField, lSortAsc);
    },

    search : function(component, event, helper)
    {
        var lField = component.get("v.listSelectedStatus");
        if (!lField || lField === 'Selecione')
        {
            helper.showToast("error", '', 'Insira um valor para a busca e mude o Filtro em "Pesquisar em" ', '');
            component.set('v.loading', false);
            return;
        }

        var lSearch;
        if(lField === 'EX3_Protocolo__r.EX3_Prioridade__c') { lSearch = component.get("v.listSelectedPriority"); }
        else if(lField === 'EX3_Protocolo__r.EX3_Motivo_da_Situacao__c') { lSearch = component.get("v.listSelectedStatusRegister"); }
        else { lSearch = component.find("search").get("v.value"); }

        if (lField !== 'EX3_Protocolo__r.EX3_Data_de_Entrada__c' && (!lSearch || lSearch === 'Selecione') )
        {
            helper.showToast('error', '', 'Insira um valor para a busca.', 'sticky');
            component.set('v.loading', false);
            return;
        }

        var lDataInicio = (component.get('v.dataInicio')) ? component.get('v.dataInicio') : null;
        var lDataFim = (component.get('v.dataFim')) ? component.get('v.dataFim') : null;

        if(lField === 'EX3_Protocolo__r.EX3_Data_de_Entrada__c' && (!lDataInicio || !lDataFim)  )
        {
            helper.showToast('error', '', 'Necessário inserir Data inicial e Data final', '');
            component.set('v.loading', false);
            return;
        }
        else if (lField === 'EX3_Protocolo__r.EX3_Data_de_Entrada__c' && (lDataInicio > lDataFim))
        {
            helper.showToast('error', '', 'Data inicial não pode ser maior que data final', '');
            component.set('v.loading', false);
            return;
        }

        helper.search(component, lField, lSearch, lDataInicio, lDataFim);
    },

    exportExcel : function(component, event, helper)
    {
        component.set('v.loading', true);
        helper.exportExcel(component);
    },

    onChangeCombo : function(component, event, helper) 
    {
        helper.onChangeCombo(component);
    },

    handleSelectAll: function(component, event, helper) 
    {
        helper.handleSelectAll(component);
    },

    handleSelectAllCiente: function(component, event, helper) 
    {
        helper.handleSelectAllCiente(component);
    },

    cancelar: function(component, event, helper) 
    {
        component.set("v.showModal", false);
    },

    renderPage: function(component, event, helper) 
    {
        helper.renderPage(component);
    },

    selecionar : function(component, event, helper)
    {
        helper.selecionar(component);
    },
})