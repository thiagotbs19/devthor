({
    getRendererPage : function (component)
    {
        var action = component.get('c.getPage');

        action.setCallback(this, function(response)
        {
            component.set('v.loading', false);

            var state = response.getState();
            if (state === 'SUCCESS')
            {
                var lReturnValue = response.getReturnValue();
                var lColumns = JSON.parse(lReturnValue['columns']);
                if (lColumns && lColumns.length)
                {
                    component.set("v.listColumns", lColumns);
                    component.set('v.optionsListViewsStatus', lColumns);
                }
                else
                {
                    this.showToast('info', 'Ocorreu um erro. ',
                    'Não foi possível encontrar informações do status.', '');
                }
                var lRecords = lReturnValue['records'];
                if (lRecords && lRecords.length) 
                {
                    this.createPagination(component, lRecords); 
                }
                else
                {
                    component.set('v.recordsNotFound', 'Não foi encontrado nenhum registro!');
                }
                
                var lMotivo = JSON.parse(lReturnValue['motivo']);
                if (lMotivo && lMotivo.length) { component.set('v.optionsListMotivoSituacao', lMotivo); }
                else
                {
                    this.showToast('info', 'Ocorreu um erro. ', 'Não foi possível encontrar informações do motivo.', '');
                }
                
                var lPriority = JSON.parse(lReturnValue['priority']);
                if (lPriority && lPriority.length) { component.set('v.optionsListPriority', lPriority); }
                else
                {
                    this.showToast('info', 'Occoreu um erro. ','Não foi possível encontrar informações da prioridade.', '');
                }
            }
            else
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.showToast('error', '', errors[0].message, '');
                    }
                }
                else 
                {
                   this.showToast('error', '' ,'Erro desconhecido', '', '5000');
                }
            }
        });
        $A.enqueueAction(action);
    },

    getAllRecords : function (component)
    {
        var action = component.get("c.getCases");

        action.setCallback(this, function(result)
        {
            var state = result.getState();
            if (state === "SUCCESS") 
            {
                var records = result.getReturnValue();
                this.createPagination(component, records);

                if (!records || !records.length)
                {
                    component.set('v.recordsNotFound', 'Não foi encontrado nenhum registro!');
                }
            }
            else
            {
                helper.showToast('info', 'Ocorreu um erro.',
                'Status: ' + state + ' - Não foi possível encontrar informaçoes dos processos.', '');
            }

        });
        $A.enqueueAction(action);
    },

    sortByField: function (component, aSortField, aSortAsc) 
    {
        var action = component.get("c.sortRecords");
        var lCurrentList = component.get('v.allCases');

        action.setParams({
            'aSortField': aSortField,
            'aSortAsc': aSortAsc,
            'aCaseRecords' : JSON.stringify(lCurrentList)
        });
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS")
            {
                var retRecords = response.getReturnValue();
                this.createPagination(component,retRecords);

                component.set('v.loading', false);

            }
            else
            {
                this.showToast('error', '', 'Erro ao carregar Minhas Solicitações.', '');
                component.set('v.loading', false);
            }
        });
        $A.enqueueAction(action);
    },

    search: function (component, aField, aSearch, aDataInicio, aDataFim) 
    {
        component.set('v.loading', true);

        var action = component.get("c.filterRecords");

        action.setParams({
            'aField': aField,
            'aSearch': aSearch,
            'aDataInicio':  aDataInicio,
            'aDataFim': aDataFim
        });

        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var retRecords = response.getReturnValue();
                if (!retRecords || !retRecords.length)
                {
                    this.showToast('error', '', 'Nenhum item encontrado', '');
                    component.set('v.loading', false);
                    return;
                }

                component.set('v.auxCurrentList', retRecords);
                component.set("v.isSelectAll", true);

                var arrayParam = [];

                for (var i in retRecords)
                {
                    if(aField === "EX3_Protocolo__r.EX3_Numero_do_Protocolo__c")
                    {
                        arrayParam.push(retRecords[i].EX3_Protocolo__r.EX3_Numero_do_Protocolo__c);
                    }
                    else if(aField === "EX3_Protocolo__r.EX3_Numero_do_Processo__c")
                    {
                        arrayParam.push(retRecords[i].EX3_Protocolo__r.EX3_Numero_do_Processo__c);
                    }
                    /*else if(aField === "EX3_Protocolo__r.EX3_Data_de_Entrada__c")
                    {
                        arrayParam.push(new Date(retRecords[i].EX3_Protocolo__r.EX3_Data_de_Entrada__c).toLocaleString('pt-br'));
                    }*/
                }

                component.set("v.currentListFilter", arrayParam);

                if(arrayParam && arrayParam.length)
                {
                    var checkFilter = component.find("checkFilter");

                    for(var i=0; i<checkFilter.length; i++)
                    {
                        checkFilter[i].set("v.value",true);
                    }
                }

                if((arrayParam && arrayParam.length === 1)
                    || aField === 'EX3_Protocolo__r.EX3_Prioridade__c'
                    || aField === 'EX3_Protocolo__r.EX3_Motivo_da_Situacao__c'
                  	|| aField === 'EX3_Protocolo__r.EX3_Data_de_Entrada__c')
                {
                    //Paginação
                    var records = component.get('v.auxCurrentList');
                    this.createPagination(component, records);
                }
                else
                {
                    component.set("v.showModal", true);
                }
          component.set('v.loading', false);
            }else {
                component.set('v.loading', false);
                this.showToast('error', '', 'Erro ao carregar Minhas Solicitações.', '');
            }
        });
        $A.enqueueAction(action);
    },

    exportExcel : function(component)
    {
        var lCurrentList = component.get("v.currentList");
        var lLstProtocolo = [];

        for (var index in lCurrentList)
        {

            lLstProtocolo.push(lCurrentList[index].EX3_Protocolo__r);
        }

        var action = component.get('c.getExportExcelValues');

        action.setParams({
            'aLstProtocolo' : JSON.stringify(lLstProtocolo)
        });


        action.setCallback(this, function(result)
        {
            var state = result.getState();

            if (state === "SUCCESS")
            {
                var result = JSON.parse(result.getReturnValue());

                var lDocument = document.createElement("a"); 
                lDocument.href = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+(result); 
                lDocument.target = '_self';  
                lDocument.download = "Diligências Pendentes " +new Date().toLocaleString()+ ".xls";

                component.set('v.loading', false);

                lDocument.click();
            }
            else
            {
                component.set('v.loading', false);
                helper.showToast('info', 'Ocorreu um erro. ',
                'Status: ' + state + ' - Não foi possível encontrar informações da coluna e/ou filtro de buscas.', '');
            }
        });
        $A.enqueueAction(action);
    },

    toCase : function (component, event)
    {
        var recordid = event.target.getAttribute('data-recid');
        var navService = component.find("navService");

        var pageReference = {
            "type": "standard__recordPage",
            "attributes": {
                "recordId": recordid,
                "objectApiName": "Case",
                "actionName": "view"
            }
        };
        component.set("v.pageReference", pageReference);
        // Set the URL on the link or use the default if there's an error
        var defaultUrl = "#";

        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            component.set("v.url", url ? url : defaultUrl);

            event.preventDefault();
            navService.navigate(pageReference);
            component.set('v.loading', false);

        }), $A.getCallback(function(error) {
            component.set("v.url", defaultUrl);
            component.set('v.loading', false);
        }));
    },

    onChangeCombo : function (component)
    {
        component.set('v.valueSearch','');
        component.set('v.dataInicio','');
        component.set('v.dataFim','');

        var aField = component.get("v.listSelectedStatus");

        component.set('v.isData', (aField === "EX3_Protocolo__r.EX3_Data_de_Entrada__c"));
        component.set('v.isPriority', (aField === "EX3_Protocolo__r.EX3_Prioridade__c"));
        component.set('v.isMotivo', (aField === "EX3_Protocolo__r.EX3_Motivo_da_Situacao__c"));

        if (aField === 'Selecione') { this.getAllRecords(component); }
    },

    handleSelectAll : function (component)
    {
        var checkvalue = component.find("selectAll").get("v.value");
        var checkFilter = component.find("checkFilter");
        if(checkvalue)
        {
            for(var i=0; i<checkFilter.length; i++)
            {
                checkFilter[i].set("v.value",true);
            }
        }
        else
        {
            for(var i=0; i<checkFilter.length; i++)
            {
                checkFilter[i].set("v.value",false);
            }
        }
    },

    handleSelectAllCiente : function (component)
    {
        var checkvalue = component.find("selectAllCiente").get("v.value");
        var checkFilter = component.find("checkCiente");
        var lCurrentList = component.get('v.currentList');

        for (var index in checkFilter)
        {
            if (!lCurrentList[index] || !lCurrentList[index].EX3_Protocolo__c || !lCurrentList[index].EX3_Protocolo__r.EX3_Motivo_da_Situacao__c) { continue; }

            if (checkvalue) { checkFilter[index].set("v.value",true); }
            else { checkFilter[index].set("v.value",false); }
        }
    },

    selecionar : function (component)
    {
        component.set('v.currentList' ,component.get("v.auxCurrentList"));

        var selectedFilters = [];
        var checkvalue = component.find("checkFilter");

        if(!Array.isArray(checkvalue))
        {
            if (checkvalue.get("v.value"))
            {
                selectedFilters.push( component.get('v.currentList')[checkvalue.get("v.text")] );
            }
        }
        else
        {
            for (var i = 0; i < checkvalue.length; i++) 
            {
                if (checkvalue[i].get("v.value")) 
                {
                    selectedFilters.push( component.get('v.currentList')[checkvalue[i].get("v.text")] );
                }
            }
        }

        if(selectedFilters && selectedFilters.length) { component.set('v.currentList', selectedFilters); }

        var records = component.get('v.currentList');
        this.createPagination(component, records);

        component.set("v.showModal", false);
    },

    createPagination : function(component, aRecords)
    {
        component.set("v.allCases", aRecords);
        component.set("v.page", 1);
        component.set("v.total",aRecords.length);
        component.set("v.pages",Math.ceil(aRecords.length/component.get("v.pageSize")));
        var pageNumber = component.get("v.page"),
        pageRecords = aRecords.slice((pageNumber-1)*10, pageNumber*10);
        component.set("v.currentList", pageRecords);
        component.set('v.showPag', true);
    },

    renderPage: function(component)
    {
        var records = component.get("v.allCases"),
        pageNumber = component.get("v.page"),
        pageRecords = records.slice((pageNumber-1)*10, pageNumber*10);
        component.set("v.currentList", pageRecords);
    },

    showToast : function (aType, aTitle, aMessage, aMode)
    {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams
        ({
            "type" : aType,
            "title" : aTitle,
            "message" : aMessage,
            "mode" : aMode
        });
        toastEvent.fire();
    }
})