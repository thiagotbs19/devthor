({
	doInit : function(component, event, helper) {
        setTimeout(function () {$A.get('e.force:closeQuickAction').fire();}, 1);
		helper.doInit(component, event, helper);   
	},
    doneRendering: function(component, event, helper) {
        setTimeout(function () {$A.get('e.force:closeQuickAction').fire();}, 1);
    }  
    
})