({
	doInit : function(component, event, helper) {
        
		var action = component.get("c.getHasCustomPermission"); 
		console.log('THBS ---- do Init');
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            if (state === "SUCCESS") {
                
                var lReturnValue = response.getReturnValue(); 
			    
                if(lReturnValue == false){ 
                    helper.showToast("Alerta", $A.get("$Label.c.EX3_Bloqueia_Task") ,"warning");
                    return;
                }
             else if(lReturnValue == true){
               var createRecordEvent = $A.get('e.force:createRecord');
                createRecordEvent.setParams({    
                    'entityApiName': 'Task', 
                    'defaultFieldValues': {  
                    	'WhatId': component.get("v.recordId"),
                   }   
                })
                createRecordEvent.fire();  
            	}
            }
            
        });
        $A.enqueueAction(action);  
	},
    
    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            "duration": 3000,
            "title": title,
            "message": message,
            "type": type // THE TOAST TYPE, WHICH CAN BE ERROR, WARNING, SUCCESS, OR INFO
        }); 
        
        toastEvent.fire();
    }
})