({
	createRecord: function (component) {
        var recordId = component.get("v.recordId");
        console.log('Record ID is' + recordId);
        
        var action = component.get("c.redirectToObject");
        
        action.setParams({
            'aRecordId': recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "info",
                    "title": "",
                    "message": "Solicitação de OBF enviado à fila do fornecedor!"
                });
                toastEvent.fire(); 
				$A.get('e.force:refreshView').fire();

                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                })
                .catch(function(error) {
                    console.log(error);
                });

            }
            else {
                console.log("Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);
    },

	createRecord2: function (component) {
        var recordId = component.get("v.recordId");
        console.log('Record ID is' + recordId);
        
        var action = component.get("c.redirectToObject2");
        
        action.setParams({
            'aRecordId': recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "info",
                    "title": "",
                    "message": "Solicitação de OBF enviado à fila de fechamento!"
                });
                toastEvent.fire(); 
				$A.get('e.force:refreshView').fire();

                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                })
                .catch(function(error) {
                    console.log(error);
                });

            }
            else {
                console.log("Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);
    }    
})