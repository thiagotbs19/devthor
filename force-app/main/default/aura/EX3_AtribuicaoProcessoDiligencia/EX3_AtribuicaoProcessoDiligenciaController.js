({
  doInit : function(component, event, helper) {
        helper.getMotivos(component);
  },

    updateMotivoController : function(component, event, helper) {
       helper.updateMotivoDiligencia(component);
    },

    callRefreshPage : function(component, event, helper){
        helper.refreshPage(component);
    }
})