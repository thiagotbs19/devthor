({
    getProtocolo : function(component)
    {
        var action = component.get('c.getProtocoloValues');

        action.setParams({
            'aCaseRecordId' : component.get('v.recordId')
        });

        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if (state === 'SUCCESS')
            {
                component.set('v.showSpinner',false);

                var returnValue = JSON.parse(response.getReturnValue());

                component.set('v.fieldList', returnValue);
            }
            else
            {
                component.set('v.showSpinner',false);
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.showToast('error', '', errors[0].message, '');
                    }
                }
                else { this.showToast('error', '' ,'Erro desconhecido', ''); }
            }
        });
        $A.enqueueAction(action);
    },

    acionarDiligencia : function (component)
    {
        var action = component.get('c.updateProtocolo');
        var lFieldList = component.get('v.fieldList');

        action.setParams({
            'aFieldList' : JSON.stringify(lFieldList)
        })

        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if (state === 'SUCCESS')
            {
                this.showToast('success', '', 'Acionamento realizado com sucesso', '');
                component.set('v.showSpinner',false);

                window.setTimeout(
                    $A.getCallback(function() {
                        location.reload(true);
                }), 1000);
        }
            else
            {
                component.set('v.showSpinner',false);
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.showToast('error', '', errors[0].message, '');
                    }
                    else {  this.showToast('error', '' ,'Erro desconhecido', ''); }
                }
            }
        });

        $A.enqueueAction(action);
    },

    showToast : function (aType, aTitle, aMessage, aMode)
    {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams
        ({
            "type" : aType,
            "title" : aTitle,
            "message" : aMessage,
            "mode" : aMode
        });
        toastEvent.fire();
    }
})