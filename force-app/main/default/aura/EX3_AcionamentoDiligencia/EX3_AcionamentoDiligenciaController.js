({
    doInit : function(component, event, helper)
    {
        component.set('v.showSpinner', true);
        helper.getProtocolo(component);
    },

    acionarDiligencia : function (component, event, helper)
    {
        component.set('v.showSpinner',true);
        helper.acionarDiligencia(component);
    },

    closeModal : function(component)
    {
        component.destroy();
  }
})