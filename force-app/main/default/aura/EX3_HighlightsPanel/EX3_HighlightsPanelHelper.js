({
    deleteAcc: function (component, event) {
        var action = component.get('c.DelReg');
            action.setParams({
                "idNumber" : component.get('v.recordId')
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === 'SUCCESS') {
                    let resp = response.getReturnValue();
                    component.set("v.showModalDelete", false);
                } else if (state === "ERROR") {
                    // console.log("Erro");
                    component.set("v.showModalDelete", false);
                }
                
            });
 
            $A.enqueueAction(action);
        	var Pagina = $A.get("e.force:navigateToObjectHome");
           	Pagina.setParams({
            			       "scope": "Account"
            			    });
       		Pagina.fire(); 
 
    },
})