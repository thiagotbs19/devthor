({
    doInit : function(component, event, helper){
        var fieldsList = component.get('v.fieldsList');
        component.set('v.fields', JSON.parse(fieldsList));	
        
        var objectName = component.get('v.objectName');
        console.log(objectName);
        console.log(JSON.parse(fieldsList));
        component.set('v.objectIconName',objectName.toLowerCase());		
        component.set('v.objectAPI',objectName.charAt(0).toUpperCase() + objectName.slice(1).toLocaleLowerCase());
        
    },
    
    refreshPage : function(component, event, helper){
        
    },
    
    updateAcc : function(component, event, helper) {
        
    },  
    
    handleRecordUpdated: function(component, event, helper){
        
    },
    openModalDelete: function(component, event, helper) {
        component.set("v.showModalDelete", true);
    },
    
    closeModalDelete: function(component, event, helper) {
        component.set("v.showModalDelete", false);
    },
    
    deleteAcc: function(component, event, helper) {
        helper.deleteAcc(component, event);
    },
    
    openModalEdit: function(component, event, helper) {
        var recordTypeAcc = component.get("v.recordId");
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": recordTypeAcc
        });
        editRecordEvent.fire();
    },
    
})