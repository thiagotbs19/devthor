({	
    getHasPermission : function(component, event, helper){

        var action = component.get("c.getHasPermissionSet");
      
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue(); 
              	component.set("v.isDisabledJuridico" , !lReturnValue);  
         }    
         else if (state === "INCOMPLETE" || state === "ERROR") { 
              var erro = $A.get("$Label.c.EX3_Erro");  
              helper.showToast("Erro", erro ,"error");
              
      
              var errors = response.getError();
              if (errors) {
                  if (errors[0] && errors[0].message) {  
                      helper.showToast("Erro", errors[0].message ,"error");   
                  }
              } else {     
                  helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
              }
          } 
          });
      
          $A.enqueueAction(action);
    },
    
    getRecordDetails : function (component, event, helper) {
        var action = component.get('c.getRecordDetails');
        var lOBF, lPagamento, lEncerramento;

        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();

                lOBF = lReturnValue['OBF'];
                lPagamento = lReturnValue['Pagamento'];
                lEncerramento = lReturnValue['Encerramento'];
                if(lOBF || lPagamento || lEncerramento){
                    component.set('v.hideButtons',true);
                }
                component.set('v.isOBF',lOBF); 
                component.set('v.isPagamento',lPagamento);
                component.set('v.isEncerramento',lEncerramento);
            }
        });
        $A.enqueueAction(action);
    },
    processFlow : function(component,flowName,helper){
        component.set("v.hide", true);
        var flow = component.find("flowData");
        var inputVariables = [
            {
                name: "recordId",
                type: "String",
                value: component.get("v.recordId")
            }
        ]; 
        flow.startFlow(flowName, inputVariables);
        helper.showToast("Sucesso!", 'Registro criado com sucesso! ',"Success"); 
        component.set('v.hide', false);
        $A.get('e.force:refreshView').fire(); 
        window.location.reload(); 
        
    },
    
    getRecordTypeId : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var action = component.get("c.getRecordTypeADJ");
         action.setParams({
            aRecordId: recordId
          });
      
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();
                
                if(lReturnValue != null){  
                	component.set("v.hideButtons", true);   
                }   
         }    
         else if (state === "INCOMPLETE" || state === "ERROR") {
              var erro = $A.get("$Label.c.EX3_Erro");  
              helper.showToast("Erro", erro ,"error");
              
      
              var errors = response.getError();
              if (errors) {
                  if (errors[0] && errors[0].message) {  
                      helper.showToast("Erro", errors[0].message ,"error");   
                  }
              } else {     
                  helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
              }
          } 
          });
      
          $A.enqueueAction(action);
    },
    showToast: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            duration: 3000,
            title: title,
            message: message,
            type: type
        });

        toastEvent.fire();
    }
});