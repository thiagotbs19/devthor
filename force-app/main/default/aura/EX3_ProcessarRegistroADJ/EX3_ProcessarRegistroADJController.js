({
    doInit : function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
        helper.getHasPermission(component, event, helper);  
        helper.getRecordTypeId(component, event, helper);    
        helper.getRecordDetails(component,event,helper); 
        $A.get('e.force:refreshView').fire();
    },
    callFlowOBF: function (component, event, helper) {
        var flowName = "EX3_CumprimentoOBF";
        helper.processFlow(component,flowName,helper);
    },
    callFlowPagamento: function (component, event, helper) {
        var flowName = 'EX3_CumprimentoOBP'; 
        helper.processFlow(component,flowName,helper);
    },
    callFlowEncerramento: function (component, event, helper) {
        var flowName = 'EX3_EncerramentoADJ';
        helper.processFlow(component,flowName,helper);
    },
});