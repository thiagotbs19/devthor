({
    doInit : function(component, event, helper){
        var recordId = component.get("v.recordId"); 
        var action = component.get("c.getDefinicaoEstrategia");
        
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
    	component.set('v.today', today);
        action.setParams({
            'aRecordId': recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set("v.isOpenButtonAtPrevia", true);  
                }
            }
            else if (state === "INCOMPLETE" || state === "ERROR") {
                var erro = $A.get("$Label.c.EX3_Erro"); 
                helper.showToast("Erro", erro ,"error");
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.showToast("Erro", caseCreation ,"error");   
                    }
                } else {     
                    helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido") ,"error");
                }
            } 
        });
        $A.enqueueAction(action);
    },  
    
    getHasPermission : function(component, event, helper){
        var action = component.get("c.getHasPermissionSet");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue(); 
                component.set("v.isDisabledJuridico", !returnValue);
            }
            else if (state === "INCOMPLETE" || state === "ERROR") {
                var erro = $A.get("$Label.c.EX3_Erro"); 
                helper.showToast("Erro", erro ,"error");
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.showToast("Erro", caseCreation ,"error");   
                    }
                } else {     
                    helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido") ,"error");
                }
            } 
        });
        $A.enqueueAction(action);
    },
    
    createRecord: function (component, event, helper) {
        var recordId = component.get("v.recordId");
        
        var action = component.get("c.getFieldsCadastro");
        
        action.setParams({
            'aRecordId': recordId
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var lReturnValue = response.getReturnValue();
                var lCadastros = lReturnValue['cadastro']; 
                var lRTCalc = lReturnValue['recordTypeCalc'];
                var lqueueIdCalc = lReturnValue['queueIdCalc'];
                var lCaseId = lReturnValue['IdCase'];
                var lSubsidio = lReturnValue['subsidio'];
                var lDocumento = lReturnValue['documento'];
                var lLaudo = lReturnValue['laudo']; 
                var lEstrategia = lReturnValue['estrategia'];

                
                var lResultNumeroPasta;
                var lResultNumeroProtocolo;
                if(lCadastros){
                    lResultNumeroPasta = lReturnValue['cadastro'].EX3_Numero_Protocolo__c;
                    lResultNumeroProtocolo = lReturnValue['cadastro'].EX3_Numero_do_Processo__c;
                }
                if(lSubsidio){ 
                    lResultNumeroPasta = lReturnValue['subsidio'].EX3_Cod_Pasta__c;
                    lResultNumeroProtocolo = lReturnValue['subsidio'].EX3_Numero_Protocolo__c
                }
                if(lDocumento){ 
                   lResultNumeroPasta =  lReturnValue['documento'].EX3_Numero_do_processo__c;
                   lResultNumeroProtocolo = lReturnValue['documento'].EX3_Numero_do_processo__c;
                }
                if(lLaudo){
				  lResultNumeroPasta = lReturnValue['laudo'].EX3_Codigo_da_pasta__c;
                  lResultNumeroProtocolo = lReturnValue['laudo'].EX3_Numero_protocolo__c;
                } 
                if(lEstrategia){
                  lResultNumeroPasta = lReturnValue['estrategia'].EX3_Codigo_da_pasta__c;
                  lResultNumeroProtocolo = (lReturnValue['estrategia'] != null) ? lReturnValue['estrategia'].EX3_Numero_do_protocolo__c : '';
                }

                var today = new Date();
                var createRecordEvent = $A.get('e.force:createRecord');
                if ( createRecordEvent ) {  
                    if(lReturnValue){    
                        createRecordEvent.setParams({  
                            'entityApiName': 'EX3_Calculo__c',  
                            'recordTypeId': lRTCalc, //EX3_Solicitacao_Calculo  
                            'defaultFieldValues': {  
                                'EX3_Numero_da_Pasta__c' : lResultNumeroPasta,
                                'EX3_Protocolo__c' :  lCadastros.EX3_Protocolo__c,
                                'EX3_Caso__c' : lCaseId,
                                'OwnerId' : lqueueIdCalc, 
                                'EX3_Data_de_Entrada__c' : component.get('v.today'),
                                'EX3_Carteira__c' : lCadastros.EX3_Carteira__c,     
                                'EX3_Numero_do_processo__c' : lResultNumeroProtocolo
                            } 
                        });
                    }else {  
                        createRecordEvent.setParams({ 
                            'entityApiName': 'EX3_Calculo__c',  
                            'recordTypeId': lRTCalc, //EX3_Solicitacao_Calculo 
                            'defaultFieldValues': { 
                             'OwnerId' : lReturnValue['lqueueIdCalcEmpty'], 
                             'EX3_Caso__c' : lCaseId,
                             'EX3_Data_de_Entrada__c' : component.get('v.today')
                            }
                            
                        });    
                    }
                    createRecordEvent.fire();
                    return;   
                }  
                else { 
                    var caseCreation = $A.get("$Label.c.EX3_Case_Creation");  
                    helper.showToast("Erro", caseCreation,"error"); 
                }
            }  
            else if (state === "INCOMPLETE" || state === "ERROR") {
                var erro = $A.get("$Label.c.EX3_Erro"); 
                helper.showToast("Erro", erro ,"error");
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) { 
                        helper.showToast("Erro",errors[0].message,"error");
                    } 
                } else {
                    helper.showToast("Erro", erro ,"error"); 
                }
            } 
        });
        
        $A.enqueueAction(action);
    }, 
    
    createRecord2: function (component, event, helper) {
        var recordId = component.get("v.recordId"); 
        
        var action = component.get("c.getFieldsCadastro");

        action.setParams({
            'aRecordId': recordId
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            if (state === "SUCCESS") {
                var today = new Date();
                var lReturnValue = response.getReturnValue();
                var lCadastros = lReturnValue['cadastro'];
                var recordTypeId = lReturnValue['recordType'];  
                var queueId = lReturnValue['queueId'];
                var lCaseId = lReturnValue['IdCase']; 
                var lSubsidio = lReturnValue['subsidio'];
                var lDocumento = lReturnValue['documento'];
                var lLaudo = lReturnValue['laudo']; 
                var lEstrategia = lReturnValue['estrategia'];

                
                var lResultNumeroPasta; 
                var lResultNumeroProtocolo; 
                var lResultNumeroProcesso;
                if(lCadastros){
                    lResultNumeroPasta = lReturnValue['cadastro'].EX3_Numero_Protocolo__c;
                    lResultNumeroProtocolo = lReturnValue['cadastro'].EX3_Numero_do_Processo__c;
                    lResultNumeroProcesso = lReturnValue['cadastro'].EX3_Numero_do_Processo__c;
                }
                if(lSubsidio){ 
                    lResultNumeroPasta = lReturnValue['subsidio'].EX3_Cod_Pasta__c;
                    lResultNumeroProtocolo = lReturnValue['subsidio'].EX3_Numero_Protocolo__c,
                    lResultNumeroProcesso = lReturnValue['subsidio'].EX3_Numero_do_Processo__c;
                }
                if(lDocumento){ 
                   lResultNumeroPasta =  lReturnValue['documento'].EX3_Numero_do_processo__c;
                   lResultNumeroProtocolo = ''; 
                   lResultNumeroProcesso = lReturnValue['documento'].EX3_Numero_do_processo__c
                }
                if(lLaudo){
				  lResultNumeroPasta = lReturnValue['laudo'].EX3_Codigo_da_pasta__c;
                  lResultNumeroProtocolo = lReturnValue['laudo'].EX3_Numero_protocolo__c;
                  lResultNumeroProcesso = lReturnValue['laudo'].EX3_Numero_do_processo__c; 
                } 
                if(lEstrategia){
                  lResultNumeroPasta = lReturnValue['estrategia'].EX3_Codigo_da_pasta__c; 
                  lResultNumeroProtocolo = lReturnValue['estrategia'].EX3_Numero_do_protocolo__c; 
                  lResultNumeroProcesso = (lEstrategia.EX3_Numero_do_Processo__c) ? lEstrategia.EX3_Numero_do_Processo__c : ''; 
                }

                var createRecordEvent = $A.get('e.force:createRecord');
                if ( createRecordEvent ) {  
                    if(lCadastros != null || lCadastros != ''){
                        createRecordEvent.setParams({ 
                            'entityApiName': 'EX3_ADJ__c',  
                            'recordTypeId': recordTypeId, //EX3_Solicitacao_ADJ
                            'defaultFieldValues': { 
                                'EX3_Numero_da_Pasta__c' : lResultNumeroPasta,
                                'EX3_Protocolo__c' :  lCadastros.EX3_Protocolo__c,
                                'EX3_Caso__c' : lCaseId,  
                                'OwnerId' : queueId,
                                'EX3_Numero_do_Processo__c': lResultNumeroProcesso,
                                'EX3_area_de_origem__c' : 'Cadastro',
                                'EX3_Data_de_entrada__c': component.get('v.today'),
                            }
                        });
                    }else {   
                         createRecordEvent.setParams({   
                            'entityApiName': 'EX3_ADJ__c',     
                            'recordTypeId': recordTypeId, //EX3_Solicitacao_ADJ,
                          'defaultFieldValues': { 
                            'OwnerId' : queueId,
                            'EX3_area_de_origem__c': 'Cálculo',
                            'EX3_Caso__c' : lCaseId,
                            'EX3_Data_de_entrada__c' : component.get('v.today')
                             }
                        });    
                    } 
                    createRecordEvent.fire(); 
                    return;  
                } else {
                    var caseCreation = $A.get("$Label.c.EX3_Case_Creation");   
                    helper.showToast("Erro", caseCreation ,"error"); 
                }
            } 
            else if (state === "INCOMPLETE" || state === "ERROR") {
                var erro = $A.get("$Label.c.EX3_Erro"); 
                helper.showToast("Erro", erro ,"error");
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.showToast("Erro", caseCreation ,"error");   
                    }
                } else {     
                    helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido") ,"error");
                }
            } 
        });
        
        $A.enqueueAction(action);
    }, 
    
    createRecordAtuaPrev: function(component, event, helper){

        var action = component.get("c.getTaskRecordType");   
        
        
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            if (state === "SUCCESS") {
                
                var lReturnValue = response.getReturnValue(); 

                var createRecordEvent = $A.get('e.force:createRecord');
                
                createRecordEvent.setParams({    
                    'entityApiName': 'Task',  
                    'recordTypeId': lReturnValue 
                });
                createRecordEvent.fire();  
            }   
            
        });
        $A.enqueueAction(action);
        
    },
    
    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            "duration": 3000,
            "title": title,
            "message": message,
            "type": type // THE TOAST TYPE, WHICH CAN BE ERROR, WARNING, SUCCESS, OR INFO
        }); 
        
        toastEvent.fire();
    }
})