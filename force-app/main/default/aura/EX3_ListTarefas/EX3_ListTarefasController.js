({
    doInit: function(component, event, helper){   
        helper.doInit(component, event, helper); 
        helper.getHasPermission(component, event, helper);

    },  
     
    createRecord: function (component, event, helper) {
        helper.createRecord(component, event, helper);
    },

	createRecord2: function (component, event, helper) {
       helper.createRecord2(component, event, helper)
    },
    
    createRecordAtuaPrev : function(component, event, helper){
        helper.createRecordAtuaPrev(component, event, helper); 
    } 
     
})