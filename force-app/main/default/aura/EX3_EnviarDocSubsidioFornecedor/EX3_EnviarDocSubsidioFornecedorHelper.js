({
  changeOwner: function(component, event, helper) {
    var recordId = component.get("v.recordId");

    var action = component.get("c.mudarProprietarioRegistro");

    action.setParams({
      aRecordId: recordId
    });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var lReturnValue = response.getReturnValue();
        $A.get("e.force:refreshView").fire();
        helper.showToast("Sucesso", $A.get("$Label.c.EX3_Fornecedor_Subsidio"),"info");
        var toastEvent = $A.get("e.force:showToast");
      } else if (state === "INCOMPLETE" || state === "ERROR") {
        var erro = $A.get("$Label.c.EX3_Erro");
        helper.showToast("Erro", erro, "error");

        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            helper.showToast("Erro", errors[0].message, "error");
          }
        } else {  
          helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error");
        }
      }
    });

    $A.enqueueAction(action);
  },

  acionarTarefas: function(component, event, helper) {
    component.set("v.viewComponent", !component.get("v.viewComponent"));
  },

  showToast: function(title, message, type) {
    var toastEvent = $A.get("e.force:showToast");

    toastEvent.setParams({
      duration: 3000,
      title: title,
      message: message,
      type: type // THE TOAST TYPE, WHICH CAN BE ERROR, WARNING, SUCCESS, OR INFO
    });

    toastEvent.fire();
  }
});