({
  changeOwner: function(component) {
    var recordId = component.get("v.recordId");

    var action = component.get("c.mudarProprietarioRegistro");

    action.setParams({
      aRecordId: recordId
    });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var lReturnValue = response.getReturnValue();
        $A.get("e.force:refreshView").fire();
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          recordId: lReturnValue,
          slideDevName: "related"
        });
        navEvt.fire();
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          type: "info",
          title: "",
          message: "Enviado ao fornecedor de subsidio."
        });
        toastEvent.fire();
      } else {
        console.log("Failed with state: " + state);
      }
    });

    $A.enqueueAction(action);
  },

  acionarTarefas : function(component, event, helper)
    {    
        component.set('v.viewComponent', !component.get('v.viewComponent'));
    }   
});