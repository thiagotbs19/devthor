({

    doInit: function (component, event, helper) 
    {
        component.set('v.isMobile', $A.get("$Browser.formFactor") !== 'DESKTOP');
        helper.centralizador(component); 
    },

    openModalInfo: function (component, event, helper) {
        component.set("v.isModalInformation",true);
    },

    closeModalInfo: function (component, event, helper) {
        component.set("v.isModalInformation",false);
    },

    handleUploadFinished: function (component, event, helper) {
        component.set('v.loading', true);
        helper.handleUploadFinished(component, event);
    },

    canSend : function (component, event, helper)
    {
        var value = event.getSource().get("v.value");
        if(value)
        {
            var value = value.replace(/\D/g,"");
            event.getSource().set('v.value', value);
        }
        helper.haveValue(component);
    },

    enviar: function(component, event, helper) 
    {
        component.set('v.loading', true);
        let lCentral = component.get('v.newCentral');
        let lNumProcess = component.get('v.numProcess');

        if( !lNumProcess )
        {
            component.set('v.loading', false);
            helper.showToast('info','','Insira o Número do processo.', '', '5000');
            return;
        }
        if(!lCentral.EX3_hasDocuments__c)
        {
            component.set('v.loading', false);
            helper.showToast('info','','Necessário anexar um documento.', '', '5000');
            return;
        }
        component.set('v.isAbleToSend',true);
        helper.enviar(component,event); 
    },

    cancelar: function(component, event) {   
        var refreshComponent = $A.get("e.force:refreshView"); 
        refreshComponent.fire(); 
    },

})