({

    centralizador: function(component) 
    {
        var action = component.get("c.getCentralizador");
        action.setCallback(this, function(response) 
        {
            let state = response.getState();
            if( state === "SUCCESS" )
            {
                var data = JSON.parse(response.getReturnValue());
                component.set('v.recordId', data.Id);
                component.set('v.newCentral', data); 
            } 
            else
            {
                this.showToast('error','','Entrar em contato com o suporte! '+
                +'Ocorreu um erro ao inserir o objeto intermediário na base de dados!', '', '5000');
            }
            component.set('v.loading', false);
        });
        $A.enqueueAction(action);
    },

    handleUploadFinished: function(component, event) 
    {
        var action = component.get("c.getDocuments");
        action.setParams({
            'aCentral' : JSON.stringify(component.get('v.newCentral'))
        });
        action.setCallback(this, function(response) 
        {
            let state = response.getState();
            component.set('v.loading', false);
            if (state === "SUCCESS") 
            {
                var lReturnValue = JSON.parse(response.getReturnValue());
                component.set("v.files",lReturnValue['listDocument']);
                component.set("v.newCentral", lReturnValue['central']);
                component.set("v.hasAnexo", true);
                this.haveValue(component); 
                this.showToast('success','','Arquivos anexados com sucesso!', '', '5000');            
            }
            else 
            {
                this.showToast('error','','Falha ao anexar os arquivos!', '', '5000');
            }
        });
        $A.enqueueAction(action);
    },

    enviar: function(component) 
    {
        var action = component.get('c.criarProtocolo');

        action.setParams({
            'aNumProcesso' : component.get('v.numProcess'),
            'aCentral' : JSON.stringify(component.get('v.newCentral'))
        });

        action.setCallback(this,function(response)
        {
            let state = response.getState();
            if (state === 'SUCCESS')
            {
                var lReturnValue = response.getReturnValue();
                var lParams = new Map();
                lParams['classe'] = 'EX3_UploadFilesController';
                lParams['callback'] = 'enviarProtocoloCallback';
                lParams['protocolo'] = lReturnValue;
                
                this.enviarProtocolo(component, lParams);
            }
            else
            {
                this.showToast('error','','Erro ao enviar o documento', '', '5000');
                component.set('v.isAbleToSend', false);
                component.set('v.loading', false);
            }
        });

        $A.enqueueAction(action);
    },

    enviarProtocolo: function(component,aParams) {
        component.find("Service").callApex(component,"c.enviarProtocolo",
            {"aParams":JSON.stringify(aParams)},this.getResponseEnviarProtocolo,this);
    },

    getResponseEnviarProtocolo: function(component,returnValue,ref) 
    {
        var lReturnValue = JSON.parse(returnValue);
        if (!lReturnValue || lReturnValue['error'])
        {
            ref.showToast('error','','Erro: ' + lReturnValue['status'] + ' - ' + lReturnValue['error'], '');
            component.set('v.isAbleToSend', false);
            component.set('v.loading', false);
            return;
        }
        
        var lParams = new Map();
        lParams['documentos'] = lReturnValue['documentos'];
        lParams['protocolo'] = lReturnValue['protocolo'];
        lParams['classe'] = 'EX3_UploadFilesController';
        lParams['callback'] = 'enviarDocumentosCallback';
        
        ref.enviarDocumentos(component, lParams);
    }, 

    enviarDocumentos: function(component,aParams) {
        component.find("Service").callApex(component,"c.enviarDocumentos",
            {"aParams":JSON.stringify(aParams)},this.getResponseEnviarDocumentos,this);
    },

    getResponseEnviarDocumentos: function(component,returnValue,ref) 
    {
        var lReturnValue = JSON.parse(returnValue);
        if (!lReturnValue || lReturnValue['error'])
        {
            ref.showToast('error','','Erro: ' + lReturnValue['status'] + ' - ' + lReturnValue['error'], '','');
            component.set('v.isAbleToSend', false);
            component.set('v.loading', false);
            return;
        }
        var lProtocolo = JSON.parse(lReturnValue['protocolo']);
        
        ref.showToast('success','','O processo "' +lProtocolo.EX3_Numero_do_Processo__c +'" foi cadastrado com sucesso. '
                                    +'O número do Protocolo é '+ lProtocolo.EX3_Numero_do_Protocolo__c , '','');
        ref.refreshPage(2000);
    },

    haveValue : function (component)
    {
        let lNumProcess = component.find('numProcess').get('v.value');
        let lHasAnexo = component.get('v.hasAnexo');
        let lPermissionToSend = (!lNumProcess || !lHasAnexo);
        component.set('v.isAbleToSend', lPermissionToSend);
    },

    refreshPage : function (aTimeout) 
    {
        window.setTimeout(
        $A.getCallback(function() {
            location.reload(true);
        }), aTimeout);
    },

    showToast : function (aType, aTitle, aMessage, aMode, aDuration)
    {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams
        ({
            "type" : aType, 
            "title" : aTitle,
            "message" : aMessage, 
            "mode" : aMode,
            "duration" : aDuration
        });
        toastEvent.fire();
    },

})