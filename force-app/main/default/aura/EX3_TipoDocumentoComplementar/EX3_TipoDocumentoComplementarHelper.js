({

    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId"); 
        var action = component.get("c.getListTypeDocCompl");    
           
        action.setParams({ 
            'aRecordId' : recordId  
        }); 
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.mycolumns", response.getReturnValue());
            } 
 
            else if (state === "INCOMPLETE" || state === "ERROR") {
                var erro = $A.get("$Label.c.EX3_Erro");
                helper.showToast("Erro", erro, "error");
        
                var errors = response.getError();
                if (errors) {
                  if (errors[0] && errors[0].message) {
                    helper.showToast("Erro", errors[0].message, "error");
                  }
                } else {  
                  helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error");
                }
              }
        });
        $A.enqueueAction(action);
    },  

    getRelatedList : function (component, event, helper) {
        var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        relatedListEvent.setParams({
            "relatedListId": "Tipo_Documentos__r",  
            "parentRecordId": component.get("v.recordId")
        });
        relatedListEvent.fire();
    },

    navigateToSobject : function (component, event, helper) {
        var recordId = component.get("v.recordId");  
        var iTipoDoc = event.currentTarget.id;  

        var navEvt = $A.get("e.force:navigateToSObject"); 
                
                navEvt.setParams({           
                    "recordId": iTipoDoc,      
                    "slideDevName": "related" 
                });
                navEvt.fire(); 
    },
    
    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            "duration": 3000,
            "title": title,
            "message": message,
            "type": type // THE TOAST TYPE, WHICH CAN BE ERROR, WARNING, SUCCESS, OR INFO
        });

        toastEvent.fire();
  } 
})