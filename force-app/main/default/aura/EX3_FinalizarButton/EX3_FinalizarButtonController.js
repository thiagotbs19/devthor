({  
    doInit : function(component, event, helper){
        
        helper.doInit(component, event, helper); 
        helper.getHasPermission(component, event, helper);
    },
    callFlow: function(component, event, helper) {
        helper.hasAttachment(component, event, helper);  
        
    },
    callFlowFornecedor : function(component, event, helper){
      	helper.callFlowFornecedor(component, event, helper);
    },
    closeModal: function(component, event, helper) {
        component.set('v.openModal', false); 
    }, 
    handleStatusChange : function(component, event, helper){
        if(event.getParam('status') === "FINISHED_SCREEN"){
            component.set("v.hide", false);
            helper.toastQueue(component, event);            
        }
    }
})