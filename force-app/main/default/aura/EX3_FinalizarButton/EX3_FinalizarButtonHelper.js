({	
    doInit : function(component, event, helper){
        var action = component.get('c.getRecordTypeOBF'); 
        action.setParams({
            "aRecordId": component.get("v.recordId"),
        });
        action.setCallback(this, function(response) { 
            component.set("v.isCumprimentoOBF", response.getReturnValue()); 
        });  
        $A.enqueueAction(action);
    },
    
    getHasPermission : function(component, event, helper){
        var action = component.get('c.getHasPermissionSet'); 
        
        action.setCallback(this, function(response) { 
            component.set("v.isDisabledJuridico", !response.getReturnValue()); 
        });  
        $A.enqueueAction(action); 
    },
    
    hasAttachment: function(component, event) {
        var action = component.get('c.getAttachment');
        action.setParams({
            "recordId": component.get("v.recordId"),
            "fieldsToQuery": component.get("v.fieldNames")
        });
        action.setCallback(this, function(response) {
            component.set('v.hasAttachment', response.getReturnValue());
            this.startFlow(component, event);
        });
        $A.enqueueAction(action);
    },
    
    startFlow: function(component, event) {
        var executeFlow = component.get("v.isFlowExecute");
        var hasAttachment = component.get('v.hasAttachment');
        if(hasAttachment == false){   
			this.showToast("warning", 'Alerta!', $A.get("$Label.c.EX3_Arquivo_Anexado"), "success");
            return; 
        }
        component.set("v.isDisabled", false);
        component.set("v.hide", true);
        var flow = component.find("flowData");
        var inputVariables = [{
            name: "recordId",
            type: "SObject",
            value: component.get("v.recordId")
        }];
        
        var flowName = component.get("v.flowName");
        flow.startFlow(flowName, inputVariables);
      
    },  
    
    validateField: function(component, fieldsToValidate) {
        var action = component.get('c.validateFields');
        
        action.setParams({
            "recordId": component.get("v.recordId"),
            "fieldsToValidate": fieldsToValidate
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var lReturnValue = response.getReturnValue();
                if (lReturnValue.includes('Sucesso')) {
                    component.set('v.isValidFields', true);
                } else {
                    component.set('v.fieldsToNotify', lReturnValue);
                    component.set('v.isValidFields', false);
                } 
                
                this.callFlow(component);   
                
            }
        });
        $A.enqueueAction(action); 
    },
    
    callFlowFornecedor : function(component, event, helper){
        var flow = component.find("flowData");
        var inputVariables = [{
            name: "recordId",
            type: "SObject",
            value: component.get("v.recordId")
        }];
        
        var flowName = 'EX3_Fornecedor';
        flow.startFlow(flowName, inputVariables);
    }, 
    
    showToast: function(aType, aTitle, aMessage, aMode, aDuration) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": aType,
            "title": aTitle,
            "message": aMessage,
            "mode": aMode,
            "duration": aDuration
        });
        toastEvent.fire();
    },
    
    toastQueue : function(component, event){
        var isFlowExecute = component.get("v.isFlowExecute"); 
        var action = component.get("c.getQueue");
        action.setParams({
            "aRecordId": component.get("v.recordId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                var lReturnValue = response.getReturnValue();
                component.set("v.messageQueue", lReturnValue.Owner.Name);  
                component.set("v.isDisabled", true);  
                component.set("v.isFlowExecute", true);
                
                this.showToast("Success", 'Sucesso!', $A.get("$Label.c.EX3_Fechmanto_Flow") + ' :' + component.get("v.messageQueue"), "success");
                component.set("v.hide", false);
                $A.get('e.force:refreshView').fire();
                 component.set("v.isDisabled", false); 
                  
            }   
        });
        $A.enqueueAction(action);
    }
})