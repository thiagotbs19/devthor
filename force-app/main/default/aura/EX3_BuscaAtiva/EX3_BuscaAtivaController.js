({

    doInit : function(component, event, helper) 
    {
        console.log('subiu');
        component.set('v.files', null);
        component.set('v.isMobile', $A.get("$Browser.formFactor") !== 'DESKTOP');
        component.set('v.loading', false);
        helper.centralizador(component,event);
        helper.getFields(component, event);
    },

    handleUploadFinished: function (component, event, helper) {
        component.set('v.loading', true);
        helper.handleUploadFinished(component, event, helper);
    },

    enviar: function(component, event, helper) 
    {
        component.set('v.loading', true);

        var getFields = component.find("form");
        var validForm = getFields.reduce(function (validSoFar, inputCmp) {           
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
       
        if(!validForm){
            component.set('v.loading', false);
            var fieldList = component.get('v.fieldList');
            var lMessage = '';
            for (var index in fieldList)  
            {
                var field = fieldList[index];
                if (field.required && !field.value)
                {
                    lMessage += (!lMessage) ? field.label : ', '+ field.label;
                }
            } 
            helper.showToast('error','', 'Por favor preencher os campos obrigatórios: ' +lMessage, '');
            return; 
        }else{
            helper.enviar(component, event, helper);
        }
    },

    clearForm : function (component, event, helper)
    {
        var fieldList = component.get('v.fieldList');
        for (var index in fieldList)
        {
            var field = fieldList[index];
            if (field.api == 'EX3_Canal_de_Entrada__c') { continue; }
            field.value = '';
        }
        component.set('v.fieldList', fieldList);
        component.set('v.files', null);
        helper.callInit(component, event);
    },

    showMask : function (component,event, helper)
    {
       var name = event.getSource().get("v.name");
       var lSetMaskedFields = new Set();
       lSetMaskedFields.add('EX3_CPF__c');
       lSetMaskedFields.add('EX3_Numero_do_Processo__c');
       lSetMaskedFields.add('EX3_Prazo_dias__c');
       var value;
       if (name && lSetMaskedFields.has(name))
       {
            value = event.getSource().get("v.value");
            value = value.replace(/\D/g,"");
            if (name == 'EX3_CPF__c')
            {
                value = value.replace(/(\d{3})(\d)/,"$1.$2");
                value = value.replace(/(\d{3})(\d)/,"$1.$2");
                value = value.replace(/(\d{3})(\d{1,2}){0,10}$/,"$1-$2");
            }
            event.getSource().set('v.value', value);
       }
       helper.disabledField(component, name, value)
    }
})