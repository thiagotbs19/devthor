({

    centralizador : function (component, event)
    {
        var action = component.get("c.getCentralizador");
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if( state === "SUCCESS" )
            {
                var lReturnValue = response.getReturnValue();
                component.set('v.recordId', lReturnValue.Id);
                component.set('v.newCentral', lReturnValue);
            } 
            else 
            {
                this.showToast('error', '', 'Erro ao anexar o arquivo!', '', '5000');
            }
        });
        $A.enqueueAction(action);
    },

    getFields : function(component, event)
    {
        var action = component.get('c.getCaseFields');
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if (state === 'SUCCESS')
            {
                var returnValue = JSON.parse(response.getReturnValue());
                component.set('v.fieldList', returnValue);
                console.log('returnValue');
                console.dir(returnValue);
            }
            else
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.showToast('error', '', errors[0].message, '');
                        this.refreshPage(component);
                    }
                }
                else {
                   this.showToast('error', '' ,'Erro desconhecido', '', '5000');
                   this.refreshPage(component);
                }
            }
        });
        $A.enqueueAction(action);
    },

    enviar : function (component)
    {
        var lFiles = (component.get('v.files'))
                     ? JSON.stringify(component.get('v.files'))
                     : '';
        var action = component.get('c.criarProtocolo');

        action.setParams({
            'aFieldList' : JSON.stringify(component.get('v.fieldList')),
            'aCentral' : JSON.stringify(component.get('v.newCentral'))
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === 'SUCCESS')
            {
                var lParams = new Map();
                lParams['classe'] = 'EX3_BuscaAtivaController';
                lParams['callback'] = 'enviarProtocoloCallback';
                lParams['central'] = JSON.stringify(component.get('v.newCentral'));
                lParams['files'] = JSON.stringify(lFiles);
                lParams['protocolo'] = response.getReturnValue();
        
                this.enviarProtocolo(component,lParams);
            }
            else 
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.showToast('error', '', errors[0].message, '');
                    }
                }
                else {
                   this.showToast('error', '' ,'Erro desconhecido', '', '5000');
                }
                component.set('v.isDisable', false);
                component.set('v.loading', false);
            }
        }); 

        $A.enqueueAction(action);
    },

    
    enviarProtocolo: function(component,lParams)
    {
        component.find("Service").callApex(component,"c.enviarProtocolo",
                                           {"aParams":JSON.stringify(lParams)},this.getResponseEnviarProtocolo,this);
    },

    getResponseEnviarProtocolo: function(component,returnValue,ref)
    {
        var lReturnValue = JSON.parse(returnValue);
        if (!lReturnValue || lReturnValue['error'])
        {
            ref.showToast('error','','Erro: ' + lReturnValue['status'] + ' - ' + lReturnValue['error'], '');
            component.set('v.isDisable', false);
            component.set('v.loading', false);
            return;
        }

        if (!component.get('v.files')) 
        { 
            ref.showToast('success', '' ,'Protocolo enviado com sucesso!', '', '5000');
            ref.refreshPage(2000);
            //return; aqui
        }
        var action = component.get('c.criarDocumentos');

        action.setParams({
            'aProtocolo' : JSON.stringify(lReturnValue['protocolo']),
            'aCentral' : JSON.stringify(component.get('v.newCentral')),
            'aFuncional' : lReturnValue['operador']
        });

        action.setCallback(ref, function(response)
        {
            var state = response.getState();
            if (state === 'SUCCESS')
            {
                var responseValue = response.getReturnValue();

				if(responseValue === 'Sem documento') return; //aqui
                
                var lParams = new Map();
                lParams['classe'] = 'EX3_BuscaAtivaController';
                lParams['callback'] = 'enviarArquivosCallback';
                lParams['aBody'] = responseValue;
                lParams['aProtocolo'] = JSON.stringify(lReturnValue['protocolo']);

                ref.enviarArquivosEProtocolo(component, lParams);
            }
            else 
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.showToast('error', '', errors[0].message, '');
                    }
                }
                else {
                   this.showToast('error', '' ,'Erro desconhecido', '', '5000');
                }
                component.set('v.isDisable', false);
                component.set('v.loading', false);
            }
        });

        $A.enqueueAction(action);
    },

    enviarArquivosEProtocolo: function(component,lParams)
    {
        component.find("Service").callApex(component,"c.enviarArquivosEAtualizarProtocolo",
                                           {"aParams":JSON.stringify(lParams)},this.getResponseEnviarArquivosEProtocolo,this);
    },

    getResponseEnviarArquivosEProtocolo : function(component,returnValue,ref)
    {
        var lReturnValue = JSON.parse(returnValue);
        ref.showToast('success', '' ,'Protocolo enviado com sucesso!', '', '2000');
        ref.refreshPage(2000);
    },
        
    handleUploadFinished : function (component, event, helper) 
    {
        var actionGetDocs = component.get("c.getDocuments");
        actionGetDocs.setParams({
            'aCentral' : JSON.stringify( component.get('v.newCentral'))
        });
        actionGetDocs.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            var data = response.getReturnValue();
            component.set("v.files",data.listDocument);
            component.set("v.newCentral", data.central);
            component.set('v.loading', false);
            this.showToast('success', '', 'Arquivos anexados com sucesso', '', '5000');
        }
        else {
            component.set('v.loading', false);
            this.showToast('error', '', 'Erro ao anexar os arquivos', '', '5000');
        }});
        $A.enqueueAction(actionGetDocs);
    },
    
    disabledField : function (component, name, value)
    {
        if (name != 'EX3_Prazo_dias__c') { return; }

        var fieldList = component.get('v.fieldList');
        for (var index in fieldList)
        {
             var field = fieldList[index];
             if (field.api === 'EX3_Hora_da_audiencia__c' || field.api === 'EX3_Tipo_audiencia__c')
             {
                 field.isDisable = Boolean(name === 'EX3_Prazo_dias__c' && value);
                 field.required = Boolean(name === 'EX3_Prazo_dias__c' && !value && field.api === 'EX3_Hora_da_audiencia__c');
             }
        }
        component.set('v.fieldList', fieldList);
    },

    refreshPage : function (aTimeout) 
    {
        window.setTimeout(
        $A.getCallback(function() {
            location.reload(true);
        }), aTimeout);
    },

    showToast : function (aType, aTitle, aMessage, aMode, aDuration)
    {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams
        ({
            "type" : aType,
            "title" : aTitle,
            "message" : aMessage,
            "mode" : aMode,
            "duration" : aDuration
        });
        toastEvent.fire();
    },
})