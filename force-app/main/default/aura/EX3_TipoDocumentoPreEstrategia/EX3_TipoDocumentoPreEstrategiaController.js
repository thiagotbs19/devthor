({
    doInit : function(component, event, helper) {
        helper.doInit(component, event, helper);
    }, 

    getRelatedList : function (component, event, helper) {
        helper.getRelatedList(component, event, helper);
    },

    navigateToSobject : function (component, event, helper) {
       helper.navigateToSobject(component, event, helper);      
    }
})