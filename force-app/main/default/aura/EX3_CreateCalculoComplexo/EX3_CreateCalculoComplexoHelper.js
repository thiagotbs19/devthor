({
    doInit : function(component, event, helper){
        var recordId = component.get("v.recordId");

        var action = component.get("c.getRecordTypeADJ"); 

        action.setParams({
            aRecordId: recordId
          });
      
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.isOpenOBP", response.getReturnValue());  
              
            } 
            else if (state === "INCOMPLETE" || state === "ERROR") {
              var erro = $A.get("$Label.c.EX3_Erro");  
              helper.showToast("Erro", erro ,"error");
      
              var errors = response.getError();
              if (errors) {
                  if (errors[0] && errors[0].message) {  
                      helper.showToast("Erro", errors[0].message ,"error");   
                  }
              } else {    
                  helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
              }
          } 
          });
      
          $A.enqueueAction(action);
    }, 
    
    getHasPermission : function(component, event, helper){
         var action = component.get("c.getHasPermissionSet"); 
      
          action.setCallback(this, function(response) { 
            var state = response.getState();
            if (state === "SUCCESS") { 
                
                component.set("v.isDisabledJuridico", !response.getReturnValue());  
              
            } 
            else if (state === "INCOMPLETE" || state === "ERROR") {
              var erro = $A.get("$Label.c.EX3_Erro");  
              helper.showToast("Erro", erro ,"error");
      
              var errors = response.getError();
              if (errors) {
                  if (errors[0] && errors[0].message) {  
                      helper.showToast("Erro", errors[0].message ,"error");   
                  }
              } else {    
                  helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
              }
          } 
          });
      
          $A.enqueueAction(action);
    },
      
    acionarTarefas: function(component, event, helper){ 
        var recordId = component.get("v.recordId");

        var action = component.get("c.getFieldsADJ");  

        action.setParams({
            aRecordId: recordId
          });
      
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();
                var lADJ = lReturnValue['adj'];   
                var lRTCalc = lReturnValue['recordTypeCalc'];
                var lQueueCalc = lReturnValue['queueId'];
                var lQueueCalculista = lReturnValue['queueCalc'];  
                console.log('THBS ---- lADJ' + lADJ);  
                var createRecordEvent = $A.get('e.force:createRecord');
                if ( createRecordEvent ) {  
                    
                    if(lADJ != null){   
                        console.log('THBS lADJ caiu no if');   
                        createRecordEvent.setParams({  
                            'entityApiName': 'EX3_Calculo__c',  
                            'recordTypeId': lRTCalc, //EX3_Solicitacao_Calculo  
                            'defaultFieldValues': {      
                                'OwnerId' : lQueueCalculista,          
                                'EX3_Caso__c' : lADJ.EX3_Caso__c,
                                'EX3_Numero_do_processo__c' : lADJ.EX3_Numero_do_Processo__c, 
                                'EX3_Numero_da_Pasta__c' : lADJ.EX3_Numero_da_Pasta__c  
                            } 
                        }); 
                        createRecordEvent.fire();   
                         
              
            }  
                   
        }  
        
    }
            else if (state === "INCOMPLETE" || state === "ERROR") {
              var erro = $A.get("$Label.c.EX3_Erro");  
              helper.showToast("Erro", erro ,"error");
              
      
              var errors = response.getError();
              if (errors) {
                  if (errors[0] && errors[0].message) {  
                      helper.showToast("Erro", errors[0].message ,"error");   
                  }
              } else {     
                  helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
              }
          } 
          });
          
          $A.enqueueAction(action);   
    },

    enviarAceite : function(component, event, helper){
        var recordId = component.get("v.recordId"); 

        var action = component.get("c.getUpdateFilaAceite");  

        action.setParams({
            aRecordId: recordId
          });
      
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue(); 
                if(lReturnValue != null){ 
                    helper.showToast("success",$A.get("$Label.c.EX3_Mensagem_Fila_Aceite"),"Sucesso");   
                }  
    }
            else if (state === "INCOMPLETE" || state === "ERROR") {
              var erro = $A.get("$Label.c.EX3_Erro");  
              helper.showToast("Erro", erro ,"error");
      
              var errors = response.getError();
              if (errors) {
                  if (errors[0] && errors[0].message) {  
                      helper.showToast("Erro", errors[0].message ,"error");   
                  }
              } else {    
                  helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
              }
          } 
          });
      
          $A.enqueueAction(action);

    },  

    showToast : function (aType, aTitle, aMessage, aMode, aDuration)
    {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams
        ({
           "type" : aType,
           "title" : aTitle,
           "message" : aMessage,
           "mode" : aMode,
           "duration" : aDuration
        });
        toastEvent.fire();
    }  


    
})