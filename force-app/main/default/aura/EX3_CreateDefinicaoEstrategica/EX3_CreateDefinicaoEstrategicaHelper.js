({ 
    doInit : function(component, event, helper){
        
        var action = component.get("c.getHasPermissionSet"); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                
                var lReturnValue = response.getReturnValue(); 
                component.set("v.isDisabledJuridico", !lReturnValue);  
                
            }  
            else if (state === "INCOMPLETE" || state === "ERROR") {
                var erro = $A.get("$Label.c.EX3_Erro");  
                helper.showToast("Erro", erro ,"error");
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {  
                        helper.showToast("Erro", errors[0].message ,"error");   
                    }
                } else {    
                    helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
                }
            } 
        });
        
        $A.enqueueAction(action);
    },
    createRecord: function(component, event, helper) {
        var recordId = component.get("v.recordId");
        
        var action = component.get("c.redirectToObject");
        
        action.setParams({
            aRecordId: recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var lReturnValue = response.getReturnValue();
                console.log('lReturnValue' + lReturnValue);  
                var finalizarCaptura = $A.get("$Label.c.EX3_Finalizar_Captura");  
                
                var mensagemDefEstrag = $A.get("$Label.c.EX3_Definicao_Estrategica");
                if(lReturnValue == $A.get("$Label.c.EX3_Empty_Doc")){   
                    helper.showToast("Alerta",$A.get("$Label.c.EX3_Empty_Doc"),"warning");
                    return;  
                }    
                if (lReturnValue == finalizarCaptura || lReturnValue == '') { 
                    console.log('lReturnValue' + lReturnValue);        
                    helper.showToast("Alerta",finalizarCaptura,"warning");
                    return;  
                }   
                helper.showToast("Successo",mensagemDefEstrag,"success");   
            } 
            else if (state === "INCOMPLETE" || state === "ERROR") {
                var erro = $A.get("$Label.c.EX3_Erro");  
                helper.showToast("Erro", erro ,"error");
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {  
                        helper.showToast("Erro", errors[0].message ,"error");   
                    }
                } else {    
                    helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
                }
            } 
        });
        
        $A.enqueueAction(action);
    },
    
    acionarTarefas: function(component, event, helper) {
        component.set("v.viewComponent", !component.get("v.viewComponent"));
    }, 
    
    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            "duration": 3000,
            "title": title,
            "message": message,
            "type": type // THE TOAST TYPE, WHICH CAN BE ERROR, WARNING, SUCCESS, OR INFO
        }); 
        
        toastEvent.fire();
    }
});