({
  createRecordRegister: function (component, event, helper) {
    var recordId = component.get("v.recordId");

    var action = component.get("c.redirectToObject");

    action.setParams({
      aRecordId: recordId
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var lReturnValue = response.getReturnValue();  
        helper.showToast("Successo",$A.get("$Label.c.EX3_Cadastro_Criado"),"success");  
        return;  
      } 
      else if (state === "INCOMPLETE" || state === "ERROR") {
				var erro = $A.get("$Label.c.EX3_Erro");  
				helper.showToast("Erro", erro ,"error");
		 
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {  
						helper.showToast("Erro", errors[0].message ,"error");   
					}
				} else {    
					helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
				}
			}  

    });

    $A.enqueueAction(action);
  },

  createRecordProtocol: function (component, event, helper) {
    var recordId = component.get("v.recordId");

    var action = component.get("c.getFieldsCase");

    action.setParams({
      aRecordId: recordId
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var lReturnValue = response.getReturnValue();
        helper.showToast("Successo",$A.get("$Label.c.EX3_Motivo_Situacao_Finalizado"),"success");  
        return;    
         
      }   
      else if (state === "INCOMPLETE" || state === "ERROR") {
				var erro = $A.get("$Label.c.EX3_Erro");  
				helper.showToast("Erro", erro ,"error");
		 
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {  
						helper.showToast("Erro", errors[0].message ,"error");   
					}
				} else {    
					helper.showToast("Erro", $A.get("$Label.c.EX3_Erro_Desconhecido"),"error"); 
				}
			} 

    });

    $A.enqueueAction(action);
  },

  showToast: function (title, message, type) {
    var toastEvent = $A.get("e.force:showToast");

    toastEvent.setParams({
      duration: 3000,
      title: title,
      message: message,
      type: type
    });

    toastEvent.fire();
  }
});