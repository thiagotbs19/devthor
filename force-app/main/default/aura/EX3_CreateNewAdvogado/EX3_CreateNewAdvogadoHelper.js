({
    closeModel : function(component, event, helper) {
        component.set("v.showModalEdit", false); 
    },

    saveNewRecord : function(component, event, helper){
        var lFields = component.get("v.lFieldsInfo"); 

        var action = component.get("c.getSaveAdvog"); 

        console.log('THBS ---- Caiu na função'); 
        action.setParams({  
         'aFields'   : lFields  
        });  
        action.setCallback(this, function(response) {
            var state = response.getState(); 

            if (component.isValid() && state === "SUCCESS") {
                component.set("v.lFieldsInfo", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);  
    }

})
