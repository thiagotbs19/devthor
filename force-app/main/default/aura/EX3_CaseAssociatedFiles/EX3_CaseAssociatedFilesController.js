({

    doInit: function (component, event, helper)
    {
        helper.getCentralizador(component, null);
    },    

    closeModalInfo: function (component)
     {
        component.set("v.isModalInformation",false);
    },

    handleUploadFinished: function (component, event, helper) 
    {   
        helper.handleUploadFinished(component, event);   
    }, 

    handleCancelEvent : function (component, event)
    {   
        component.set("v.files", []);
        component.set('v.sendIntegration', true);
        let refreshComponent = $A.get("e.force:refreshView"); 
        refreshComponent.fire(); 
    },

    enviar: function(component, event, helper) 
    {
        var lCentral = component.get('v.newCentral');
        if(!lCentral.EX3_hasDocuments__c)
        {
            helper.showToast('error', '', $A.get("$Label.c.EX3_Necessario_Anexar"), '', '5000'); 
            return ; 
        }   
        component.set('v.sendIntegration', true);
        helper.enviar(component, event, helper);
        //let refreshComponent = $A.get("e.force:refreshView"); 
        //refreshComponent.fire();      
    }
})