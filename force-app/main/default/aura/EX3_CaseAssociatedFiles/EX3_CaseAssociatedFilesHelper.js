({
    getCentralizador : function(component, aCancel)
    {
        var action = component.get("c.getCentralizador");
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if( state === "SUCCESS" )
            {
                var data = JSON.parse(response.getReturnValue());
                component.set('v.centralRecordId', data.Id);
                component.set('v.newCentral', data);    

                if (aCancel) 
                { 
                    component.set('v.files', null);
                    component.set('v.noFile', !(component.get('v.filesAttributes')));
                    
                    
                }
                else { this.getRelatedDocuments(component); }
            } 
            else 
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.showToast('error', '', errors[0].message, '');
                    }
                }
                else { this.showToast('error', '' ,$A.get("$Label.c.EX3_Erro_Desconhecido"), ''); } 	
            }
        });
        $A.enqueueAction(action);
    },

    getRelatedDocuments : function(component)
    {        
        var action = component.get("c.getDocuments");
        action.setParams({
            "aCaseId" : component.get("v.recordId"),
            "camposToQuery" : component.get("v.fieldNames")
          });

        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if( state === "SUCCESS" )
            {
               var returnValue = JSON.parse(response.getReturnValue());
               if (returnValue && returnValue.length) { component.set('v.filesAttributes',returnValue); }
               else { component.set('v.noFile', true); }
            }
            else
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.showToast('error', '', errors[0].message, '');
                    }
                }
                else { this.showToast('error', '' ,$A.get("$Label.c.EX3_Erro_Desconhecido"), ''); }
            }
        });
        $A.enqueueAction(action);
    },


    handleUploadFinished : function(component, event) 
    {
        component.set('v.sendIntegration', false);
        var action = component.get("c.setDocuments"); 
        console.log('recordId' + component.get('v.recordId'));
          
        action.setParams({   
            'aCaseId' : component.get('v.recordId'),
            'aParamCentral' : JSON.stringify( component.get('v.newCentral') ) 
             
        });  
        action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") { 
            var data = JSON.parse(response.getReturnValue());
            console.log('data' + data); 
            component.set('v.noFile', false);  
            component.set("v.files",data.listDocument);
            component.set("v.newCentral", data.central);
            component.set("v.hasAnexo", true);    
            this.showToast('success', '', $A.get("$Label.c.EX3_Arquivos_Anexados_Sucesso"), '', '2000');               

        }   
        else { 
           this.showToast('error', '', $A.get("$Label.c.EX3_Arquivos_Anexados_Erro"), '', '5000');
        }});
        $A.enqueueAction(action);
    },


    enviar : function(component, event, helper)
    {

        var action = component.get("c.associarArquivos");
        action.setParams({ 
          'aCentral': JSON.stringify(component.get('v.newCentral')),
          'camposToQuery' : component.get("v.fieldNames"),
          'aCaseId': component.get('v.recordId') 
        });
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var lReturnValue = JSON.parse(response.getReturnValue());
                var lParams = new Map(); 
                lParams['classe'] = 'EX3_CaseAssociatedFilesController';
                lParams['service'] ='incluirDocumento';
                lParams['objeto'] = lReturnValue['objeto'];    
                lParams['documentos'] = lReturnValue['documentos'];
                lParams['documentsIds'] = lReturnValue['documentsIds'];
                component.set("v.loading", true);   
                this.enviarDocumentos(component, lParams);
                $A.get('e.force:refreshView').fire();    
                component.set('v.loading', false);       
            }
            else 
            {
                component.set('v.loading', false);
                component.set('v.sendIntegration', true);
    
                var errors = response.getError(); 

                if (errors && errors[0] && errors[0].message)
                {
                    this.showToast('error', '', errors[0].message, '');
                }
                else { this.showToast('error', '' ,$A.get("$Label.c.EX3_Erro_Desconhecido"), ''); }

                this.showToast('error', '', $A.get("$Label.c.EX3_Erro_Enviar_Documento"), '', '5000');
            }
        });
        $A.enqueueAction(action);
    },

    enviarDocumentos: function(component,lParams)
    {	console.log('lParams' + JSON.stringify(lParams));
        component.find("Service").callApex(component,"c.enviarArquivos",
        {"aParams":JSON.stringify(lParams)},this.getResponseEnviarArquivos,this);

    }, 


    getResponseEnviarArquivos: function(component,returnValue,ref)
    {
        var lReturnValue = JSON.parse(returnValue);
        console.log('THBS --- lReturnValue' + lReturnValue);
        if ((!lReturnValue || lReturnValue['error']) && lReturnValue != null)
        {
            helper.showToast('error','','Erro: ' + lReturnValue['status'] + ' - ' + lReturnValue['error'], '');
            component.set('v.loading', false);  
            component.set('v.sendIntegration', false);
            return;
        } else if(lReturnValue == null){
            console.log('error','','Falha ao integrar o arquivo!' , '');
        }
		$A.get('e.force:refreshView').fire(); 
        ref.showToast('success','',$A.get("$Label.c.EX3_Arquivo_Associado_Sucesso"), '', '2000'); 
    },

    refreshPage : function (aTimeout) 
    {
        window.setTimeout(
        $A.getCallback(function() {
            location.reload(true);
        }), aTimeout);
    },

    showToast : function (aType, aTitle, aMessage, aMode, aDuration)
    {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams
        ({
           "type" : aType,
           "title" : aTitle,
           "message" : aMessage,
           "mode" : aMode,
           "duration" : aDuration
        });
        toastEvent.fire();
    }  	
    
})