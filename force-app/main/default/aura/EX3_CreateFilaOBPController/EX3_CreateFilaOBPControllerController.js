({
	createRecordOBP: function (component) {
        var recordId = component.get("v.recordId");
        console.log('Record ID is' + recordId);
        
        var action = component.get("c.redirectToObjectOBP");
        
        action.setParams({
            'aRecordId': recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "info",
                    "title": "",
                    "message": "Solicitação de OBP enviado à fila!"
                });
                toastEvent.fire(); 
				$A.get('e.force:refreshView').fire();

                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                })
                .catch(function(error) {
                    console.log(error);
                });

            }
            else {
                console.log("Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);
    },
    
    createRecordOBF: function (component) {
        var recordId = component.get("v.recordId");
        console.log('Record ID is' + recordId);
        
        var action = component.get("c.redirectToObjectOBF");
        
        action.setParams({
            'aRecordId': recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "info",
                    "title": "",
                    "message": "Solicitação de OBF enviado à fila!"
                });
                toastEvent.fire(); 
				$A.get('e.force:refreshView').fire();

                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                })
                .catch(function(error) {
                    console.log(error);
                });

            }
            else {
                console.log("Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);
    },
    
    createRecordENC: function (component) {
        var recordId = component.get("v.recordId");
        console.log('Record ID is' + recordId);
        
        var action = component.get("c.redirectToObjectENC");
        
        action.setParams({
            'aRecordId': recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "info",
                    "title": "",
                    "message": "Solicitação de Encerramento enviado à fila!"
                });
                toastEvent.fire(); 
				$A.get('e.force:refreshView').fire();

                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                })
                .catch(function(error) {
                    console.log(error);
                });

            }
            else {
                console.log("Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);
    },
    
    createRecordFIM: function (component) {
        var recordId = component.get("v.recordId");
        console.log('Record ID is' + recordId);
        
        var action = component.get("c.redirectToObjectFIM");
        
        action.setParams({
            'aRecordId': recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "info",
                    "title": "",
                    "message": "Solicitação de Finalizar enviado à fila!"
                });
                toastEvent.fire(); 
				$A.get('e.force:refreshView').fire();

                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                })
                .catch(function(error) {
                    console.log(error);
                });

            }
            else {
                console.log("Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);
    }, 
      
    acionarTarefas : function(component, event, helper)
    {    
        component.set('v.viewComponent', !component.get('v.viewComponent'));
    }
     
})