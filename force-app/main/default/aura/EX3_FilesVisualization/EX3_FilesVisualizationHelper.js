({
    files : function(component)
    {
        var action = component.get("c.getFiles");
        action.setParams({
            "aCaseId" : component.get("v.recordId")
          });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === "SUCCESS" )
            {
               var returnValue = JSON.parse(response.getReturnValue());
               if (returnValue && returnValue.length) { component.set('v.filesAttributes',returnValue); }
               else { component.set('v.noFile', true); }
            }
            else
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.showToast('error', '', errors[0].message, '');
                    }
                }
                else { this.showToast('error', '' ,'Erro desconhecido', ''); }
            }
        });
        $A.enqueueAction(action);
    },

    showToast : function (aType, aTitle, aMessage, aMode)
    {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams
        ({
            "type" : aType,
            "title" : aTitle,
            "message" : aMessage,
            "mode" : aMode
        });
        toastEvent.fire();
    }

})