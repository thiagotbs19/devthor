({
    startProgress : function(component, event, helper) {
        
        var recordId = component.get("v.recordId");
        var action = component.get("c.getCurrentStage");
        
        action.setParams({
            'aRecordId': recordId
        });
        

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var lReturnValue = response.getReturnValue();
               	component.set("v.currentIds", lReturnValue.listStageId);
                component.set("v.currentSteps", lReturnValue.currentStep);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);

	},
    
    goTo : function(component, event, helper) {
        
         var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                  "recordId": "a07f000000FVtlZAAT",
                  "slideDevName": "Detail"
                });
                $A.get('e.force:refreshView').fire();
                navEvt.fire();
    }
})