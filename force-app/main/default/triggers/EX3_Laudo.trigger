trigger EX3_Laudo on EX3_Laudo__c (after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(EX3_Laudo__c.sObjectType);
}