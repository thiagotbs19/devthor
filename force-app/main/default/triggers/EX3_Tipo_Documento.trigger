trigger EX3_Tipo_Documento on EX3_Tipo_Documento__c  (after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(EX3_Tipo_Documento__c.sObjectType);  
}