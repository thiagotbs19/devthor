trigger EX3_DefinicaodeEstrategia on EX3_DefinicaoEstrategia__c (after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(EX3_DefinicaoEstrategia__c.sObjectType);
}