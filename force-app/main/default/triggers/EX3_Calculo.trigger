trigger EX3_Calculo on EX3_Calculo__c (after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(EX3_Calculo__c.sObjectType);
}