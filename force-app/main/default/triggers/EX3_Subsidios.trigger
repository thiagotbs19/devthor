trigger EX3_Subsidios on EX3_Subsidios__c (after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(EX3_Subsidios__c.sObjectType); 
}