trigger EX3_Cadastro on EX3_Cadastro__c (after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(EX3_Cadastro__c.sObjectType);
}