trigger EX3_ADJ on EX3_ADJ__c  (after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(EX3_ADJ__c.sObjectType);  
}